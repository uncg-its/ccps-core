<?php

namespace Uncgits\Ccps;

use Carbon\Carbon;
use App\CcpsCore\Job;
use Livewire\Livewire;
use Cron\CronExpression;
use App\CcpsCore\DbConfig;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\ServiceProvider;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Symfony\Component\Console\Output\ConsoleOutput;

class CcpsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // fix database for older MySQL
        \Schema::defaultStringLength(191);

        // are we running in Console?
        $consoleMode = $this->app->runningInConsole();
        if ($consoleMode) {
            $consoleOutput = new ConsoleOutput();
        }

        // are we able to connect to the database?
        try {
            $result = \DB::connection()->getPdo();
            $canConnectToDb = true;
        } catch (\PDOException $e) {
            if ($consoleMode) {
                $consoleOutput->writeln('Could not connect to database!');
            }
            $canConnectToDb = false;
        }


        // Actions to take if running via Http kernel
        if (!$consoleMode) {
            // bind the current user to all views as $user
            \View::composer('*', function ($view) {
                $view->with('user', \Auth::user());
            });
        }

        // Actions to take regardless of kernel

        if ($canConnectToDb) {
            // bind dbConfig to container for use
            if (config('app.key') != '' && \Schema::hasTable('ccps_db_configs')) {
                // bind config variables to the container, only run if key has been generated.
                $this->app->singleton('dbConfig', function () {
                    return DbConfig::getConfigCollection();
                });
            }

            // Queue-handler events - only if using 'database' queue driver
            \Queue::before(function (JobProcessing $event) {
                $driver = config('queue.connections.' . $event->connectionName . '.driver');
                if ($driver == 'database') {
                    $id = $event->job->getJobId();
                    $job = Job::where('id', $id)->first();
                    $queueName = $job->queue;

                    session(['job' => $job]);
                    \Log::channel('queue')->debug('Job ' . $id . ' pulled from Queue \'' . $queueName . '\' and stored in session.');
                } else {
                    \Log::channel('queue')->debug('Queue Job processing on queue driver ' . $driver);
                }
            });

            \Queue::after(function (JobProcessed $event) {
                $driver = config('queue.connections.' . $event->connectionName . '.driver');
                if ($driver == 'database') {
                    $job = session('job');

                    \Log::channel('queue')->debug('Job ' . $job->id . ' pulled from session');

                    try {
                        $successfulJob = $job->getRowArray();
                        $successfulJob['completed_at'] = Carbon::now();

                        // detect manual-run job and alter insertion from there
                        if ($job->id === 0) {
                            $successfulJob['run_method'] = 'manual';
                        }

                        $jobId = \DB::table('ccps_jobs_successful')->insert($successfulJob);

                        \DB::table('ccps_sent_email')->where('job_id', 0)->update(['job_id' => $jobId]);

                        \Log::channel('queue')->info('Job ' . $job->id . ' logged as successful.', [
                            'category'  => 'queue',
                            'operation' => 'run',
                            'result'    => 'success',
                            'data'      => [
                                'job' => $job
                            ]
                        ]);
                    } catch (\Exception $e) {
                        \Log::channel('queue')->warning('Job ' . $job->id . ' was successful but could not be logged in database!', [
                            'category'  => 'database',
                            'operation' => 'store',
                            'result'    => 'failure',
                            'data'      => [
                                'job'     => $job,
                                'message' => $e->getMessage()
                            ]
                        ]);
                    }
                }
            });

            \Queue::failing(function (JobFailed $event) {
                $driver = config('queue.connections.' . $event->connectionName . '.driver');
                if ($driver == 'database') {
                    $job = session('job');
                    \Log::channel('queue')->error('Job ' . $job->id . ' has failed.', [
                        'category'  => 'queue',
                        'operation' => 'run',
                        'result'    => 'failure',
                        'data'      => [
                            'message' => $event->exception->getMessage(),
                            'job'     => $job
                        ]
                    ]);
                }
            });
        }


        // register custom Blade directives

        // Call to Laratrust::hasRole
        \Blade::directive('norole', function ($expression) {
            return "<?php if (!app('laratrust')->hasRole({$expression})) : ?>";
        });

        // Call to Laratrust::can
        \Blade::directive('nopermission', function ($expression) {
            return "<?php if (!app('laratrust')->isAbleTo({$expression})) : ?>";
        });

        // Call to Laratrust::ability
        \Blade::directive('noability', function ($expression) {
            return "<?php if (!app('laratrust')->ability({$expression})) : ?>";
        });

        \Blade::directive('endnorole', function () {
            return "<?php endif; // !app('laratrust')->hasRole ?>";
        });

        \Blade::directive('endnopermission', function () {
            return "<?php endif; // !app('laratrust')->isAbleTo ?>";
        });

        \Blade::directive('endnoability', function () {
            return "<?php endif; // !app('laratrust')->ability ?>";
        });

        // custom validation rules

        \Validator::extend('valid_cron_expression', function ($attribute, $value, $parameters, $validator) {
            return CronExpression::isValidExpression($value);
        });

        // View Component registration
        \Blade::component('button', \Uncgits\Ccps\View\Components\Button::class);
        \Blade::component('toggle', \Uncgits\Ccps\View\Components\Toggle::class);
        \Blade::component('paginated-table', \Uncgits\Ccps\View\Components\PaginatedTable::class);

        // Livewire
        Livewire::component('api-token', \Uncgits\Ccps\Livewire\ApiToken::class);
        Livewire::component('api-tokens', \Uncgits\Ccps\Livewire\ApiTokens::class);
        Livewire::component('cronjob-list', \Uncgits\Ccps\Livewire\CronjobList::class);
        Livewire::component('cronjob-row', \Uncgits\Ccps\Livewire\CronjobRow::class);
        Livewire::component('log-viewer', \Uncgits\Ccps\Livewire\LogViewer::class);
        Livewire::component('notification-channel', \Uncgits\Ccps\Livewire\NotificationChannel::class);
        Livewire::component('notification-channels', \Uncgits\Ccps\Livewire\NotificationChannels::class);
        Livewire::component('notification-settings', \Uncgits\Ccps\Livewire\NotificationSettings::class);
        Livewire::component('notification-toggle', \Uncgits\Ccps\Livewire\NotificationToggle::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            // load development-only service providers and facades
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
