<?php

namespace Uncgits\Ccps\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailListContainsEmailAddresses implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $emails = collect(preg_split('/[\r\n,;]+/', strtolower($value), -1, PREG_SPLIT_NO_EMPTY));

        $invalid = $emails->filter(function ($email) {
            $validator = \Validator::make(['email' => $email], [
                'email' => 'required|email'
            ]);

            return $validator->fails();
        });

        return $invalid->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'One or more entries is not a valid email address.';
    }
}
