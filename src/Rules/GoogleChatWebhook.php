<?php

namespace Uncgits\Ccps\Rules;

use Illuminate\Contracts\Validation\Rule;

class GoogleChatWebhook implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strpos($value, 'https://chat.googleapis.com/v1/spaces/') !== false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Google Chat Webhook URL.';
    }
}
