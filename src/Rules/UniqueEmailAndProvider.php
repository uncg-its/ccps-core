<?php

namespace Uncgits\Ccps\Rules;

use App\CcpsCore\User;
use Illuminate\Contracts\Validation\Rule;

class UniqueEmailAndProvider implements Rule
{
    protected $email;
    protected $provider;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->email = $value;

        $usersWithEmailAndProvider = User::where('provider', $this->provider)->where('email', $value)->get();
        return ($usersWithEmailAndProvider->count() == 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A user with email address \'' . $this->email . '\' and provider \'' . $this->provider . '\' already exists.';
    }
}
