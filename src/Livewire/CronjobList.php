<?php

namespace Uncgits\Ccps\Livewire;

use Livewire\Component;
use App\CcpsCore\CronjobMeta;
use Illuminate\Support\Collection;

class CronjobList extends Component
{
    public $cronjobs;
    public $lockFiles;

    public $search = '';

    public $showMinChars = false;

    protected $listeners = ['lockFileCleared' => 'getCronList'];

    public function mount()
    {
        $this->getCronList();
    }

    public function updatedSearch($value)
    {
        $this->getCronList();
    }

    public function render()
    {
        return view('livewire.cronjob-list');
    }

    public function getCronList()
    {
        $this->showMinChars = false;

        $value = strtolower(trim($this->search));

        if ($value === '') {
            $cronjobs = CronjobMeta::get();
            $this->cronjobs = $cronjobs->pluck('id')->toArray();
            $this->lockFiles = $this->getLockFiles($cronjobs);

            return;
        }

        if (strlen($value) < 3) {
            $this->showMinChars = true;
            return;
        }

        $cronjobs = CronjobMeta::get()->filter(function ($meta) use ($value) {
            $cronjob = new $meta->class;
            if (\Str::of($cronjob->getDisplayName())->lower()->contains($value)) {
                return true;
            }
            if (\Str::of($cronjob->getClassName())->lower()->contains($value)) {
                return true;
            }
            foreach ($cronjob->getTags() as $tag) {
                if (\Str::of($tag)->lower()->contains($value)) {
                    return true;
                }
            }

            return false;
        });

        $this->cronjobs = $cronjobs->pluck('id')->toArray();
        $this->lockFiles = $this->getLockFiles($cronjobs);
    }

    public function clearSearch()
    {
        $this->reset(['search']);
        $this->getCronList();
    }

    private function getLockFiles(Collection $cronjobs)
    {
        return $cronjobs->mapWithKeys(function ($cronjob) {
            $class = new $cronjob->class;
            return [$class->getDisplayName() => $class->getLockFileName()];
        })->filter(function ($lockFile) {
            return file_exists(storage_path('cron/' . $lockFile));
        })->toArray();
    }
}
