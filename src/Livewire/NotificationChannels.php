<?php

namespace Uncgits\Ccps\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Validation\Rule;
use Uncgits\Ccps\Rules\SlackWebhook;
use App\CcpsCore\ChannelVerification;
use App\CcpsCore\NotificationChannel;
use Uncgits\Ccps\Rules\GoogleChatWebhook;
use App\Notifications\CcpsCore\VerifyChannel;

class NotificationChannels extends Component
{
    public $notificationChannels;
    public $notificationEvents;
    public $types;
    public $carriers;

    public $showCreateForm = false;

    public $channelName;
    public $channelType = 'email';
    public $channelKey;
    public $channelCarrier;

    protected $listeners = ['channelDeleted' => '$refresh'];

    public function mount($notificationChannels, $notificationEvents, $types, $carriers)
    {
        $this->notificationChannels = $notificationChannels;
        $this->notificationEvents = $notificationEvents;
        $this->types = $types;
        $this->carriers = $carriers;
    }

    public function render()
    {
        return view('livewire.notification-channels');
    }

    public function showCreateForm()
    {
        $this->showCreateForm = true;
    }

    public function createChannel()
    {
        $validated = $this->validateChannel();

        $insertArray = [
            'type' => $this->channelType,
            'name' => $this->channelName,
            'key'  => $this->channelKey,
        ];

        // massage
        if ($this->channelType == 'sms') {
            // keep only numbers for phone number
            $insertArray['key'] = preg_replace('/[^0-9]/', '', $this->channelKey);
            // we care about carrier field
            $insertArray['carrier'] = $this->channelCarrier;
        }

        $insertArray['user_id'] = \Auth::user()->id;

        try {
            \DB::beginTransaction();
            $channel = NotificationChannel::create($insertArray);
            \Log::channel('notifications')->info('Notification Channel ' . $channel->id . ' created by user ' . \Auth::user()->email, [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'success',
                'data'      => [
                    'notification_channel' => $channel,
                    'current_user'         => \Auth::user()
                ]
            ]);

            // create initial verification
            $verificationArray = [
                'notification_channel_id' => $channel->id,
                'code'                    => rand(10000, 999999),
                'expires_at'              => Carbon::now()->addMinutes(config('ccps.notifications.channel_verification_code_ttl_minutes')),
            ];
            $verification = ChannelVerification::create($verificationArray);

            $verifyWithinString = config('ccps.notifications.channel_verification_code_ttl_minutes'). ' '
                . \Str::plural('minute', config('ccps.notifications.channel_verification_code_ttl_minutes'));


            // queue job
            \Log::channel('notifications')->info('Initial Verification ID ' . $verification->id
                . ' generated for Channel ' . $channel->id . '. Must be verified within ' . $verifyWithinString, [
                    'category'  => 'model',
                    'operation' => 'create',
                    'result'    => 'success',
                    'data'      => [
                        'channel_verification' => $verification,
                        'current_user'         => \Auth::user(),
                        'channel'              => $channel
                    ]
            ]);

            // send synchronously
            $channel->notify(new VerifyChannel($verification));

            $this->notificationChannels->push($channel);

            $this->dispatchBrowserEvent('channel-created'); // for alpine
            flash('Channel added - verification required. Code sent - please verify within ' . $verifyWithinString)->success()->livewire($this);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('notifications')
                ->error('Unexpected error while attempting to add Notification Channel', [
                    'category'  => 'model',
                    'operation' => 'create',
                    'result'    => 'error',
                    'data'      => [
                        'message' => $e->getMessage()
                    ]
                ]);

            flash('Error while trying to add channel. Please contact an administrator.')->error()->livewire($this);
        }

        $this->reset(['channelType', 'channelName', 'channelKey', 'channelCarrier', 'showCreateForm']);
    }

    protected function validateChannel(NotificationChannel $channel = null)
    {
        $rules = [
            'channelType' => 'required|in:email,sms,google-chat,slack',
            'channelName' => [
                'required',
                Rule::unique('ccps_notification_channels', 'name')->where(function ($query) use ($channel) {
                    $query = $query->where('user_id', \Auth::user()->id);

                    if (!is_null($channel)) {
                        $query = $query->where('id', '!=', $channel->id);
                    }

                    return $query;
                })
            ],
        ];

        switch ($this->channelType) {
            case 'email':
                $rules['channelKey'] = [
                    'required',
                    'email',
                ];
                break;
            case 'sms':
                $rules['channelKey'] = [
                    'required',
                    'phone:US',
                ];
                $rules['channelCarrier'] = 'in:verizon,att,sprint,tmobile';
                break;
            case 'google-chat':
                $rules['channelKey'] = [
                    'required',
                    'url',
                    new GoogleChatWebhook,
                ];
                break;
            case 'slack':
                $rules['channelKey'] = [
                    'required',
                    'url',
                    new SlackWebhook,
                ];
                break;
            default:
                break;
        }

        $rules['channelKey'][] = Rule::unique('ccps_notification_channels', 'key')->where(function ($query) use ($channel) {
            $query = $query->where('user_id', \Auth::user()->id);

            if (!is_null($channel)) {
                $query = $query->where('id', '!=', $channel->id);
            }

            return $query;
        });

        return $this->validate($rules);
    }
}
