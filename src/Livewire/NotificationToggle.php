<?php

namespace Uncgits\Ccps\Livewire;

use Livewire\Component;
use App\CcpsCore\NotificationChannel;

class NotificationToggle extends Component
{
    public $active;
    public $channel;
    public $event;

    public function mount($channel, $event, $active = false)
    {
        $this->channel = $channel;
        $this->event = $event;
        $this->active = $active;
    }

    public function render()
    {
        return view('ccps::livewire.notification-toggle');
    }

    public function updatingActive($value)
    {
        if ($value) {
            NotificationChannel::find($this->channel)->notification_events()->attach($this->event);
        } else {
            NotificationChannel::find($this->channel)->notification_events()->detach($this->event);
        }
    }
}
