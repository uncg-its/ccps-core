<?php

namespace Uncgits\Ccps\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use App\CcpsCore\ChannelVerification;
use App\Notifications\CcpsCore\VerifyChannel;
use App\Notifications\CcpsCore\TestNotification;

class NotificationChannel extends Component
{
    public $channel;
    public $showVerifyForm = false;
    public $code;

    public function mount($channel)
    {
        $this->channel = $channel;
    }

    public function render()
    {
        return view('ccps::livewire.notification-channel');
    }

    public function sendTestNotification()
    {
        try {
            if (auth()->user()->notification_channels->where('id', $this->channel->id)->isEmpty()) {
                throw new \Exception('Channel ' . $this->channel->id . ' either does not exist or does not belong to user');
            }
            $this->channel->notify(new TestNotification);
            flash('Test notification sent!')->success()->livewire($this);
        } catch (\Throwable $th) {
            \Log::channel('notifications')->error('Error sending test notification to channel ' . $this->channel->id, [
                'category'  => 'notification',
                'operation' => 'send',
                'result'    => 'failure',
                'data'      => [
                    'notification_channel'     => $this->channel,
                    'notification_class'       => TestNotification::class,
                    'success'                  => false,
                    'message'                  => $th->getMessage()
                ]
            ]);
            flash('Error sending test notification - please contact an admin.')->error()->livewire($this);
        }
    }

    public function resendVerification()
    {
        // invalidate all previous verifications for this channel
        $existingVerifications = ChannelVerification::where('notification_channel_id', $this->channel->id)
            ->whereNull('used_at');

        $existingVerifications->delete();

        // generate a new verification
        $verification = ChannelVerification::create([
            'notification_channel_id' => $this->channel->id,
            'code'                    => rand(10000, 999999),
            'expires_at'              => Carbon::now()->addMinutes(config('ccps.notifications.channel_verification_code_ttl_minutes')),
        ]);

        $verifyWithinString = config('ccps.notifications.channel_verification_code_ttl_minutes') . ' ' . \Str::plural(
            'minute',
            config('ccps.notifications.channel_verification_code_ttl_minutes')
        );

        // queue job
        \Log::channel('notifications')->info('User ' . auth()->user()->email . ' requested verification code re-send for Channel ID ' . $this->channel->id . '. Verification generated, ID ' . $verification->id . '. Must be verified within ' . $verifyWithinString, [
            'category'  => 'model',
            'operation' => 'create',
            'result'    => 'success',
            'data'      => [
                'channel_verification' => $verification,
                'current_user'         => auth()->user(),
                'channel'              => $this->channel
            ]
        ]);

        $this->channel->notify(new VerifyChannel($verification));

        flash('Verification code re-sent - please verify within ' . $verifyWithinString)->success()->livewire($this);
    }

    public function showVerifyForm()
    {
        $this->showVerifyForm = true;
    }

    public function verify()
    {
        try {
            $verification = ChannelVerification::where('notification_channel_id', $this->channel->id)
                ->whereNull('used_at')
                ->firstOrFail();

            $now = Carbon::now();

            if ($verification->expires_at <= $now) {
                \DB::transaction(function () use ($verification) {
                    $verification->delete();
                });
                \Log::channel('notifications')->notice('User ' . auth()->user()->email . ' attempted to verify Channel ID ' . $this->channel->id . ', but Verification ID ' . $verification->id . ' was expired.', [
                    'category'  => 'model',
                    'operation' => 'update',
                    'result'    => 'failure',
                    'data'      => [
                        'action'               => 'verify',
                        'channel_verification' => $verification,
                        'current_user'         => auth()->user(),
                        'channel'              => $this->channel
                    ]
                ]);
                throw new \ErrorException('Request has expired. Please create a new one.');
            }

            if ($this->code) {
                // do the verification

                if ($verification->code == $this->code) {
                    \DB::beginTransaction();
                    // success
                    $verification->update([
                        'used_at' => $now,
                    ]);

                    $this->channel->update([
                        'verified_at' => $now,
                    ]);

                    \Log::channel('notifications')->info('User ' . auth()->user()->email . ' successfully verified Channel ID ' . $this->channel->id . ' via Verification ID ' . $verification->id . '.', [
                        'category'  => 'model',
                        'operation' => 'update',
                        'result'    => 'success',
                        'data'      => [
                            'action'               => 'verify',
                            'channel_verification' => $verification,
                            'current_user'         => auth()->user(),
                            'channel'              => $this->channel
                        ]
                    ]);
                    flash('Successfully verified notification channel. You may begin using this channel.')->success()->livewire($this);
                    \DB::commit();
                } else {
                    \Log::channel('notifications')->notice('User ' . auth()->user()->email . ' attempted to verify Channel ID ' . $this->channel->id . ', but the provided code (' . $this->code . ') was incorrect.', [
                        'category'  => 'model',
                        'operation' => 'update',
                        'result'    => 'success',
                        'data'      => [
                            'action'               => 'verify',
                            'channel_verification' => $verification,
                            'current_user'         => auth()->user(),
                            'channel'              => $this->channel,
                            'code'                 => $this->code
                        ]
                    ]);
                    flash('Incorrect verification code.')->error()->livewire($this);
                }
            } else {
                return view('account.notifications.channels.verify')->with(compact('channel'));
            }
        } catch (ModelNotFoundException $e) {
            \Log::channel('notifications')->notice('User ' . auth()->user()->email . ' attempted to verify Channel ID ' . $this->channel->id . ', but Verification code ' . $this->code . ' was not found.', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'failure',
                'data'      => [
                    'action'       => 'verify',
                    'current_user' => auth()->user(),
                    'channel'      => $this->channel,
                    'code'         => $this->code
                ]
            ]);
            flash('Verification request not found.')->error()->livewire($this);
        } catch (\ErrorException $e) {
            \DB::rollBack();
            flash($e->getMessage())->error()->livewire($this);
        } catch (\Exception $e) {
            \Log::channel('notifications')->warning('Unexpected error while verifying notification channel ' . $this->channel->id, [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'action'       => 'verify',
                    'current_user' => auth()->user(),
                    'channel'      => $this->channel,
                    'message'      => $e->getMessage()
                ]
            ]);
        }

        $this->code = null;
        $this->showVerifyForm = false;

        $this->emit('channelVerified', $this->channel->id);
    }

    public function deleteChannel()
    {
        try {
            \DB::beginTransaction();
            $this->channel->notification_events()->sync([]);
            $this->channel->delete();
            \Log::channel('notifications')->info('Notification Channel ' . $this->channel->id . ' deleted by user ' . auth()->user()->email, [
                'category'  => 'model',
                'operation' => 'delete',
                'result'    => 'success',
                'data'      => [
                    'notification_channel' => $this->channel,
                    'current_user'         => auth()->user()
                ]
            ]);
            flash('Channel deleted successfully.')->success()->livewire($this);

            $this->emit('channelDeleted', $this->channel->id);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('notifications')->error('Unexpected error while attempting to delete Notification Channel ' . $this->channel->id, [
                'category'  => 'model',
                'operation' => 'delete',
                'result'    => 'error',
                'data'      => [
                    'notification_channel' => $this->channel,
                    'current_user'         => auth()->user(),
                    'message'              => $e->getMessage()
                ]
            ]);
            flash('Error while deleting channel. Please contact an administrator.')->error()->livewire($this);
        }
    }
}
