<?php

namespace Uncgits\Ccps\Livewire;

use App\CcpsCore\CronjobMeta;
use Livewire\Component;

class CronjobRow extends Component
{
    public $cronjobId;

    public function mount($cronjobId)
    {
        $this->cronjobId = $cronjobId;
    }

    public function getCronjobProperty()
    {
        $meta = CronjobMeta::find($this->cronjobId);
        $class = new $meta->class;
        // $class->meta = $meta;

        return $class;
    }

    public function render()
    {
        return view('livewire.cronjob-row');
    }

    public function run()
    {
        \Log::channel('cron')->info('Manual run for cron job ' . $this->cronjob->getDisplayName() . ' initiated.', [
            'category'  => 'cron',
            'operation' => 'start',
            'result'    => 'success',
            'data'      => [
                'source'       => 'manual',
                'cronjob'      => $this->cronjob,
                'current_user' => auth()->user()
            ]
        ]);

        $result = $this->cronjob->run(); // returns implementation of CronjobResultInterface

        if (! $result->wasSuccessful()) {
            // the cronjob failed in an expected way. e.g. the result was not what we expected.
            flash("Cron job {$this->cronjob->getDisplayName()} failed. Message: " . $result->getMessage())->error()->livewire($this);
        } else {
            flash("Cron job {$this->cronjob->getDisplayName()} executed successfully.")->success()->livewire($this);
        }
    }

    public function enable()
    {
        $this->changeStatus('enabled');
    }

    public function disable()
    {
        $this->changeStatus('disabled');
    }

    public function clearLockFile()
    {
        $lockFile = storage_path('cron/' . $this->cronjob->getLockFileName());
        unlink($lockFile);
        if (file_exists($lockFile)) {
            flash('Error: could not delete lock file. Check logs.')->error()->livewire($this);
        } else {
            flash('Lock file ' . $this->cronjob->getDisplayName() . ' deleted.')->success()->livewire($this);
        }

        $this->emit('lockFileCleared');
    }

    private function changeStatus($status)
    {
        $result = $this->cronjob->getMeta()->update(['status' => $status]);

        if ($result) {
            flash("Cron job {$this->cronjob->getMeta()->display_name} set to $status.")->success()->livewire($this);
        } else {
            flash('Could not change cron job status!')->error()->livewire($this);
        }
    }
}
