<?php

namespace Uncgits\Ccps\Livewire;

use App\CcpsCore\NotificationChannel;
use Livewire\Component;

class NotificationSettings extends Component
{
    public $channels;
    public $events;

    protected $listeners = ['channelVerified', 'channelDeleted'];

    public function mount($channels, $events)
    {
        $this->channels = $channels;
        $this->events = $events;
    }

    public function render()
    {
        return view('livewire.notification-settings');
    }

    public function channelVerified($channelId)
    {
        $channelAdded = NotificationChannel::with('notification_events')->find($channelId);
        $this->channels->push($channelAdded);
    }

    public function channelDeleted($channelId)
    {
        //
    }
}
