<?php

namespace Uncgits\Ccps\Livewire;

use Livewire\Component;

class ApiTokens extends Component
{
    public $showAll;
    public $tokens;
    public $showCreateForm = false;

    public $tokenName = null;
    public $tokenExpires = null;
    public $plainTextToken = null;

    protected $listeners = ['tokenRevoked'];

    public function mount($tokens, $showAll = false)
    {
        $this->tokens = $tokens;
        $this->showAll = $showAll;
    }

    public function render()
    {
        return view('livewire.api-tokens');
    }

    public function showCreateForm()
    {
        $this->showCreateForm = true;
        $this->reset('plainTextToken');
    }

    public function createToken()
    {
        $this->validate([
            'tokenName'    => 'required|unique:personal_access_tokens,name,NULL,id,tokenable_id,' . \Auth::user()->id,
            'tokenExpires' => 'nullable',
        ]);

        $expiresAt = is_null($this->tokenExpires) ? null : now()->addMonths($this->tokenExpires);

        try {
            \DB::beginTransaction();

            $token = \Auth::user()->createToken($this->tokenName);
            $token->accessToken->expires_at = $expiresAt;
            $token->accessToken->abilities = [];
            $token->accessToken->save();

            flash('Token created successfully.')->success()->livewire($this);
            \Log::channel('access')->info('API Token created', [
                'category'  => 'api-tokens',
                'operation' => 'create',
                'result'    => 'success',
                'data'      => [
                    'token_id'     => $token->accessToken->id,
                    'current_user' => \Auth::user()->email
                ]
            ]);

            $this->plainTextToken = $token->plainTextToken;
            $this->tokens->push($token->accessToken);

            $this->reset(['tokenName', 'tokenExpires', 'showCreateForm']);

            $this->dispatchBrowserEvent('token-created');

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            flash('Error creating token: ' . $th->getMessage())->error()->livewire($this);

            \Log::channel('access')->error('API Token creation error', [
                'category'  => 'api-tokens',
                'operation' => 'create',
                'result'    => 'failure',
                'data'      => [
                    'current_user' => \Auth::user()->email,
                    'message'      => $th->getMessage()
                ]
            ]);
        }
    }

    public function tokenRevoked($revokedTokenId)
    {
        // $this->tokens = $this->tokens->reject(function ($token) use ($revokedTokenId) {
        //     return $token->id == $revokedTokenId;
        // });
    }
}
