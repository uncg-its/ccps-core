<?php

namespace Uncgits\Ccps\Livewire;

use Livewire\Component;
use Symfony\Component\Finder\Finder;
use Dubture\Monolog\Reader\LogReader;
use Illuminate\Support\LazyCollection;

class LogViewer extends Component
{
    public $logFiles;
    public $selectedFile;
    public $lines;
    public $page = 0;
    public $perPage = 0;
    public $pages;
    public $search = '';
    public $sort = 'desc';

    public $level = 'all';

    public $perPageOptions;
    public $levels = [
        'all'       => 'all',
        'DEBUG'     => 'debug',
        'INFO'      => 'info',
        'NOTICE'    => 'notice',
        'WARNING'   => 'warning',
        'ERROR'     => 'error',
        'CRITICAL'  => 'critical',
        'ALERT'     => 'alert',
        'EMERGENCY' => 'emergency',
    ];
    public $pageList = [];

    private function getFilesList($folder = null)
    {
        $logPath = config('ccps.log-viewer.storage_path');
        $searchPath = is_null($folder) ? $logPath : $logPath .= '/' . $folder;

        $finder = new Finder;
        $finder->files()->in($searchPath); // is already recursive

        $files = [];
        foreach ($finder as $file) {
            $path = $file->getRealPath();
            $name = \Str::replaceFirst($logPath . '/', '', $file->getRealPath());
            $files[$name] = $path;
        }

        ksort($files);
        return $files;
    }

    private function calculatePageList()
    {
        $current = $this->page; // zero-index
        $max = $this->pages; // literal count

        if ($max === 0) {
            return [];
        }

        $buttonRange = 5; // on each end, or in middle

        $ellipsis = '...';

        $pages = [];

        // if the total page count is less than 3+1+3 = 7, then just show them all
        if ($max < ($buttonRange * 2 + 1)) {
            foreach (range(1, $max) as $number) {
                $pages[] = $number;
            }
            return $pages;
        }

        // if the page is within the first 3 or last 3, then render both ends with ellipsis in middle
        if ($current < $buttonRange || $current >= $max - $buttonRange) {
            // start range
            foreach (range(1, $buttonRange) as $number) {
                $pages[] = $number;
            }

            $pages[] = $ellipsis;

            // end range
            foreach (range($max - $buttonRange + 1, $max) as $number) {
                $pages[] = $number;
            }

            return $pages;
        }

        // if the page is in the middle, render a group of 3, and the start/end
        $pages[] = '1';

        if ($current >= $buttonRange) {
            $pages[] = $ellipsis;
        }

        $surrounding = floor($buttonRange / 2); // on each side.
        foreach (range($current + 1 - $surrounding, $current + 1 + $surrounding) as $number) {
            $pages[] = $number;
        }

        if ($current < $max - $buttonRange + 1) {
            $pages[] = $ellipsis;
        }

        $pages[] = $max;

        return $pages;
    }

    public function mount()
    {
        $this->perPageOptions = config('ccps.log-viewer.per_page_options');
        $this->logFiles = $this->getFilesList();
        $this->selectedFile = array_key_first($this->logFiles);
        $this->loadLines();
    }

    protected function loadLines()
    {
        ini_set('max_execution_time', 180); // hacky but necessary for larger files

        // generator from file
        $generator = LazyCollection::make(function () {
            $reader = new LogReader($this->logFiles[$this->selectedFile], '/\[(?P<date>.*)\] (?P<logger>\w+).(?P<level>\w+): (?P<message>.+)(?P<context>)(?P<extra>)/');
            foreach ($reader as $log) {
                if ($log !== []) {
                    if ($this->level === 'all' || \Str::lower($this->level) === \Str::lower($log['level'])) {
                        if (empty($this->search) || \Str::contains($log['message'], $this->search)) {
                            yield $log;
                        }
                    }
                }
            }
        });

        $count = $generator->count();

        $this->pages = (int)ceil($count / $this->linesPerPage);

        if ($this->sort === 'asc') {
            $entries = $generator->skip($this->linesPerPage * $this->page)->take($this->linesPerPage);
        } else {
            $skip = $count - ($this->linesPerPage * ($this->page + 1));
            if ($skip < 0) {
                $skip = 0;
            }
            $entries = $generator->skip($skip)->take($this->linesPerPage)->reverse()->values();
        }

        $this->lines = $entries->toArray();
        $this->pageList = $this->calculatePageList();
    }

    public function previousPage()
    {
        if ($this->page > 0) {
            $this->setPage($this->page - 1);
        }
    }

    public function nextPage()
    {
        if ($this->page < $this->pages - 1) {
            $this->setPage($this->page + 1);
        }
    }

    public function setPage($page)
    {
        $this->page = $page;
        $this->loadLines();
    }

    public function updatedPerPage($value)
    {
        $this->reset(['page']);
        $this->loadLines();
    }

    public function updatedSelectedFile($value)
    {
        $this->reset(['page', 'level', 'search']);
        $this->loadLines();
    }

    public function updatedSearch($value)
    {
        $this->reset(['page']);
        $this->loadLines();
    }

    public function updatedSort($value)
    {
        $this->reset(['page']);
        $this->loadLines();
    }

    public function updatedLevel($value)
    {
        $this->loadLines();
    }

    public function render()
    {
        return view('livewire.log-viewer');
    }

    public function getLinesPerPageProperty()
    {
        return $this->perPageOptions[$this->perPage];
    }

    public function reload()
    {
        $this->loadLines();
    }
}
