<?php

namespace Uncgits\Ccps\Livewire;

use Livewire\Component;
use Laravel\Sanctum\PersonalAccessToken;

class ApiToken extends Component
{
    public $token;

    public function mount($token)
    {
        $this->token = $token;
    }

    public function render()
    {
        return view('livewire.api-token');
    }

    public function revokeToken()
    {
        $userTokens = \Auth::user()->tokens;
        if (! \Auth::user()->hasPermission('tokens.admin') && ! $userTokens->contains('id', $this->token->id)) {
            flash('Error: you may not revoke this token.')->error()->livewire($this);
        }

        try {
            \DB::beginTransaction();

            PersonalAccessToken::where('id', $this->token->id)->delete();
            flash('Token revoked successfully.')->success()->livewire($this);
            \Log::channel('access')->info('API Token revoked', [
                'category'  => 'api-tokens',
                'operation' => 'revoke',
                'result'    => 'success',
                'data'      => [
                    'token_id'     => $this->token->id,
                    'current_user' => \Auth::user()->email
                ]
            ]);

            $this->emit('tokenRevoked', $this->token->id);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();

            flash('Error revoking token.')->error()->livewire($this);
            \Log::channel('access')->error('API Token revocation error', [
                'category'  => 'api-tokens',
                'operation' => 'revoke',
                'result'    => 'error',
                'data'      => [
                    'token_id'     => $this->token->id,
                    'current_user' => \Auth::user()->email,
                    'message'      => $th->getMessage()
                ]
            ]);
        }
    }
}
