<?php

namespace Uncgits\Ccps\View\Components;

use Illuminate\View\Component;

class Toggle extends Component
{
    public $on;
    public $offTextColor;
    public $onTextColor;
    public $offBgColor;
    public $onBgColor;
    public $offIcon;
    public $onIcon;

    protected $bgColorMap = [
        'gray'   => 'bg-gray-300',
        'green'  => 'bg-green-500',
        'red'    => 'bg-red-500',
        'blue'   => 'bg-blue-500',
        'yellow' => 'bg-yellow-500'
    ];

    protected $textColorMap = [
        'gray'   => 'text-gray-300',
        'green'  => 'text-green-500',
        'red'    => 'text-red-500',
        'blue'   => 'text-blue-500',
        'yellow' => 'text-yellow-500'
    ];
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($on = false, $offColor = 'gray', $onColor = 'green', $offIcon = 'times', $onIcon = 'check')
    {
        $this->on = $on;
        $this->offBgColor = $this->bgColorMap[$offColor] ?: $this->bgColorMap['gray'];
        $this->onBgColor = $this->bgColorMap[$onColor] ?: $this->bgColorMap['gray'];
        $this->offTextColor = $this->textColorMap[$offColor] ?: $this->textColorMap['gray'];
        $this->onTextColor = $this->textColorMap[$onColor] ?: $this->textColorMap['gray'];
        $this->onIcon = $onIcon;
        $this->offIcon = $offIcon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.toggle');
    }
}
