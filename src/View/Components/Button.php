<?php

namespace Uncgits\Ccps\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $baseClasses;
    public $href;
    public $disabled;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($size = 'sm', $color = 'blue', $textColor = 'white', $href = false, $disabled = false)
    {
        $sizeClasses = [
            'sm' => 'px-2 py-1 text-xs leading-4 rounded',
            'md' => 'px-4 py-2 text-sm leading-5 rounded-md',
            'lg' => 'px-6 py-3 text-base leading-6 rounded-md',
        ];

        $this->baseClasses = "flex items-center gap-1 border border-transparent font-medium text-$textColor disabled:opacity-50 disabled:cursor-wait bg-$color-600 hover:bg-$color-500 focus:outline-none focus:border-$color-700 focus:shadow-outline-$color active:bg-$color-700 transition ease-in-out duration-150 " . $sizeClasses[$size];

        $this->href = $href;
        $this->disabled = $disabled;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.button');
    }
}
