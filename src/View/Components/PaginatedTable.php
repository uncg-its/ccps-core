<?php

namespace Uncgits\Ccps\View\Components;

use Illuminate\View\Component;

class PaginatedTable extends Component
{
    public $collection;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.paginated-table');
    }
}
