<?php

namespace Uncgits\Ccps\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Uncgits\Ccps\Messages\GoogleChatMessage;

class TestNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        // $notifiable should be an instance of NotificationChannel
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.test-notification', ['channel' => $notifiable]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)->content('This is a test message from ' . config('app.name') . ' to notification channel "' . $notifiable->name . '"')->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->view('emails.plaintext.test-notification', ['channel' => $notifiable]);
    }

    public function toSlack($notifiable)
    {
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content('This is a test message from ' . config('app.name') . ' to notification channel "' . $notifiable->name . '"');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
