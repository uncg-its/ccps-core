<?php

namespace Uncgits\Ccps\Notifications;

use Illuminate\Bus\Queueable;
use App\CcpsCore\ChannelVerification;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class VerifyChannel extends Notification
{
    use Queueable;
    /**
     * @var ChannelVerification
     */
    private $verification;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ChannelVerification $verification)
    {
        $this->verification = $verification;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        // $notifiable should be an instance of NotificationChannel
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.channel-verification', ['verification' => $this->verification]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)->content('Please verify this device as a notification channel in the ' . config('app.name') . ' application with code: ' . $this->verification->code . ' - you may log in at ' . config('app.url') . ' and browse to Notification Settings')->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->view('emails.plaintext.channel-verification', ['verification' => $this->verification]);
    }

    public function toSlack($notifiable)
    {
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content('Please verify this device as a notification channel in the ' . config('app.name') . ' application with code: ' . $this->verification->code . ' - you may log in at ' . config('app.url') . ' and browse to Notification Settings');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
