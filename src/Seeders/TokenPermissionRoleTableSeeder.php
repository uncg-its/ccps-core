<?php

namespace Uncgits\Ccps\Seeders;

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Seeder;

class TokenPermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // mapping
        $rolePermissionMap = [
            // enter mappings here

            'admin' => [
               'tokens.create',
               'tokens.edit',
               'tokens.revoke',
               'tokens.admin'
            ]
        ];


        // laratrust configuration

        try {
            \DB::beginTransaction();
            $permissionRoleArrayConstruction = ["role_id" => ""];

            foreach ($rolePermissionMap as $role => $permissions) {
                $roleModel = Role::where('name', $role)->firstOrFail();
                foreach ($permissions as $permission) {
                    $permissionModel = Permission::where('name', $permission)->firstOrFail();
                    $insertArray = [
                        'role_id'       => $roleModel->id,
                        'permission_id' => $permissionModel->id
                    ];

                    \DB::table('ccps_permission_role')->insert($insertArray);
                }
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            $output->error('Error during seeding: ' . $e->getMessage());
        }
    }
}
