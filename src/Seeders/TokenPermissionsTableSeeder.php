<?php

namespace Uncgits\Ccps\Seeders;

use App\CcpsCore\Permission;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

class TokenPermissionsTableSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        // fill in permissions here
        [
           "name"         => "tokens.create",
           "display_name" => "API Tokens - create",
           "description"  => "Create API Tokens",
        ],
        [
           "name"         => "tokens.edit",
           "display_name" => "API Tokens - edit",
           "description"  => "Edit API Tokens",
        ],
        [
           "name"         => "tokens.revoke",
           "display_name" => "API Tokens - revoke",
           "description"  => "Revoke API Tokens",
        ],
        [
           "name"         => "tokens.admin",
           "display_name" => "API Tokens - admin",
           "description"  => "Administer API Tokens on behalf of other users",
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'uncgits/ccps-core',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
