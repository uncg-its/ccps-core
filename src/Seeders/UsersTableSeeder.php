<?php

namespace Uncgits\Ccps\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ccps_users')->insert([
            'username'   => "admin",
            'password'   => bcrypt("admin"),
            'first_name' => "Admin",
            'last_name'  => "McAdmin",
            'email'      => "admin@admin.com",
            'time_zone'  => config('app.timezone', 'UTC'),
            'created_at' => date("Y-m-d H:i:s", time()),
            'updated_at' => date("Y-m-d H:i:s", time())
        ]);
    }
}
