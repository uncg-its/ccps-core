<?php

namespace Uncgits\Ccps\Seeders;

use App\CcpsCore\Permission;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

class PermissionsTableSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        // user list
        [
            "name"         => "users.view",
            "display_name" => "Users - View",
            "description"  => "View the user list and details",
        ],
        [
            "name"         => "users.edit",
            "display_name" => "Users - Edit",
            "description"  => "Edit users of the application and their roles / permissions"
        ],
        [
            "name"         => "users.delete",
            "display_name" => "Users - Delete",
            "description"  => "Delete users of the application"
        ],
        [
            "name"         => "users.create",
            "display_name" => "Users - Create",
            "description"  => "Create new users in the application"
        ],

        // ACL
        [
            "name"         => "acl.view",
            "display_name" => "ACL - View",
            "description"  => "View the application ACL (roles and permissions)"
        ],
        [
            "name"         => "acl.edit",
            "display_name" => "ACL - Edit",
            "description"  => "Edit the application ACL (roles and permissions)"
        ],
        [
            "name"         => "acl.delete",
            "display_name" => "ACL - Delete",
            "description"  => "Delete roles and permissions from the ACL"
        ],
        [
            "name"         => "acl.create",
            "display_name" => "ACL - Create",
            "description"  => "Create new roles and permissions in the ACL"
        ],

        // EMAIL
        [
            "name"         => "email.view",
            "display_name" => "Email - View",
            "description"  => "View the application Email Queue and Recipient Lists"
        ],

        // Queues
        [
            "name"         => "queues.view",
            "display_name" => "Queues - View",
            "description"  => "View application Queues"
        ],

        [
            "name"         => "queues.edit",
            "display_name" => "Queues - Edit",
            "description"  => "Edit application Queues (requeue, delete, etc.)"
        ],

        // Cron Jobs
        [
            "name"         => "cronjobs.view",
            "display_name" => "Cron Jobs - View",
            "description"  => "View application Cron Jobs"
        ],

        [
            "name"         => "cronjobs.edit",
            "display_name" => "Cron Jobs - Edit",
            "description"  => "Edit application Cron Jobs, statuses, etc."
        ],

        [
            "name"         => "cronjobs.run",
            "display_name" => "Cron Jobs - Run",
            "description"  => "Manually run application Cron Jobs"
        ],

        // Application Config
        [
            "name"         => "config.view",
            "display_name" => "Application Configuration - View",
            "description"  => "View the application's configuration variables"
        ],
        [
            "name"         => "config.edit",
            "display_name" => "Application Configuration - Edit",
            "description"  => "Edit the application's configuration variables"
        ],

        // Logs Config
        [
            "name"         => "logs.view",
            "display_name" => "Logs - View",
            "description"  => "View the application's logs"
        ],

        // Cache
        [
            "name"         => "cache.clear",
            "display_name" => "Cache - Clear",
            "description"  => "View and modify the application's cache"
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'uncgits/ccps-core',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
