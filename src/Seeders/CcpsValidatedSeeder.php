<?php

namespace Uncgits\Ccps\Seeders;

use Illuminate\Database\Seeder;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

abstract class CcpsValidatedSeeder extends Seeder
{
    protected $permissionArrayConstruction = ["name" => "", "display_name" => "", "description" => ""];
    protected $roleArrayConstruction = ["name" => "", "display_name" => "", "description" => ""];



    public function validateSeedData($input, $construction)
    {
        foreach ($input as $index => $seedData) {
            $diff = array_diff_key($seedData, $construction);
            if (!empty($diff)) {
                throw new InvalidSeedDataException('Seed data invalid at index ' . $index . '; format incorrect.');
            }
        }

        return true;
    }

    public function checkForExistingSeedData($input, $existingDataCollection, $key = 'name')
    {
        foreach ($input as $index => $seedData) {
            if ($existingDataCollection->contains($key, $seedData[$key])) {
                throw new InvalidSeedDataException('Seed data invalid at index ' . $index . '; item with \'' . $key . '\' = \'' . $seedData[$key] . '\' already exists in table.');
            }
        }

        return true;
    }

    public function commitSeedData($seedRows, $destinationTable, $mergeData = null)
    {
        \DB::transaction(function () use ($seedRows, $destinationTable, $mergeData) {
            foreach ($seedRows as $row) {
                if (!is_null($mergeData)) {
                    $insertArray = array_merge($row, $mergeData);
                } else {
                    $insertArray = $row;
                }
                \DB::table($destinationTable)->insert($insertArray);
            }
        });
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // override in child
    }
}
