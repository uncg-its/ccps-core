<?php

namespace Uncgits\Ccps\Seeders;

use App\CcpsCore\Role;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

class RolesTableSeeder extends CcpsValidatedSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        $roles = [
            [
                "name"         => "admin",
                "display_name" => "Administrator",
                "description"  => "Application Administrator"
            ],
            [
                "name"         => "authenticated-user",
                "display_name" => "Authenticated User",
                "description"  => "Default logged-in user"
            ],
        ];



        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($roles, $this->roleArrayConstruction);
            $this->checkForExistingSeedData($roles, Role::all());

            $mergeData = [
                'source_package' => 'uncgits/ccps-core',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($roles, 'ccps_roles', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
