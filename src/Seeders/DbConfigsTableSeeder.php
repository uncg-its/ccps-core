<?php

namespace Uncgits\Ccps\Seeders;

use Illuminate\Database\Seeder;
use App\CcpsCore\DbConfig;

class DbConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            "debugBar" => "disabled"
        ];


        foreach($configs as $key => $value) {
            DbConfig::updateOrCreate(['key' => $key], ['value' => $value]);
        }

    }
}
