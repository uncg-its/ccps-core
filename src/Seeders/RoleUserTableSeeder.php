<?php

namespace Uncgits\Ccps\Seeders;

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // laratrust configuration

        $roleUserArrayConstruction = ["user_id" => "", "role_id" => "", "user_type" => ""];

        $roleUserMappings = [
            [
                "user_id"   => "1",
                "role_id"   => "1",
                "user_type" => "App\\CcpsCore\\User"
            ],
        ];

        // validate
        $valid = true;
        foreach ($roleUserMappings as $index => $mappingArray) {
            $diff = array_diff_key($mappingArray, $roleUserArrayConstruction);
            if (!empty($diff)) {
                $valid = false;
                if ($writeConsoleOutput) {
                    $output->text('Cannot seed - source data format is invalid at index ' . $index);
                    return;
                }
            }
        }

        foreach ($roleUserMappings as $mappingArray) {
            \DB::table('ccps_role_user')->insert($mappingArray);
        }
    }
}
