<?php

namespace Uncgits\Ccps\Seeders;

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // laratrust configuration

        $permissionRoleArrayConstruction = ["role_id" => ""];

        $permissionRoleShell = [
            "role_id" => "1",
        ];

        // validate
        $valid = true;

        $diff = array_diff_key($permissionRoleShell, $permissionRoleArrayConstruction);
        if (!empty($diff)) {
            $valid = false;
            if ($writeConsoleOutput) {
                $output->text('Cannot seed - source data format is invalid.');
                return;
            }
        }

        $permissionsSeeder = new PermissionsTableSeeder();

        foreach ($permissionsSeeder->permissions as $x => $permission) {
            $mappingArray = array_merge($permissionRoleShell, ['permission_id' => $x + 1]);
            \DB::table('ccps_permission_role')->insert($mappingArray);
        }
    }
}
