<?php

namespace Uncgits\Ccps\Traits;

trait ResolvesViewPathFromModuleConfig
{
    protected $viewPath;

    public function resolveViewPath()
    {
        $moduleSettings = config('ccps.modules')[$this->moduleName];
        if ($moduleSettings['custom_view_path']) {
            $viewPath = $moduleSettings['custom_view_path'] . '.';
        } else {
            $viewPath = 'ccps::modules.';
        }

        $this->viewPath = $viewPath;
    }
}
