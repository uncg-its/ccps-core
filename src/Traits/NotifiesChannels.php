<?php

namespace Uncgits\Ccps\Traits;

use Carbon\Carbon;
use App\CcpsCore\User;
use App\CcpsCore\Notification;
use App\CcpsCore\NotificationEvent;
use App\CcpsCore\NotificationChannel;

trait NotifiesChannels
{
    protected $notificationClass = null; // should override in implementing class
    protected $notificationClassArgs = []; // should override in implementing class if the notification class takes arguments
    protected $userIdsToNotify = []; // can override this in each implementing class if only a subset of users should be used

    public function notifySubscribedChannels()
    {
        if (is_null($this->notificationClass)) {
            \Log::channel('notifications')->error('$notificationClass not set on Notification object ' . self::class . '. Notifications cannot proceed for this event.', [
                'category'  => 'notification',
                'operation' => 'send',
                'result'    => 'error',
                'data'      => [
                    'notification_class' => self::class
                ]
            ]);
            return false;
        }

        if (empty($this->userIdsToNotify)) {
            // defaults to all users
            $this->userIdsToNotify = User::all()->pluck('id');
        }

        $channelsToNotify = NotificationChannel::with('notification_events')
            ->whereIn('user_id', $this->userIdsToNotify)
            ->whereHas('notification_events', function ($query) {
                return $query->where('event_class', self::class);
            })
            ->get();

        $channelsToNotify->each(function ($channel) {
            try {
                $event = NotificationEvent::where('event_class', self::class)->firstOrFail();

                if (!$event->event_class::canBeNotified($channel->user)) {
                    \Log::channel('notifications')->warning('User ' . $channel->user->id . ' is configured to receive a notificaiton for event ' . $event->id . ', but the Event policy in ' . $event->event_class . '::canBeNotified() contradicts this. No notification will be sent.', [
                        'category'  => 'notification',
                        'operation' => 'send',
                        'result'    => 'failure',
                        'data'      => [
                            'notification_channel' => $channel,
                            'notification_event'   => $event,
                        ]
                    ]);
                    return;
                }

                $channel->notify(new $this->notificationClass(...$this->notificationClassArgs));

                Notification::create([
                    'notification_channel_id' => $channel->id,
                    'notification_event_id'   => $event->id,
                    'queued_at'               => Carbon::now()->toDateTimeString(),
                ]);

                // will be logged in LogNotificationSent listener class
            } catch (\Throwable $th) {
                \Log::channel('notifications')->error('Error during notification for channel ' . $channel->id . ' during event ' . $event->id . ': ' . $th->getMessage(), [
                    'category'  => 'notification',
                    'operation' => 'send',
                    'result'    => 'error',
                    'data'      => [
                        'notification_channel' => $channel,
                        'notification_event'   => $event,
                    ]
                ]);
            }
        });
    }
}
