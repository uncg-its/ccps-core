<?php


namespace Uncgits\Ccps\Traits;

trait Encryptable
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (config('ccps.encryption') && in_array($key, $this->encryptable)) {
            return decrypt($value);
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (config('ccps.encryption') && in_array($key, $this->encryptable)) {
            $value = encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }

    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        if (config('ccps.encryption')) {
            foreach ($this->encryptable as $key) {
                if (isset($attributes[$key])) {
                    $attributes[$key] = decrypt($attributes[$key]);
                }
            }
        }

        return $attributes;
    }
}
