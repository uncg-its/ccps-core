<?php

namespace Uncgits\Ccps\Models;

use Carbon\Carbon;
use App\CcpsCore\CronjobMeta;
use App\Events\CcpsCore\CronjobFailed;
use Uncgits\Ccps\Helpers\CronSchedule;
use App\Events\CcpsCore\CronjobStarted;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Events\CcpsCore\CronjobFinished;
use App\Events\CcpsCore\CronLockFileConflict;
use Uncgits\Ccps\Interfaces\CronjobResultInterface;

abstract class Cronjob
{
    protected $schedule = '* * * * *';
    protected $display_name = 'cronjob';
    protected $description = 'No description provided';
    protected $tags = [];

    protected $meta;

    public function __construct()
    {
        $this->meta = CronjobMeta::where('class', $this->getClassName())->first();
    }

    public function getMeta()
    {
        return $this->meta;
    }

    public function getDisplayName()
    {
        // if the display name is overridden in the database, use that one; otherwise use the one defined on the class.
        return $this->meta->display_name ?? $this->display_name;
    }

    public function getClassName()
    {
        return get_class($this);
    }

    public function getSchedule()
    {
        // if the schedule is overridden in the database, use that one; otherwise use the one defined on the class.
        return $this->meta->schedule ?? $this->schedule;
    }

    public function getTags()
    {
        // if the tags are overridden in the database, use those values; otherwise use the ones defined on the class.
        $tags = $this->meta->tags ?? $this->tags;
        return is_array($tags) ? $tags : explode(',', $tags); // support both array and comma-separated list
    }

    public function getScheduleAsNaturalLanguage()
    {
        return CronSchedule::fromCronString($this->getSchedule())->asNaturalLanguage();
    }

    public function getDescription()
    {
        // if the description is overridden in the database, use that one; otherwise use the one defined on the class.
        return $this->meta->description ?? $this->description;
    }

    public function getLastRun()
    {
        $value = $this->meta->last_run;

        if (is_null($value)) {
            return 'never';
        }

        $parsed = Carbon::parse($value);
        $now = Carbon::now();
        $daysAgo = $parsed->diffInDays($now);

        if ($daysAgo > 31) {
            return $value;
        }

        return $parsed->diffForHumans();
    }

    public function getLockFileName()
    {
        return \Str::slug($this->getClassName()) . '.lock';
    }

    public function isEnabled()
    {
        return $this->meta->status === 'enabled';
    }

    public function lockFileExists()
    {
        return file_exists(storage_path('cron/' . $this->getLockFileName()));
    }


    private function writeLockFile()
    {
        $cronFolder = storage_path('cron');
        if (!file_exists($cronFolder)) {
            mkdir($cronFolder);
        }
        touch($cronFolder . '/' . $this->getLockFileName());
    }

    private function deleteLockFile()
    {
        $cronFolder = storage_path('cron');
        if (file_exists($cronFolder)) {
            unlink($cronFolder . '/' . $this->getLockFileName());
        }
    }

    // ---- EXECUTION ----

    public function run(): CronjobResultInterface
    {
        $this->preExecution();
        if ($this->lockfileExists()) {
            event(new CronLockFileConflict($this));
            return new CronjobResult(false, 'Lock File exists for this cron job.');
        }

        $this->writeLockFile();

        try {
            $result = $this->execute();
        } catch (\Exception $e) {
            event(new CronjobFailed($this, $e));
            throw $e;
        } finally {
            $this->deleteLockFile();
            $this->postExecution();
        }

        return $result;
    }

    protected function execute()
    {
        // work to be done when command executes - override this in child cronjob class
        // return null on success; return any other value on failure

        return new CronjobResult();
    }

    protected function preExecution()
    {
        // override this in child cronjob class to do more pre-execution
        event(new CronjobStarted($this));
    }

    protected function postExecution()
    {
        // override this in child cronjob class to do more post-execution
        event(new CronjobFinished($this));
    }
}
