<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelVerification extends Model
{
    protected $table = 'ccps_channel_verifications';

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    // relationships

    public function notification_channel()
    {
        return $this->belongsTo(NotificationChannel::class);
    }
}
