<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SentEmail extends Model
{
    use Sortable;

    protected $table = 'ccps_sent_email';
    protected $fillable = ['job_id', 'message_object'];

    public $sortable = ['id', 'job_id', 'created_at'];

    protected $hidden = [
        'message_object',
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'to',
        'from',
        'subject',
        'preview_url'
    ];

    protected function parseRecipients($list, $asString = true)
    {
        if (is_null($list)) {
            return null;
        }

        $recipients = [];
        foreach ($list as $email => $name) {
            if ($name !== null) {
                $recipients[] = $email . " ($name)";
            } else {
                $recipients[] = $email;
            }
        }

        return $asString ? implode(', ', $recipients) : $recipients;
    }

    public function successful_job()
    {
        return $this->belongsTo(SuccessfulJob::class, 'job_id');
    }

    public function getMessageObjectAttribute($item)
    {
        return unserialize($item);
    }


    public function getToAttribute()
    {
        return $this->parseRecipients($this->message_object->getTo());
    }

    public function getFromAttribute()
    {
        return $this->parseRecipients($this->message_object->getFrom());
    }

    public function getCcAttribute()
    {
        return $this->parseRecipients($this->message_object->getCc());
    }

    public function getBccAttribute()
    {
        return $this->parseRecipients($this->message_object->getBcc());
    }

    public function getReplyToAttribute()
    {
        return $this->parseRecipients($this->message_object->getReplyTo());
    }

    public function getSubjectAttribute()
    {
        return $this->message_object->getSubject();
    }

    public function getPreviewUrlAttribute()
    {
        return route('email.sent.show', ['sentEmail' => $this->id]);
    }
}
