<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationEvent extends Model
{
    protected $table = 'ccps_notification_events';

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    // relationships

    public function notification_channels()
    {
        return $this->belongsToMany(NotificationChannel::class, 'ccps_notification_channel_notification_event');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    // other methods

    public static function notifiableForUser()
    {
        return self::all()->filter(function ($event) {
            try {
                $reflection = new \ReflectionMethod($event->event_class, 'canBeNotified');
                if (!$reflection->isStatic()) {
                    throw new \Exception('canBeNotified method on event class ' . $event->event_class . ' must be static.');
                }

                return $event->event_class::canBeNotified();
            } catch (\ReflectionException $e) {
                // class is not defined.
                return true;
            } catch (\Exception $e) {
                throw $e;
            }
        });
    }
}
