<?php

namespace Uncgits\Ccps\Models;

use App\CcpsCore\MappedRoleGroup;
use Kyslik\ColumnSortable\Sortable;
use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    use Sortable;

    protected $guarded = [];

    public $sortable = ['source_package', 'name', 'display_name'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'permissions',
        'users',
    ];

    protected $appends = [
        'permission_list',
        'user_list'
    ];

    // relationships
    public function mapped_role_groups()
    {
        return $this->hasMany(MappedRoleGroup::class, 'role_id');
    }

    public function getDisplayNameAttribute($value)
    {
        return (empty($value)) ? $this->name : $value;
    }

    public function getPermissionListAttribute()
    {
        return $this->permissions->pluck('name');
    }

    public function getUserListAttribute()
    {
        return $this->users->pluck('email');
    }
}
