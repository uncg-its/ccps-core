<?php

namespace Uncgits\Ccps\Models;

use App\CcpsCore\MappedRoleGroup;
use Illuminate\Database\Eloquent\Model;

class RoleMapping extends Model
{
    // attributes

    protected $fillable = ['email', 'mapped_role_group_id'];

    protected $table = 'ccps_role_mappings';

    // relationships

    public function mapped_role_group()
    {
        return $this->belongsTo(MappedRoleGroup::class, 'mapped_role_group_id');
    }
}
