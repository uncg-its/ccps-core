<?php

namespace Uncgits\Ccps\Models;

use App\CcpsCore\Role;
use App\CcpsCore\RoleMapping;
use Illuminate\Database\Eloquent\Model;

class MappedRoleGroup extends Model
{
    // properties

    protected $guarded = ['id'];

    protected $table = 'ccps_mapped_role_groups';

    // relationships

    public function role_mappings()
    {
        return $this->hasMany(RoleMapping::class, 'mapped_role_group_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    // accessors

    public function getSourceAttribute()
    {
        if (is_null($this->sync_service)) {
            return 'n/a';
        }
        return $this->sync_entity_id . ' (' . $this->sync_service . ')';
    }
}
