<?php

namespace Uncgits\Ccps\Models;

class EmailQueue extends Job {

    public $sortable = ['id', 'attempts', 'created_at'];

    public $casts = [
        'reserved_at' => 'datetime:Y-m-d H:i:s',
        'available_at' => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function getRecipientsAttribute() {
        $payload = $this->decodePayload();
        return $payload->data->command->mailable->to;
    }

    public function getMailableClassAttribute() {
        $payload = $this->decodePayload();
        return $payload->displayName;
    }

    public function getMailableAttribute() {
        $payload = $this->decodePayload();
        return $payload->data->command->mailable;
    }

    public function getSubjectAttribute() {
        return $this->mailable->subject;
    }

    public function getRecipientsArrayAttribute() {
        return implode(', ', array_column($this->recipients, 'address'));
    }

}