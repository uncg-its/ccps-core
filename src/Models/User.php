<?php

namespace Uncgits\Ccps\Models;

use Laratrust\Traits\LaratrustUserTrait;

class User extends BaseUser
{
    use LaratrustUserTrait;
}
