<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;

class DbConfig extends Model
{
    protected $table = "ccps_db_configs";
    protected $fillable = ['key', 'value'];

    public $timestamps = false; // not using timestamps

    public static function getConfigCollection()
    {
        return self::all()->mapWithKeys(function ($item) {
            return [$item->key => $item->value];
        });
    }

    public static function getConfigArray()
    {
        $configs = self::all()->mapWithKeys(function ($item) {
            return [$item->key => $item->value];
        });

        return $configs->toArray();
    }

    public static function getConfig($key, $default = null)
    {
        $item = self::where('key', $key)->first();
        return $item ? $item->value : $default;
    }
}
