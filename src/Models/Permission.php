<?php

namespace Uncgits\Ccps\Models;

use Laratrust\Models\LaratrustPermission;
use Kyslik\ColumnSortable\Sortable;

class Permission extends LaratrustPermission
{
    use Sortable;

    protected $guarded = [];

    public $sortable = ['source_package', 'name', 'display_name'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'roles',
    ];

    protected $appends = [
        'role_list',
    ];

    public function getDisplayNameAttribute($value)
    {
        return (empty($value)) ? $this->name : $value;
    }

    public function getRoleListAttribute()
    {
        return $this->roles->pluck('name');
    }
}
