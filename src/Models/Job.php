<?php

namespace Uncgits\Ccps\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Job extends Model
{
    use Sortable;

    protected $table = 'ccps_jobs';
    public $timestamps = false;
    public $casts = [
        'reserved_at' => 'datetime:Y-m-d H:i:s',
        'available_at' => 'datetime:Y-m-d H:i:s',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public $sortable = ['id', 'queue', 'email', 'attempts', 'reserved_at', 'available_at', 'created_at'];

    public function scopeInQueue($query, $queue) {
        if (!is_array($queue)) {
            $queue = explode(',', $queue);
        }

        return $query->whereIn('queue', $queue);

    }

    protected function decodePayload() {
        $payload = json_decode($this->payload);
        $payload->data->command = unserialize($payload->data->command);

        return $payload;
    }

    protected function parseTimestamp($item) {
        return is_null($item) ? null : Carbon::createFromTimestamp($item)->toDateTimeString();
    }


    public function getDecodedPayloadAttribute() {
        return $this->decodePayload();
    }

    public function getClassAttribute() {
        $payload = $this->decodePayload();
        return $payload->displayName;
    }

    public function getRowArray() {
        return [
            'id' => $this->id,
            'queue' => $this->queue,
            'payload' => $this->payload,
            'attempts' => $this->attempts,
            'reserved_at' => strtotime($this->reserved_at),
            'available_at' => strtotime($this->available_at),
            'created_at' => $this->created_at
        ];
    }
}
