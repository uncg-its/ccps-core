<?php

namespace Uncgits\Ccps\Models;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Uncgits\Ccps\Channels\GoogleChatWebhookChannel;

class NotificationChannel extends Model
{
    use Notifiable;

    protected $table = 'ccps_notification_channels';

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    // relationships

    public function notification_events()
    {
        return $this->belongsToMany(NotificationEvent::class, 'ccps_notification_channel_notification_event');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function channel_verifications()
    {
        return $this->hasMany(ChannelVerification::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    // accessors

    public function getKeyFormattedAttribute()
    {
        if ($this->type == 'sms') {
            // format as phone number
            return '(' . substr($this->key, 0, 3) . ') ' . substr($this->key, 3, 3) . '-' . substr($this->key, 6, 4);
        }

        return $this->key;
    }

    public function getVerifiedAttribute()
    {
        return !is_null($this->verified_at);
    }

    public function getChannelAttribute()
    {
        if ($this->type == 'email') {
            return ['mail'];
        }

        if ($this->type == 'sms') {
            return ['mail'];
        }

        if ($this->type == 'google-chat') {
            return [GoogleChatWebhookChannel::class];
        }

        if ($this->type == 'slack') {
            return ['slack'];
        }

        return null;
    }

    public function routeNotificationForMail($notification)
    {
        if ($this->type == 'email') {
            return $this->key;
        }

        if ($this->type == 'sms') {
            // construct the email addresses from the phone numbers...

            switch ($this->carrier) {
                case 'verizon':
                    $address = $this->key . '@vtext.com';
                    break;
                case 'att':
                    $address = $this->key . '@txt.att.net';
                    break;
                case 'sprint':
                    $address = $this->key . '@messaging.sprintpcs.com';
                    break;
                case 'tmobile':
                    $address = $this->key . '@tmomail.net';
                    break;
                default:
                    $address = null;
                    break;
            }

            return $address;
        }

        return null;
    }

    public function routeNotificationForSlack($notification)
    {
        return $this->key;
    }
}
