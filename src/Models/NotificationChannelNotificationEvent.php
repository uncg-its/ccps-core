<?php

namespace Uncgits\Ccps\Models;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class NotificationChannelNotificationEvent extends Model
{
    protected $table = 'ccps_notification_channel_notification_event';

    // relationships

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
