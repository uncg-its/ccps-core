<?php

namespace Uncgits\Ccps\Models;

class FileCache extends \Cache
{

    /**
     * getCache() - pulls cache files (must be file mode) and sorts by active / expired
     * (many thanks to TerrePorter on laravel.io for this solution)
     *
     * @return array
     */
    public static function getCache()
    {
        // make a storage disk for the cache location
        $cacheDisk = [
            'driver' => 'local',
            'root'   => storage_path('framework/cache')
        ];
        \Config::set("filesystems.disks.fcache", $cacheDisk);

        // grab the cache files
        $d = \Storage::disk('fcache')->allFiles();

        $expired = [];
        $notExpired = [];
        // loop the files
        foreach ($d as $key => $cachefile) {

            // ignore that file
            if ($cachefile == '.gitignore') {
                continue;
            }

            // grab the contents of a file
            $d1 = \Storage::disk('fcache')->get($cachefile);

            // grab the expire time from the file
            $expire = substr($contents = $d1, 0, 10);


            // check if it has expired
            if (time() >= $expire) {
                // sort
                $expired[] = [
                    'file'    => $cachefile,
                    'expires' => $expire
                ];
            } else {
                $notExpired[] = [
                    'file'    => $cachefile,
                    'expires' => $expire
                ];
            }
        }

        return [
            'expired'    => $expired,
            'notExpired' => $notExpired
        ];
    }

    /**
     * flushExpired() - get cache files and delete the expired ones
     *
     * @return array
     */
    public static function flushExpired()
    {
        $cacheFiles = self::getCache();

        foreach ($cacheFiles['expired'] as $e) {
            \Storage::disk('fcache')->delete($e['file']);
        }

        return $cacheFiles;
    }
}
