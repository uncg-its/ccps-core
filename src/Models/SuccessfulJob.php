<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SuccessfulJob extends Job
{
    protected $table = 'ccps_jobs_successful';

    public $sortable = ['id', 'queue', 'attempts', 'reserved_at', 'available_at', 'created_at', 'completed_at', 'run_method'];

    public function sent_email() {
        // optional relationship for email queue item
        return $this->hasOne(SentEmail::class, 'id', 'job_id');
    }

    public function getRunMethodWithIconAttribute() {
        $iconClass = ($this->run_method == 'auto') ? 'cog' : 'user';
        return "<i class='fas fa-$iconClass'></i> " . $this->run_method;
    }
}
