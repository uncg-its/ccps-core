<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;

class FailedJob extends Job {

    use Sortable;

    protected $table = 'ccps_jobs_failed';

    public $sortable = ['id', 'queue', 'exception', 'failed_at'];

    public function getExceptionMessageAttribute() {
        $messageEnd = strpos($this->exception, 'Stack trace:');
//        dd($this->exception, $messageEnd);
        return substr($this->exception, 0, $messageEnd);
    }
}
