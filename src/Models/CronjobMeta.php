<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CronjobMeta extends Model
{
    use Sortable;

    protected $table = 'ccps_cronjob_meta';
    protected $casts = [
        'last_run' => 'datetime:Y-m-d H:i:s'
    ];
    protected $guarded = [];

    public $sortable = ['status', 'display_name', 'description', 'schedule', 'last_run'];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
