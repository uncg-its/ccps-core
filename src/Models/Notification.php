<?php

namespace Uncgits\Ccps\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'ccps_notifications';

    protected $guarded = [];
    public $timestamps = false;

    // relationships

    public function notification_channel()
    {
        return $this->belongsTo(NotificationChannel::class);
    }

    public function notification_event()
    {
        return $this->belongsTo(NotificationEvent::class);
    }
}
