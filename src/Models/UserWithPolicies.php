<?php

namespace Uncgits\Ccps\Models;

use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Foundation\Auth\Access\Authorizable;

class UserWithPolicies extends BaseUser
{
    use LaratrustUserTrait;

    use Authorizable {
        Authorizable::can insteadof LaratrustUserTrait;
        LaratrustUserTrait::can as laratrustCan;
    }
}
