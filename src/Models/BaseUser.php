<?php

namespace Uncgits\Ccps\Models;

use Carbon\Carbon;
use App\CcpsCore\RoleMapping;
use Laravel\Sanctum\HasApiTokens;
use Kyslik\ColumnSortable\Sortable;
use App\CcpsCore\NotificationChannel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

abstract class BaseUser extends Authenticatable
{
    use Notifiable;
    use CanResetPassword;
    use Sortable;
    use HasApiTokens;

    protected $table = 'ccps_users';

    protected $casts = [
        'last_login' => 'datetime:Y-m-d H:i:s'
    ];

    public $sortable = ['email', 'provider', 'last_login'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'time_zone',
        'provider',
        'id_from_provider',
        'settings'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at',
        'roles',
        'permissions',
    ];

    protected $appends = [
        'role_list',
    ];

    // relationships

    public function notification_channels()
    {
        return $this->hasMany(NotificationChannel::class);
    }

    // accessors

    public function getUsernameAttribute($value)
    {
        // return email address instead of username
        return $this->email;
    }

    public function getProviderIconAttribute()
    {
        // return the FontAwesome icon name based on provider
        switch ($this->provider) {
            case 'google':
                return 'fab fa-google-plus';
            case 'azure':
                return 'fab fa-windows';
        }

        return 'fas fa-address-book';
    }

    public function getLastLoginAttribute($value)
    {
        if (is_null($value)) {
            return 'never';
        }

        $parsed = Carbon::parse($value);
        $now = Carbon::now();
        $daysAgo = $parsed->diffInDays($now);

        if ($daysAgo > 31) {
            return $value;
        }

        return $parsed->diffForHumans();
    }

    public function getSettingsAttribute($value)
    {
        return json_decode($value);
    }

    public function getSkinPathAttribute()
    {
        $skins = config('ccps.bootstrap_skins');
        return $skins[$this->settings->skin ?? 'default']['asset_path'];
    }

    public function getRoleListAttribute()
    {
        return $this->roles->pluck('name');
    }

    // other methods

    public function grantMappedRoles(array $extraRoles = [])
    {
        // check mapped role groups for roles that should be applied to this user
        $roleMappings = RoleMapping::where('email', strtolower($this->email))
            ->with('mapped_role_group')
            ->get()
            ->where('mapped_role_group.provider', $this->provider)
            ->pluck('mapped_role_group.role_id')
            ->toArray();

        $rolesToGrant = array_merge($roleMappings, $extraRoles);

        $this->roles()->sync($rolesToGrant);

        \Log::channel('acl')->info('Mapped roles granted', [
            'category'  => 'role_mappings',
            'operation' => 'attach',
            'result'    => 'success',
            'data'      => [
                'user'     => $this,
                'role_ids' => $rolesToGrant,
            ]
        ]);

        return $this;
    }
}
