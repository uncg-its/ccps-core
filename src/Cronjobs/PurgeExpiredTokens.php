<?php

namespace Uncgits\Ccps\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Helpers\CronjobResult;
use Laravel\Sanctum\PersonalAccessToken;

class PurgeExpiredTokens extends Cronjob
{
    protected $schedule = '5 0 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Purge Expired API Tokens'; // default display name (overridable in database)
    protected $description = 'Purge expired API Tokens for all users'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        $toPurge = PersonalAccessToken::where('expires_at', '<', now()->toDateTimeString())->get();
        list($purged, $failed) = $toPurge->partition(function ($token) {
            return $token->delete();
        });

        if ($failed->count() > 0) {
            \Log::channel('access')->notice('API token purge complete but with errors', [
                'category'  => 'api-tokens',
                'operation' => 'purge',
                'result'    => 'success',
                'data'      => [
                    'purged' => $purged->implode(',', 'id'),
                    'failed' => $failed->implode(',', 'id')
                ]
            ]);
            return new CronjobResult(false, 'Some tokens could not be deleted, IDs: ' . $failed->implode(', ', 'id'));
        }

        \Log::channel('access')->info('API token purge complete', [
            'category'  => 'api-tokens',
            'operation' => 'purge',
            'result'    => 'success',
            'data'      => [
                'purged' => $purged->implode(',', 'id'),
            ]
        ]);

        return new CronjobResult(true);
    }
}
