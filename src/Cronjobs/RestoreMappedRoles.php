<?php

namespace Uncgits\Ccps\Cronjobs;

use App\CcpsCore\User;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class RestoreMappedRoles extends Cronjob
{
    protected $schedule = '0 1 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Restore Mapped Roles'; // default display name (overridable in database)
    protected $description = 'Restore user roles to those granted by the established Mapped Role Groups'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        // if config does not match, then we need to abort.
        if (config('ccps.role_mapping.method') !== 'cronjob') {
            \Log::channel('acl')->notice('Role Mapping skipped due to configuration', [
                'category'  => 'role_mapping',
                'operation' => 'restore',
                'result'    => 'skipped',
                'data'      => [
                    'config' => config('ccps.role_mapping.method')
                ]
            ]);
            return new CronjobResult(true);
        }

        // restore each user's roles to their mapped roles

        $users = User::get()->map(function ($user) {
            return $user->grantMappedRoles();
        });

        \Log::channel('acl')->info('Updated user roles according to mapped role groups', [
            'category'  => 'role_mapping',
            'operation' => 'restore',
            'result'    => 'success',
            'data'      => [
                'users_updated' => $users->implode('id', ',')
            ]
        ]);

        return new CronjobResult(true);
    }
}
