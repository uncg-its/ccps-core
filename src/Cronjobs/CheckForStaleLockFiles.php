<?php

namespace Uncgits\Ccps\Cronjobs;

use Carbon\Carbon;
use Uncgits\Ccps\Models\Cronjob;
use Symfony\Component\Finder\Finder;
use App\Events\CcpsCore\StaleLockFileDetected;
use Uncgits\Ccps\Helpers\CronjobResult;

class CheckForStaleLockFiles extends Cronjob
{
    protected $schedule = '*/15 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Check For Stale Lock Files'; // default display name (overridable in database)
    protected $description = 'Checks for lock files that have existed for more than one hour.'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        // work to be done when command executes
        // an uncaught exception indicates job failure and will be logged.

        $cronPath = storage_path('cron/');
        $finder = new Finder();
        $finder->files()->in($cronPath)->name('*.lock');

        $oneHourAgo = Carbon::now()->subHours(1);

        foreach ($finder as $file) {
            $createdAt = Carbon::createFromTimestamp(filectime($file));
            if ($createdAt->lte($oneHourAgo)) {
                // this is a problem!
                $fileBits = explode('/', $file);
                $filename = array_pop($fileBits);

                event(new StaleLockFileDetected($filename, $createdAt));
            }
        }

        return new CronjobResult(true);
    }
}
