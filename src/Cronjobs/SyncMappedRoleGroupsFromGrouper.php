<?php

namespace Uncgits\Ccps\Cronjobs;

use App\CcpsCore\MappedRoleGroup;
use App\CcpsCore\RoleMapping;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;
use Uncgits\GrouperApi\Exceptions\GroupNotFoundException;

class SyncMappedRoleGroupsFromGrouper extends Cronjob
{
    protected $schedule = '0 0 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Sync Mapped Role Groups (Grouper)'; // default display name (overridable in database)
    protected $description = 'Pulls latest membership for Mapped Role Groups via the Grouper API'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        // if config does not match, then we need to abort.
        if (is_null(config('grouper-api'))) {
            \Log::channel('acl')->notice('No valid Grouper API config exists', [
                'category'  => 'role_mapping',
                'operation' => 'sync',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        $roleGroupsToSync = MappedRoleGroup::where('type', 'synced')
            ->where('sync_service', 'grouper')
            ->with('role')
            ->get();

        if ($roleGroupsToSync->isEmpty()) {
            \Log::channel('acl')->info('No mapped role groups to sync with Grouper', [
                'category'  => 'role_mapping',
                'operation' => 'sync',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        // get each group's population and make sure that the Mapped Role Group emails match exactly.

        // where is the uncgemail or mail key in the config?
        $fetchedAttributes = config('grouper-api.subject_attribute_names');

        // cn is always returned, add it if not already there.
        if (!in_array('cn', $fetchedAttributes)) {
            $fetchedAttributes[] = 'cn';
        }

        // check for email attribute - if not there then add it.
        $emailIndex = null;
        foreach ($fetchedAttributes as $key => $attribute) {
            if ($attribute === 'uncgemail') {
                $emailIndex = $key;
            }
        }

        if (is_null($emailIndex)) {
            $emailIndex = count($fetchedAttributes);
            $fetchedAttributes[] = 'uncgemail';
        }

        $failed = $roleGroupsToSync->filter(function ($group) use ($fetchedAttributes, $emailIndex) {
            try {
                $result = \Grouper::withSubjectAttributeNames($fetchedAttributes)
                    ->getAllMembers($group->sync_entity_id);
                $members = collect($result[$group->sync_entity_id]);
                $emails = $members->whereIn('sourceId', config('grouper-api.person_identifiers'))->pluck('attributeValues.' . $emailIndex);

                $existingGroupEmails = $group->role_mappings->pluck('email');

                // add missing
                $toAdd = $emails->diff($existingGroupEmails);
                $added = $toAdd->each(function ($email) use ($group) {
                    return RoleMapping::create([
                        'email'                => $email,
                        'mapped_role_group_id' => $group->id,
                    ])->id;
                });

                // remove extra
                $toRemove = $existingGroupEmails->diff($emails);
                RoleMapping::whereIn('email', $toRemove)->delete();

                // update base group updated_at
                $group->updated_at = now();
                $group->save();

                \Log::channel('acl')->info('Mapped Role Group sync results', [
                    'category'  => 'role_mapping',
                    'operation' => 'sync',
                    'result'    => 'success',
                    'data'      => [
                        'mapped_role_group_id'   => $group->id,
                        'mapped_role_group_role' => $group->role->name,
                        'added'                  => $added->count(),
                        'removed'                => $toRemove->count(),
                    ]
                ]);

                return false; // for filter()
            } catch (GroupNotFoundException $e) {
                \Log::channel('acl')->error('Mapped Role Group not found in Grouper', [
                    'category'  => 'role_mapping',
                    'operation' => 'sync',
                    'result'    => 'error',
                    'data'      => [
                        'message' => $e->getMessage()
                    ]
                ]);

                return true(); // for filter()
            }
        });

        if ($failed->isNotEmpty()) {
            return new CronjobResult(false, $failed->count() . ' sync(s) failed. Check logs for details.');
        }

        return new CronjobResult(true);
    }
}
