<?php

namespace Uncgits\Ccps\Cronjobs;

use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class LogApplicationSnapshot extends Cronjob
{
    protected $schedule = '0 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Log Application Snapshot'; // default display name (overridable in database)
    protected $description = 'Logs an Application Snapshot, including installed packages and versions, and curent application version'; // default description name (overridable in database)
    protected $tags = ['maintenance']; // default tags (overridable in database)

    protected function execute()
    {
        $logChannel = config('ccps.app_snapshots.log_channel');
        $existingChannels = config('logging.channels');

        if (is_null($logChannel) || !isset($existingChannels[$logChannel])) {
            $logChannel = 'general';
        }

        $composerLock = file_get_contents(base_path('composer.lock'));
        $packages = collect(json_decode($composerLock)->packages);
        $packageInfo = $packages->mapWithKeys(function ($package) {
            return [$package->name => $package->version];
        })->toArray();

        $composerJson = json_decode(file_get_contents(base_path('composer.json')), true);

        \Log::channel($logChannel)->info('Application Snapshot for ' . config('app.name'), [
            'category'  => 'app-snapshots',
            'operation' => 'generate',
            'result'    => 'success',
            'data'      => [
                'application_name'   => config('app.name'),
                'package_name'       => $composerJson['name'],
                'current_commit'     => exec("git log --pretty=format:'%h' -n 1"),
                'git_remote'         => str_replace("\t", ': ', exec("git remote -v")),
                'required_packages'  => $composerJson['require'],
                'installed_packages' => $packageInfo,
            ]
        ]);

        return new CronjobResult(true);
    }
}
