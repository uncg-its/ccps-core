<?php

namespace Uncgits\Ccps;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register artisan commands, other CLI stuff
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Uncgits\Ccps\Command\Init::class,
                \Uncgits\Ccps\Command\Deploy::class,
                \Uncgits\Ccps\Command\Upgrade::class,
                \Uncgits\Ccps\Command\InstallDusk::class,
                \Uncgits\Ccps\Command\MakeCronjob::class,
                \Uncgits\Ccps\Command\MakeService::class,
                \Uncgits\Ccps\Command\MakeRepository::class,
                \Uncgits\Ccps\Command\MakeUpgrade::class,
                \Uncgits\Ccps\Command\ShimHorizon::class,
                \Uncgits\Ccps\Command\MakeAclSeeders::class,
                \Uncgits\Ccps\Command\MakeNotification::class,
                \Uncgits\Ccps\Command\ComposerDumpAutoload::class,
            ]);

            // publish stuff
            $this->publishes([
                __DIR__ . '/publish/config'       => base_path('config'),
                // app config files, including breadcrumbs and debugbar
                __DIR__ . '/publish/.env.example' => base_path('.env.example'),
                // new .env.example
                __DIR__ . '/publish/routes'       => base_path('routes')
                // routes - breadcrumbs, web, api
            ], 'config');

            $this->publishes([
                __DIR__ . '/publish/assets'             => base_path('resources/assets'), // assets (js and sass)
                __DIR__ . '/publish/webpack.mix.js'     => base_path('webpack.mix.js'), // webpack file
                __DIR__ . '/publish/tailwind.config.js' => base_path('tailwind.config.js'), // tailwind config file
                __DIR__ . '/publish/package.json'       => base_path('package.json'), // npm dependency list
                __DIR__ . '/publish/public'             => public_path(), // compiled css and js
            ], 'assets');

            $this->publishes([
                __DIR__ . '/publish/assets/sass/ccps-core.scss' => base_path('resources/assets/sass/ccps-core.scss'),
                // sass
                __DIR__ . '/publish/public/css/ccps-core.css'   => public_path('css/ccps-core.css'),
                // compiled css
                __DIR__ . '/publish/webpack.mix.js' => base_path('webpack.mix.js'), // webpack file
            ], 'css');

            $this->publishes([
                __DIR__ . '/publish/assets/js/ccps-core.js' => base_path('resources/assets/js/ccps-core.js'),
                // uncompiled
                __DIR__ . '/publish/public/js/ccps-core.js' => public_path('js/ccps-core.js'),
                // compiled js
                __DIR__ . '/publish/webpack.mix.js'     => base_path('webpack.mix.js'), // webpack file
                __DIR__ . '/publish/tailwind.config.js' => base_path('tailwind.config.js'), // tailwind file
                __DIR__ . '/publish/package.json'       => base_path('package.json'), // npm dependency list
            ], 'js');

            $this->publishes([
                __DIR__ . '/publish/providers' => base_path('app/Providers'), // providers
            ], 'providers');

            $this->publishes([
                __DIR__ . '/publish/controllers' => base_path('app/Http/Controllers'), // controllers
            ], 'controllers');

            $this->publishes([
                __DIR__ . '/publish/models' => base_path('app'), // models
            ], 'models');

            $this->publishes([
                __DIR__ . '/publish/requests' => base_path('app/Http/Requests'), // requests
            ], 'requests');

            $this->publishes([
                __DIR__ . '/publish/middleware' => base_path('app/Http/Middleware'), // middleware
            ], 'middleware');

            $this->publishes([
                __DIR__ . '/publish/listeners' => base_path('app/Listeners'), // listeners
            ], 'listeners');

            $this->publishes([
                __DIR__ . '/publish/events' => base_path('app/Events'), // events
            ], 'events');

            $this->publishes([
                __DIR__ . '/publish/kernel' => base_path('app/Http'),
                // Http Kernel with Laratrust and DefineAppMenu middlewares
            ], 'kernel');

            $this->publishes([
                __DIR__ . '/publish/console-kernel' => base_path('app/Console'),
                // Console Kernel with scheduler for Cronjobs
            ], 'console-kernel');


            $this->publishes([
                __DIR__ . '/publish/exception-handler' => base_path('app/Exceptions'),
            ], 'exception-handler');

            $this->publishes([
                __DIR__ . '/publish/dusk' => base_path('tests'),
                // New DuskTestCase that includes --no-sandbox directive for chrome
            ], 'dusk');

            $this->publishes([
                __DIR__ . '/publish/notifications'       => base_path('app/Notifications'),
            ], 'notifications');

            $this->publishes([
                __DIR__ . '/publish/cronjobs'       => base_path('app/Cronjobs'),
            ], 'cronjobs');

            $this->publishes([
                __DIR__ . '/publish/deploy.sh' => base_path('deploy.sh'), // sample deployment script
            ], 'deploy-script');
        }

        // views
        $this->loadViewsFrom(__DIR__ . '/views/', 'ccps');

        // migrations
        $this->loadMigrationsFrom(__DIR__ . '/migrations/');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
