<?php

namespace Uncgits\Ccps\Channels;

use Illuminate\Notifications\Notification;
use GuzzleHttp\Client as HttpClient;
use Uncgits\Ccps\Messages\GoogleChatMessage;

class GoogleChatWebhookChannel
{
    /**
     * The HTTP client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * Create a new GoogleChat channel instance.
     *
     * @param  \GuzzleHttp\Client  $http
     * @return void
     */
    public function __construct(HttpClient $http)
    {
        $this->http = $http;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $url = $notifiable->routeNotificationFor(GoogleChatWebhookChannel::class, $notification)) {
            $url = $this->getWebhookUrlFromMessage($notification->toGoogleChat($notifiable));
        }


        $this->http->post($url, $this->buildJsonPayload(
            $notification->toGoogleChat($notifiable)
        ));
    }

    protected function getWebhookUrlFromMessage(GoogleChatMessage $message) {
        return $message->webhookUrl;
    }

    /**
     * Build up a JSON payload for the GoogleChat webhook.
     *
     * @param  \App\Messages\GoogleChatMessage  $message
     * @return array
     */
    protected function buildJsonPayload(GoogleChatMessage $message)
    {

        $proxyValue = NULL;
        if (config('ccps.http_proxy.enabled')) {
            $proxyValue = config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port');
        }

        return array_merge([
            'proxy' => $proxyValue,
            'json' => array_merge([
                'text' => data_get($message, 'content')
            ]),
        ]);

    }
}