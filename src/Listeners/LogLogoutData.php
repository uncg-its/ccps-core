<?php

namespace Uncgits\Ccps\Listeners;

use Illuminate\Auth\Events\Logout;

class LogLogoutData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        \Log::channel('access')->info('User ' . $event->user->username . ' logged out', [
            'category'  => 'access',
            'operation' => 'logout',
            'result'    => 'success',
            'data'      => [
                'user' => $event->user
            ]
        ]);
    }
}
