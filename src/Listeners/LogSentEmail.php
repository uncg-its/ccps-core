<?php

namespace Uncgits\Ccps\Listeners;

use App\CcpsCore\SentEmail;

class LogSentEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //

        try {
            $job = session('job');

            $id = is_null($job) ? null : $job->id;

            SentEmail::create([
               'job_id'         => $id,
               'message_object' => serialize($event->message),
            ]);
        } catch (\Exception $e) {
            \Log::channel('queue')->warning('Error logging sent email.', [
                'category'  => 'database',
                'operation' => 'store',
                'result'    => 'failure',
                'data'      => [
                    'job'          => $job,
                    'mail_message' => $event->message,
                    'message'      => $event->exception->getMessage()
                ]
            ]);
        }
    }
}
