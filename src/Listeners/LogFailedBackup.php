<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\BackupHasFailed;

class LogFailedBackup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupHasFailed  $event
     * @return void
     */
    public function handle(BackupHasFailed $event)
    {
        if (is_null($event->backupDestination)) {
            \Log::channel('backup')->error("Backup failed before zip creation!", [
                'category'  => 'backup',
                'operation' => 'create',
                'result'    => 'failure',
                'data'      => [
                    'message'            => $event->exception->getMessage(),
                    'backup_destination' => null,
                ]
            ]);
        } else {
            $backupFolder = $event->backupDestination->backupName();
            $backupDisk = $event->backupDestination->diskName();

            \Log::channel('backup')->error("Backup failed after zip creation! Backup was not saved to '$backupFolder' ('$backupDisk' storage).", [
                'category'  => 'backup',
                'operation' => 'create',
                'result'    => 'failure',
                'data'      => [
                    'message'            => $event->exception->getMessage(),
                    'backup_destination' => null,
                ]
            ]);
        }
    }
}
