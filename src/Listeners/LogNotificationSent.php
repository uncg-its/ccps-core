<?php

namespace Uncgits\Ccps\Listeners;

use Illuminate\Notifications\Events\NotificationSent;

class LogNotificationSent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSent  $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        // general message
        \Log::channel('notifications')->info('Notification sent', [
            'category'  => 'notification',
            'operation' => 'send',
            'result'    => 'success',
            'data'      => [
                'notification_channel' => $event->channel,
                'notifiable'           => $event->notifiable,
                'notification_class'   => get_class($event->notification),
                'response'             => $event->response,
                'current_user'         => auth()->user()
            ]
        ]);
    }
}
