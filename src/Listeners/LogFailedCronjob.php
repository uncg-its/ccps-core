<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\CronjobFailed;

class LogFailedCronjob
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CronjobFailed  $event
     * @return void
     */
    public function handle(CronjobFailed $event)
    {
        // general message
        \Log::channel('cron')->error('Cronjob failed', [
            'category'  => 'cron',
            'operation' => 'run',
            'type'      => 'failure',
            'data'      => [
                'cronjob' => $event->cronjob,
                'message' => $event->exception->getMessage(),
            ]
        ]);
    }
}
