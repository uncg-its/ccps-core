<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\BackupZipWasCreated;

class LogBackupZipCreationInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupZipWasCreated  $event
     * @return void
     */
    public function handle(BackupZipWasCreated $event)
    {
        $zipPath = $event->pathToZip;
        \Log::channel('backup')->debug("Backup zip created at '$zipPath'");
    }
}
