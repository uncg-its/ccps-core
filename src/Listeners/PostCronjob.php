<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\CronjobFinished;

class PostCronjob
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CronjobFinished  $event
     * @return void
     */
    public function handle(CronjobFinished $event)
    {
        // Log cronjob end
        \Log::channel('cron')->info('Cronjob ' . $event->cronjob->getDisplayName() . ' ended.', [
            'category'  => 'cron',
            'operation' => 'end',
            'data'      => [
                'cronjob_meta' => $event->cronjob->getMeta()
            ]
        ]);
    }
}
