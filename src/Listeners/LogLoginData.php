<?php

namespace Uncgits\Ccps\Listeners;

use Illuminate\Auth\Events\Login;

class LogLoginData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        \Log::channel('access')->info('User ' . $event->user->username . ' logged in', [
            'category'  => 'access',
            'operation' => 'login',
            'result'    => 'success',
            'data'      => [
                'user' => $event->user
            ]
        ]);
    }
}
