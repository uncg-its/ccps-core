<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\CacheCleared;

class LogCacheClear
{
    protected $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Handle the event.
     *
     * @param  CacheCleared  $event
     * @return void
     */
    public function handle(CacheCleared $event)
    {
        \Log::channel('general')->info('Cache was cleared', [
            'category'  => 'cache',
            'operation' => 'clear',
            'result'    => 'success',
            'data'      => [
                'current_user' => auth()->user()
            ]
        ]);
    }
}
