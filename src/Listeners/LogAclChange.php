<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\AclChanged;

class LogAclChange
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AclChanged  $event
     * @return void
     */
    public function handle(AclChanged $event)
    {
        // general message
        \Log::channel('acl')->info('ACL event', [
            'category'  => 'acl',
            'operation' => $event->changeType,
            'result'    => 'success',
            'data'      => [
                'entity'       => $event->affectedType,
                'current_user' => $event->user,
                'objects'      => $event->aclObjects
            ]
        ]);
    }
}
