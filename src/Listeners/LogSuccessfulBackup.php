<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\BackupWasSuccessful;

class LogSuccessfulBackup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupWasSuccessful  $event
     * @return void
     */
    public function handle(BackupWasSuccessful $event)
    {
        \Log::channel('backup')->info('Backup successful', [
            'category'  => 'backup',
            'operation' => 'storage',
            'result'    => 'success',
            'data'      => [
                'backup_folder' => $event->backupDestination->backupName(),
                'backup_disk'   => $event->backupDestination->diskName(),
            ]
        ]);
    }
}
