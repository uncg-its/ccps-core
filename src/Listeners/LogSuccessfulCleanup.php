<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\CleanupWasSuccessful;

class LogSuccessfulCleanup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CleanupWasSuccessful  $event
     * @return void
     */
    public function handle(CleanupWasSuccessful $event)
    {
        $backupDisk = $event->backupDestination->diskName();
        \Log::channel('backup')->info("Cleanup of '$backupDisk' storage successful", [
            'category'  => 'backup',
            'operation' => 'cleanup',
            'result'    => 'success',
            'data'      => [
                'backup_disk' => $event->backupDestination
            ]
        ]);
    }
}
