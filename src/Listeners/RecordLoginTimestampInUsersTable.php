<?php

namespace Uncgits\Ccps\Listeners;

use App\CcpsCore\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordLoginTimestampInUsersTable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        try {
            User::where('id', $event->user->id)->update([
                'last_login' => Carbon::now()->toDateTimeString()
            ]);
        } catch (\Exception $e) {
            flash('Error recording user login time to database!')->error();
        }

    }
}
