<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\ConfigUpdated;

class LogConfigUpdate
{
    protected $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Handle the event.
     *
     * @param  CacheCleared  $event
     * @return void
     */
    public function handle(ConfigUpdated $event)
    {
        \Log::channel('general')->info('App configuration updated', [
            'category'  => 'config',
            'operation' => 'update',
            'result'    => 'success',
            'data'      => [
                'current_user'    => $this->user,
                'configs_updated' => $event->configsUpdated
            ]
        ]);
    }
}
