<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\CronjobStarted;
use Carbon\Carbon;

class PreCronjob
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CronjobStarted  $event
     * @return void
     */
    public function handle(CronjobStarted $event)
    {
        // Log cronjob start
        \Log::channel('cron')->info('Cronjob ' . $event->cronjob->getDisplayName() . ' started.', [
            'category'  => 'cron',
            'operation' => 'start',
            'data'      => [
                'source'  => 'auto',
                'cronjob' => $event->cronjob
            ]
        ]);

        // insert into last_run in cronjob meta
        $meta = $event->cronjob->getMeta();
        $meta->update(['last_run' => Carbon::now()]);
    }
}
