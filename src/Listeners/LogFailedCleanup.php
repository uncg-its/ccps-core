<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\CleanupHasFailed;

class LogFailedCleanup
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CleanupHasFailed  $event
     * @return void
     */
    public function handle(CleanupHasFailed $event)
    {
        if (is_null($event->backupDestination)) {
            \Log::channel('backup')->error("Cleanup failed before connecting to storage!", [
                'category'  => 'backup',
                'operation' => 'cleanup',
                'result'    => 'failure',
                'data'      => [
                    'message' => $event->exception->getMessage()
                ]
            ]);
        } else {
            $backupDisk = $event->backupDestination->diskName();

            \Log::channel('backup')->error("Cleanup of '$backupDisk' storage failed!", [
                'category'  => 'backup',
                'operation' => 'cleanup',
                'result'    => 'failure',
                'data'      => [
                    'message' => $event->exception->getMessage(),
                    'disk'    => $backupDisk
                ]
            ]);
        }
    }
}
