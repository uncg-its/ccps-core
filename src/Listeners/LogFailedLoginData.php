<?php

namespace Uncgits\Ccps\Listeners;

use Illuminate\Auth\Events\Failed;

class LogFailedLoginData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        \Log::channel('access')->warning('Failed login attempt', [
            'category'  => 'access',
            'operation' => 'login',
            'result'    => 'failure',
            'data'      => [
                'email' => $event->credentials['email']
            ]
        ]);
    }
}
