<?php

namespace Uncgits\Ccps\Listeners;

use Spatie\Backup\Events\BackupManifestWasCreated;

class LogBackupManifestInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupManifestWasCreated  $event
     * @return void
     */
    public function handle(BackupManifestWasCreated $event)
    {
        $manifestPath = $event->manifest->path();
        \Log::channel('backup')->debug("Backup manifest saved to '$manifestPath'");
    }
}
