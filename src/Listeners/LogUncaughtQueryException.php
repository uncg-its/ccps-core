<?php

namespace Uncgits\Ccps\Listeners;

use App\Events\CcpsCore\UncaughtQueryException;

class LogUncaughtQueryException
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UncaughtQueryException  $event
     * @return void
     */
    public function handle(UncaughtQueryException $event)
    {
        \Log::channel('database')->error('Uncaught QueryException', [
            'category'  => 'database',
            'operation' => 'error',
            'result'    => 'failure',
            'data'      => [
                'message' => $event->exception->getMessage()
            ]
        ]);
    }
}
