<?php

namespace Uncgits\Ccps\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostLogEntry
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        /**
         * MessageLogged object $event
         *
         * $event->level
         * $event->message
         * $event->context
         */

        // EXAMPLES
        /*
        if ($event->level == 'alert') {
            // take action if the log message was an alert

        }

        if ($event->context['sms'] === true) {
            // take action if the log message was sent with specific context information

        }
        */
    }
}
