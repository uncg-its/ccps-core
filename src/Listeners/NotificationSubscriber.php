<?php

namespace Uncgits\Ccps\Listeners;

class NotificationSubscriber
{
    /**
     * Send notifications
     */
    public function sendNotifications($event)
    {
        if (method_exists($event, 'notifySubscribedChannels')) {
            $event->notifySubscribedChannels();
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
    }
}
