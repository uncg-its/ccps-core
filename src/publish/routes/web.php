<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');
