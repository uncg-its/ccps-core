<?php

namespace App\Http\Controllers\Auth;

use Uncgits\Ccps\Controllers\Auth\ForgotPasswordController as BaseController;

class ForgotPasswordController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
