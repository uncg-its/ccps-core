<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver'   => 'stack',
            'channels' => ['default'],
        ],

        'default' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        /* Examples

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
        */


        /*
         * Default CCPS Core channels
         */

        'general' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/general.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'queue' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/queue.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'cron' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/cron.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'access' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/access.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'database' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/database.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'acl' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/acl.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'notifications' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/notifications.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'backup' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/backup.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'application-snapshots' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/application-snapshots.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'exceptions' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/exceptions.log'),
            'level'  => 'info',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],
    ],

];
