<?php

namespace Uncgits\Ccps\Command;

class Init extends Installer
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:init {--p|prompt : Steps through each initialization step with yes/no prompts, allowing you to choose which to apply}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes CCPS Framework by copying app assets and files, and configuring/migrating/seeding database.';

    /**
     * Paths to the upgrades folder in both the app and vendor folders
     *
     * @var string
     */
    protected $installedUpgradesFolder = '';
    protected $vendorUpgradesFolder = '';

    /**
     * Name of the signature file that we will install in upgrades/ folder after successful init (with extension)
     *
     * @var string
     */
    protected $signatureFilename = 'ccps-init.txt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Initializing CCPS Framework...');

            $this->checkForSignatureFile();

            // ------ publish config
            if (!$this->option('prompt') || $this->confirm('Copy config files?')) {
                // remove old .env.example
                \File::delete(base_path('.env.example'));
                $this->info('Removed old .env.example file');

                \File::delete(base_path('routes/web.php'));
                $this->info('Removed old web routes file');

                \File::delete(base_path('routes/api.php'));
                $this->info('Removed old api routes file');

                \File::delete(base_path('config/queue.php'));
                $this->info('Removed old queue config file');

                \File::delete(base_path('config/services.php'));
                $this->info('Removed old services config file');

                \File::delete(base_path('config/auth.php'));
                $this->info('Removed old auth config file');

                \File::delete(base_path('config/logging.php'));
                $this->info('Removed old logging config file');

                \File::delete(base_path('config/view.php'));
                $this->info('Removed old view config file');

                \File::delete(resource_path('views/welcome.blade.php'));
                $this->info('Removed old welcome blade view file');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'config'
                ]);
                $this->info('Config files published!');
            } else {
                $this->info('Skipped config file publish.');
            }

            // ------ copy .env.example to .env
            $this->copyEnv();

            // ------ link /public/storage to /storage/app/public
            $this->linkStoragePublicFolder();

            // ------ timezone
            if (!$this->option('prompt') || $this->confirm('Modify config/app.php for APP_TIMEZONE setting?')) {
                // get config/app.php ready
                $appConfigFile = file_get_contents('config/app.php');
                $logStringToFind = "'timezone' => 'UTC',";

                $replaceWith = "'timezone' => env('APP_TIMEZONE', 'UTC'),";

                $appConfigFile = str_replace($logStringToFind, $replaceWith, $appConfigFile);

                if (file_put_contents('config/app.php', $appConfigFile)) {
                    $this->info('New config/app.php file written successfully.');
                } else {
                    $this->error('New config/app.php file file could not be written!');
                }
            } else {
                $this->info('Skipped app config file modification');
            }

            $this->setUpTimezoneInEnv();

            // ------ assets
            if (!$this->option('prompt') || $this->confirm('Install assets (js/sass/tailwind/mix)?')) {

                // webpack file
                \File::delete(base_path('webpack.mix.js'));
                $this->info('Removed old webpack.mix.js file');

                // assets
                \File::deleteDirectory(base_path('resources/assets'));
                $this->info('Removed old assets folder.');

                // public
                \File::deleteDirectory(public_path('css'));
                $this->info('Removed old public/css folder.');

                \File::deleteDirectory(public_path('js'));
                $this->info('Removed old public/js folder.');


                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'assets',
                    '--force'    => true
                ]);

                $this->info('Assets installed. Run "npm install && npm run dev" to generate first css and js files.');
            } else {
                $this->info('Skipped asset installation.');
            }


            // ------ controllers
            if (!$this->option('prompt') || $this->confirm('Publish core controllers?')) {
                \File::deleteDirectory(base_path('app/Http/Controllers/Auth'));
                $this->info('Removed old app/Http/Controllers/Auth folder.');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'controllers'
                ]);

                $this->info('Core Controllers published.');
            } else {
                $this->info('Skipped publishing core controllers.');
            }

            // ------ models
            if (!$this->option('prompt') || $this->confirm('Publish core models?')) {
                \File::delete(base_path('app/User.php'));
                $this->info('Removed default User model.');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'models'
                ]);

                $this->info('Core models published.');
            } else {
                $this->info('Skipped publishing core models.');
            }

            // ------ requests
            if (!$this->option('prompt') || $this->confirm('Publish core requests?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'requests'
                ]);

                $this->info('Core requests published.');
            } else {
                $this->info('Skipped publishing core requests.');
            }

            // ------ middleware
            if (!$this->option('prompt') || $this->confirm('Publish core middleware?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'middleware'
                ]);

                $this->info('Core middleware published.');
            } else {
                $this->info('Skipped publishing core middleware.');
            }

            // ------ providers
            if (!$this->option('prompt') || $this->confirm('Publish core service providers?')) {
                \File::delete(base_path('app/Providers/EventServiceProvider.php'));
                $this->info('Removed default EventServiceProvider.php');
                \File::delete(base_path('app/Providers/RouteServiceProvider.php'));
                $this->info('Removed default RouteServiceProvider.php');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'providers'
                ]);

                $this->info('Core service providers published.');
            } else {
                $this->info('Skipped publishing core service providers.');
            }

            // ------ events
            if (!$this->option('prompt') || $this->confirm('Publish core events?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'events'
                ]);

                $this->info('Core events published.');
            } else {
                $this->info('Skipped publishing core events.');
            }

            // ------ listeners
            if (!$this->option('prompt') || $this->confirm('Publish core listeners?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'listeners'
                ]);

                $this->info('Core listeners published.');
            } else {
                $this->info('Skipped publishing core listeners.');
            }

            // ------ notifications
            if (!$this->option('prompt') || $this->confirm('Publish core notifications?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'notifications'
                ]);

                $this->info('Core notifications published.');
            } else {
                $this->info('Skipped publishing core notifications.');
            }

            // ------ cronjobs
            if (!$this->option('prompt') || $this->confirm('Publish core cronjobs?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'cronjobs'
                ]);

                $this->info('Core cronjobs published.');
            } else {
                $this->info('Skipped publishing core cronjobs.');
            }


            // ------ publish new Http kernel
            if (!$this->option('prompt') || $this->confirm('Replace default Http Kernel?')) {
                \File::delete(base_path('app/Http/Kernel.php'));
                $this->info('Removed default Http Kernel.');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'kernel'
                ]);

                $this->info('New Kernel published.');
            } else {
                $this->info('Skipped replacing Http Kernel.');
            }

            // ------ publish new Console kernel
            if (!$this->option('prompt') || $this->confirm('Replace default Console Kernel?')) {
                \File::delete(base_path('app/Console/Kernel.php'));
                $this->info('Removed default Console Kernel.');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'console-kernel'
                ]);

                $this->info('New Kernel published.');
            } else {
                $this->info('Skipped replacing Console Kernel.');
            }

            // ------ publish new Exception Handler
            if (!$this->option('prompt') || $this->confirm('Replace default Exception Handler?')) {
                \File::delete(base_path('app/Exceptions/Handler.php'));
                $this->info('Removed default Exception Handler.');

                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'exception-handler'
                ]);

                $this->info('New Exception Handler published.');
            } else {
                $this->info('Skipped replacing Exception Handler.');
            }

            // ------ remove laravel default migrations
            if (!$this->option('prompt') || $this->confirm('Remove Laravel default migrations?')) {
                \File::delete(base_path('database/migrations/2014_10_12_000000_create_users_table.php'));
                \File::delete(base_path('database/migrations/2014_10_12_100000_create_password_resets_table.php'));
                $this->info('Removed Laravel default migrations.');
            } else {
                $this->info('Skipped removing Laravel default migrations.');
            }

            // ------ make Laravel use db table prefix
            if (!$this->option('prompt') || $this->confirm('Apply database prefix settings?')) {
                $file = 'config/database.php';

                $config = file_get_contents(base_path($file));
                $replaceString = "'prefix' => ''";
                $replaceWith = "'prefix' => env('DB_TABLE_PREFIX', '')";

                if (strpos($config, $replaceString)) {
                    $config = str_replace($replaceString, $replaceWith, $config);
                } else {
                    $this->error('Could not find default database prefix string in ' . $file . ' - has it been changed already?');
                }

                if (file_put_contents(base_path($file), $config)) {
                    $this->info('New ' . $file . ' file written successfully; replacement complete.');
                } else {
                    $this->error('New ' . $file . ' file could not be written.');
                };
            } else {
                $this->info('Skipped database prefix settings (WARNING - this means your DB_TABLE_PREFIX in .env will be meaningless!)');
            }

            // ------ fill in Database info
            $this->setUpDatabaseInEnv();

            // ------ db migrations and seeds
            $this->databaseMigrationsAndSeeds();

            // ------ fill in Mail info
            $this->setUpMailInEnv();

            // ------ fill in App info
            $this->setUpAppInfoInEnv();

            // ------ allow user signups?
            $this->setUpSignupInfoInEnv();

            // ------ log days config
            $this->setUpAppLogsInEnv();

            // ------ admin password
            $this->setAdminPassword();

            // ------ option for Splunk logging out of the box
            if ($this->confirm('Augment existing logging config with Splunk logging?')) {
                // copy() will overwrite original
                copy(base_path('vendor/uncgits/ccps-core/src/stubs/logging-splunk.stub'), config_path('logging.php'));
            } else {
                $this->info('Sticking with default logging.');
            }

            // ------ option for adding deploy script
            if ($this->confirm('Publish default deploy script?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'deploy-script'
                ]);
                $this->info('Published default deploy script.');
            } else {
                $this->info('Skipped publishing default deploy script.');
            }



            // ------ seed upgrades from ccps-core, so that they do not get picked up when upgrading next time.
            $upgradeFiles = $this->getDirectoryContents($this->vendorUpgradesFolder);
            if ($upgradeFiles) {
                foreach ($upgradeFiles as $file) {
                    $this->writeSignatureFile($file . '.txt', 'Initialization');
                }
            }

            // ------ generate app key
            $this->generateAppKey();

            // ------ write signature file
            $this->writeSignatureFile($this->signatureFilename, 'Initialization');


            // FINAL CONFIRMATION
            $this->info('------------------------------------------');
            $this->info('CCPS FRAMEWORK INITIALIZATION IS COMPLETE!');
            $this->info('------------------------------------------');
        } catch (\Exception $e) {
            $this->error('CCPS Framework initialization failed: ' . $e->getMessage());
        }

        return true;
    }
}
