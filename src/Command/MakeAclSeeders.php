<?php

namespace Uncgits\Ccps\Command;

use Carbon\Carbon;
use Illuminate\Console\Command;

class MakeAclSeeders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:acl-seeders {model : The name of the model with which to prefix the seeders and migration} {package : The name of the package to which to register the roles/permissions in the database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a set of ACL seeders and migrations for a CCPS Core application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // get arguments
            $model = ucwords($this->argument('model'));
            $package = $this->argument('package');

            // define paths
            $seederPath = base_path('database/seeds');
            $migrationPath = base_path('database/migrations');

            $stubsPath = 'vendor/uncgits/ccps-core/src/stubs/';

            // prep date prefix for migration
            $datePrefix = Carbon::now()->format('Y_m_d_His_');
            $nameSuffix = 'add_' . strtolower($model) . '_role_and_permissions';

            // generate filenames
            $destinationFiles = [
                'roleSeeder' => [
                    'filename' => $model . 'RolesTableSeeder.php',
                    'path'     => $seederPath,
                    'stub'     => 'RolesTableSeeder.stub'
                ],
                'permissionSeeder' => [
                    'filename' => $model . 'PermissionsTableSeeder.php',
                    'path'     => $seederPath,
                    'stub'     => 'PermissionsTableSeeder.stub'
                ],
                'permissionRoleSeeder' => [
                    'filename' => $model . 'PermissionRoleTableSeeder.php',
                    'path'     => $seederPath,
                    'stub'     => 'PermissionRoleTableSeeder.stub'
                ],
                'migration' => [
                    'filename' => $datePrefix . $nameSuffix . '.php',
                    'path'     => $migrationPath,
                    'stub'     => 'aclMigration.stub'
                ]
            ];

            foreach ($destinationFiles as $file) {
                $destinationPath = $file['path'] . '/' . $file['filename'];
                if (file_exists($destinationPath)) {
                    $this->error("Destination File '$destinationPath' already exists. Skipping.");
                    continue;
                }

                // generate stub
                $stub = file_get_contents($stubsPath . $file['stub']);
                $stub = str_replace('MODEL', $model, $stub);
                $stub = str_replace('PACKAGE', $package, $stub);

                if (file_put_contents($destinationPath, $stub)) {
                    $this->info("New file generated at '$destinationPath'.");
                }
            }

            $this->info('Executing \'composer dump-autoload\'...');
            \Artisan::call('ccps:dump-autoload');
            $this->info('Success.');
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
