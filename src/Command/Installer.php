<?php

namespace Uncgits\Ccps\Command;

use Carbon\Carbon;
use App\CcpsCore\User;
use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;

abstract class Installer extends Command
{
    // REFERENCE: verbosity level for logging purposes
    /*
     * from Symfony\Component\Console\Output\OutputInterface
     * -v		OutputInterface::VERBOSITY_VERBOSE
     * -vv		OutputInterface::VERBOSITY_VERY_VERBOSE
     * -vvv		OutputInterface::VERBOSITY_DEBUG
     */

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature;

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description;

    // database information to be used during initialization

    protected $database = [];

    // timezone information to be used during initialization

    protected $timezone;

    /**
     * Paths to the upgrades folder in both the app and vendor folders
     *
     * @var string
     */
    protected $installedUpgradesFolder;
    protected $vendorUpgradesFolder;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        // set upgrade folder paths
        $this->installedUpgradesFolder = base_path('upgrades');
        $this->vendorUpgradesFolder = base_path('vendor/uncgits/ccps-core/src/upgrades');
        parent::__construct();
    }

    // HELPER METHODS

    // protected function getVerbosity() {
    //     $this->verbosity = $this->getOutput()->getVerbosity();
    // }

    public function chmodRecursive($dir, $dirPermissions, $filePermissions)
    {
        // directory itself
        //echo('DIR:' . $fullPath . "\n");
        if (!chmod($dir, $dirPermissions)) {
            return false;
        }

        // contents
        $dp = opendir($dir);
        while ($file = readdir($dp)) {
            if (($file == '.') || ($file == '..')) {
                continue;
            }

            $fullPath = $dir . '/' . $file;

            if (is_dir($fullPath)) {
                $this->chmodRecursive($fullPath, $dirPermissions, $filePermissions);
            } else {
                //echo('FILE:' . $fullPath . "\n");
                if (!chmod($fullPath, $filePermissions)) {
                    return false;
                };
            }
        }
        closedir($dp);

        return true;
    }

    protected function copyEnv()
    {
        // ------ copy .env.example to .env
        if (!$this->option('prompt') || $this->confirm('Copy .env.example to new .env file?')) {
            \File::copy('.env.example', '.env');
        } else {
            $this->info('Skipped .env copy.');
        }
    }

    protected function generateAppKey()
    {
        if (!$this->option('prompt') || $this->confirm('Generate application key?')) {
            // replicate key:generate functionality
            $key = 'base64:' . base64_encode(Encrypter::generateKey($this->laravel['config']['app.cipher']));

            $path = base_path('.env');

            if (file_exists($path)) {
                file_put_contents($path, str_replace(
                    'APP_KEY=',
                    'APP_KEY=' . $key,
                    file_get_contents($path)
                ));
            }

            $this->laravel['config']['app.key'] = $key;
            $this->info('Generated new app key');
        } else {
            $this->info('Skipped key generation.');
        }
    }

    protected function checkForSignatureFile()
    {
        $commandName = $this->getName();
        if (\File::exists(base_path('upgrades/' . $this->signatureFilename))) {
            $this->error('WARNING: It appears that you have already run the ' . $commandName . ' command!');
            if (!$this->confirm('Continue anyway?')) {
                throw new \Exception('Operation aborted');
            }
        }
    }

    protected function writeSignatureFile($signatureFilename, $operation)
    {
        if (!\File::isDirectory($this->installedUpgradesFolder)) {
            $this->info('/upgrades folder does not exist; creating.');
            \File::makeDirectory($this->installedUpgradesFolder);
        }

        // is app/upgrades folder writable?
        if (!\File::isWritable($this->installedUpgradesFolder)) {
            throw new \Exception('app/upgrades folder is not writable! Cannot write signature file. Check permissions.');
        }

        // put timestamp data in new signature file, to be inserted into app/upgrades
        $contents = Carbon::now()->toDateTimeString();
        $bytesWritten = \File::put($this->installedUpgradesFolder . '/' . $signatureFilename, $contents);
        if ($bytesWritten === false) {
            throw new Exception($operation . ' completed but corresponding signature file could not be written to app/upgrades. Please check before continuing.');
        } else {
            $this->info($operation . ' signature file ' . $signatureFilename . ' written to /upgrades successfully.');
        }
    }

    protected function updateEnvWithUserInput($fields, $propertyToUpdate = null)
    {
        $env = file_get_contents('.env');

        foreach ($fields as $field => $info) {
            $placeholder = '*' . $field . '*';
            $promptText = 'Value for ' . $field;

            if (isset($info['extraText'])) {
                $promptText .= ' ' . $info['extraText'];
            }

            if (strpos($env, $placeholder)) {
                $inputType = $info['type'];
                switch ($inputType) {
                    case 'anticipate':
                    case 'choice':
                        $value = $this->$inputType($promptText, $info['values']);
                        break;
                    case 'secret':
                    case 'ask':
                        $value = $this->$inputType($promptText);
                        break;
                    default:
                        $value = $this->ask($promptText);
                        break;
                }

                $env = str_replace($placeholder, $value, $env);

                // update local object property if we need to do so
                if (!is_null($propertyToUpdate)) {
                    $this->$propertyToUpdate[$field] = $value;
                }

                $this->info('Value set.');
            } else {
                $this->info('Placeholder value for ' . $field . ' not found - value must already be entered. Skipping.');
            }
        }

        if (file_put_contents('.env', $env)) {
            $this->info('New .env written successfully.');
        } else {
            $this->error('New .env file could not be written!');
        }
    }

    protected function setUpDatabaseInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up database info in .env?')) {
            $fields = [
                'DB_HOST'         => [
                    'type'   => 'anticipate',
                    'values' => ['instance-mariadb', '127.0.0.1', 'localhost']
                ],
                'DB_PORT'         => ['type' => 'anticipate', 'values' => ['3306']],
                'DB_DATABASE'     => ['type' => 'ask'],
                'DB_USERNAME'     => ['type' => 'ask'],
                'DB_PASSWORD'     => ['type' => 'secret'],
                'DB_TABLE_PREFIX' => ['type' => 'ask']
            ];

            $this->updateEnvWithUserInput($fields, 'database');
        } else {
            $this->info('Skipped database setup');

            // use values from env file
            $this->database['DB_HOST'] = env('DB_HOST');
            $this->database['DB_PORT'] = env('DB_PORT');
            $this->database['DB_DATABASE'] = env('DB_DATABASE');
            $this->database['DB_USERNAME'] = env('DB_USERNAME');
            $this->database['DB_PASSWORD'] = env('DB_PASSWORD');
            $this->database['DB_TABLE_PREFIX'] = env('DB_TABLE_PREFIX');
        }
    }

    protected function databaseMigrationsAndSeeds()
    {
        if (!$this->option('prompt') || $this->confirm('Continue with database migrations and/or seeds? (requires reconnection to DB)')) {
            // ------ reconnect to database using provided creds

            \DB::purge('mysql');

            config(['database.connections.mysql.host' => $this->database['DB_HOST']]);
            config(['database.connections.mysql.port' => $this->database['DB_PORT']]);
            config(['database.connections.mysql.database' => $this->database['DB_DATABASE']]);
            config(['database.connections.mysql.username' => $this->database['DB_USERNAME']]);
            config(['database.connections.mysql.password' => $this->database['DB_PASSWORD']]);
            config(['database.connections.mysql.prefix' => $this->database['DB_TABLE_PREFIX']]);

            \DB::reconnect('mysql');

            $this->info('Initializing database for CCPS Framework...');

            // ------ migrate database tables

            // laratrust config
            config(['laratrust.tables.roles' => 'ccps_roles']);
            config(['laratrust.tables.permissions' => 'ccps_permissions']);
            config(['laratrust.tables.teams' => 'ccps_teams']);
            config(['laratrust.tables.role_user' => 'ccps_role_user']);
            config(['laratrust.tables.permission_user' => 'ccps_permission_user']);
            config(['laratrust.tables.permission_role' => 'ccps_permission_role']);

            // ------ set current config item for use in user seeder
            config(['app.timezone' => $this->timezone['APP_TIMEZONE']]);

            if (!$this->option('prompt') || $this->confirm('Run initial database migrations?')) {
                // first: Laravel / CCPS Core migrations (priority)
                $this->call('migrate', [
                    '--path'  => base_path('vendor/uncgits/ccps-core/migrations'),
                    '--force' => true
                ]);

                $this->info('CCPS Core migrations run successfully!');

                // regardless: run all un-run migrations in default date order
                $this->call('migrate', [
                    '--force' => true
                ]);

                $this->info('All app migrations run successfully!');

                $this->info('Migrations successful!');
            } else {
                $this->info('Skipped migrations.');
            }

            $this->info('CCPS Framework database initialized successfully.');
        } else {
            $this->info('Skipped database migrations and seeding.');
        }
    }

    protected function setStorageFolderPermissions()
    {
        // ------ make sure /storage folder has proper permissions
        if (!$this->option('prompt') || $this->confirm('Set 777 permissions on /storage folder and contents?')) {
            if ($this->chmodRecursive(base_path('storage'), 0777, 0777)) {
                $this->info('/storage folder permissions set to 777.');
            } else {
                throw new \Exception('Could not set /storage folder permissions to 777.');
            }
        } else {
            $this->info('Skipped setting /storage folder permissions.');
        }
    }

    protected function setAdminPassword()
    {
        // allow user to change admin password (not skippable via omitting prompt)
        if ($this->confirm('Change default admin password? (default is "admin")')) {
            $continue = false;
            $this->line('Password must be at least 3 characters (please make it stronger...)');
            do {
                $password = $this->secret('New password');
                $password2 = $this->secret('New password (repeat)');

                if (strlen($password) < 3) {
                    $this->error('Password must be 3 or more characters.');
                } else {
                    if ($password != $password2) {
                        $this->error('Passwords do not match.');
                    } else {
                        $admin = User::find(1);
                        $admin->update(['password' => bcrypt($password)]);
                        $continue = true;
                    }
                }
            } while (!$continue);
        } else {
            $this->info('Skipped changing default admin password.');
        }
    }

    protected function setUpMailInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up Mail info in .env?')) {
            $fields = [
                'MAIL_MAILER'       => ['type' => 'choice', 'values' => ['log', 'smtp']],
                'MAIL_HOST'         => ['type' => 'choice', 'values' => ['smtp.uncg.edu', 'host.docker.internal', 'smtp.mailtrap.io']],
                'MAIL_PORT'         => ['type' => 'choice', 'values' => ['25', '2525']],
                'MAIL_USERNAME'     => ['type' => 'anticipate', 'values' => ['null']],
                'MAIL_PASSWORD'     => ['type' => 'secret'],
                'MAIL_ENCRYPTION'   => ['type' => 'anticipate', 'values' => ['null']],
                'MAIL_FROM_ADDRESS' => [
                    'type'      => 'choice',
                    'values'    => ['noreply@uncg.edu', ''],
                    'extraText' => '(leave blank to set your own value in .env later)'
                ],
                'MAIL_FROM_NAME'    => [
                    'type'      => 'choice',
                    'values'    => ['CCPS-No-Reply', ''],
                    'extraText' => '(leave blank to set your own value in .env later)'
                ]
            ];

            $this->updateEnvWithUserInput($fields);
        } else {
            $this->info('Skipped Mail setup');
        }
    }

    protected function setUpAppInfoInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up App info in .env?')) {
            $fields = [
                'APP_NAME' => ['type' => 'ask', 'extraText' => '(do not include quotes)'],
                'APP_URL'  => ['type' => 'ask', 'extraText' => '(include http:// or https://)'],
            ];

            $this->updateEnvWithUserInput($fields);
        } else {
            $this->info('Skipped App info setup');
        }
    }

    // ------ allow user signups?
    protected function setUpSignupInfoInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up User Signup / Auth info in .env?')) {
            $fields = [
                'APP_ALLOW_SIGNUPS' => ['type' => 'choice', 'values' => ['false', 'true']],
            ];

            $this->updateEnvWithUserInput($fields);
        } else {
            $this->info('Skipped Signup info setup');
        }
    }

    // ------ log days config
    protected function setUpAppLogsInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up log file configuration in .env?')) {
            // .env field

            $fields = [
                'APP_LOG_MAX_FILES' => [
                    'type'      => 'choice',
                    'values'    => ['1', '3', '5', '7', '14', '30', '60', '90', '365'],
                    'extraText' => '(# days to keep all daily log files)'
                ],
            ];

            $this->updateEnvWithUserInput($fields);
        } else {
            $this->info('Skipped .env setup for App log');
        }
    }

    // ------ timezone config
    protected function setUpTimezoneInEnv()
    {
        if (!$this->option('prompt') || $this->confirm('Set up time zone in .env?')) {
            // .env field
            $fields = [
                'APP_TIMEZONE' => [
                    'type'   => 'choice',
                    'values' => [
                        'America/New_York',
                        'America/Chicago',
                        'America/Denver',
                        'America/Phoenix',
                        'America/Los_Angeles',
                        'America/Anchorage',
                        'America/Adak',
                        'Pacific/Honolulu',
                        'UTC'
                    ]
                ],
            ];

            $this->updateEnvWithUserInput($fields, 'timezone');
        } else {
            $this->info('Skipped .env setup for App log');
        }
    }

    // ------ link storage/app/public
    protected function linkStoragePublicFolder()
    {
        if (!$this->option('prompt') || $this->confirm('Link /public/storage to /storage/app/public?')) {
            $this->call('storage:link');
            $this->info('Storage folders linked successfully.');
        } else {
            $this->info('Skipped storage link');
        }
    }

    // helper for getting directory contents
    protected function getDirectoryContents($directory)
    {
        if (\File::isDirectory($directory)) {
            $splInfoObjectArray = \File::allFiles($directory);
            $filenameArray = [];
            foreach ($splInfoObjectArray as $item) {
                $filenameArray[] = $item->getBasename('.' . $item->getExtension()); // diff on filename without extension
            }
            return $filenameArray;
        } else {
            return false;
        }
    }
}
