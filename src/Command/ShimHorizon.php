<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Console\Command;
use Symfony\Component\Finder\Finder;

class ShimHorizon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:horizon:shim {--p|prompt : Steps through each installation step with yes/no prompts, allowing you to choose which to apply}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shims Laravel Horizon to be usable in subfolder installs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Shimming Laravel Horizon...');

            if (!file_exists(base_path('config/horizon.php'))) {
                throw new \Exception('Horizon config not found. Please run horizon:install first before shimming.');
            }

            // ------ add Horizon route
            if (!$this->option('prompt') || $this->confirm('Add custom Horizon route?')) {

                // find view partial file in app and look for these lines
                $routesPath = base_path('routes');
                $finder = new Finder();
                $finder->files()->in($routesPath)->name('web.php');

                $routeText = <<<PHP
// Horizon override
Route::group([
    'prefix'     => 'horizon',
    'namespace'  => 'Laravel\Horizon\Http\Controllers',
    'middleware' => config('horizon.middleware', 'web'),
], function () {
// Catch-all Route...
    Route::get('/{view?}', [\App\Http\Controllers\CcpsCore\HomeController::class, 'horizon'])->where('view',
        '(.*)')->name('horizon.index');
});
PHP;

                foreach ($finder as $file) {
                    $path = $file->getRealPath();
                    $contents = file_get_contents($path);
                    if (strpos($contents, $routeText) !== false) {
                        $this->error('Custom route already exists! Skipping.');
                        break;
                    }

                    $contents .= PHP_EOL . $routeText;

                    file_put_contents($path, $contents);
                }

                $this->info('Custom Horizon route added to routes/web.php');
            } else {
                $this->info('Skipped adding custom Horizon route.');
            }

            // ------ add Horizon override to HomeController
            if (!$this->option('prompt') || $this->confirm('Add custom Horizon controller method in HomeController?')) {
                $search = "public function __construct() {
        parent::__construct();
    }";

                $replace = "public function __construct()
    {
        parent::__construct();
    }

    public function horizon()
    {
        \$horizonScriptVariables = Horizon::scriptVariables();
        \$horizonScriptVariables['path'] = config('horizon.subfolder') . \$horizonScriptVariables['path'];

        return view('horizon::layout', [
            'cssFile'                => Horizon::\$useDarkTheme ? 'app-dark.css' : 'app.css',
            'horizonScriptVariables' => \$horizonScriptVariables,
            'assetsAreCurrent' => Horizon::assetsAreCurrent(),
            'isDownForMaintenance' => \App::isDownForMaintenance(),
        ]);
    }";
                // find view partial file in app and look for these lines
                $controllerPath = base_path('app/Http/Controllers/CcpsCore');
                $finder = new Finder();
                $finder->files()->in($controllerPath)->name('HomeController.php');

                foreach ($finder as $file) {
                    $path = $file->getRealPath();
                    $contents = file_get_contents($path);

                    if (strpos($contents, $replace) !== false) {
                        $this->error('Custom controller method already exists! Skipping.');
                        break;
                    }

                    $contents = str_replace($search, $replace, $contents);
                    file_put_contents($path, $contents);
                }

                // use statement
                $search = 'use Uncgits\Ccps\Controllers\HomeController as BaseController;';
                $replace = 'use Laravel\Horizon\Horizon;
use Uncgits\Ccps\Controllers\HomeController as BaseController;';

                foreach ($finder as $file) {
                    $path = $file->getRealPath();
                    $contents = file_get_contents($path);

                    if (strpos($contents, $replace) !== false) {
                        $this->error('Use statement in HomeController already exists! Skipping.');
                        break;
                    }

                    $contents = str_replace($search, $replace, $contents);
                    file_put_contents($path, $contents);
                }

                $this->info('Custom Horizon controller method added to HomeController.');
            } else {
                $this->info('Skipped adding custom Horizon controller method to HomeController.');
            }

            // ------ shim config file to read the HORIZON_SUBFOLDER entry in .env

            if (!$this->option('prompt') || $this->confirm('Add custom property in Horizon config file?')) {
                $search = "/*
    |--------------------------------------------------------------------------
    | Horizon Redis Connection
    |--------------------------------------------------------------------------";

                $replace = "'subfolder' => env('HORIZON_SUBFOLDER', ''),

    /*
    |--------------------------------------------------------------------------
    | Horizon Redis Connection
    |--------------------------------------------------------------------------";

                // find config file in app and look for these lines
                $config = base_path('config/');
                $finder = new Finder();
                $finder->files()->in($config)->name('horizon.php');

                foreach ($finder as $file) {
                    $path = $file->getRealPath();
                    $contents = file_get_contents($path);

                    if (strpos($contents, $replace) !== false) {
                        $this->error('Custom config property already exists! Skipping.');
                        break;
                    }

                    $contents = str_replace($search, $replace, $contents);
                    file_put_contents($path, $contents);
                }

                $this->info('Custom config property added to Horizon config file.');
            } else {
                $this->info('Skipped adding custom property to Horizon config file.');
            }

            // FINAL CONFIRMATION

            $this->info('---------------------------------');
            $this->info('LARAVEL HORIZON SHIM IS COMPLETE!');
            $this->info('---------------------------------');
        } catch (\Exception $e) {
            $this->error('Laravel Horizon shim installation failed: ' . $e->getMessage());
        }

        return true;
    }
}
