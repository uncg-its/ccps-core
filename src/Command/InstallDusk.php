<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Console\Command;

class InstallDusk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:dusk:install {--p|prompt : Steps through each installation step with yes/no prompts, allowing you to choose which to apply}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs Laravel Dusk assets onto CCPS Framework (replaces dusk:install) - copies files and changes permissions on executables.';

    public function chmodRecursive($dir, $dirPermissions, $filePermissions)
    {
        // directory itself
        //echo('DIR:' . $fullPath . "\n");
        if (!chmod($dir, $dirPermissions)) {
            return false;
        }

        // contents
        $dp = opendir($dir);
        while ($file = readdir($dp)) {
            if (($file == ".") || ($file == "..")) {
                continue;
            }

            $fullPath = $dir . "/" . $file;

            if (is_dir($fullPath)) {
                $this->chmodRecursive($fullPath, $dirPermissions, $filePermissions);
            } else {
                //echo('FILE:' . $fullPath . "\n");
                if (!chmod($fullPath, $filePermissions)) {
                    return false;
                };
            }
        }
        closedir($dp);

        return true;
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = []; // return array init

        try {
            $this->info('Installing Laravel Dusk onto CCPS Framework...');

            // ------ publish Laravel Dusk assets (including replacement DuskTestCase and ExampleTest)
            if (!$this->option('prompt') || $this->confirm('Install Laravel Dusk assets?')) {
                $this->call('vendor:publish', [
                    '--provider' => 'Uncgits\\Ccps\\ServiceProvider',
                    '--tag'      => 'dusk'
                ]);

                $this->info('Laravel Dusk assets installed.');
            } else {
                $this->info('Skipped installing Laravel Dusk assets.');
            }

            // ------ change permissions on vendor/laravel/dusk/bin/*
            if (!$this->option('prompt') || $this->confirm('Change permissions on Dusk executables?')) {
                if ($this->chmodRecursive(base_path('vendor/laravel/dusk/bin'), 0755, 0755)) {
                    $this->info('Permissions changed on Laravel Dusk executables.');
                } else {
                    $this->error('Permissions could not be changed on Laravel Dusk executables. You will need to run \'chmod -R 755 vendor/laravel/dusk/bin/\' manually.');
                }
            } else {
                $this->info('Skipped changing permissions on Dusk executables.');
            }

            // FINAL CONFIRMATION

            $this->info('---------------------------------------');
            $this->info('LARAVEL DUSK CONFIGURATION IS COMPLETE!');
            $this->info('---------------------------------------');
        } catch (\Exception $e) {
            $this->error('Laravel Dusk installation failed: ' . $e->getMessage());
        }

        return true;
    }
}
