<?php

namespace Uncgits\Ccps\Command;

use Carbon\Carbon;
use Illuminate\Console\Command;

class MakeUpgrade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:upgrade {name : The name of the Upgrade class to be created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an upgrade script for a CCPS Core application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Notice: this script is useful for development purposes - upgrades created with this script will be saved to the application\'s \'upgrades\' folder, and should be copied out to your development package.');
            if ($this->confirm('Would you like to proceed?')) {
                $name = $this->argument('name');
                $upgradePath = base_path('upgrades/');

                $datePrefix = Carbon::now()->format('Y_m_d_His_');
                $nameSuffix = \Str::snake($name);

                // check to see if file already exists
                $destinationFile = $upgradePath . $datePrefix . $nameSuffix . '.php';
                if (file_exists($destinationFile)) {
                    throw new \Exception("Upgrade File '$destinationFile' already exists.");
                }

                // stub information
                $file = 'vendor/uncgits/ccps-core/src/stubs/upgrade.stub';
                $stub = file_get_contents(base_path($file));
                $replaceString = "CLASSNAME";

                $className = \Str::studly($name);

                // replace stub data
                $stub = str_replace($replaceString, $className, $stub);

                // create directory if it does not exist
                if (!is_dir($upgradePath)) {
                    mkdir($upgradePath);
                    $this->info("Created 'upgrades' folder.");
                }

                // create Upgrade from stub
                if (file_put_contents($destinationFile, $stub)) {
                    $this->info("New upgrade script file '$destinationFile' generated and saved to 'upgrades' folder.");
                    return true;
                } else {
                    throw new \Exception('Could not create Upgrade file. Check permissions and try again.');
                }
            } else {
                $this->info('Exiting...');
            }
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
