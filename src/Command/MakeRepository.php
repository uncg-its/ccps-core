<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Console\Command;

class MakeRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:repository {model : The class (without namespace) that this Repository assists}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a basic Repository class with Eloquent shims. If using a namespace for your models other than the default, you will need to change it in your imports after generating.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $model = $this->argument('model');
            $modelNamePlaceholder = "{{MODELNAME}}";
            $name = $model . 'Repository';

            $repositoryPath = app_path('Repositories/');

            // check to see if repository class with that name exists
            $destinationFile = $repositoryPath . $name . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Repository File '$destinationFile' already exists.");
            }

            // stub information
            $file = 'vendor/uncgits/ccps-core/src/stubs/repository.stub';
            $stub = file_get_contents(base_path($file));

            // replace stub data
            $stub = str_replace($modelNamePlaceholder, ucwords($model), $stub);

            // create directory if it does not exist
            if (!is_dir($repositoryPath)) {
                mkdir($repositoryPath);
                $this->info("Created 'app\Repositories' folder.");
            }

            // create Cronjob from stub
            if (file_put_contents($destinationFile, $stub)) {
                $this->info("New Repository Class '$name.php' generated and saved to 'app\Repositories' folder.");

                return true;
            } else {
                throw new \Exception('Could not create Repository file. Check permissions and try again.');
            }
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
