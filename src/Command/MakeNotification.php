<?php

namespace Uncgits\Ccps\Command;

use Uncgits\Ccps\Helper;
use Illuminate\Console\Command;
use App\CcpsCore\NotificationEvent;

class MakeNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:notification {notification : The name of the Notification class to be created} {event : The name of the Event to be created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Notification (and accompanying Event and email views) compatible with CCPS Core user notifications.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // --------------------- notification first
            $notification = $this->argument('notification');
            $notificationPath = base_path('app/Notifications/');

            // stub information
            $file = 'vendor/uncgits/ccps-core/src/stubs/notification.stub';
            $notificationStub = file_get_contents(base_path($file));
            $notificationPlaceholder = "NOTIFICATIONNAME";
            $emailPlaceholder = "EMAILNAME";

            $notificationClassName = \Str::studly($notification);
            $emailName = strtolower(preg_replace('%([a-z])([A-Z])%', '\1-\2', $notificationClassName));

            // replace stub data
            $notificationStub = str_replace($notificationPlaceholder, $notificationClassName, $notificationStub);
            $notificationStub = str_replace($emailPlaceholder, $emailName, $notificationStub);

            // check to see if file already exists
            $destinationFile = $notificationPath . $notificationClassName . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Notification class '$destinationFile' already exists.");
            }

            // create directory if it does not exist
            if (!is_dir($notificationPath)) {
                mkdir($notificationPath);
                $this->info("Created 'app/Notifications' folder.");
            }

            // create file from stub
            if (!file_put_contents($destinationFile, $notificationStub)) {
                throw new \Exception('Could not create Notification class file. Check permissions and try again.');
            }

            $this->info("New Notification class '$destinationFile' generated and saved.");


            // --------------------- event second
            $event = $this->argument('event');
            $eventPath = base_path('app/Events/');

            // stub information
            $file = 'vendor/uncgits/ccps-core/src/stubs/notification-event.stub';
            $eventStub = file_get_contents(base_path($file));
            $eventPlaceholder = "EVENTNAME";
            $notificationPlaceholder = "NOTIFICATIONNAME";

            $eventClassName = \Str::studly($event);

            // replace stub data
            $eventStub = str_replace($eventPlaceholder, $eventClassName, $eventStub);
            $eventStub = str_replace($notificationPlaceholder, $notificationClassName, $eventStub);

            // check to see if file already exists
            $destinationFile = $eventPath . $eventClassName . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Event class '$destinationFile' already exists.");
            }

            // create directory if it does not exist
            if (!is_dir($eventPath)) {
                mkdir($eventPath);
                $this->info("Created 'app/Events' folder.");
            }

            // create file from stub
            if (!file_put_contents($destinationFile, $eventStub)) {
                throw new \Exception('Could not create Event class file. Check permissions and try again.');
            }

            $this->info("New Event class '$destinationFile' generated and saved.");


            // --------------------- markdown email third
            $file = 'vendor/uncgits/ccps-core/src/stubs/notification-email.stub';
            $emailMarkdownStub = file_get_contents(base_path($file));
            $emailPath = base_path('resources/views/emails/');

            // check to see if file already exists
            $destinationFile = $emailPath . $emailName . '.blade.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Email view '$destinationFile' already exists.");
            }

            // create directory if it does not exist
            if (!is_dir($emailPath)) {
                mkdir($emailPath);
                $this->info("Created 'resources/views/emails' folder.");
            }

            // create file from stub
            if (!file_put_contents($destinationFile, $emailMarkdownStub)) {
                throw new \Exception('Could not create Email view file. Check permissions and try again.');
            }

            $this->info("New Email view file '$destinationFile' generated and saved.");


            // --------------------- finally the plaintext email
            $file = 'vendor/uncgits/ccps-core/src/stubs/notification-email-plaintext.stub';
            $emailPlaintextStub = file_get_contents(base_path($file));
            $emailPath = base_path('resources/views/emails/plaintext/');

            // check to see if file already exists
            $destinationFile = $emailPath . $emailName . '.blade.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Email view '$destinationFile' already exists.");
            }

            // create directory if it does not exist
            if (!is_dir($emailPath)) {
                mkdir($emailPath);
                $this->info("Created 'resources/views/emails' folder.");
            }

            // create file from stub
            if (!file_put_contents($destinationFile, $emailPlaintextStub)) {
                throw new \Exception('Could not create Email view file. Check permissions and try again.');
            }

            $this->info("New Email view file '$destinationFile' generated and saved.");

            // --------------------- add entry to NotificationEvent table
            $notificationEventName = preg_replace('%([a-z])([A-Z])%', '\1 \2', $notificationClassName);
            // generate migration for metadata
            if ($filename = Helper::makeNotificationEventMigrationFromStub($notificationEventName, $eventClassName)) {
                $this->info("Created migration file '$filename'. Run 'php artisan migrate' to register notification event.");
            }

            $this->info('Executing \'composer dump-autoload\'...');
            \Artisan::call('ccps:dump-autoload');
            $this->info('Success.');

            $this->info("Generation complete. After migration, add the new Event class to your \App\Listeners\CcpsCore\NotificationSubscriber@subscribe method.");
            return true;
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
