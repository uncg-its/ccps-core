<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Support\Str;
use Uncgits\Ccps\Support\Upgrader;
use Symfony\Component\Console\Output\OutputInterface;

class Upgrade extends Installer
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:upgrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrades CCPS Framework to latest version, based on files in the /upgrades folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // warn about doing this in production
            if (config('app.env') == 'production') {
                $this->error('WARNING: application in production! Upgrades are generally done on development apps, and changes pushed to the repository. It is STRONGLY recommended that you follow this process and NOT run ccps:upgrade in production.');
                if (!$this->confirm('Are you SURE you want to proceed?')) {
                    throw new \Exception('Operation aborted');
                }
            }

            // fetch vendor /upgrades folder
            $vendorUpgrades = [];

            $this->info('Checking vendor upgrades folder...', OutputInterface::VERBOSITY_VERBOSE);
            if (\File::isDirectory($this->vendorUpgradesFolder)) {
                $vendorUpgrades = $this->getDirectoryContents($this->vendorUpgradesFolder);
                $numberOfVendorUpgrades = count($vendorUpgrades);
                $this->info('vendor upgrades folder contains ' . $numberOfVendorUpgrades . ' ' . Str::plural('upgrade', $numberOfVendorUpgrades), OutputInterface::VERBOSITY_VERBOSE);
            }

            // fetch app /upgrades folder
            $installedUpgrades = [];

            $this->info('Checking app upgrades folder...', OutputInterface::VERBOSITY_VERBOSE);
            if (\File::isDirectory($this->installedUpgradesFolder)) {
                $this->info('app upgrades folder exists.', OutputInterface::VERBOSITY_VERBOSE);
                $installedUpgrades = $this->getDirectoryContents($this->installedUpgradesFolder);
                $numberOfInstalledUpgrades = count($installedUpgrades);
                $this->info('app upgrades folder contains ' . $numberOfInstalledUpgrades . ' ' . Str::plural('upgrade', $numberOfInstalledUpgrades), OutputInterface::VERBOSITY_VERBOSE);
            } else {
                $this->info('/upgrades folder does not exist; creating.', OutputInterface::VERBOSITY_VERBOSE);
                \File::makeDirectory($this->installedUpgradesFolder);
            }

            // is app/upgrades folder writable?
            if (!\File::isWritable($this->installedUpgradesFolder)) {
                throw new \Exception('app/upgrades folder is not writable! Cannot perform upgrade. Check permissions.');
            }

            // compare to see which ones need to be run
            $upgradesToRun = array_diff($vendorUpgrades, $installedUpgrades);

            $numberOfUpgradesToRun = count($upgradesToRun);
            $this->line('Found <fg=blue>' . $numberOfUpgradesToRun . ' new CCPS Core ' . Str::plural('upgrade', $numberOfUpgradesToRun) . '</> to be run.');

            // execute each one programmatically
            $upgrader = new Upgrader();

            foreach ($upgradesToRun as $filename) {
                $upgradeFilename = $filename . '.php'; // add .php extension since filename is extension-less at this point

                $this->info('Running <fg=blue>' . $upgradeFilename . '</>', OutputInterface::VERBOSITY_VERBOSE);
                $class = $upgrader->resolve($upgradeFilename);

                $result = $class->upgrade(); // expecting array - ['success' => true, 'reboot' => false, 'exception' => null]

                if ($result['success']) {
                    $this->info('Execution of upgrade file <fg=blue>' . $upgradeFilename . '</> successful!');

                    $signatureFilename = $filename . '.txt'; // add .txt file extension
                    $operation = 'Upgrade file ' . $upgradeFilename;
                    $this->writeSignatureFile($signatureFilename, $operation);// put timestamp data in new signature file, to be inserted into app/upgrades

                    if ($result['reboot']) {
                        $this->line('Notice: a <fg=red>restart</> of the Laravel framework is required after upgrade item ' . $filename . '. Please run \'php artisan ccps:upgrade\' again.');
                        exit;
                    }
                } else {
                    $this->error('Execution of upgrade file ' . $filename . ' failed! Aborting.');
                    throw $result['exception']; // rethrow exception
                }
            }
        } catch (\Exception $e) {
            $this->error('CCPS Core upgrade failed: ' . $e->getMessage());
        }

        return true;
    }
}
