<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Console\Command;

class MakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:service {name : The name of the Service class to be created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an empty Service class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $name = $this->argument('name');
            $servicePath = app_path('Services/');

            // check to see if service class with that name exists
            $destinationFile = $servicePath . $name . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Service File '$destinationFile' already exists.");
            }

            // stub information
            $file = 'vendor/uncgits/ccps-core/src/stubs/service.stub';
            $stub = file_get_contents(base_path($file));
            $replaceString = "{{CLASSNAME}}";

            // replace stub data
            $stub = str_replace($replaceString, $name, $stub);

            // create directory if it does not exist
            if (!is_dir($servicePath)) {
                mkdir($servicePath);
                $this->info("Created 'app\Services' folder.");
            }

            // create Cronjob from stub
            if (file_put_contents($destinationFile, $stub)) {
                $this->info("New Service Class '$name.php' generated and saved to 'app\Services' folder.");

                return true;
            } else {
                throw new \Exception('Could not create Service file. Check permissions and try again.');
            }
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
