<?php

namespace Uncgits\Ccps\Command;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;

class ComposerDumpAutoload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:dump-autoload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs \'composer dump-autoload\'';

    protected $composer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Composer $composer)
    {
        parent::__construct();
        $this->composer = $composer;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->composer->dumpAutoloads();
        $this->composer->dumpOptimized();
    }
}
