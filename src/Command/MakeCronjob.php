<?php

namespace Uncgits\Ccps\Command;

use Uncgits\Ccps\Helper;
use App\CcpsCore\CronjobMeta;
use Illuminate\Console\Command;

class MakeCronjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:cronjob {name : The name of the Cronjob class to be created} {--e|enabled : Set the Cronjob to be Enabled by default} {--N|nodump : Do not execute composer dump-autoload after creation}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a cronjob for a CCPS Core application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $name = $this->argument('name');
            $cronjobPath = app_path('Cronjobs/');

            // check to see if cronjob with that name exists
            $cronjobMetas = CronjobMeta::where('class', 'App\\Cronjobs\\' . $name)->first();
            if ($cronjobMetas) {
                throw new \Exception("Metadata for Cronjob App\\Cronjobs\\$name already exists.");
            }

            $destinationFile = $cronjobPath . $name . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Cronjob File '$destinationFile' already exists.");
            }

            // stub information
            $file = 'vendor/uncgits/ccps-core/src/stubs/cronjob.stub';
            $stub = file_get_contents(base_path($file));
            $replaceString = "CLASSNAME";

            // replace stub data
            $stub = str_replace($replaceString, $name, $stub);

            // create directory if it does not exist
            if (!is_dir($cronjobPath)) {
                mkdir($cronjobPath);
                $this->info("Created 'app\Cronjobs' folder.");
            }

            // create Cronjob from stub
            if (file_put_contents($destinationFile, $stub)) {
                $this->info("New cronjob '$name.php' generated and saved to 'app\Cronjobs' folder.");

                // generate migration for metadata
                if ($filename = Helper::makeCronjobMigrationFromStub(
                    $name,
                    $this->option('enabled') ? 'enabled' : 'disabled'
                )) {
                    $this->info("Created migration file '$filename'. Run 'php artisan migrate' to register cronjob metadata.");
                }

                if (!$this->option('nodump')) {
                    $this->info('Executing \'composer dump-autoload\'...');
                    \Artisan::call('ccps:dump-autoload');
                    $this->info('Success.');
                }

                return true;
            } else {
                throw new \Exception('Could not create Cronjob file. Check permissions and try again.');
            }
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
