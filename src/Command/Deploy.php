<?php

namespace Uncgits\Ccps\Command;

class Deploy extends Installer
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:deploy {--p|prompt : Steps through each deployment step with yes/no prompts, allowing you to choose which to apply}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy an app built from CCPS Framework to a new server. Handles .env, migrations, seeds.';

    /**
     * Name of the signature file that we will install in upgrades/ folder after successful init (with extension)
     *
     * @var string
     */
    protected $signatureFilename = 'ccps-deploy.txt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Deploying CCPS Framework app...');

            $this->checkForSignatureFile();

            $this->copyEnv();

            // ------ link /public/storage to /storage/app/public
            $this->linkStoragePublicFolder();

            // ------ timezone
            $this->setUpTimezoneInEnv();

            $this->setUpDatabaseInEnv();

            $this->databaseMigrationsAndSeeds();

            // ------ set permissions on /storage
            $this->setStorageFolderPermissions();

            // ------ fill in Mail info0
            $this->setUpMailInEnv();

            // ------ fill in App info
            $this->setUpAppInfoInEnv();

            // ------ allow user signups?
            $this->setUpSignupInfoInEnv();

            // ------ log days config
            $this->setUpAppLogsInEnv();

            // ------ admin password
            $this->setAdminPassword();

            // ------ generate app key
            $this->generateAppKey();

            // ------ write signature file
            $this->writeSignatureFile($this->signatureFilename, 'Deployment');

            // FINAL CONFIRMATION
            $this->info('--------------------------------------');
            $this->info('CCPS FRAMEWORK DEPLOYMENT IS COMPLETE!');
            $this->info('--------------------------------------');
        } catch (\Exception $e) {
            $this->error('CCPS Framework deployment failed: ' . $e->getMessage());
        }

        return true;
    }
}
