<?php

use Symfony\Component\Finder\Finder;

class OverhaulExceptionHandlerRenderMethod
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "public function render(\$request, Throwable \$throwable)
    {
        if (config('ccps.redirect_to_login') && \$throwable instanceof HttpException) {
            if (\$throwable->getStatusCode() == 403) {
                if (\$request->expectsJson()) {
                    return response()->json(['error' => 'Unauthorized.'], 403);
                }
                if (auth()->user()) {
                    return response()->view('errors.403', ['exception' => \$throwable], 403);
                }
                flash('You must be logged in to access this page.')->warning();
                return redirect()->route('login');
            }
        }

        // if we are not in debug mode...
        if (!config('app.debug') && !\$request->expectsJson()) {
            // render model-not-found and method-not-allowed exceptions as 404
            if (\$throwable instanceof ModelNotFoundException || \$throwable instanceof MethodNotAllowedHttpException) {
                return \$this->loadErrorMiddleware(\$request, function (\$request) {
                    return parent::render(\$request, new NotFoundHttpException());
                });
            } elseif (!\$this->isHttpException(\$throwable)) {
                // make sure validation errors get handled properly
                if (\$throwable instanceof ValidationException) {
                    return redirect()->back()->withInput()->withErrors(\$throwable->validator->getMessageBag()->toArray());
                }
                // otherwise render the exception as 500
                return \$this->loadErrorMiddleware(\$request, function (\$request) use (\$throwable) {
                    return response()->view('errors.500', ['exception' => \$throwable], 500);
                });
            } else {
                // is another flavor of HTTP exception...
                \$httpCode = \$throwable->getStatusCode();
                return \$this->loadErrorMiddleware(\$request, function (\$request) use (\$throwable, \$httpCode) {
                    \$message = \$throwable->getMessage() ?: '';
                    return response()->view('errors.' . \$httpCode, ['message' => \$message, 'exception' => \$throwable], \$httpCode);
                });
            }
        }

        if (\$throwable instanceof QueryException) {
            event(new UncaughtQueryException(\$throwable));
        }

        // make sure we get session data if we need to output a webpage
        if (is_a(\$throwable, 'Symfony\Component\HttpKernel\Exception\HttpException')) {
            \$httpCode = \$throwable->getStatusCode();
            if (\$httpCode != 403) {
                // 403 errors actually do this already, so we don't need to do this with 403s.
                return \$this->loadErrorMiddleware(\$request, function (\$request) use (\$throwable, \$httpCode) {
                    \$message = \$throwable->getMessage() ?: '';
                    return response()->view('errors.' . \$httpCode, ['message' => \$message, 'exception' => \$throwable], \$httpCode);
                });
            }
        }

        // regular ol' exception rendered with regular ol' details (debug only, hopefully)
        return parent::render(\$request, \$throwable);
    }"   => "public function render(\$request, Throwable \$throwable)
    {
        // defer to default for JSON responses
        if (\$request->expectsJson()) {
            return parent::render(\$request, \$throwable);
        }

        // redirect to login if 403
        if (config('ccps.redirect_to_login') && \$throwable instanceof HttpException) {
            if (\$throwable->getStatusCode() === 403) {
                if (auth()->user()) {
                    return response()->view('errors.403', ['exception' => \$throwable], 403);
                }
                flash('You must be logged in to access this page.')->warning();
                return redirect()->route('login');
            }
        }

        // fallback to catch any edge cases we haven't accounted for.
        return parent::render(\$request, \$throwable);
    }",
                "/**
     * Load the middleware required to show state/session-enabled error pages.
     * @param Request \$request
     * @param \$callback
     * @return mixed
     */
    protected function loadErrorMiddleware(Request \$request, \$callback)
    {
        \$middleware = (\Route::getMiddlewareGroups()['web_errors']);
        return (new Pipeline(\$this->container))
            ->send(\$request)
            ->through(\$middleware)
            ->then(\$callback);
    }"   => "",
    "use Illuminate\Http\Request;" => "",
    "use Illuminate\Pipeline\Pipeline;" => "",
    "use Illuminate\Validation\ValidationException;" => "",
    "use Illuminate\Database\Eloquent\ModelNotFoundException;" => "",
    "use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;" => "",
    "use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;" => "",
            ];


            // find Handler file in app and look for these lines
            $filePath = base_path('app/Exceptions/');
            $finder = new Finder();
            $finder->files()->in($filePath)->name('Handler.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
