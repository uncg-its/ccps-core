<?php

use Symfony\Component\Finder\Finder;

class AddProfileSettingsForm
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "<div class=\"col-md-12\"><p>Nothing to show.</p></div>" => "<div class=\"col-md-12\">
            <form action=\"{{ route('account.settings.update') }}\" method=\"post\">
                <div class=\"form-group\">
                    <label for=\"skin\">Interface Skin</label>
                    <select class=\"form-control\" id=\"skin\" name=\"skin\">
                        @foreach(\$skins as \$name => \$attributes)
                            <option value=\"{{ \$name }}\" @if(\$name == \$selected)
                            selected
                                    @endif
                            >{{ \$attributes['display_name'] }}</option>
                        @endforeach
                    </select>
                    <small class=\"form-text form-muted\">Change the appearance (\"theme\") of the application</small>
                </div>

                <button type=\"submit\" class=\"btn btn-primary\">Submit</button>
                {!! csrf_field() !!}
            </form>
        </div>",
            ];


            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/account/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('settings.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
