<?php

use Symfony\Component\Finder\Finder;

class ChangeFlashComponentForLaracastsFlashPackage
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/components/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('flash.blade.php');

            $contents = "<section class=\"content-flash\">
    @include('flash::message')
</section>";

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                file_put_contents($path, $contents);
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
