<?php

use Symfony\Component\Finder\Finder;

class AddNotificationBreadcrumbs
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "// ADMIN"             => "// Notifications
Breadcrumbs::register('notifications.index', function (\$breadcrumbs) {
    \$breadcrumbs->parent('account');
    \$breadcrumbs->push('Notifications', route('account.notifications.index'));
});
Breadcrumbs::register('notifications.channels.create', function (\$breadcrumbs) {
    \$breadcrumbs->parent('notifications.index');
    \$breadcrumbs->push('Add New Channel', route('account.notifications.channels.create'));
});
Breadcrumbs::register('notifications.configure', function (\$breadcrumbs) {
    \$breadcrumbs->parent('notifications.index');
    \$breadcrumbs->push('Configure', route('account.notifications.configure'));
});

// ADMIN",
            ];


            // find breadcrumbs file in app and look for these lines
            $viewPath = base_path('routes/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('breadcrumbs.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
