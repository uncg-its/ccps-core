<?php

use Symfony\Component\Finder\Finder;

class ChangeWebRoutesToPhpCallableSyntax
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');"   => "Route::get('/', [\App\Http\Controllers\CcpsCore\HomeController::class, 'index'])->name('home');
Route::get('home', [\App\Http\Controllers\CcpsCore\HomeController::class, 'index']);

Route::group(['prefix' => 'account'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'index'])->name('account');
    Route::get('/settings', [\App\Http\Controllers\CcpsCore\AccountController::class, 'settings'])->name('account.settings');
    Route::post('/settings', [\App\Http\Controllers\CcpsCore\AccountController::class, 'updateSettings'])->name('account.settings.update');
    Route::get('/profile', [\App\Http\Controllers\CcpsCore\AccountController::class, 'profile'])->name('profile.show');
    Route::get('/profile/edit', [\App\Http\Controllers\CcpsCore\AccountController::class, 'editProfile'])->name('profile.edit');
    Route::patch('/profile/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'updateProfile'])->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'tokens'])->name('index');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'notifications'])->name('account.notifications.index');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', [\App\Http\Controllers\CcpsCore\AuthController::class, 'redirectToProvider'])->name('oauth');
Route::get('auth/{provider}/callback', [\App\Http\Controllers\CcpsCore\AuthController::class, 'handleProviderCallback'])->name('oauth.callback');",
            ];


            // find affected file(s) in app and look for these lines
            $filePath = base_path('routes');
            $finder = new Finder();
            $finder->files()->in($filePath)->name('web.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
