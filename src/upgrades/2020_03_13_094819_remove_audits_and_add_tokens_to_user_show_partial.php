<?php

use Symfony\Component\Finder\Finder;

class RemoveAuditsAndAddTokensToUserShowPartial
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "

        @include('partials.audit-history-card', ['modelType' => 'user', 'model' => \$userToShow])"   => "
        <div class=\"card\">
            <div class=\"card-header\"><h5 class=\"mb-0\">API Tokens</h5></div>
            <div class=\"card-body\">
                @if (\$userToShow->tokens->isNotEmpty())
                    <ul>
                        @foreach (\$userToShow->tokens as \$token)
                            <li>{{ \$token->name }} (expires: {{ is_null(\$token->expires_at) ? 'never' : \$token->expires_at }})</li>
                        @endforeach
                    </ul>
                @else
                    <p>No tokens to show</p>
                @endif
                <a href=\"{{ route('account.tokens.index', ['show' => 'all']) }}\" class=\"btn btn-sm btn-info\">
                    <i class=\"fas fa-code\"></i> Manage Tokens
                </a>
            </div>
        </div>",
                "

                    <p><strong>API Key</strong>: {{ \$userToShow->api_key ?? 'not set' }}</p>" => "",
            ];


            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/partials');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('user-show.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
