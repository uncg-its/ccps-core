<?php

use Symfony\Component\Filesystem\Filesystem;

class ReplaceNotificationcontrollerWithNotificationlogcontroller
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // copy views
            $filesystem = new Filesystem;

            $filesystem->copy(__DIR__ . '/../publish/controllers/CcpsCore/NotificationLogController.php', base_path('app/Http/Controllers/CcpsCore/NotificationLogController.php'));

            // get rid of old controller
            unlink(base_path('app/Http/Controllers/CcpsCore/NotificationController.php'));
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
