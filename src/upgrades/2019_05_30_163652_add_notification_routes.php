<?php

use Symfony\Component\Finder\Finder;

class AddNotificationRoutes
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');
"   => "Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
        Route::get('/channels/create', 'CcpsCore\AccountController@createChannel')->name('account.notifications.channels.create');
        Route::post('/channels/create', 'CcpsCore\AccountController@storeChannel')->name('account.notifications.channels.store');
        Route::delete('/channels/{channel}', 'CcpsCore\AccountController@destroyChannel')->name('account.notifications.channels.destroy');
        Route::get(
            '/channels/{channel}/resend-verification',
            'CcpsCore\ChannelVerificationController@resend'
        )->name('account.notifications.channels.resend-verification');
        Route::get(
            '/channels/{channel}/verify',
            'CcpsCore\ChannelVerificationController@verify'
        )->name('account.notifications.channels.verify');
        Route::get(
            '/channels/{channel}/test',
            'CcpsCore\ChannelVerificationController@test'
        )->name('account.notifications.channels.test');

        Route::get('/configure', 'CcpsCore\AccountController@configure')->name('account.notifications.configure');
        Route::post('/configure', 'CcpsCore\AccountController@configureSave')->name('account.notifications.configure.save');
    });
"
            ];


            // find routes file in app and look for these lines
            $viewPath = base_path('routes/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('web.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
