<?php

use Symfony\Component\Finder\Finder;

class AddProfileSettingsRoute
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');" => "Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');",
            ];


            // find view partial file in app and look for these lines
            $filePath = base_path('routes/');
            $finder = new Finder();
            $finder->files()->in($filePath)->name('web.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
