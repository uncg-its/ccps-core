<?php

use Symfony\Component\Filesystem\Filesystem;

class InstallNewLaratrustConfigFile
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // copy new laratrust config file and overwrite old
            $filesystem = new Filesystem;
            $filesystem->copy(__DIR__ . '/../publish/config/laratrust.php', config_path('laratrust.php'), true);
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
