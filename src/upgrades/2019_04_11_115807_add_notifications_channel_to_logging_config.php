<?php

use Symfony\Component\Finder\Finder;

class AddNotificationsChannelToLoggingConfig
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            if (isset(config('logging')['notifications'])) {
                // already in there
                return [
                    'success'   => true,
                    'reboot'    => false,
                    'exception' => null
                ];
            }

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "'backup' => ["        => "'notifications' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/notifications.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7),
        ],

        'backup' => [",

            ];


            // find file in app and look for these lines
            $configPath = base_path('config/');
            $finder = new Finder();
            $finder->files()->in($configPath)->name('logging.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
