<?php

use Symfony\Component\Finder\Finder;

class AddHomePropertyToRouteserviceprovider
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "/**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()"   => "/**
     * The path to the \"home\" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()",
            ];


            // find provider file in app and look for these lines
            $viewPath = app_path('Providers/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('RouteServiceProvider.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);

                if (strpos($contents, 'public const HOME') !== false) {
                    // don't override this if CCPS Core 1.11.* was installed with a version of Laravel that already has this.
                    continue;
                }

                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
