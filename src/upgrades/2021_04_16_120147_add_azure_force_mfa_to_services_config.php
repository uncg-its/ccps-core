<?php

use Symfony\Component\Finder\Finder;

class AddAzureForceMfaToServicesConfig {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "env('SOCIALITE_AZURE_REDIRECT_URI'),"   => "env('SOCIALITE_AZURE_REDIRECT_URI'),
        'force_mfa' => env('SOCIALITE_AZURE_FORCE_MFA', false),",
            ];


            // find affected file(s) in app and look for these lines
            $filePath = base_path('config');
            $finder = new Finder();
            $finder->files()->in($filePath)->name('services.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);

                if (strpos('SOCIALITE_AZURE_FORCE_MFA', $contents) !== false) {
                    continue;
                }

                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
