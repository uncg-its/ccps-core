<?php

use Symfony\Component\Finder\Finder;

class ExceptionHandlerUseStatement {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            // replace offending text with correct text in Handler file

            $textToReplace = "use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;";


            $replacementText = "use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Validation\ValidationException;";

            // find Handler file in app and look for these lines
            $handlerPath = base_path('app/Exceptions');
            $finder = new Finder();
            $finder->files()->in($handlerPath)->name('Handler.php');

            foreach($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
