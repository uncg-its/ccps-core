<?php

use Symfony\Component\Finder\Finder;

class AddProfileSettingsToAccountController
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "public function __construct() {
        parent::__construct();
    }"                                                                              => "public function __construct()
    {
        parent::__construct();
    }

    public function settings()
    {
        \$skins = config('ccps.bootstrap_skins');
        \$selected = auth()->user()->settings->skin ?? 'default';

        return view('account.settings')->with([
            'skins'    => \$skins,
            'selected' => \$selected
        ]);
    }

    public function updateSettings(Request \$request)
    {
        \$approvedSettings = ['skin'];
        \$skins = array_keys(config('ccps.bootstrap_skins'));

        \$validated = \$request->validate([
            'skin' => [
                'required',
                Rule::in(\$skins),
            ]
        ]);

        \$user = auth()->user();

        \$existingSettings = \$user->settings ?? new \stdClass();
        foreach (\$approvedSettings as \$setting) {
            \$existingSettings->{\$setting} = \$request->\$setting;
        }

        try {
            \$user->settings = json_encode(\$existingSettings);
            \$user->save();

            flash('Settings updated!')->success();
        } catch (\Exception \$e) {
            \Log::channel('general')->error('Error while updating settings for user ' . \$user->id, [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'current_user' => \$user,
                    'request'      => \$request->all(),
                    'message'      => \$e->getMessage()
                ]
            ]);

            flash('There was an error updating your account settings. Please contact an administrator.')->error();
        }
        return redirect()->back();
    }",
                "use Uncgits\Ccps\Controllers\AccountController as BaseController;" => "use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Uncgits\Ccps\Controllers\AccountController as BaseController;"

            ];


            // find view partial file in app and look for these lines
            $controllerPath = base_path('app/Http/Controllers/CcpsCore/');
            $finder = new Finder();
            $finder->files()->in($controllerPath)->name('AccountController.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
