<?php

use Symfony\Component\Finder\Finder;

class AddNewTextToTokenCreationModal
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "<div class=\"text-wrap lead\">
                        <code>{{ session('newToken')->plainTextToken }}</code>
                    </div>"   => "<div class=\"text-wrap lead\">
                        <code>{{ session('newToken')->plainTextToken }}</code>
                    </div>
                    <p class=\"mt-3\"><strong>Note that your token is not granted any abilities by default</strong>. However, this comes into play only if the application has implemented ability checks in the API.</p>",
            ];


            // find view file in app and look for these lines
            $viewPath = base_path('resources/views/account/tokens/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('index.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
