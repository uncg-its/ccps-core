<?php

use Symfony\Component\Finder\Finder;

class AddStrAndArrAliasesToAppConfig
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "'App' => Illuminate\Support\Facades\App::class," => "'App' => Illuminate\Support\Facades\App::class,
        'Arr' => Illuminate\Support\Arr::class,",
                "'Storage' => Illuminate\Support\Facades\Storage::class," => "'Storage' => Illuminate\Support\Facades\Storage::class,
        'Str' => Illuminate\Support\Str::class,",

            ];


            // find config file in app and look for these lines
            $viewPath = base_path('config/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('app.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
