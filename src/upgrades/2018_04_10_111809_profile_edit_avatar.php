<?php

use Symfony\Component\Finder\Finder;

class ProfileEditAvatar {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            // replace offending text with correct text in view

            $textToReplace = "@else
            <p><em>Note: your email address and password cannot be changed because you registered using a third party provider: {{ ucwords(\$userToEdit->provider) }}.</em></p>
        @endif";


            $replacementText = "<div class=\"form-group row {{ empty(\$errors->get('avatar_url')) ? \"\" : \" has-error\" }}\">
                <label for=\"avatar_url\" class=\"col-sm-2 col-form-label\">Avatar URL:</label>
                <div class=\"col\">
                    <input type=\"file\" class=\"form-control\" id=\"avatar\" name=\"avatar\">
                    @if(!is_null(\$userToEdit->avatar_url))
                        <small class=\"form-text text-muted\">Current file: <a href=\"{{ url(\$userToEdit->avatar_url) }}\" target=\"_blank\">{{ \$userToEdit->avatar_filename }}</a></small>
                    @endif
                </div>
            </div>

            <div class=\"form-group row\">
                <div class=\"col offset-2\">
                    <div class=\"form-check\">
                        <input type=\"checkbox\" value=\"1\" name=\"remove_avatar\"
                               @if(old('remove_avatar') !== null)
                               checked
                                @endif
                        >
                        <label for=\"remove_avatar\" class=\"form-check-label\">Remove avatar?</label>
                        <small class=\"form-text text-muted\">Checking this box will remove the user avatar and revert it to the placeholder image.</small>
                    </div>
                </div>
            </div>

        @else
            <p><em>Note: your email address, password, and avatar cannot be changed because you registered using a third party provider: {{ ucwords(\$userToEdit->provider) }}. Please adjust these items by visiting your account at that provider's website.</em></p>
        @endif";


            // find view partial file in app and look for these lines
            $viewPartialsPath = base_path('resources/views/account/profile');
            $finder = new Finder();
            $finder->files()->in($viewPartialsPath)->name('edit.blade.php');

            foreach($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}