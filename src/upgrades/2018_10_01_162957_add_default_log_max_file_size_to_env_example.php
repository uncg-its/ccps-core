<?php

use Symfony\Component\Finder\Finder;

class AddDefaultLogMaxFileSizeToEnvExample {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            $textToReplace = "DB_CONNECTION=";


            $replacementText = "LOG_MAX_FILE_SIZE=

DB_CONNECTION=";


            // find .env.example and make the replacement
            $envPath = base_path();
            $finder = new Finder();
            $finder->files()->ignoreDotFiles(false)->in($envPath)->name('.env.example');

            foreach($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                if (strpos($contents, "LOG_MAX_FILE_SIZE=") === false) {
                    // only do this if it's not already there
                    $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                    if ($replacementsMade > 0) {
                        file_put_contents($path, $contents);
                    }
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
