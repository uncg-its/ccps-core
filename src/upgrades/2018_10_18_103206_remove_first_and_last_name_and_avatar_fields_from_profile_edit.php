<?php

use Symfony\Component\Finder\Finder;

class RemoveFirstAndLastNameAndAvatarFieldsFromProfileEdit
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "<div class=\"form-group row {{ empty(\$errors->get('first_name')) ? \"\" : \" has-error\" }}\">
            <label for=\"first_name\" class=\"col-sm-2 col-form-label\">First Name:*</label>
            <div class=\"col\">
                <input type=\"text\" class=\"form-control\" id=\"first_name\" name=\"first_name\"
                       placeholder=\"First Name\" value=\"{{ old('first_name', \$userToEdit->first_name) }}\">
            </div>
        </div>
        <div class=\"form-group row {{ empty(\$errors->get('last_name')) ? \"\" : \" has-error\" }}\">
            <label for=\"last_name\" class=\"col-sm-2 col-form-label\">Last Name:*</label>
            <div class=\"col\">
                <input type=\"text\" class=\"form-control\" id=\"last_name\" name=\"last_name\"
                       placeholder=\"Last Name\" value=\"{{ old('last_name', \$userToEdit->last_name) }}\">
            </div>
        </div>" => "",

                "<div class=\"form-group row {{ empty(\$errors->get('avatar_url')) ? \"\" : \" has-error\" }}\">
                <label for=\"avatar_url\" class=\"col-sm-2 col-form-label\">Avatar URL:</label>
                <div class=\"col\">
                    <input type=\"file\" class=\"form-control\" id=\"avatar\" name=\"avatar\">
                    @if(!is_null(\$userToEdit->avatar_url))
                        <small class=\"form-text text-muted\">Current file: <a href=\"{{ url(\$userToEdit->avatar_url) }}\" target=\"_blank\">{{ \$userToEdit->avatar_filename }}</a></small>
                    @endif
                </div>
            </div>

            <div class=\"form-group row\">
                <div class=\"col offset-2\">
                    <div class=\"form-check\">
                        <input type=\"checkbox\" value=\"1\" name=\"remove_avatar\"
                               @if(old('remove_avatar') !== null)
                               checked
                                @endif
                        >
                        <label for=\"remove_avatar\" class=\"form-check-label\">Remove avatar?</label>
                        <small class=\"form-text text-muted\">Checking this box will remove the user avatar and revert it to the placeholder image.</small>
                    </div>
                </div>
            </div>" => "",

                "<p><em>Note: your email address, password, and avatar cannot be changed because you registered using a third party provider: {{ ucwords(\$userToEdit->provider) }}. Please adjust these items by visiting your account at that provider's website.</em></p>" => "<p><em>Note: your email address and password cannot be changed because you registered using a third party provider: {{ ucwords(\$userToEdit->provider) }}. Please adjust these items by visiting your account at that provider's website.</em></p>",
            ];


            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/account/profile/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('edit.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}