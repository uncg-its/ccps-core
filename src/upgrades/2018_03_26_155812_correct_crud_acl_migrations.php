<?php

use Symfony\Component\Finder\Finder;

class CorrectCrudAclMigrations
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // replace offending text with correct text in ACL migrations

            $textToReplace = "// get the permissions
        foreach(\$this->bindings as \$role => \$permissions) {
            \$roleModel = Role::where('name', \$role)->first();
            \$roleModel->detachPermissions(\$permissions);
            \$deletedPermissions = Permission::whereIn('name', \$permissions)->delete();
            \$deletedRole = \$roleModel->delete();
        }";


            $replacementText = "// detach the permissions, figure out which to delete
        \$permissionsToDelete = [];

        foreach(\$this->bindings as \$role => \$permissions) {
            \$roleModel = Role::where('name', \$role)->first();
            \$roleModel->detachPermissions(\$permissions);

            \$permissionsToDelete = array_merge(\$permissionsToDelete, \$permissions);

            // delete role (only if editable)
            if (\$roleModel->editable) {
                \$deletedRole = \$roleModel->delete();
            }
        }

        // delete permissions
        \$permissionsToDelete = array_unique(\$permissionsToDelete);
        \$deletedPermissions = Permission::whereIn('name', \$permissionsToDelete)->delete();";


            // find migration files in app and look for these lines
            $migrationsPath = base_path('database/migrations');
            $finder = new Finder();
            $finder->files()->in($migrationsPath);

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e,
            ];
        }

        return [
            'success' => true,
            'reboot'  => false
        ];
    }
}
