<?php

use Symfony\Component\Finder\Finder;

class MoveModuleRoutesToFacade
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "// Routes from Modules
\$ccpsModules = collect(config('ccps.modules'));
\$adminModules = \$ccpsModules->where('parent', 'admin')->where('use_custom_routes', false);

Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function() {
    Route::get('/', 'CcpsCore\AdminController@index')->name('admin');

    foreach(config('ccps.modules') as \$name => \$module) {
        if (!\$module['use_custom_routes'] && \$module['parent'] == 'admin') {
            require_once(base_path('vendor/' . \$module['package'] . '/src/routes/' . \$name . '.php'));
        }
    }
});

foreach(config('ccps.modules') as \$name => \$module) {
    if (!\$module['use_custom_routes'] && empty(\$module['parent'])) {
        require_once(base_path('vendor/' . \$module['package'] . '/src/routes/' . \$name . '.php'));
    }
}
" => "// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();",

            ];


            // find routes file in app and look for these lines
            $routesPath = base_path('routes/');
            $finder = new Finder();
            $finder->files()->in($routesPath)->name('web.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}