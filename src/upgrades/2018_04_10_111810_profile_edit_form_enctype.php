<?php

use Symfony\Component\Finder\Finder;

class ProfileEditFormEnctype {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            // replace offending text with correct text in view

            $textToReplace = "<form action=\"{{ route('profile.update') }}\" method=\"POST\">";


            $replacementText = "<form action=\"{{ route('profile.update') }}\" method=\"POST\" enctype=\"multipart/form-data\">";


            // find view partial file in app and look for these lines
            $viewPartialsPath = base_path('resources/views/account/profile');
            $finder = new Finder();
            $finder->files()->in($viewPartialsPath)->name('edit.blade.php');

            foreach($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
