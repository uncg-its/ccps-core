<?php

use Symfony\Component\Finder\Finder;

class AddCronjobMetaParsingToConsoleKernel
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "protected function schedule(Schedule \$schedule)
    {"                                 => "protected function schedule(Schedule \$schedule)
    {
        \$cronjobsToRun = CronjobMeta::where('status', 'enabled')->get();

        \$cronjobsToRun->each(function (\$cronjobMeta) use (\$schedule) {
            \$cronClass = new \$cronjobMeta->class;
            \$schedule->call(\Closure::fromCallable([\$cronClass, 'run']))
                ->cron(\$cronClass->getSchedule())
                ->name(\Str::studly(\$cronClass->getClassName()))
                ->withoutOverlapping();
        });",
                "use Illuminate\Console\Scheduling\Schedule;"   => "use App\CcpsCore\CronjobMeta;
use Illuminate\Console\Scheduling\Schedule;",
            ];


            // find kernel file in app and look for these lines
            $viewPath = base_path('app/Console/');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('Kernel.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
