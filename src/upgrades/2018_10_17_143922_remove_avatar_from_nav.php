<?php

use Symfony\Component\Finder\Finder;

class RemoveAvatarFromNav
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {
            $textToReplace = "<img src=\"{{ \$user->avatar_url ?? asset('images/default-avatar.jpg') }}\" class=\"rounded-circle ccps-nav-thumb\" alt=\"user profile image\">";
            $replacementText = "";

            // find view and make the replacement
            $viewPath = base_path('resources/views/components');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('nav.blade.php');

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);

                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }

            }


        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
