<?php

use Symfony\Component\Finder\Finder;

class AddApiTokenRoutes
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');"   => "Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
        Route::get('create', 'CcpsCore\AccountController@createToken')->name('create');
        Route::post('/', 'CcpsCore\AccountController@storeToken')->name('store');
        Route::get('{token}', 'CcpsCore\AccountController@editToken')->name('edit');
        Route::patch('{token}', 'CcpsCore\AccountController@updateToken')->name('update');
        Route::delete('{token}', 'CcpsCore\AccountController@revokeToken')->name('revoke');
    });",
            ];


            // find routes file in app and look for these lines
            $viewPath = base_path('routes');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('web.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
