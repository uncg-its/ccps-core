<?php

use Symfony\Component\Finder\Finder;

class AddApplicationSnapshotChannelConfig
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "/*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
    |--------------------------------------------------------------------------
    |
    */
"                                      => "'app_snapshots' => [
        'log_channel' => env('APPLICATION_SNAPSHOT_LOGGING_CHANNEL', 'application-snapshots')
    ],

    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
    |--------------------------------------------------------------------------
    |
    */",
            ];


            // find config file in app and look for these lines
            $viewPath = config_path();
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('ccps.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
