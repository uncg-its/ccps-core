<?php

use Symfony\Component\Finder\Finder;

class UpdateLavaryMenuBladeDirectives
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            $replacement = <<<HTML
@foreach(\$items as \$item)
    <li @lm-attrs(\$item) class="m-{{ \$margin ?? '2' }} @if(\$item->hasChildren()) dropdown @endif " @lm-endattrs>
        @if(\$item->link) <a @lm-attrs(\$item->link) @if(\$item->hasChildren()) class="{{ \$linkClass ?? 'nav-link' }} dropdown-toggle" data-toggle="dropdown" @else class="{{ \$linkClass ?? 'nav-link' }}" @endif @lm-endattrs href="{!! \$item->url() !!}">
        {!! \$item->title !!}
        @if(\$item->hasChildren()) <b class="caret"></b> @endif
        </a>
        @else
            {!! \$item->title !!}
        @endif
        @if(\$item->hasChildren())
            <ul class="dropdown-menu">
                @include('components.bs4-menu-items', ['items' => \$item->children(), 'linkClass' => 'dropdown-item', 'margin' => '0'])
            </ul>
        @endif
    </li>
    @if(\$item->divider)
        <li{!! Lavary\Menu\Builder::attributes(\$item->divider) !!}></li>
    @endif
@endforeach

HTML;


            // your operations here; default example for replacing text in a file
            $textToReplace = [
                $replacement => "@foreach(\$items as \$item)
    <li @lm_attrs(\$item) class=\"m-{{ \$margin ?? '2' }} @if(\$item->hasChildren()) dropdown @endif \" @lm_endattrs>
        @if(\$link = \$item->link) <a @lm_attrs(\$link) @if(\$item->hasChildren()) class=\"{{ \$linkClass ?? 'nav-link' }}
                dropdown-toggle\" data-toggle=\"dropdown\" @else class=\"{{ \$linkClass ?? 'nav-link' }}\" @endif @lm_endattrs
                                    href=\"{!! \$item->url() !!}\">
            {!! \$item->title !!}
            @if(\$item->hasChildren()) <b class=\"caret\"></b> @endif
        </a>
        @else
            {!! \$item->title !!}
        @endif
        @if(\$item->hasChildren())
            <ul class=\"dropdown-menu\">
                @include('components.bs4-menu-items', ['items' => \$item->children(), 'linkClass' => 'dropdown-item', 'margin' => '0'])
            </ul>
        @endif
    </li>
    @if(\$item->divider)
        <li{!! Lavary\Menu\Builder::attributes(\$item->divider) !!}></li>
    @endif
@endforeach",

            ];


            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/components');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('bs4-menu-items.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}