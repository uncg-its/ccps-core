<?php

use Symfony\Component\Finder\Finder;

class AddModelEncryptionToEnvExample
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // find view partial file in app and look for these lines
            $envPath = base_path();
            $finder = new Finder();
            $finder->files()->in($envPath)->ignoreDotFiles(false)->name('.env.example');

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);

                $contents .= <<<TXT

MODEL_ENCRYPTION=true

TXT;

                file_put_contents($path, $contents);
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
