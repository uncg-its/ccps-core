<?php

use Uncgits\Ccps\Helper;
use App\CcpsCore\CronjobMeta;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateCronjobMetaMigrations
{
    /**
     * @var Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $output;

    public function __construct()
    {
        $this->output = new ConsoleOutput();
    }

    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // sniff cronjobs folder, get classnames
            $finder = new Finder();
            $finder->in(app_path('Cronjobs'))->name('*.php'); // if folder is not found, Finder will throw InvalidArgumentException, which we will catch later.

            $cronjobFiles = [];

            foreach ($finder as $file) {
                $cronjobFiles[] = str_replace('.php', '', $file->getFilename());
            }
            $cronjobFiles = collect($cronjobFiles);

            // figure out what expected migration file names will be
            $cronjobExpectedMigrationFileNames = $cronjobFiles->map(function ($item) {
                return "Add{$item}CronjobMeta";
            });

            $cronjobMap = array_combine($cronjobExpectedMigrationFileNames->toArray(), $cronjobFiles->toArray());

            // get list of cronjobs with meta already in db
            $cronjobMetas = CronjobMeta::all()->pluck('class');
            $cronjobMetaClassNames = $cronjobMetas->map(function ($item) {
                $itemBits = explode('\\', $item);
                return $itemBits[count($itemBits) - 1];
            });

            // get migrations
            $finder = new Finder();
            $finder->in(database_path('migrations'))->name('*.php');

            $migrationFilesParsed = [];

            foreach ($finder as $file) {
                $migrationFilename = str_replace('.php', '', $file->getFilename());
                $nameWithoutDate = preg_replace('/[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{6}_/', '', $migrationFilename);
                $migrationFilesParsed[] = \Str::studly($nameWithoutDate);
            }
            $migrationFilesParsed = collect($migrationFilesParsed);

            // using snake_case on the classnames, figure out which ones already have migrations
            $cronjobsWithoutMigrations = $cronjobExpectedMigrationFileNames->diff($migrationFilesParsed);

            if ($cronjobsWithoutMigrations->count() > 0) {
                // foreach class that doesn't already have a migration:
                // create it

                $migrationFilesCreated = [];

                foreach ($cronjobsWithoutMigrations as $cronjob) {
                    $migrationFilesCreated[] = Helper::makeCronjobMigrationFromStub($cronjobMap[$cronjob]);
                }

                // if it's already in the DB, add the migration to the 'migrations' table (fake it basically)
                $existingCronjobMetas = CronjobMeta::all()->pluck('class');

                $migrations = \DB::table('migrations')->select('*')->get();
                $migrationNames = $migrations->pluck('migration')->toArray();
                $nextBatch = $migrations->max('batch') + 1;

                $migrationsToFake = array_diff($migrationFilesCreated, $migrationNames);

                if (count($migrationsToFake) > 0) {
                    $insertArray = [];
                    foreach ($migrationsToFake as $migration) {
                        // check to be sure it does exist in the cronjob_meta table first! if not the migration should not be faked.
                        $classNameToCheck = preg_replace('/[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{6}_/', '', $migration);
                        $fqnToCheck = 'App\\Cronjobs\\' . $cronjobMap[\Str::studly($classNameToCheck)];
                        if ($existingCronjobMetas->contains($fqnToCheck)) {
                            $insertArray[] = [
                                'migration' => $migration,
                                'batch'     => $nextBatch
                            ];
                        }
                    }

                    \DB::table('migrations')->insert($insertArray);
                }
            }
        } catch (\InvalidArgumentException $e) {
            $this->output->writeln('<info>No Cronjobs folder found. Nothing to do.</info>');
            return [
                'success'   => true,
                'reboot'    => false,
                'exception' => null
            ];
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
