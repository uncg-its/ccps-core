<?php

use Symfony\Component\Finder\Finder;

class AddNotificationSubscriberToEventserviceprovider
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "class EventServiceProvider extends ServiceProvider
{"                                                                       => "class EventServiceProvider extends ServiceProvider
{
    protected \$subscribe = [
        NotificationSubscriber::class,
    ];
    ",
                "class EventServiceProvider extends ServiceProvider {"   => "class EventServiceProvider extends ServiceProvider
{
    protected \$subscribe = [
        NotificationSubscriber::class,
    ];
    ",
                "use Illuminate\Support\Facades\Event;" => "use Illuminate\Support\Facades\Event;
use App\Listeners\CcpsCore\NotificationSubscriber;",

            ];


            // find file in app and look for these lines
            $providerPath = base_path('app/Providers/');
            $finder = new Finder();
            $finder->files()->in($providerPath)->name('EventServiceProvider.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);

                if (strpos($contents, 'protected $subscribe') !== false) {
                    // don't add if it's already there.
                    continue;
                }

                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
