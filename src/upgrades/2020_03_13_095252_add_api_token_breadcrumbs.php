<?php

use Symfony\Component\Finder\Finder;

class AddApiTokenBreadcrumbs
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "// ADMIN"             => "// API Tokens
Breadcrumbs::register('tokens.index', function (\$breadcrumbs) {
    \$breadcrumbs->parent('account');
    \$breadcrumbs->push('API Tokens', route('account.tokens.index'));
});
Breadcrumbs::register('tokens.create', function (\$breadcrumbs) {
    \$breadcrumbs->parent('tokens.index');
    \$breadcrumbs->push('Create API Token', route('account.tokens.create'));
});
Breadcrumbs::register('tokens.edit', function (\$breadcrumbs, \$token) {
    \$breadcrumbs->parent('tokens.index');
    \$breadcrumbs->push('Edit API Token ' . \$token->id, route('account.tokens.edit', \$token));
});

// ADMIN",
            ];


            // find breadcrumbs file in app and look for these lines
            $viewPath = base_path('routes');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('breadcrumbs.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
