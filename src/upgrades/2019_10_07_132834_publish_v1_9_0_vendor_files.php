<?php



class PublishV190VendorFiles
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // new ignition config
            \Artisan::call('vendor:publish', [
                '--tag'      => 'config',
                '--provider' => 'Uncgits\\Ccps\\ServiceProvider'
            ]);
            // new flash message views
            \Artisan::call('vendor:publish', [
                '--tag'      => 'vendor-views',
                '--provider' => 'Uncgits\\Ccps\\ServiceProvider'
            ]);
            // replace JS assets
            \Artisan::call('vendor:publish', [
                '--force'    => true,
                '--tag'      => 'js',
                '--provider' => 'Uncgits\\Ccps\\ServiceProvider'
            ]);
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }


        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
