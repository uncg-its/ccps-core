<?php

class ChangeAirlockConfigToSanctum
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            \Artisan::call('vendor:publish', [
                '--force'    => true,
                '--provider' => 'Laravel\Sanctum\SanctumServiceProvider'
            ]);

            if (file_exists(config_path('airlock.php'))) {
                unlink(config_path('airlock.php'));
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
