<?php

use Symfony\Component\Finder\Finder;

class PublishTableComponents
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // identify source files
            $finder = new Finder();
            $finder->files()->in(base_path('vendor/uncgits/ccps-core/src/publish/views/core/components/'))->name('*table.blade.php');

            $targetPath = base_path('resources/views/components/');

            foreach ($finder as $file) {
                $contents = file_get_contents($file);
                file_put_contents($targetPath . $file->getRelativePathName(), $contents);
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
