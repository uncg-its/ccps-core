<?php

use Symfony\Component\Finder\Finder;

class UpdateExceptionHandlerForLoginRedirectOn403
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;"   => "use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;",

                "public function render(\$request, Exception \$exception)
    {"                                                                                => "public function render(\$request, Exception \$exception)
    {
        if (config('ccps.redirect_to_login') && \$exception instanceof HttpException) {
            if (\$exception->getStatusCode() == 403) {
                if (\$request->expectsJson()) {
                    return response()->json(['error' => 'Unauthorized.'], 403);
                }
                if (auth()->user()) {
                    return response()->view('errors.403', ['exception' => \$exception], 403);
                }
                flash('You must be logged in to access this page.')->warning();
                return redirect()->route('login');
            }
        }",

            ];


            // find exception handler in app and look for these lines
            $viewPath = base_path('app/Exceptions');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('Handler.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
