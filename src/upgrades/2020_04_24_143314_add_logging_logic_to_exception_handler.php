<?php

use Symfony\Component\Finder\Finder;

class AddLoggingLogicToExceptionHandler
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "public function report(Throwable \$throwable)
    {
        parent::report(\$throwable);
    }"   => "public function report(Throwable \$throwable)
    {
        // log uncaught query exceptions but continue processing.
        if (\$throwable instanceof QueryException) {
            event(new UncaughtQueryException(\$throwable));
        }

        // log to exception reporting channel(s)
        if (config('ccps.exceptions.should_log')) {
            \Log::channel(config('ccps.exceptions.log_channel'))->error(\$throwable->getMessage(), [
                'category'  => 'exception',
                'operation' => 'exception',
                'result'    => 'exception',
                'data'      => [
                    'exception' => \$throwable
                ]
            ]);
        }

        parent::report(\$throwable);
    }",
            ];


            // find handler in app and look for these lines
            $handler = app_path('Exceptions');
            $finder = new Finder();
            $finder->files()->in($handler)->name('Handler.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
