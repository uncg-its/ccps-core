<?php

use Symfony\Component\Finder\Finder;

class AddEncryptionAndNotificationsToCcpsConfig
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            if (isset(config('ccps')['encrpytion'])) {
                return [
                    'success'   => true,
                    'reboot'    => false,
                    'exception' => null,
                ];
            }

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels" => "    'encryption' => env('MODEL_ENCRYPTION', 'true') == true, // for models using Encryptable trait

    'notifications' => [
        'channel_verification_code_ttl_minutes' => env('NOTIFICATION_CHANNEL_VERIFICATION_CODE_TTL_MINUTES', 15),
    ],

    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels",

                "'config' => [" => "'notifications' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-bell',
            'title'                => 'Notifications Log',
            'index'                => 'notifications.log',
            'parent'               => 'admin',
            'required_permissions' => 'logs.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false
        ],

        'config' => ["
            ];


            // find file in app and look for these lines
            $configPath = base_path('config/');
            $finder = new Finder();
            $finder->files()->in($configPath)->name('ccps.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
