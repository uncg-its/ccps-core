<?php

use Symfony\Component\Finder\Finder;

class AddSkinsDefinitionsToConfig {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "];"   => "    /*
    |--------------------------------------------------------------------------
    | Bootstrap Skins
    |--------------------------------------------------------------------------
    |
    | Collection of Bootstrap 4 skins that can be applied to the base app theme
    | May be added or removed as the app developer sees fit.
    |
    | Files must be accessible from the public/ folder.
    |
    */

    'bootstrap_skins' => [
        'default' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/default.min.css',
            'display_name' => 'Default'
        ],

        'cerulean'  => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/cerulean.min.css',
            'display_name' => 'Cerulean'
        ],
        'cosmo'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/cosmo.min.css',
            'display_name' => 'Cosmo'
        ],
        'darkly'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/darkly.min.css',
            'display_name' => 'Darkly'
        ],
        'flatly'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/flatly.min.css',
            'display_name' => 'Flatly'
        ],
        'journal'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/journal.min.css',
            'display_name' => 'Journal'
        ],
        'litera'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/litera.min.css',
            'display_name' => 'Litera'
        ],
        'lumen'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/lumen.min.css',
            'display_name' => 'Lumen'
        ],
        'lux'       => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/lux.min.css',
            'display_name' => 'Lux'
        ],
        'materia'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/materia.min.css',
            'display_name' => 'Materia'
        ],
        'minty'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/minty.min.css',
            'display_name' => 'Minty'
        ],
        'pulse'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/pulse.min.css',
            'display_name' => 'Pulse'
        ],
        'sandstone' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/sandstone.min.css',
            'display_name' => 'Sandstone'
        ],
        'simplex'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/simplex.min.css',
            'display_name' => 'Simplex'
        ],
        'slate'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/slate.min.css',
            'display_name' => 'Slate'
        ],
        'solar'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/solar.min.css',
            'display_name' => 'Solar'
        ],
        'superhero' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/superhero.min.css',
            'display_name' => 'Superhero'
        ],
        'united'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/united.min.css',
            'display_name' => 'United'
        ],
        'yeti'      => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/yeti.min.css',
            'display_name' => 'Yeti'
        ],

    ],

];",
            ];


            // find view partial file in app and look for these lines
            $configPath = base_path('config/');
            $finder = new Finder();
            $finder->files()->in($configPath)->name('ccps.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}