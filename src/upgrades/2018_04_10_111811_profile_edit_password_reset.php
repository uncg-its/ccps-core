<?php

use Symfony\Component\Finder\Finder;

class ProfileEditPasswordReset
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // replace offending text with correct text in view

            $textToReplace = "<div class=\"form-group row\">
                <div class=\"col offset-2\">
                    <div class=\"form-check\">
                        <input type=\"checkbox\" value=\"1\" name=\"reset_user_password\"
                        @if(old('reset_user_password') !== null)
                            checked
                        @endif
                        >
                        <label for=\"reset_user_password\" class=\"form-check-label\">Reset your password?</label>
                        <small class=\"form-text text-muted\">Checking this box will invalidate your current password and you a password
                            reset email.</small>
                    </div>
                </div>
            </div>";


            $replacementText = "<div class=\"form-group row\">
                <div class=\"col-2\">
                    <p class=\"form-text\">Reset your password?</p>
                </div>
                <div class=\"col\">
                    <div class=\"form-check\">
                        <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"reset_user_password\"
                               @if(old('reset_user_password') !== null)
                               checked
                                @endif
                        >
                        <label class=\"form-check-label\" for=\"reset_user_password\">
                            Reset password
                        </label>
                        <small class=\"form-text text-muted\">Checking this box will invalidate your current password and you a password
                            reset email.
                        </small>
                    </div>
                </div>
            </div>";


            // find view partial file in app and look for these lines
            $viewPartialsPath = base_path('resources/views/account/profile');
            $finder = new Finder();
            $finder->files()->in($viewPartialsPath)->name('edit.blade.php');

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
