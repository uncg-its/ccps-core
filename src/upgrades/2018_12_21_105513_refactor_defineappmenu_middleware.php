<?php

use Symfony\Component\Finder\Finder;

class RefactorDefineappmenuMiddleware
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "// for logged-in users
            if (\$user = Auth::user()) {

                // read modules from config and auto-generate menu
                \$adminPrivs = [];
                foreach(config('ccps.modules') as \$key => \$module) {
                    if (\$user->can(\$module['required_permissions'])) {
                        \$adminPrivs[] = \$module;
                    }
                }

                if (count(\$adminPrivs) > 0) {
                    // Define Admin menu
                    \$menu->add('Admin', route('admin'));

                    foreach(\$adminPrivs as \$priv => \$module) {
                        if (\$module['parent'] == 'admin') {
                            \$menu->admin->add(\$module['title'], route(\$module['index']));
                        } else if (empty(\$module['parent'])) {
                            \$menu->add(\$module['title'], route(\$module['index']));
                        }
                    }
                }
            }" => "// add custom here to show before Admin menu
            // \$menu->add('MyLink', route('myroute');

            // for authenticated users, generate links and Admin menu from modules
            \$this->generateModuleLinks(\$menu);

            // add custom here to show after Admin menu
            // \$menu->add('MyLink', route('myroute');",


                "class DefineAppMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @param  \Closure  \$next
     * @return mixed
     */
    public function handle(\$request, Closure \$next)
" => "class DefineAppMenu
{
    private function generateModuleLinks(\$menu)
    {
        if (\$user = Auth::user()) {
            // read modules from config and auto-generate menu
            \$adminPrivs = [];
            foreach (config('ccps.modules') as \$key => \$module) {
                if (\$user->can(\$module['required_permissions'])) {
                    \$adminPrivs[] = \$module;
                }
            }

            if (count(\$adminPrivs) > 0) {
                // Define Admin menu
                \$menu->add('Admin', route('admin'));

                foreach (\$adminPrivs as \$priv => \$module) {
                    if (\$module['parent'] == 'admin') {
                        \$menu->admin->add(\$module['title'], route(\$module['index']));
                    } else {
                        if (empty(\$module['parent'])) {
                            \$menu->add(\$module['title'], route(\$module['index']));
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request \$request
     * @param  \Closure \$next
     *
     * @return mixed
     */
    public function handle(\$request, Closure \$next)",

                "class DefineAppMenu {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @param  \Closure  \$next
     * @return mixed
     */
    public function handle(\$request, Closure \$next)
" => "class DefineAppMenu
{
    private function generateModuleLinks(\$menu)
    {
        if (\$user = Auth::user()) {
            // read modules from config and auto-generate menu
            \$adminPrivs = [];
            foreach (config('ccps.modules') as \$key => \$module) {
                if (\$user->can(\$module['required_permissions'])) {
                    \$adminPrivs[] = \$module;
                }
            }

            if (count(\$adminPrivs) > 0) {
                // Define Admin menu
                \$menu->add('Admin', route('admin'));

                foreach (\$adminPrivs as \$priv => \$module) {
                    if (\$module['parent'] == 'admin') {
                        \$menu->admin->add(\$module['title'], route(\$module['index']));
                    } else {
                        if (empty(\$module['parent'])) {
                            \$menu->add(\$module['title'], route(\$module['index']));
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request \$request
     * @param  \Closure \$next
     *
     * @return mixed
     */
    public function handle(\$request, Closure \$next)",
            ];


            // find middleware file in app and look for these lines
            $middlewarePath = base_path('app/Http/Middleware/CcpsCore/');
            $finder = new Finder();
            $finder->files()->in($middlewarePath)->name('DefineAppMenu.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}