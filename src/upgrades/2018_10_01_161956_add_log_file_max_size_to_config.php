<?php

use Symfony\Component\Finder\Finder;

class AddLogFileMaxSizeToConfig {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            $config = config('ccps');

            if (!array_key_exists('log_viewer', $config)) {

                $findString = <<<PHP
/*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
PHP;


                $replaceString = <<<PHP
'log_viewer' => [
        'max_file_size' => env('LOG_MAX_FILE_SIZE', 52428800), // default is the default from the package
    ],

    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
PHP;

                $textToReplace = [
                    $findString => $replaceString,
                ];

                // find config file in app and look for these lines
                $configPath = base_path('config/');
                $finder = new Finder();
                $finder->files()->in($configPath)->name('ccps.php');

                $totalReplacementsMade = 0;

                foreach ($finder as $file) {
                    $path = $file->getRealPath();
                    $contents = file_get_contents($path);
                    foreach ($textToReplace as $search => $replace) {
                        $contents = str_replace($search, $replace, $contents, $replacementsMade);
                        $totalReplacementsMade += $replacementsMade;
                    }
                    if ($totalReplacementsMade > 0) {
                        file_put_contents($path, $contents);
                    }
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}