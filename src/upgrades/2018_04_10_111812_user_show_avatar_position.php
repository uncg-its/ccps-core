<?php

use Symfony\Component\Finder\Finder;

class UserShowAvatarPosition {
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade() {

        try {
            // replace offending text with correct text in view

            $textToReplace = "<div class=\"card-body\">
                <img class=\"rounded-circle d-block mx-auto img-fluid\"
                     src=\"{{ \$userToShow->avatar_url ?? asset('images/default-avatar.jpg')}}\">
                <p><strong>Name</strong>: {{ \$userToShow->fullname }}</p>
                <p><strong>Provider</strong>: <i class=\"{{ \$userToShow->provider_icon ?? 'fas fa-address-book' }} mr-1\"
                                                 title=\"Provider: {{ ucwords(\$userToShow->provider) }}\"></i> {{ ucwords(\$userToShow->provider) }}
                </p>
                @if(\$userToShow->provider != 'local')
                    <p><strong>Provider ID</strong>: {{ \$userToShow->id_from_provider }}</p>
                @endif
                <p><strong>Time Zone</strong>: {{ \$userToShow->time_zone }}</p>
            </div>";


            $replacementText = "<div class=\"card-body\">
                <div class=\"row\">
                    <div class=\"col-3\">
                        <img class=\"rounded-circle d-block mx-auto img-fluid\" src=\"{{ \$userToShow->avatar_url ?? asset('images/default-avatar.jpg')}}\">
                    </div>
                    <div class=\"col\">
                        <p><strong>Name</strong>: {{ \$userToShow->fullname }}</p>
                        <p><strong>Provider</strong>: <i class=\"{{ \$userToShow->provider_icon ?? 'fas fa-address-book' }} mr-1\"
                                                         title=\"Provider: {{ ucwords(\$userToShow->provider) }}\"></i> {{ ucwords(\$userToShow->provider) }}
                        </p>
                        @if(\$userToShow->provider != 'local')
                            <p><strong>Provider ID</strong>: {{ \$userToShow->id_from_provider }}</p>
                        @endif
                        <p><strong>Time Zone</strong>: {{ \$userToShow->time_zone }}</p>
                    </div>
                </div>
            </div>";


            // find view partial file in app and look for these lines
            $viewPartialsPath = base_path('resources/views/partials');
            $finder = new Finder();
            $finder->files()->in($viewPartialsPath)->name('user-show.blade.php');

            foreach($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'reboot' => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success' => true,
            'reboot' => false,
            'exception' => null
        ];
    }
}
