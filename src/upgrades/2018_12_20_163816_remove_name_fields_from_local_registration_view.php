<?php

use Symfony\Component\Finder\Finder;

class RemoveNameFieldsFromLocalRegistrationView
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {

        try {

            // your operations here; default example for replacing text in a file
            $textToReplace = [
                "<div class=\"form-group{{ \$errors->has('first_name') ? ' has-error' : '' }}\">
                <label for=\"first_name\" class=\"control-label\">First Name</label>
                <input id=\"first_name\" type=\"text\" class=\"form-control\" name=\"first_name\" value=\"{{ old('first_name') }}\" required autofocus>
                @if (\$errors->has('first_name'))
                    <div class=\"invalid-feedback\">
                        {{ \$errors->first('first_name') }}
                    </div>
                @endif
            </div>

            <div class=\"form-group{{ \$errors->has('last_name') ? ' has-error' : '' }}\">
                <label for=\"last_name\" class=\"control-label\">Last Name</label>
                <input id=\"last_name\" type=\"text\" class=\"form-control\" name=\"last_name\" value=\"{{ old('last_name') }}\" required autofocus>
                @if (\$errors->has('last_name'))
                    <div class=\"invalid-feedback\">
                        {{ \$errors->first('last_name') }}
                    </div>
                @endif
            </div>" => "",
            ];


            // find view partial file in app and look for these lines
            $viewPath = base_path('resources/views/auth/partials/register');
            $finder = new Finder();
            $finder->files()->in($viewPath)->name('local.blade.php');

            $totalReplacementsMade = 0;

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                foreach ($textToReplace as $search => $replace) {
                    $contents = str_replace($search, $replace, $contents, $replacementsMade);
                    $totalReplacementsMade += $replacementsMade;
                }
                if ($totalReplacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }

        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}