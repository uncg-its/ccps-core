<?php

use Symfony\Component\Finder\Finder;

class AddHttpProxyInfoToCcpsConfigFile
{
    /**
     * Run the upgrade.
     *
     * return @array - ['success' => boolean, 'reboot' => boolean, 'exception' => Exception (optional)]
     */
    public function upgrade()
    {
        try {
            // replace text in config/ccps.php

            $textToReplace = "/*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
    |--------------------------------------------------------------------------
    |
    */";


            $replacementText = "'http_proxy' => [
        'enabled' => env('HTTP_REQUEST_USE_PROXY' == 'true', false),
        'host' => env('HTTP_REQUEST_PROXY_HOST', null),
        'port' => env('HTTP_REQUEST_PROXY_PORT', null),
    ],


    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
    |--------------------------------------------------------------------------
    |
    */";


            // find migration files in app and look for these lines
            $configPath = base_path('config');
            $finder = new Finder();
            $finder->files()->in($configPath)->name('ccps.php');

            foreach ($finder as $file) {
                $path = $file->getRealPath();
                $contents = file_get_contents($path);
                $contents = str_replace($textToReplace, $replacementText, $contents, $replacementsMade);
                if ($replacementsMade > 0) {
                    file_put_contents($path, $contents);
                }
            }
        } catch (\Exception $e) {
            return [
                'success'   => false,
                'reboot'    => false,
                'exception' => $e, // return the exception to be rethrown by Upgrade script
            ];
        }

        // successful run
        return [
            'success'   => true,
            'reboot'    => false,
            'exception' => null
        ];
    }
}
