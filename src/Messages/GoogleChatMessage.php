<?php

namespace Uncgits\Ccps\Messages;

class GoogleChatMessage
{

    /**
     * The text content of the message.
     *
     * @var string
     */
    public $content;

    /**
     * The webhook URL for the Google Chat room
     *
     * @var string
     */
    public $webhookUrl;

    /**
     * Set the content of the Google Chat message.
     *
     * @param  string  $content
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }

    public function to($webhookUrl)
    {
        $this->webhookUrl = $webhookUrl;

        return $this;
    }
}
