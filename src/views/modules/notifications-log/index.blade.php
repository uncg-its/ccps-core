@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications Log'
])

@section('content')
    {!! Breadcrumbs::render('notifications-log') !!}
    <x-h1>Notifications Log</x-h1>

    <x-card title="Filters">
        <x-form-get action="{{ route('notifications-log.index') }}">
            <div class="flex gap-3 items-end">
                <x-input-text name="user" value="{{ old('user', request()->user) }}" />
                <x-input-select name="type" :options="$types" selected="{{ old('type', request()->type) }}" />
                <x-input-select name="event" :options="$events" selected="{{ old('event', request()->event) }}" />
                <div class="my-4">
                    <x-button color="green">Apply</x-button>
                    <x-button color="red" href="{{ route('notifications-log.index') }}">Reset</x-button>
                </div>
            </div>
        </x-form-get>
    </x-card>

    @if($notificationsSent->isNotEmpty())
        <x-paginated-table :collection="$notificationsSent">
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>User</x-th>
                <x-th>Type</x-th>
                <x-th>Notification</x-th>
                <x-th>Queued At</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach($notificationsSent as $notification)
                    <x-tr>
                        <x-td>{{ $notification->id }}</x-td>
                        <x-td>{{ $notification->notification_channel->user->email }}</x-td>
                        <x-td>{{ $notification->notification_channel->type }}</x-td>
                        <x-td>{{ $notification->notification_event->display_name }}</x-td>
                        <x-td>{{ $notification->queued_at }}</x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>
    @else
        <p>No notifications to show.</p>
    @endif
@endsection
