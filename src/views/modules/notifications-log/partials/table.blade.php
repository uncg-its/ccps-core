@component('components.table')
    @slot('th')
        <th>ID</th>
        <th>User</th>
        <th>Type</th>
        <th>Notification</th>
        <th>Queued At</th>
    @endslot
    @slot('tbody')
        @foreach($notificationsSent as $notification)
            <tr>
                <td>{{ $notification->id }}</td>
                <td>{{ $notification->notification_channel->user->email }}</td>
                <td>{{ $notification->notification_channel->type }}</td>
                <td>{{ $notification->notification_event->display_name }}</td>
                <td>{{ $notification->queued_at }}</td>
            </tr>
        @endforeach
    @endslot
@endcomponent