@extends('layouts.wrapper', [
    'pageTitle' => 'Mapped Role Groups - Index'
])

@section('content')
    {!! Breadcrumbs::render('mapped-role-groups.index') !!}

    <div class="flex justify-between items-center">
        <x-h1>Mapped Role Groups</x-h1>
        @permission('acl.create')
        <x-button size="md" href="{{ route('mapped-role-groups.create') }}" color="green">
            <x-fas>plus</x-fas> Add New
        </x-button>
        @endpermission
    </div>

    <p class="mb-3">Here you can define groups of users who should receive certain roles upon login. You can manage these ad-hoc, or sync them via a service like Grouper (if the application is configured to do so).</p>
    <p class="mb-3">You will first create a <strong>Mapped Role Group</strong> and then you will determine how you want it populated.</p>
    <hr class="mb-3">
    @if ($mappedRoleGroups->isNotEmpty())
    <x-paginated-table :collection="$mappedRoleGroups">
        <x-slot name="th">
            <x-th>Type</x-th>
            <x-th>Mapped Role</x-th>
            <x-th>Mapping Source</x-th>
            <x-th>Provider</x-th>
            <x-th>Last Updated</x-th>
            <x-th>Actions</x-th>
        </x-slot>
        <x-slot name="tbody">
            @foreach ($mappedRoleGroups as $group)
                <x-tr>
                    <x-td>{{ $group->type }}</x-td>
                    <x-td>{{ $group->role->name }}</x-td>
                    <x-td>{{ $group->source }}</x-td>
                    <x-td>{{ $group->provider }}</x-td>
                    <x-td>{{ $group->updated_at }}</x-td>
                    <x-td>
                        <div class="flex">
                            @permission('acl.view')
                                <x-button href="{{ route('role-mappings.index', $group) }}">
                                    <x-fas>map</x-fas> Role Mappings
                                </x-button>
                            @endpermission
                            @permission('acl.edit')
                                <x-button href="{{ route('mapped-role-groups.edit', $group) }}" color="yellow">
                                    <x-fas>edit</x-fas> Edit
                                </x-button>
                            @endpermission
                            @permission('acl.delete')
                                <x-form-delete action="{{ route('mapped-role-groups.destroy', ['mapped_role_group' => $group]) }}" confirm="Are you sure you want to delete this Mapped Role Group? This cannot be undone.">
                                    <x-button color="red">
                                        <x-fas>trash</x-fas> Delete
                                    </x-button>
                                </x-form-delete>
                            @endpermission
                        </div>
                    </x-td>
                </x-tr>
            @endforeach
        </x-slot>
    </x-paginated-table>
    @else
    <p>No Mapped Role Groups exist.</p>
    @endif
@endsection()
