@extends('layouts.wrapper', [
    'pageTitle' => 'Mapped Role Groups | Create'
])

@section('content')
    {!! Breadcrumbs::render('mapped-role-groups.create') !!}

    <x-h1>New Mapped Role Group</x-h1>

    <x-form-post action="{{ route('mapped-role-groups.store') }}">
        @include($viewPath . 'acl.forms.mapped-role-group')
    </x-form-post>
@endsection
