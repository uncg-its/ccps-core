@extends('layouts.wrapper', [
    'pageTitle' => 'Mapped Role Groups | Edit'
])

@section('content')
    {!! Breadcrumbs::render('mapped-role-groups.edit', $mappedRoleGroup) !!}

    <x-h1>Edit Mapped Role Group</x-h1>

    <x-form-patch action="{{ route('mapped-role-groups.update', $mappedRoleGroup) }}">
        @include($viewPath . 'acl.forms.mapped-role-group')
    </x-form-patch>

@endsection
