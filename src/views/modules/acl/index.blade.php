@extends('layouts.wrapper', [
    'pageTitle' => 'ACL - Index'
])

@section('content')
    {!! Breadcrumbs::render('acl') !!}

    <x-h1>ACL</x-h1>

    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('roles'),
            'fa' => 'fas fa-id-badge',
            'title' => 'Roles'
        ])
        @include('components.panel-nav', [
            'url' => route('permissions'),
            'fa' => 'fas fa-lock',
            'title' => 'Permissions'
        ])
        @include('components.panel-nav', [
            'url' => route('mapped-role-groups.index'),
            'fa' => 'fas fa-map',
            'title' => 'Mapped Role Groups'
        ])
        @include('components.panel-nav', [
            'url' => route('token-abilities.index'),
            'fa' => 'fas fa-microscope',
            'title' => 'API Token Abilities'
        ])
    </div>
@endsection
