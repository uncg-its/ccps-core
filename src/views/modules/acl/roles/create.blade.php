@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - Add New'
])

@section('content')
    {!! Breadcrumbs::render('role.create') !!}

    <x-h1>Add New Role</x-h1>

    <x-form-post action="{{ route('role.store') }}">
        @include($viewPath . 'acl.forms.role')
    </x-form-post>
@endsection
