@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - Edit'
])

@section('content')
    {!! Breadcrumbs::render('role.create') !!}

    <x-h1>Edit Role</x-h1>

    <x-form-patch action="{{ route('role.update', $role) }}">
        @include($viewPath . 'acl.forms.role')
    </x-form-patch>
@endsection
