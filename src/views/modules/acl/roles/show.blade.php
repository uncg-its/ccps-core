@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - ' . $role->name
])

@section('content')
    {!! Breadcrumbs::render('role.show', $role) !!}

    <x-h1>Role - {{ $role->name }}</x-h1>

    <div class="flex flex-wrap">
        <x-card title="Role Information" class="w-1/2">
            <p><strong>Display Name</strong>: {{ $role->display_name}}</p>
            <p><strong>Key</strong>: <code>{{ $role->name}}</code></p>
            <p><strong>Description</strong>: {{ $role->description }}</p>
        </x-card>
        <x-card title="Actions" class="w-1/2">
            <div class="flex">
                @permission('acl.edit')
                <x-button color="yellow" href="{{ route('role.edit', $role) }}">
                    <x-fas>edit</x-fas> Edit
                </x-button>
                @endpermission

                @permission('acl.delete')
                @if($role->editable)
                    <x-form-delete action="{{ route('role.destroy', $role) }}" confirm="Do you really want to delete this role? Any Mapped Role Groups for this role will also be deleted.">
                        <x-button color="red">
                            <x-fas>trash</x-fas> Delete
                        </x-button>
                    </x-form-delete>
                @endif
                @endpermission
            </div>
        </x-card>
        <x-card title="Assignment Information" class="w-1/2">
            <p><strong>Permissions for this role</strong></p>
            @if(count($role->permissions) > 0)
                <x-ul>
                    @foreach($role->permissions as $index => $permission)
                        <li><a href="{{ route('permission.show', $permission->id) }}">{{ $permission->display_name }}</a></li>
                    @endforeach
                </x-ul>
            @else
                <p>No permissions are currently assigned to this role.</p>
            @endif

            <p><strong>Users currently assigned this role</strong></p>
            @if(count($role->users) > 0)
                <x-ul>
                    @foreach($role->users as $index => $user)
                        <li><a href="{{ route('user.show', $user->id) }}">{{ $user->first_name . " " . $user->last_name}} ({{ $user->email }})</a></li>
                    @endforeach
                </x-ul>
            @else
                <p>No users currently hold this role.</p>
            @endif
        </x-card>
    </div>

@endsection
