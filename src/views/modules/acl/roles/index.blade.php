@extends('layouts.wrapper', [
    'pageTitle' => 'Roles - index'
])

@section('content')

    {!! Breadcrumbs::render('roles') !!}

    <div class="flex justify-between items-center">
        <x-h1>Roles Manager</x-h1>
        @permission('acl.create')
        <x-button size="md" href="{{ route('role.create') }}" color="green">
            <x-fas>plus</x-fas> Add New
        </x-button>
        @endpermission
    </div>

    @if($roles->isNotEmpty())
        <x-paginated-table :collection="$roles">
            <x-slot name="th">
                <x-th>Name</x-th>
                <x-th>Key</x-th>
                <x-th>Source Package</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($roles as $role)
                    <x-tr>
                        <x-td>{{ $role->display_name ?? 'N/A' }}</x-td>
                        <x-td><x-code>{{ $role->name }}</x-code></x-td>
                        <x-td>{{ $role->source_package }}</x-td>
                        <x-td>
                            <div class="flex">
                                <x-button href="{{ route('role.show', $role) }}">
                                    <x-fas>list</x-fas> Details
                                </x-button>
                                @permission('acl.edit')
                                    <x-button href="{{ route('role.edit', $role) }}" color="yellow">
                                        <x-fas>edit</x-fas> Edit
                                    </x-button>
                                @endpermission
                                @permission('acl.delete')
                                    @if ($role->editable)
                                    <x-form-delete action="{{ route('role.destroy', $role) }}" confirm="Do you really want to delete this role? Any Mapped Role Groups for this role will also be deleted.">
                                        <x-button color="red">
                                            <x-fas>trash</x-fas> Delete
                                        </x-button>
                                    </x-form-delete>
                                    @endif
                                @endpermission
                            </div>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>

        <p class="text-right"><em>Note: core roles may not be deleted.</em></p>

    @else
        <p>No roles exist.</p>
    @endif
@endsection
