@extends('layouts.wrapper', [
    'pageTitle' => 'Token Abilities | Index'
])

@section('content')
    <x-h1>Token Abilities</x-h1>
    <p>Use this page to assign abilities to current API tokens.</p>
    <hr class="my-4">
    @if ($tokens->isEmpty())
        <p>No tokens exist.</p>
    @else
        <x-paginated-table :collection="$tokens">
            <x-slot name="th">
                <x-th>Name</x-th>
                <x-th>User</x-th>
                <x-th>Current Abilities</x-th>
                <x-th>Last Used</x-th>
                <x-th>Expires</x-th>
                <x-th>Created</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                 @foreach ($tokens as $token)
                    <x-tr>
                        <x-td>{{ $token->name }}</x-td>
                        <x-td>{{ $token->tokenable->email }}</x-td>
                        <x-td>{{ implode(',', $token->abilities) }}</x-td>
                        <x-td>{{ $token->last_used_at }}</x-td>
                        <x-td>{{ $token->expires_at }}</x-td>
                        <x-td>{{ $token->created_at }}</x-td>
                        <x-td>
                            <x-button href="{{ route('token-abilities.edit', $token) }}">
                                <x-fas>edit</x-fas> Edit
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>
    @endif
@endsection
