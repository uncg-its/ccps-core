@extends('layouts.wrapper', [
    'pageTitle' => 'Token Abilities | Edit'
])

@section('content')
    <x-h1>Edit Token Abilities: {{ $token->name }}</x-h1>
    <p class="mb-3">Below, enter the abilities for the token as a comma-separated string. Example: <em>model.view,model.edit</em></p>
    <p class="mb-3"><strong>Note that * is an all-encompassing ability.</strong></p>
    <hr clas="mb-3">

    <x-form-patch action="{{ route('token-abilities.update', $token) }}">
        <x-input-text
            name="abilities"
            :value="old('abilities', implode(',', $token->abilities))"
            help="Comma-separated string"
        />
        <x-button size="md" color="green">
            <x-fas>check</x-fas> Submit
        </x-button>
    </x-form-patch>
@endsection
