@extends('layouts.wrapper', [
    'pageTitle' => 'Role Mappings | Create'
])

@section('content')
    {!! Breadcrumbs::render('mapped-role-groups.mappings.create', $mappedRoleGroup) !!}

    <x-h1>New Role Mappings for Group #{{ $mappedRoleGroup->id }}</x-h1>

    <x-card title="New Mappings">
        <x-form-post action="{{ route('role-mappings.store', $mappedRoleGroup) }}">
            <x-input-textarea
                name="emails"
                label="Email Addresses"
                required
                help="Enter email addresses here. Separate by newline, comma, or semicolon"
            />

            <x-button size="md" color="green">
                <x-fas>check</x-fas> Submit
            </x-button>
        </x-form-post>
    </x-card>
@endsection
