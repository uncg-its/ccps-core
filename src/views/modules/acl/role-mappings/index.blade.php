@extends('layouts.wrapper', [
    'pageTitle' => 'Role Mappings | Index'
])

@section('content')
    {!! Breadcrumbs::render('mapped-role-groups.mappings.index', $mappedRoleGroup) !!}

    <div class="flex justify-between items-center">
        <x-h1>Role Mappings for Group #{{ $mappedRoleGroup->id }}</x-h1>
        @if ($mappedRoleGroup->type !== 'synced')
        @permission('acl.create')
        <x-button size="md" color="green" href="{{ route('role-mappings.create', $mappedRoleGroup) }}">
            <x-fas>plus</x-fas> Add New
        </x-button>
        @endpermission
        @endif
    </div>

    <p class="pb-3"><strong>Role Mappings</strong> map an application user to an application role. When an account is created with one of the below email addresses, it will receive the role attached to this Mapped Role Group automatically. In addition, there are other triggers to "restore" these roles on a recurring basis (cronjob, login trigger, etc.) so that the roles can stay in sync.</p>
    <p class="pb-3">These are the individual mappings assigned to this group. Depending on the type of the Mapped Role Group you created, these are either manually filled in (ad-hoc) or synced from an external source (synced).</p>
    <p class="pb-3"><em>Note that you may only edit these mappings for an ad-hoc group.</em></p>
    <hr class="mb-3">
    <x-h2>Current Mappings</x-h2>
    @if ($mappedRoleGroup->role_mappings->isEmpty())
        <p>No role mappings exist for this group. @if ($mappedRoleGroup->type !== 'synced') @permission('acl.create') <x-a href="{{ route('role-mappings.create', $mappedRoleGroup) }}">Add some now</x-a> @endpermission @endif</p>
    @else
        <x-paginated-table :collection="$mappedRoleGroup->role_mappings">
            <x-slot name="th">
                <x-th>Email address</x-th>
                <x-th>Created At</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($mappedRoleGroup->role_mappings as $mapping)
                    <x-tr>
                        <x-td>{{ $mapping->email }}</x-td>
                        <x-td>{{ $mapping->created_at }}</x-td>
                        <x-td>
                            @if($mappedRoleGroup->type !== 'synced')
                            @permission('acl.delete')
                                <x-form-delete action="{{ route('role-mappings.destroy', ['role_mapping' => $mapping, 'mapped_role_group' => $mappedRoleGroup]) }}" confirm="Are you sure you want to delete this Role Mapping? This cannot be undone.">
                                    <x-button color="red">
                                        <x-fas>trash</x-fas> Delete
                                    </x-button>
                                </x-form-delete>
                            @endpermission
                            @endif
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>
    @endif
@endsection
