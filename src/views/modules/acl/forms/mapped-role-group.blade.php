<div class="flex gap-4">
    <div class="w-1/2">
        <x-card title="Basic Information">
            <x-input-select
                name="role_id"
                label="Role"
                :options="$roles"
                required
                :selected="old('role_id', $mappedRoleGroup->role_id ?? '')"
                help="The role to grant to a matched user"
            />
            <x-input-select
                name="provider"
                :options="$providers"
                required
                :selected="old('role_id', $mappedRoleGroup->provider ?? '')"
                help="The logged in user will need to come from this provider to be mapped"
            />
        </x-card>
    </div>
    <div class="w-1/2" x-data="{ groupType: '{{ $mappedRoleGroup->type ?? 'ad-hoc' }}'}">
        <x-card title="Population Information">
            <x-input-select
                name="type"
                label="Group Type"
                :options="['ad-hoc' => 'Ad Hoc', 'synced' => 'Synced']"
                required
                x-model="groupType"
                :selected="old('role_id', $mappedRoleGroup->type ?? '')"
                help="Ad Hoc groups are manually-managed; Synced groups require coordination with a third party service"
            />
            <template x-if="groupType==='synced'">
                <div>
                    <x-input-select
                        name="sync_service"
                        :options="['' => '--- Select ---', 'grouper' => 'Grouper']"
                        :selected="old('sync_service', $mappedRoleGroup->sync_service ?? '')"
                        help="The external service for the Synced group. Only required if using the <strong>Synced</strong> group type"
                    />
                    <x-input-text
                        name="sync_entity_id"
                        label="Synced Service Entity ID"
                        :value="old('sync_entity_id', $mappedRoleGroup->sync_entity_id ?? '')"
                        help="The ID (in the remote service) of the group or entity containing the user information for a Synced group"
                    />
                </div>
            </template>
        </x-card>
    </div>
</div>

<x-button size="md" color="green">
    <x-fas>check</x-fas> Submit
</x-button>
