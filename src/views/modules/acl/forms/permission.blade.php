@if(isset($permission->editable) && !$permission->editable)
    <x-alert-warning :dismissable="false"><strong>Notice</strong>: This permission's display information cannot be changed.</x-alert-warning>
@else
    <x-input-text
        name="display_name"
        placeholder="Sample - View, Sample - Edit, etc."
        value="{{ old('display_name', $permission->display_name) }}"
        required
    />

    <div class="flex gap-4" x-data="databaseKey()">
        <div class="w-1/2">
            <x-input-text
                name="database_key"
                placeholder="sample.view, sample.edit, etc."
                value="{{ old('database_key', $permission->name) }}"
                x-model.debounce="key"
                required
                help="Please use only letters, numbers, and periods (.); other characters will be stripped out."
            />
        </div>
        <div class="w-1/2">
            <x-input-text
                name="database_key_processed"
                x-model="processedKey()"
                readonly
                class="bg-gray-300"
            />
        </div>
    </div>

    <x-input-text
        name="description"
        value="{{ old('description', $permission->description) }}"
        required
    />
@endif

@if(count($roles) > 0)
    <x-input-group title="Apply to roles">
        @foreach($roles as $role)
            <x-input-checkbox
                name="roles[]"
                value="{{ $role->id }}"
                id="role-{{ $role->id }}"
                label="{{ $role->display_name }}"
                :checked="in_array($role->id, old('roles', [])) || $permission->roles->contains($role->id)"
            />
        @endforeach
    </x-input-group>
@endif

<x-button size="md" color="green" class="mt-3">
    <x-fas>check</x-fas> Submit
</x-button>

@push('scripts')
    <script>
        function databaseKey() {
            return {
                key: '{{ $permission->name ?? '' }}',
                processedKey() { return str_slug(this.key, '.') }
            }
        }
    </script>
@endpush
