@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - index'
])

@section('content')
    {!! Breadcrumbs::render('permissions') !!}

    <div class="flex justify-between items-center">
        <x-h1>Permissions Manager</x-h1>
        @permission('acl.create')
        <x-button size="md" color="green" href="{{ route('permission.create') }}">
            <x-fas>plus</x-fas> Add New
        </x-button>
        @endpermission
    </div>

    @if($permissions->isNotEmpty())
        <x-paginated-table :collection="$permissions">
            <x-slot name="th">
                <x-th>Name</x-th>
                <x-th>Key</x-th>
                <x-th>Source Package</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($permissions as $permission)
                    <x-tr>
                        <x-td>{{ $permission->display_name ?? 'N/A' }}</x-td>
                        <x-td><x-code>{{ $permission->name }}</x-code></x-td>
                        <x-td>{{ $permission->source_package }}</x-td>
                        <x-td>
                            <div class="flex">
                                <x-button href="{{ route('permission.show', $permission) }}">
                                    <x-fas>list</x-fas> Details
                                </x-button>
                                @permission('acl.edit')
                                <x-button href="{{ route('permission.edit', $permission) }}" color="yellow">
                                    <x-fas>edit</x-fas> Edit
                                </x-button>
                                @endpermission
                                @permission('acl.delete')
                                    @if ($permission->editable)
                                        <x-form-delete action="{{ route('permission.destroy', $permission) }}" confirm="Do you really want to delete this permission?">
                                            <x-button color="red">
                                                <x-fas>trash</x-fas> Delete
                                            </x-button>
                                        </x-form-delete>
                                    @endif
                                @endpermission
                            </div>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>

        <p class="text-right"><em>Note: core permissions may not be deleted.</em></p>

    @else
        <p>No permissions exist.</p>
    @endif
@endsection
