@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - Add New'
])

@section('content')
    {!! Breadcrumbs::render('permission.create') !!}

    <x-h1>Add New Permission</x-h1>

    <x-form-post action="{{ route('permission.store') }}">
        @include($viewPath . 'acl.forms.permission')
    </x-form-post>
@endsection
