@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - ' . $permission->name
])

@section('content')
    {!! Breadcrumbs::render('permission.show', $permission) !!}

    <x-h1>Role - {{ $permission->name }}</x-h1>

    <div class="flex flex-wrap">
        <x-card title="Role Information" class="w-1/2">
            <p><strong>Display Name</strong>: {{ $permission->display_name}}</p>
            <p><strong>Key</strong>: <code>{{ $permission->name}}</code></p>
            <p><strong>Description</strong>: {{ $permission->description }}</p>
        </x-card>
        <x-card title="Actions" class="w-1/2">
            <div class="flex">
                @permission('acl.edit')
                <x-button color="yellow" href="{{ route('permission.edit', $permission) }}">
                    <x-fas>edit</x-fas> Edit
                </x-button>
                @endpermission

                @permission('acl.delete')
                @if($permission->editable)
                    <x-form-delete action="{{ route('permission.destroy', $permission) }}" confirm="Do you really want to delete this permission?">
                        <x-button color="red">
                            <x-fas>trash</x-fas> Delete
                        </x-button>
                    </x-form-delete>
                @endif
                @endpermission
            </div>
        </x-card>
        <x-card title="Assignment Information" class="w-1/2">
            <p><strong>Roles for this permission</strong></p>
            @if(count($permission->roles) > 0)
                <x-ul>
                    @foreach($permission->roles as $index => $role)
                        <li><a href="{{ route('role.show', $role->id) }}">{{ $role->display_name }}</a></li>
                    @endforeach
                </x-ul>
            @else
                <p>This permission is not currently assigned to any roles</p>
            @endif
        </x-card>
    </div>

@endsection
