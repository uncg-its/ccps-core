@extends('layouts.wrapper', [
    'pageTitle' => 'Permissions - Edit'
])

@section('content')
    {!! Breadcrumbs::render('permission.edit', $permission) !!}

    <x-h1>Edit Permission - {{ $permission->display_name }}</x-h1>

    <x-form-patch action="{{ route('permission.update', $permission) }}">
        @include($viewPath . 'acl.forms.permission')
    </x-form-patch>
@endsection
