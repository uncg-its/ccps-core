@extends('layouts.wrapper', [
    'pageTitle' => 'App Email - Index'
])

@section('content')
    {!! Breadcrumbs::render('email') !!}

    <x-h1>App Email</x-h1>

    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('email.queue'),
            'fa' => 'fas fa-hourglass-half',
            'title' => 'Email Queue'
        ])

        @include('components.panel-nav', [
            'url' => route('email.sent'),
            'fa' => 'fas fa-envelope-open',
            'title' => 'Sent Email'
        ])
    </div>
@endsection
