@extends('layouts.wrapper', [
    'pageTitle' => 'Email Queue | Index'
])

@section('content')
    {!! Breadcrumbs::render('email.sent') !!}
    <div x-data="modal()">
        <x-h1>Sent Emails</x-h1>
            <div
                class="absolute top-0 left-0 w-full h-full"
                x-show.transition="isOpen()"
                x-cloak
            >
                <template x-if="isOpen()">
                    <div>
                        <div class="relative z-10 rounded-lg bg-white w-3/4 mx-auto shadow-md border border-gray-500 mt-24" x-on:click.away="close()">
                            <div class="p-4">
                                <p><strong>To</strong>: <span x-text="email.to"></span></p>
                                <p><strong>From</strong>: <span x-text="email.from"></span></p>
                                <p><strong>Cc</strong>: <span x-text="email.cc"></span></p>
                                <p><strong>Bcc</strong>: <span x-text="email.bcc"></span></p>
                                <p><strong>Reply-to</strong>: <span x-text="email.reply_to"></span></p>
                                <p><strong>Subject</strong>: <span x-text="email.subject"></span></p>
                            </div>
                            <hr>
                            <div class="p-4 flex justify-end">
                                <x-button href="#" color="blue" size="sm" x-bind:href="email.preview_url" target="_blank">
                                    <x-fas>eye</x-fas> View Email
                                </x-button>
                                <x-button color="red" size="sm" x-on:click="close()">
                                    <x-fas>times</x-fas> Close
                                </x-button>
                            </div>
                        </div>
                        <div class="absolute top-0 left-0 w-full h-full bg-gray-700 opacity-75 z-0"></div>
                    </div>
                </template>
            </div>
        @if($sentEmails->isNotEmpty())
            <x-paginated-table :collection="$sentEmails">
                <x-slot name="th">
                    <x-th>@sortablelink('id', 'ID')</x-th>
                    <x-th>@sortablelink('job_id', 'Job ID')</x-th>
                    <x-th>@sortablelink('sender')</x-th>
                    <x-th>@sortablelink('recipients')</x-th>
                    <x-th>@sortablelink('subject')</x-th>
                    <x-th>@sortablelink('created_at', 'Sent')</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach($sentEmails as $email)
                    <x-tr>
                        <x-td>{{ $email->id}}</x-td>
                        <x-td>
                            @if($email->successful_job)
                                {{ $email->successful_job->id }}
                                <a href="{{ route('queues.successful.show', ['job' => $email->successful_job->id]) }}">
                                    <i class="fas fa-info-circle"></i>
                                </a>
                            @else
                                N/A
                            @endif
                        </x-td>
                        <x-td>{{ $email->from }}</x-td>
                        <x-td>{{ $email->to }}</x-td>
                        <x-td>{{ $email->subject }}</x-td>
                        <x-td>{{ $email->created_at }}</x-td>
                        <x-td>
                            <x-button size="sm" x-on:click="open('{{ $email }}')">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
                </x-slot>
            </x-paginated-table>
        @else
            <p>Email queue is empty.</p>
        @endif
    </div>
@endsection

@push('scripts')
    <script>
        function modal() {
            return {
                email: null,
                open(email) { this.email = JSON.parse(email) },
                close() { this.email = null },
                isOpen() { return this.email !== null },
            }
        }
    </script>
@endpush
