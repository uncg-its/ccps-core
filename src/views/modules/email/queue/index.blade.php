@extends('layouts.wrapper', [
    'pageTitle' => 'Email Queue | Index'
])

@section('content')
    {!! Breadcrumbs::render('email.queue') !!}
    <x-h1>Email Queue</x-h1>

    @if($queuedEmails->isEmpty())
        <p>Email queue is empty.</p>
    @else
        <x-paginated-table :collection="$queuedEmails">
            <x-slot name="th">
                <x-th>@sortablelink('id', 'Job ID')</x-th>
                <x-th>@sortablelink('mailable', 'Mailable Class')</x-th>
                <x-th>@sortablelink('subject')</x-th>
                <x-th>@sortablelink('recipients')</x-th>
                <x-th>@sortablelink('attempts')</x-th>
                <x-th>@sortablelink('created_at', 'Created At')</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach($queuedEmails as $email)
                <x-tr>
                    <x-td>{{ $email->id}}</x-td>
                    <x-td>{{ $email->mailable_class }}</x-td>
                    <x-td>{{ $email->subject }}</x-td>
                    <x-td>{{ $email->recipients_array }}</x-td>
                    <x-td>{{ $email->attempts }}</x-td>
                    <x-td>{{ $email->created_at }}</x-td>
                    <x-td>
                        <div class="flex">
                            <x-button href="{{ route('email.show', $email) }}">
                                <x-fas>eye</x-fas> Preview
                            </x-button>
                            <x-form-delete action="{{ route('email.delete', $email) }}" confirm="Do you really want to delete this email?">
                                <x-button color="red">
                                    <x-fas>trash</x-fas> Delete
                                </x-button>
                            </x-form-delete>
                        </div>
                    </x-td>
                </x-tr>
            @endforeach
            </x-slot>
        </x-paginated-table>
    @endif
@endsection
