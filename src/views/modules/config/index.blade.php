@extends('layouts.wrapper', [
    'pageTitle' => 'Configuration - Index'
])

@section('content')
    {!! Breadcrumbs::render('config') !!}

    <x-h1>App Configuration</x-h1>

    @nopermission('config.edit')
    <x-alert color="yellow" icon="exclamation-triangle">
        <strong>Notice</strong>: you do not have sufficient permissions to edit application configuration variables.
    </x-alert>
    @endnopermission

    @include('partials.config-form')
@endsection
