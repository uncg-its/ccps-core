@extends('layouts.wrapper', [
    'pageTitle' => 'Users - Edit'
])

@section('content')
    {!! Breadcrumbs::render('user.edit', $userToEdit) !!}

    <x-h1>Edit User - {{ $userToEdit->email }}</x-h1>

    <x-form-patch action="{{ route('user.update', $userToEdit->id) }}">
        @if ($userToEdit->provider === 'local')
            <x-input-text name="email" placeholder="user@domain.com" value="{{ old('email', $userToEdit->email) }}" required />

            <x-input-group title="Password management" help="Checking this box will invalidate this user's current password and send the user a password reset email.">
                <x-input-checkbox
                    name="reset_user_password"
                    value="1"
                    id="reset_user_password"
                    label="Reset password"
                    :checked="old('reset_user_password', false)"
                />
            </x-input-group>
        @else
            <x-alert-warning :dismissable="false">This user is registered via a third party provider. Email and password cannot be modified.</x-alert-warning>
        @endif

        <x-input-select name="time_zone" label="Time Zone" required>
            @foreach($timezone_list as $value => $display)
                <x-input-option
                    value="{{ $value }}"
                    text="{{ $display }}"
                    :selected="old('time_zone', $userToEdit->time_zone)"
                />
            @endforeach
        </x-input-select>

        <x-input-group title="Role management">
            @foreach ($roles as $role)
                <x-input-checkbox
                    name="roles[]"
                    value="{{ $role->id }}"
                    id="role-{{ $role->id }}"
                    label="{{ $role->display_name }}"
                    :checked="in_array($role->id, old('roles', [])) || $userToEdit->roles->contains($role->id)"
                />
            @endforeach
        </x-input-group>

        <x-button size="md" color="green" class="mt-3">
            <x-fas>check</x-fas> Submit
        </x-button>
    </x-form-patch>

@endsection
