@extends('layouts.wrapper', [
    'pageTitle' => 'Users - Add New'
])

@section('content')
    {!! Breadcrumbs::render('user.create') !!}

    <x-h1>Add New User</x-h1>

    <x-form-post action="{{ route('user.store') }}">
        <x-input-text name="email" placeholder="user@domain.com" required />
        <x-input-group title="Role management">
            @foreach ($roles as $role)
                <x-input-checkbox name="roles[]" value="{{ $role->id }}" id="role-{{ $role->id }}" label="{{ $role->display_name }}"/>
            @endforeach
        </x-input-group>
        <x-button size="md" color="green" class="mt-3">
            <x-fas>check</x-fas> Submit
        </x-button>
    </x-form-post>
@endsection
