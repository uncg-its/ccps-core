@extends('layouts.wrapper', [
    'pageTitle' => 'Users - ' . $userToShow->email
])

@section('content')
    {!! Breadcrumbs::render('user.show', $userToShow) !!}

    @include('partials.user-show', [
        'userToShow' => $userToShow,
        'context' => 'users'
    ])


@endsection
