@extends('layouts.wrapper', [
    'pageTitle' => 'Users - index'
])

@section('content')
    {!! Breadcrumbs::render('users') !!}
    <div class="flex justify-between items-center">
        <x-h1>Users Manager</x-h1>
        @permission('users.create')
        <x-button size="md" href="{{ route('user.create') }}" color="green">
            <x-fas>plus</x-fas> Add New
        </x-button>
        @endpermission
    </div>
    @if(count($users) > 0)
        <x-paginated-table :collection="$users">
            <x-slot name="th">
                <x-th>Email</x-th>
                <x-th>Provider</x-th>
                <x-th>Last Login</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($users as $user)
                    <x-tr>
                        <x-td>{{ $user->email }}</x-td>
                        <x-td>{{ $user->provider }}</x-td>
                        <x-td>{{ $user->last_login }}</x-td>
                        <x-td>
                            <div class="flex">
                                <x-button href="{{ route('user.show', ['userToShow' => $user->id]) }}">
                                    <x-fas>list</x-fas> Details
                                </x-button>
                                @permission('users.edit')
                                <x-button href="{{ route('user.edit', ['userToEdit' => $user->id]) }}" color="yellow">
                                    <x-fas>edit</x-fas> Edit
                                </x-button>
                                @endpermission
                                @permission('users.delete')
                                <x-form-delete action="{{ route('user.destroy', $user->id) }}" confirm="Are you sure? This action cannot be undone.">
                                    <x-button color="red">
                                        <x-fas>trash</x-fas> Delete
                                    </x-button>
                                </x-form-delete>
                                @endpermission
                            </div>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-paginated-table>
    @else
        <p>No users exist.</p>
    @endif
@endsection
