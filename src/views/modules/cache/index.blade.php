@extends('layouts.wrapper', [
    'pageTitle' => 'Cache'
])

@section('content')
    {!! Breadcrumbs::render('cache') !!}
    <x-h1>Cache Information</x-h1>

    <x-h4>Current Cache Contents</x-h4>

    <div class="flex justify-center gap-4">
        <x-card title="Active cache files">
            <p class="text-center text-xl">{{ count($cache['notExpired']) }}</p>
            <p class="text-center text-sm"><em>(3 files minimum)</em></p>
        </x-card>
        <x-card title="Expired cache files">
            <p class="text-center text-xl">{{ count($cache['expired']) }}</p>
        </x-card>
    </div>

    <x-h4>Clear Application Cache</x-h4>
    <p class="mb-4">Use these buttons to clear the application cache.</p>

    <x-alert-warning dismissable="false">
        <strong>Notice</strong>: This action cannot be undone. Clearing the cache may slow down performance of the application temporarily, but can fix issues related to permissions and other cached data.
    </x-alert-warning>

    <x-button size="md" color="red" href="{{ route('cache.clear') }}">
        <x-fas>trash</x-fas> Clear Cache
    </x-button>
    <x-button size="md" color="yellow" href="{{ route('cache.purge') }}">
        <x-fas>clock</x-fas>Purge expired
    </x-button>
@endsection
