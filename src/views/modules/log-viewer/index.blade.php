@extends('layouts.wrapper', [
    'pageTitle' => 'Log Viewer'
])

@section('content')
    <x-h1>Application Logs</x-h1>
    <livewire:log-viewer />
@endsection
