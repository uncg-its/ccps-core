@extends('layouts.wrapper', [
    'pageTitle' => 'Cron Job | Edit'
])

@section('content')
    {!! Breadcrumbs::render('cronjob.edit', $cronjob) !!}

    <div x-data="{showCronCheatsheet: false}">
        <x-h1>Edit Cron Job: {{ $cronjob->getDisplayName() }}</x-h1>

        <x-form-post action="{{ route('cronjob.update', $cronjob->getMeta()->id) }}">
            <x-input-group help="This will override what is set in the Cron Job class object">
                <x-input-text
                    name="display_name"
                    label="Display Name (override)"
                    placeholder="Display Name"
                    value="{{ old('display_name', $cronjob->getMeta()->display_name) }}"
                    autofocus
                />
            </x-input-group>
            <x-input-group>
                <x-input-textarea name="description" value="{{ old('description', $cronjob->getMeta()->description) }}" />
            </x-input-group>
            <div class="flex w-full gap-3 items-center">
                <x-input-group help="This will override what is set in the Cron Job class object." class="w-full">
                    <x-input-text
                        name="schedule"
                        placeholder="(example: * * * * *)"
                        value="{{ old('schedule', $cronjob->getMeta()->schedule) }}"
                    />
                </x-input-group>
                <x-button color="yellow" x-on:click.prevent="showCronCheatsheet=!showCronCheatsheet">
                    <x-fas>bars</x-fas> Show/Hide Cheatsheet
                </x-button>
            </div>
            <div
                x-cloak
                x-show="showCronCheatsheet"
                x-transition:enter="transition ease-out duration-300"
                x-transition:enter-start="opacity-0 transform scale-90"
                x-transition:enter-end="opacity-100 transform scale-100"
                x-transition:leave="transition ease-in duration-300"
                x-transition:leave-start="opacity-100 transform scale-100"
                x-transition:leave-end="opacity-0 transform scale-90"
            >
                <x-card title="Cron Scheduling Cheatsheet">
                    <x-ul>
                        <li>* * * * * --> minute hour day-of-month month day-of-week</li>
                        <li>Separate each item by a single space</li>
                        <li>Use <strong>no whitespace within expressions</strong> (between commas, dashes,
                            etc.)
                        </li>
                        <li>Use 24-hour time for hour (0-23)</li>
                        <li>* means 'every'</li>
                        <li>*/15 means 'every 15th'</li>
                        <li>1 means 'the first'</li>
                        <li>1-3 means 'the first through the third'</li>
                        <li>1,3,5 means 'the first, third, and fifth'</li>
                    </x-ul>
                </x-card>
            </div>
            <x-input-group help="Separated by comma, no spaces">
                <x-input-text
                    name="tags"
                    placeholder="tag1,tag2,tag3"
                    value="{{ old('tags', $cronjob->getMeta()->tags) }}"
                />
            </x-input-group>
            <x-button color="green" size="md">
                <x-fas>check</x-fas> Submit
            </x-button>
        </x-form-post>
    </div>
@endsection
