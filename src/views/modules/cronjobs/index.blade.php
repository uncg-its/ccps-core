@extends('layouts.wrapper', [
    'pageTitle' => 'Cron Jobs | Index'
])

@section('content')
    <livewire:cronjob-list />
@endsection
