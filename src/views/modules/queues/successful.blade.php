@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Successful Jobs'
])

@section('content')
    @include($viewPath . 'queues.partials.jobs-table', [
        'queueType' => 'successful'
    ])
@endsection
