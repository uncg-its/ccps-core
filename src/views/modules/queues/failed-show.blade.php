@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Failed Job: ' . $job->id
])

@section('content')
    @include($viewPath . 'queues.partials.show-job', [
        'queueType' => 'failed'
    ])
@endsection
