{!! Breadcrumbs::render('queues.' . $queueType) !!}

<div class="flex justify-between mb-3">
    <div>
        <x-h1>{{ ucwords($queueType) }} Jobs</x-h1>
        <x-h3>Queues: {{ implode(',', $queues) }}</x-h3>
    </div>
    <div class="max-w-1/2 flex">
        <x-form-get action="{{ route('queues.' . $queueType) }}">
            <x-input-text name="queues" value="{{ $queues != ['all'] ? implode(',', $queues) : '' }}" help="Comma-separated"/>
            <x-button color="green">Filter</x-button>
            <x-button color="red" href="{{ route('queues.' . $queueType) }}">Reset</x-button>
        </x-form-get>
    </div>
</div>

@if($jobs->isEmpty())
    <p>Selected queues are empty.</p>
@else
    <x-paginated-table :collection="$jobs">
        <x-slot name="th">
            <x-th scope="col">@sortablelink('id', 'ID')</x-th>
            <x-th scope="col">@sortablelink('queue')</x-th>
            <x-th scope="col">@sortablelink('class')</x-th>
            @if($queueType !== 'failed')
                <x-th scope="col">@sortablelink('attempts')</x-th>
                <x-th scope="col">@sortablelink('reserved_at', 'Reserved At')</x-th>
                <x-th scope="col">@sortablelink('available_at', 'Available At')</x-th>
                <x-th scope="col">@sortablelink('created_at', 'Created At')</x-th>
            @endif
            @if($queueType === 'successful')
                <x-th>@sortablelink('completed_at', 'Completed At')</x-th>
                <x-th>@sortablelink('run_method', 'Run Method')</x-th>
            @elseif($queueType === 'failed')
                <x-th>@sortablelink('failed_at', 'Failed At')</x-th>
                <x-th>Message</x-th>
            @endif
            <x-th>Actions</x-th>
        </x-slot>
        <x-slot name="tbody">
        @foreach($jobs as $job)
            <x-tr>
                <x-td>{{ $job->id }}</x-td>
                <x-td>{{ $job->queue }}</x-td>
                <x-td>{{ $job->class }}</x-td>
                @if ($queueType !== 'failed')
                    <x-td>{{ $job->attempts }}</x-td>
                    <x-td>{{ $job->reserved_at }}</x-td>
                    <x-td>{{ $job->available_at }}</x-td>
                    <x-td>{{ $job->created_at }}</x-td>
                @endif
                @if($queueType === 'successful')
                    <x-td>{{ $job->completed_at }}</x-td>
                    <x-td>{!! $job->run_method_with_icon !!}</x-td>
                @elseif($queueType === 'failed')
                    <x-td>{{ $job->failed_at }}</x-td>
                    <x-td>{{ $job->exception_message }}</x-td>
                @endif
                <x-td>
                    <div class="flex items-center">
                        <x-button href="{{ route('queues.' . $queueType . '.show', ['job' => $job->id]) }}">
                            <x-fas>list</x-fas> Details
                        </x-button>
                        @if($queueType === 'pending')
                            <x-button href="{{ route('queues.run', ['job' => $job->id]) }}" color="green">
                                <x-fas>play-circle</x-fas> Run
                            </x-button>

                            <x-form-delete action="{{ route('queues.delete', ['job' => $job->id]) }}" confirm="Do you really want to delete this job?">
                                <x-button color="red">
                                    <x-fas>trash</x-fas> Delete
                                </x-button>
                            </x-form-delete>
                        @elseif($queueType === 'failed')
                            <x-button color="green" href="{{ route('queues.requeuefailed', ['job' => $job->id]) }}">
                                <x-fas>sync</x-fas> Re-Queue
                            </x-button>
                            <x-form-delete action="{{ route('queues.forgetfailed', ['job' => $job->id]) }}" confirm="Do you really want to forget this failed job?">
                                <x-button color="red">
                                    <x-fas>trash</x-fas> Forget
                                </x-button>
                            </x-form-delete>
                        @endif
                    </div>
                </x-td>
            </x-tr>
        @endforeach
        </x-slot>
    </x-paginated-table>
@endif
