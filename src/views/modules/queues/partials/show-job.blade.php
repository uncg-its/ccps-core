{!! Breadcrumbs::render('queues.' . $queueType . '.show', $job) !!}

<div class="flex justify-between mb-3 items-center">
    <x-h1>{{ ucwords($queueType) }} Job: {{ $job->id }}</x-h1>
    @if($queueType === 'pending')
        <div>
            <x-form-delete action="{{ route('queues.delete', ['job' => $job->id]) }}" confirm="Do you really want to delete this job?">
                <x-button size="md" color="red">
                    <x-fas>trash</x-fas> Delete
                </x-button>
            </x-form-delete>
        </div>
    @endif
</div>

<div class="flex gap-4">
    <div class="w-1/2">
        <x-card title="Queue Data">
            <p><strong>Queue Name</strong>: {{ $job->queue }}</p>
            <p><strong>Attempts</strong>: {{ $job->attempts }}</p>
            <p><strong>Created At</strong>: {{ $job->created_at }}</p>
            @if ($queueType === 'successful')
                <p><strong>Completed At</strong>: {{ $job->completed_at }}</p>
            @endif
        </x-card>
    </div>
    <div class="w-1/2">
        <x-card title="Release Data">
            <p><strong>Reserved At</strong>: {{ $job->reserved_at ?? 'N/A'}}</p>
            <p><strong>Available At</strong>: {{ $job->available_at }}</p>
        </x-card>
    </div>
</div>

@if ($queueType === 'failed')
<div class="flex" x-data="{showRaw: false}">
    <div class="w-full">
        <x-card title="Failure Data">
            <p><strong>Exception message</strong>: {{ $job->exception_message }}</p>
            <x-button color="blue" x-on:click.prevent="showRaw=!showRaw"><x-fas>eye</x-fas> Show/Hide Raw Exception</x-button>
            <div class="bg-gray-100 p-3 rounded-md" x-show="showRaw" x-cloak>
                <pre><small>{{ $job->exception }}</small></pre>
            </div>
        </x-card>
    </div>
</div>
@endif

@include($viewPath . 'queues.partials.payload-table', ['job' => $job])
