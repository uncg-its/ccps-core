<x-card title="Payload data">
    <p><strong>Display Name</strong>: {{ $job->decoded_payload->displayName }}</p>
    <p><strong>Job</strong>: <x-code>{{ $job->decoded_payload->job }}</x-code></p>
    <p><strong>Max Tries</strong>: {{ $job->decoded_payload->maxTries ?? 'N/A'}}</p>
    <p><strong>Timeout</strong>: {{ $job->decoded_payload->timeout ?? 'N/A'}}</p>
    <p><strong>Timeout At</strong>: {{ $job->decoded_payload->timeoutAt ?? 'N/A'}}</p>
    <p><strong>Command Name</strong>: <x-code>{{ $job->decoded_payload->data->commandName }}</x-code></p>
    <div x-data="{show: false}">
        <p>
            <strong>Raw Command</strong>:
            <span @click="show=!show">
                <x-button>
                    <x-fas>search</x-fas> Show / Hide
                </x-button>
            </span>
            <div x-show="show" class="p-3 bg-gray-100" x-cloak>
                <pre>{{ var_dump($job->decoded_payload->data->command) }}</pre>
            </div>
        </p>
    </div>
</x-card>
