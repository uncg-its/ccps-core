@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Successful Job: ' . $job->id
])

@section('content')
    @include($viewPath . 'queues.partials.show-job', [
        'queueType' => 'successful'
    ])
@endsection
