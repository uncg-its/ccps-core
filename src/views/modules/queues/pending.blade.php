@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Pending Jobs'
])

@section('content')
    @include($viewPath . 'queues.partials.jobs-table', [
        'queueType' => 'pending'
    ])
@endsection
