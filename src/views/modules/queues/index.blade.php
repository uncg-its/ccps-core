@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Index'
])

@section('content')
    {!! Breadcrumbs::render('queues') !!}

    <x-h1>Application Queues</x-h1>

    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('queues.pending'),
            'fa' => 'fas fa-hourglass-half',
            'title' => 'Pending Jobs'
        ])

        @include('components.panel-nav', [
            'url' => route('queues.successful'),
            'fa' => 'fas fa-check',
            'title' => 'Successful Jobs'
        ])

        @include('components.panel-nav', [
            'url' => route('queues.failed'),
            'fa' => 'fas fa-exclamation-triangle',
            'title' => 'Failed Jobs'
        ])
    </div>
@endsection
