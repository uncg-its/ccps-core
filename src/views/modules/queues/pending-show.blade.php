@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Pending Job: ' . $job->id
])

@section('content')
    @include($viewPath . 'queues.partials.show-job', [
        'queueType' => 'pending'
    ])
@endsection
