@extends('layouts.wrapper', [
    'pageTitle' => 'Queues | Failed Jobs'
])

@section('content')
    @include($viewPath . 'queues.partials.jobs-table', [
        'queueType' => 'failed'
    ])
@endsection
