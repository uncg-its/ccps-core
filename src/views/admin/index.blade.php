@extends('layouts.wrapper', [
    'pageTitle' => 'Admin Home'
])

@section('content')
    {!! Breadcrumbs::render('admin') !!}

    <x-h1>Admin</x-h1>

    <div class="flex">
        @foreach($modules as $key => $module)
            @if($module['parent'] == 'admin')
                @permission($module['required_permissions'])
                    @include('components.panel-nav', [
                        'url' => route($module['index']),
                        'fa' => $module['icon'],
                        'title' => $module['title']
                    ])
                @endpermission
            @endif
        @endforeach
    </div>
@endsection
