<div class="p-4">
    <x-card title="Google Authentication" class="mx-auto text-center">
        <x-button size="sm" color="green" href="{{ route('oauth', ['provider' => 'google']) }}">
            <x-fa>fab fa-google</x-fa> Log in with Google
        </x-button>
    </x-card>
</div>
