<div class="p-4">
    <x-card title="Azure Authentication" class="mx-auto text-center">
        <x-button size="md" color="green" href="{{ route('oauth', ['provider' => 'azure']) }}">
            <x-fa>fab fa-windows</x-fa> Log in with Azure
        </x-button>
    </x-card>
</div>
