<div class="p-4">
    <x-card title="Local Auth" class="mx-auto">
        <x-form-post action="{{ route('register') }}">
            <x-input-text name="email" value="{{ old('email') }}" />
            <x-input-password name="password" />
            <x-input-password name="password_confirmation" label="Confirm Password" />
            <div class="text-center">
                <x-button size="md" color="green">
                    <x-fas>sign-in-alt</x-fas> Register
                </x-button>
            </div>
        </x-form-post>
    </x-card>
</div>
