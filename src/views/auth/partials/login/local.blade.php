<div class="p-4">
    <x-card title="Local Auth" class="mx-auto">
        <x-form-post action="{{ route('login') }}">
            <x-input-text name="email" value="{{ old('email') }}" />

            <x-input-password name="password" />

            <x-input-checkbox name="remember" label="Remember me" value="yes" />

            <div class="text-center">
                <x-button size="md" color="green">
                    <x-fas>sign-in-alt</x-fas> Log In
                </x-button>
            </div>
        </x-form-post>
        <p class="mt-4 text-center">
            <x-a href="{{ route('password.request') }}">Forgot your password?</x-a>
        </p>
    </x-card>
</div>
