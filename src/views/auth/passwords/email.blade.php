@extends('layouts.wrapper', [
    'pageTitle' => 'Email'
])

@section('content')

<x-h1>Request Password Reset</x-h1>

<div class="w-1/2 mx-auto">
    <div class="p-4">
        <x-card title="Verify Email" class="mx-auto">
            <x-form-post action="{{ route('password.email') }}">
                <x-input-text name="email" value="{{ old('email') }}" autofocus />
                <div class="text-center">
                    <x-button size="md" color="green">
                        <x-fas>check</x-fas> Request Password Reset
                    </x-button>
                </div>
            </x-form-post>
        </x-card>
    </div>
</div>

@endsection
