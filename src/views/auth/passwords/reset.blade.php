@extends('layouts.wrapper', [
    'pageTitle' => 'Reset'
])

@section('content')

<x-h1>Reset Password</x-h1>

<div class="w-1/2 mx-auto">
    <div class="p-4">
        <x-card title="Set New Password" class="mx-auto">
            <x-form-post action="{{ route('password.request') }}">
                <x-input-text name="email" value="{{ $email ?? old('email') }}" autofocus />
                <x-input-hidden name="token" value="{{ $token }}" />
                <x-input-password name="password" />
                <x-input-password name="password_confirmation" label="Confirm Password" />
                <div class="text-center">
                    <x-button size="md" color="green">
                        <x-fas>check</x-fas> Submit
                    </x-button>
                </div>
            </x-form-post>
        </x-card>
    </div>
</div>
@endsection
