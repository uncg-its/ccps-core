@extends('layouts.wrapper', [
    'pageTitle' => "Login"
])

@section('content')
    <div class="w-1/2 mx-auto" x-data="tabs()">
        <ul class="flex border-b w-full" role="tablist">
            <template x-for="tab in tabs" x-key="tab.id">
                <li class="-mb-px mr-1">
                    <a class="bg-white inline-block py-2 px-4 font-semibold" :class="{'border-l border-t border-r rounded-t text-blue-700': active==tab.id}" @click="active=tab.id" href="#" x-text="tab.name"></a>
                </li>
            </template>
        </ul>
        <div class="w-full">
            @foreach ($appLoginMethods as $method)
                <div x-show="active=={{ $method['id'] }}" x-cloak>
                    @include($method['partial'])
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function tabs() {
            return {
                active: 0,
                tabs: {!! json_encode($appLoginMethods) !!}
            }
        }
    </script>
@endpush
