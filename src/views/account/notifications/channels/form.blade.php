<div x-data="{carrier: '{{ old('carrier', 'email') }}'}">
    <x-input-select name="type" :options="$types" x-model="carrier" />
    <x-input-text name="name" label="Nickname" help="Name to help you recognize this channel" required />
    <x-input-text name="key" label="Value" help="The email / phone / webhook URL" required />
    <template x-if="carrier==='sms'">
        <x-input-select name="carrier" label="SMS Carrier" :options="$carriers" />
    </template>

    <x-button size="md" color="green">
        <x-fas>check</x-fas> Submit
    </x-button>
</div>
