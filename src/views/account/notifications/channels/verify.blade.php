@extends('layouts.wrapper', [
    'pageTitle' => 'Notification Channel | Verify'
])

@section('content')
    <x-h1>Verify Notification Channel</x-h1>

    <div class="flex justify-center">
        <x-card title="Enter code">
            <p>Please enter the verification code that you received for your channel: <strong>{{ $channel->name }}</strong></p>
            <x-form-get action="{{ route('account.notifications.channels.verify', $channel) }}">
                <x-input-text name="code" required />
                <x-button size="md" color="green">
                    <x-fas>shield-alt</x-fas> Verify
                </x-button>
            </x-form-get>
        </x-card>
    </div>
@endsection
