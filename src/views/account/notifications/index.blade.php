@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications | Index'
])

@section('content')
    <livewire:notification-channels
        :notificationChannels="$notificationChannels"
        :notificationEvents="$notificationEvents"
        :types="$types"
        :carriers="$carriers"
    />
@endsection
