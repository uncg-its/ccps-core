@extends('layouts.wrapper', [
    'pageTitle' => 'Account Settings'
])

@section('content')
    {!! Breadcrumbs::render('account.settings', $user) !!}

    <x-h1>My Account Settings</x-h1>

    {{-- <div class="flex">
        <div class="w-full">
            <x-form-patch action="{{ route('account.settings.update') }}">
                <x-input-select name="skin" :options="$skins" />
                <x-button size="md" color="green">
                    <x-fas>check</x-fas> Submit
                </x-button>
            </x-form-patch>
        </div>
    </div> --}}

@endsection
