@extends('layouts.wrapper', [
    'pageTitle' => 'My Account'
])

@section('content')
    {!! Breadcrumbs::render('account') !!}
    <div class="flex justify-between items-center">
        <x-h1>My Account</x-h1>
        <x-form-post action="{{ route('logout') }}">
            <x-button size="md" color="red">
                <x-fas>power-off</x-fas> Log Out
            </x-button>
        </x-form-post>
    </div>

    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('profile.show'),
            'fa' => 'fas fa-user',
            'title' => 'My Profile'
        ])
        @include('components.panel-nav', [
            'url' => route('account.settings'),
            'fa' => 'fas fa-cog',
            'title' => 'Account Settings'
        ])
        @include('components.panel-nav', [
            'url' => route('account.notifications.index'),
            'fa' => 'fas fa-bell',
            'title' => 'Notifications'
        ])
        @permission('tokens.*')
            @include('components.panel-nav', [
                'url' => route('account.tokens.index'),
                'fa' => 'fas fa-code',
                'title' => 'API Keys'
            ])
        @endpermission
    </div>
@endsection
