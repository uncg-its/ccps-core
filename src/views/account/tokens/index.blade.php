@extends('layouts.wrapper', [
    'pageTitle' => 'Account | API Tokens'
])

@section('content')
    <livewire:api-tokens :tokens="$tokens" :showAll="false" />
@endsection
