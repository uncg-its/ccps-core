@extends('layouts.wrapper', [
    'pageTitle' => 'Edit Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile.edit') !!}

    <x-h1>Edit Profile</x-h1>

    <x-form-patch action="{{ route('user.update', $userToEdit->id) }}">
        @if ($userToEdit->provider === 'local')
            <x-input-text name="email" placeholder="user@domain.com" value="{{ old('email', $userToEdit->email) }}" required />

            <x-input-group title="Password management" help="Checking this box will invalidate your current password and send you a password reset email.">
                <x-input-checkbox
                    name="reset_user_password"
                    value="1"
                    id="reset_user_password"
                    label="Reset password"
                    :checked="old('reset_user_password', false)"
                />
            </x-input-group>
        @else
            <x-alert-warning :dismissable="false">You are registered via a third party provider. Email and password cannot be modified.</x-alert-warning>
        @endif

        <x-input-select name="time_zone" label="Time Zone" required>
            @foreach($timezone_list as $value => $display)
                <x-input-option
                    value="{{ $value }}"
                    text="{{ $display }}"
                    :selected="old('time_zone', $userToEdit->time_zone)"
                />
            @endforeach
        </x-input-select>

        <x-button size="md" color="green" class="mt-3">
            <x-fas>check</x-fas> Submit
        </x-button>
    </x-form-patch>

@endsection
