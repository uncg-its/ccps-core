@if (count($breadcrumbs))
    <div class="flex bg-gray-300 py-3 px-5 rounded text-sm mb-3">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <p class="mr-2">
                    <x-a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</x-a>
                </p>
                <span class="mr-2">
                    <x-fas class="text-xs">chevron-right</x-fas>
                </span>
            @else
                <p class="text-gray-600">{{ $breadcrumb->title }}</p>
            @endif
        @endforeach
    </div>
@endif
