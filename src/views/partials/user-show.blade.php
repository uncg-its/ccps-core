@if($context == 'profile')
    <x-h1>My Profile</x-h1>
@else
    <x-h1>User - {{ $userToShow->email }}</x-h1>
@endif

<div class="flex gap-4">
    <div class="w-1/2">
        <x-card title="User Information">
            <x-p><strong>Email</strong>: {{ $userToShow->email }}</x-p>
            <x-p><strong>Provider</strong>: <x-fas>{{ $userToShow->provider_icon ?? 'fas fa-address-book' }}</x-fas> {{ ucwords($userToShow->provider) }}</x-p>
            @if($userToShow->provider != 'local')
                <x-p><strong>Provider ID</strong>: {{ $userToShow->id_from_provider }}</x-p>
            @endif
            <x-p><strong>Time Zone</strong>: {{ $userToShow->time_zone }}</x-p>
        </x-card>
    </div>
    <div class="w-1/2">
        <x-card title="Actions">
            @if($context == 'profile')
                <x-button color="yellow" href="{{ route('profile.edit') }}">
                    <x-fas>edit</x-fas> Edit
                </x-button>
            @else
                <div class="flex">
                    @permission('users.edit')
                    <x-button color="yellow" href="{{ route('user.edit', ['userToEdit' => $userToShow->id]) }}">
                        <x-fas>edit</x-fas> Edit
                    </x-button>
                    @endpermission

                    @permission('users.delete')
                    <x-form-delete action="{{ route('user.destroy', ['user' => $userToShow->id]) }}" confirm="Do you really want to delete this user?">
                        <x-button color="red">
                            <x-fas>trash</x-fas> Delete
                        </x-button>
                    </x-form-delete>
                    @endpermission
                </div>
            @endif
        </x-card>
    </div>
</div>

<div class="flex gap-4">
    <div class="w-1/2">
        <x-card title="Access Information">
            <x-p><strong>Last login</strong>: {{ $userToShow->last_login }}</x-p>
            <x-p><strong>Role(s) granted</strong>:
                @if(count($userToShow->roles) > 0)
                    <x-ul>
                        @foreach ($userToShow->roles as $thisRole)
                            <li>
                                <x-a href="{{ route('role.show', ['role' =>  $thisRole->id]) }}">{{ $thisRole->display_name }}</x-a>
                            </li>
                        @endforeach
                    </x-ul>
                @else
                    None
                @endif
            </x-p>
        </x-card>
    </div>
    <div class="w-1/2">
        <x-card title="API Tokens">
            @if ($userToShow->tokens->isNotEmpty())
                <x-ul>
                    @foreach ($userToShow->tokens as $token)
                        <li>{{ $token->name }} (expires: {{ is_null($token->expires_at) ? 'never' : $token->expires_at }})</li>
                    @endforeach
                </x-ul>
            @else
                <x-p>No tokens to show</x-p>
            @endif
            <x-button href="{{ route('account.tokens.index', ['show' => 'all']) }}">
                <x-fas>code</x-fas> Manage Tokens
            </x-button>
        </x-card>
    </div>
</div>
