<div x-data="{showCreateForm: false}" @channel-created.window="showCreateForm=false">
    {!! Breadcrumbs::render('notifications.index') !!}

    <div class="flex justify-between items-center">
        <x-h1>My Notification Channels</x-h1>
        <x-button size="md" color="green" x-on:click="showCreateForm=true">
            <x-fas>plus</x-fas> Add New
        </x-button>
    </div>

    @if($notificationChannels->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>Name</x-th>
                <x-th>Type</x-th>
                <x-th>Value</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
            @foreach($notificationChannels as $channel)
                <livewire:notification-channel :channel="$channel" :key="$channel->id" />
            @endforeach
            </x-slot>
        </x-table>
    @else
        <p>No notification channels configured.</p>
    @endif

    <x-hr />

    <div
        x-cloak
        x-show="showCreateForm"
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0 transform scale-90"
        x-transition:enter-end="opacity-100 transform scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 transform scale-100"
        x-transition:leave-end="opacity-0 transform scale-90"
    >
        <x-h2>Create New Channel</x-h2>
        <div x-data="{type: '{{ old('type', 'email') }}'}">
            <x-input-select name="type" :options="$types" x-model="type" wire:model="channelType" />
            <x-input-text name="name" label="Nickname" help="Name to help you recognize this channel" required wire:model="channelName" />
            <x-input-text name="key" label="Value" help="The email / phone / webhook URL" required wire:model="channelKey" />
            <div x-show="type==='sms'">
                <x-input-select name="carrier" label="SMS Carrier" :options="$carriers" wire:model="channelCarrier" />
            </div>

            <x-button size="md" color="green" wire:click="createChannel" wire:loading.attr="disabled" wire:target="createChannel">
                <span wire:loading.remove wire:target="createChannel"><x-fas>check</x-fas> Submit</span>
                <span wire:loading wire:target="createChannel"><x-fas>spinner fa-spin</x-fas> Creating...</span>
            </x-button>
        </div>

        <x-hr />
    </div>

    <x-h2>Notification Settings</x-h2>

    <livewire:notification-settings :channels="$notificationChannels" :events="$notificationEvents" />

</div>
