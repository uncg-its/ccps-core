<x-tr>
    <x-td>{{ $token->tokenable->email }}</x-td>
    <x-td>{{ $token->name }}</x-td>
    <x-td>{{ implode(',', $token->abilities) }}</x-td>
    <x-td>{{ $token->last_used }}</x-td>
    <x-td>{{ $token->created_at }}</x-td>
    <x-td>{{ $token->expires_at }}</x-td>
    <x-td>
        <div class="flex">
            <div>
                @permission('tokens.revoke')
                    <x-button
                        color="red"
                        onclick="return confirm('Are you sure? This cannot be undone.') || event.stopImmediatePropagation()"
                        wire:click="revokeToken"
                        wire:loading.attr="disabled"
                        wire:target="revokeToken"
                    >
                        <span wire:loading.remove wire:target="revokeToken"><x-fas>trash</x-fas> Revoke</span>
                        <span wire:loading wire:target="revokeToken"><x-fas>spinner fa-spin</x-fas> Revoking...</span>
                    </x-button>
                @endpermission
            </div>
        </div>
    </x-td>
</x-tr>
