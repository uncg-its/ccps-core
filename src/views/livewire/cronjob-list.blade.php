<div>
    {!! Breadcrumbs::render('cronjobs') !!}

    <div class="flex justify-between mb-3">
        <x-h1>Cron Jobs</x-h1>
        <div class="max-w-1/2">
            <div class="flex items-end">
                <x-input-text name="search" label="Search (name or tags)" wire:model.debounce.500ms="search" wire:loading.attr="disabled" wire:target="search" />
                <div class="mb-4 ml-1">
                    @if (!empty($search))
                        <x-button color="red" wire:click="clearSearch">
                            Clear
                        </x-button>
                    @endif
                </div>
            </div>
            <div>
                @if ($showMinChars)
                    <span class="text-red-500 text-sm" wire:loading.remove wire:target="search">
                        Enter at least 3 characters
                    </span>
                @endif
            </div>
            <span class="text-gray-700 italic text-sm mb-3" wire:loading wire:target="search">
                <x-fas>spinner fa-spin</x-fas> searching...
            </span>
        </div>
    </div>

    <div>
        @if(!empty($lockFiles))
            <x-alert-warning>Note: One or more lock files currently exist. The locked job(s) may be running, or may have terminated abnormally.</x-alert-warning>
        @endif
    </div>

    <div>
        @if (!empty($cronjobs))
        <x-table>
            <x-slot name="th">
                <x-th>Status</x-th>
                <x-th>Name & Tags</x-th>
                <x-th>Description</x-th>
                <x-th>Schedule</x-th>
                <x-th>Last Run</x-th>

                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($cronjobs as $id)
                    <livewire:cronjob-row :cronjobId="$id" :key="'row_' . $id"/>
                @endforeach
            </x-slot>
        </x-table>
        @else
        <p>No application Cron Jobs were found with this criteria.</p>
        @endif
    </div>

</div>
