<x-tr>
    <x-td>
        {{ $channel->name }}
        @if(!$channel->verified)
            <x-badge color="red">
                <x-fas>exclamation-circle</x-fas> Unverified
            </x-badge>
        @endif
    </x-td>
    <x-td>
        {{ $channel->type }} @if($channel->type == 'sms') - {{ $channel->carrier }} @endif
    </x-td>
    <x-td>{{ $channel->key_formatted }}</x-td>
    <x-td>
        <div class="flex">
            @if(!$channel->verified)
                @if($channel->channel_verifications->isNotEmpty())
                    <x-button color="green" wire:click="showVerifyForm" wire:loading.attr="disabled">
                        <x-fas>shield-alt</x-fas> Verify
                    </x-button>
                @endif

                <x-button color="teal" wire:click="resendVerification" wire:loading.attr="disabled" wire:target="resendVerification">
                    <span wire:loading.remove wire:target="resendVerification"><x-fas>redo</x-fas> Resend</span>
                    <span wire:loading wire:target="resendVerification"><x-fas>spinner fa-spin</x-fas> Sending...</span>
                </x-button>
            @endif

            @if($channel->verified)
                <x-button color="yellow" wire:click="sendTestNotification" wire:loading.attr="disabled" wire:taget='sendTestNotification'>
                    <span wire:loading.remove wire:taget='sendTestNotification'><x-fas>vial</x-fas> Send Test</span>
                    <span wire:loading wire:taget='sendTestNotification'><x-fas>spinner fa-spin</x-fas> Sending...</span>
                </x-button>
            @endif

            <x-button color="red" wire:click='deleteChannel' onclick="return confirm('Are you sure? This cannot be undone.') || event.stopImmediatePropagation()" wire:loading.attr='disabled' wire:target='deleteChannel'>
                <span wire:loading.remove wire:target="deleteChannel"><x-fas>trash</x-fas> Delete</span>
                <span wire:loading wire:target="deleteChannel"><x-fas>spinner fa-spin</x-fas> Deleting...</span>
            </x-button>
        </div>
        @if ($showVerifyForm)
        <div class="flex items-end gap-2">
            <x-input-text name="code" wire:model="code" wire:loading.attr="disabled" wire:target="verify"/>
            <div class="mb-4">
            	<x-button color="green" wire:click="verify">
                    <span wire:loading.remove wire:target="verify"><x-fas>check</x-fas> Submit</span>
                    <span wire:loading wire:target="verify"><x-fas>spinner fa-spin</x-fas>Verifying...</span>
                </x-button>
            </div>
        </div>
        @endif
    </x-td>
</x-tr>
