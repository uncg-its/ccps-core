<div x-data="{showCreateForm: false}" @token-created.window="showCreateForm=false">
    {!! Breadcrumbs::render('tokens.index') !!}

    <div class="flex justify-between items-center">
        <x-h1>API Tokens</x-h1>
        <div class="flex">
            @permission('tokens.create')
                <x-button size="md" color="green" x-on:click="showCreateForm=true">
                    <x-fas>plus</x-fas> Create New
                </x-button>
            @endpermission
            @permission('tokens.admin')
            @if ($showAll)
                <x-button size="md" href="{{ route('account.tokens.index') }}">
                    <x-fas>user</x-fas> My Tokens
                </x-button>
            @else
                <x-button size="md" href="{{ route('account.tokens.index', ['show' => 'all']) }}">
                    <x-fas>user</x-fas> Show All
                </x-button>
            @endif
            @endpermission
        </div>
    </div>

    <x-p><strong>API Tokens</strong> can be used to authenticate to this application's API endpoints as you. Generate and manage your API tokens on this page.</x-p>

    <x-p>Requests to this application should be made as described in the application's API documentation, authenticating via a Bearer token. This header should be present on all requests that require authentication: <x-code>Authorization: Bearer API_TOKEN_VALUE_HERE</x-code></x-p>
    <x-hr />
    <div>
        @if ($showAll)
            <x-alert>Showing tokens for all users</x-alert>
        @endif
    </div>

    <div x-show="showCreateForm" x-cloak>
        <x-h2>Create New Token</x-h2>
            <div class="flex gap-4">
                <div class="w-1/2">
                    <x-input-text
                        name="name"
                        help="A name for the token, so that you can tell it apart from others you have created. Typically this will refer in some way to how the token will be used."
                        required
                        wire:model="tokenName"
                    />
                </div>
                <div class="w-1/2">
                    <x-input-select
                        name="expires"
                        :options="['' => 'never', '1' => '1 month', '3' => '3 months', '6' => '6 months', '12' => '1 year']"
                        help="When this token should expire and be purged from your account"
                        wire:model="tokenExpires"
                    />
                </div>
            </div>
            <div>
                <x-button size="md" color="green" wire:click="createToken" wire:loading.attr='disabled' wire:target="createToken">
                    <span wire:loading.remove wire:target="createToken"><x-fas>check</x-fas> Submit</span>
                    <span wire:loading wire:target="createToken"><x-fas>spinner fa-spin</x-fas> Creating...</span>
                </x-button>
            </div>
        <x-hr />
    </div>

    <div>
        @if ($plainTextToken)
            <x-card title="Your New Token">
                <x-p>Your new token has been created. <strong>Copy this key</strong> in a safe place, as it will not be shown again.</x-p>
                <div class="break-all mb-3">
                    <x-code>{{ $plainTextToken }}</x-code>
                </div>
                <x-p><strong>Note that your token is not granted any abilities by default</strong>. However, this comes into play only if the application has implemented ability checks in the API.</x-p>
            </x-card>
        @endif
    </div>

    <x-h2>Existing Tokens</x-h2>
    <div>
        @if ($tokens->isEmpty())
            <p>No API Tokens have been created.</p>
        @else
            <x-table>
                <x-slot name="th">
                    <x-th>User</x-th>
                    <x-th>Name</x-th>
                    <x-th>Abilities</x-th>
                    <x-th>Last Used</x-th>
                    <x-th>Created At</x-th>
                    <x-th>Expires At</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($tokens as $token)
                        <livewire:api-token :token="$token" :key="$token->id" />
                    @endforeach
                </x-slot>
            </x-table>
        @endif
    </div>
</div>
