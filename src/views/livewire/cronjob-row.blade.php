<x-tr class="{{ $this->cronjob->lockFileExists() ? 'bg-yellow-200' : '' }}">
    @if ($this->cronjob->isEnabled())
        @if ($this->cronjob->lockFileExists())
            <x-td class="bg-yellow-300 whitespace-no-wrap">
                <x-fas class="text-yellow-900">lock</x-fas> locked
            </x-td>
        @else
            <x-td class="bg-green-300 whitespace-no-wrap">
                <x-fas class="text-green-900">check</x-fas> enabled
            </x-td>
        @endif
    @else
        <x-td class="bg-red-300 whitespace-no-wrap">
            <x-fas class="text-red-900">times</x-fas> disabled
        </x-td>
    @endif
    <x-td>
        {{ $this->cronjob->getDisplayName() }}<br>
        @if(!empty($this->cronjob->getTags()))
            @foreach($this->cronjob->getTags() as $tag)
            <x-badge color="blue">{{ $tag }}</x-badge>
            @endforeach
        @endif
    </x-td>
    <x-td>{{ $this->cronjob->getDescription() }}</x-td>
    <x-td>{{ $this->cronjob->getSchedule() }}</x-td>
    <x-td>{{ $this->cronjob->getLastRun() }}</x-td>
    <x-td>
        <div class="flex items-center">
            @if ($this->cronjob->isEnabled())
                @if (! $this->cronjob->lockFileExists())
                    <x-button color="green" wire:click="run" wire:loading.attr="disabled" wire:target="run" wire:key="run_btn_{{ $cronjobId }}">
                        <span class="whitespace-no-wrap" wire:loading.remove wire:target="run"><x-fas>play-circle</x-fas> Run</span>
                        <span class="whitespace-no-wrap" wire:loading wire:target="run"><x-fas>spinner fa-spin</x-fas> Running...</span>
                    </x-button>

                    <x-button color="red" wire:click="disable" wire:loading.attr="disabled" wire:target="disable" wire:key="disable_btn_{{ $cronjobId }}">
                        <span class="whitespace-no-wrap" wire:loading.remove wire:target="disable"><x-fas>power-off</x-fas> Disable</span>
                        <span class="whitespace-no-wrap" wire:loading wire:target="disable"><x-fas>spinner fa-spin</x-fas> Disabling...</span>
                    </x-button>
                @endif
            @else
                <x-button color="green" wire:click="enable" wire:loading.attr="disabled" wire:target="enable" wire:key="enable_btn_{{ $cronjobId }}">
                    <span class="whitespace-no-wrap" wire:loading.remove wire:target="enable"><x-fas>power-off</x-fas> Enable</span>
                    <span class="whitespace-no-wrap" wire:loading wire:target="enable"><x-fas>spinner fa-spin</x-fas> Enabling...</span>
                </x-button>
            @endif
            <x-button href="{{ route('cronjob.edit', $this->cronjob->getMeta()->id) }}">
                <x-fas>edit</x-fas> Edit
            </x-button>

            @if ($this->cronjob->lockFileExists())
                <x-button color="yellow" wire:click="clearLockFile" wire:loading.attr="disabled" wire:target="clearLockFile" wire:key="clr_btn_{{ $cronjobId }}">
                    <span class="whitespace-no-wrap" wire:loading.remove wire:target="clearLockFile"><x-fas>unlock</x-fas> Clear Lock file</span>
                    <span class="whitespace-no-wrap" wire:loading wire:target="clearLockFile"><x-fas>spinner fa-spin</x-fas> Clearing...</span>
                </x-button>
            @endif
        </div>
    </x-td>
</x-tr>
