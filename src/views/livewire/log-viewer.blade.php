<div>
    <div class="flex gap-5 items-end">
        <x-input-select name="selectedFile" label="File" wire:model="selectedFile" :options="array_combine(array_keys($logFiles), array_keys($logFiles))" />
        <x-input-select name="level" wire:model="level" :options="$levels" />
        <x-input-select name="perPage" label="Per Page" wire:model="perPage" :options="$perPageOptions" />
        <x-input-select name="sort" wire:model="sort" :options="['desc' => 'Latest', 'asc' => 'Earliest']" />
        <x-input-text name="search" wire:model="search" />
        <div class="mb-4">
            <x-button color="yellow" wire:click="reload">
                <x-fas>redo</x-fas> Reload
            </x-button>
        </div>
    </div>
    <div class="flex justify-center">
        <div>
            <div class="flex items-end pb-4">
                @if ($page > 0)
                    <x-button color="gray" wire:click.prevent="previousPage">
                        <x-fas>arrow-left</x-fas> Previous
                    </x-button>
                @endif
                @foreach ($pageList as $pageNum)
                    @if ($pageNum === '...')
                        <x-button color="gray" disabled>
                            ...
                        </x-button>
                    @else
                        <x-button color="{{ $page == $pageNum - 1 ? 'blue' : 'gray' }}" wire:click="setPage('{{ $pageNum - 1 }}')" wire:key="pgbtn_{{ $loop->index }}">
                            {{ $pageNum }}
                        </x-button>
                    @endif
                @endforeach
                @if ($page < $pages - 1)
                    <x-button color="gray" wire:click.prevent="nextPage">
                        <x-fas>arrow-right</x-fas> Next
                    </x-button>
                @endif
            </div>
        </div>
    </div>
    <div class="flex">
        <div class="w-full">
            <div wire:loading class="w-full">
                <x-alert icon="spin fa-spinner">
                    Parsing log file... this could take a while for larger files...
                </x-alert>
            </div>
            <div wire:loading.remove>
                @component('components.table')
                    @slot('th')
                        <x-th>Date</x-th>
                        <x-th>Logger</x-th>
                        <x-th>Level</x-th>
                        <x-th>Message</x-th>
                    @endslot
                    @slot('tbody')
                        @foreach ($lines as $line)
                        <x-tr wire:key="{{ $selectedFile }}_{{ $loop->index }}">
                            @if (is_array($line['date']) && isset($line['date']['date']))
                                <x-td>{{ \Carbon\Carbon::parse($line['date']['date'])->format('Y/m/d H:i:s') }}</x-td>
                            @else
                                <x-td>{{ $line['date']->format('Y/m/d H:i:s') }}</x-td>
                            @endif
                            <x-td>{{ $line['logger'] }}</x-td>
                            <x-td>
                                @include('components.badges.log.' . strtolower($line['level']))
                            </x-td>
                            <x-td class="break-all">{!! $line['message'] !!}</x-td>
                        </x-r>
                        @endforeach
                    @endslot
                @endcomponent
            </div>
        </div>
    </div>
</div>
