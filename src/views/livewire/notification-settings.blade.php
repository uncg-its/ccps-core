<div>
    @if($channels->whereNotNull('verified_at')->isNotEmpty() && $events->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th></x-th>
                @foreach($channels->whereNotNull('verified_at') as $channel)
                    <x-th>{{ $channel->name }}</x-th>
                @endforeach
            </x-slot>
            <x-slot name="tbody">
                @foreach($events as $event)
                    <x-tr wire:key="$event->id">
                        <x-th scope="row" title="{{ $event->description }}" class="w-1/4">{{ $event->display_name }}</x-th>
                        @foreach ($channels as $channel)
                            <x-td wire:key="channel_$channel->id">
                                <livewire:notification-toggle
                                    :active="$channel->notification_events->where('id', $event->id)->isNotEmpty()"
                                    :channel="$channel->id"
                                    :event="$event->id"
                                    :key="$event->id . '_' . $channel->id"
                                />
                            </x-td>
                        @endforeach
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
        @if ($channels->whereNotNull('verified_at')->isEmpty())
            <p>No notification channels have been verified.</p>
        @else
            <p>No notifiable events have been configured for this application.</p>
        @endif
    @endif
</div>
