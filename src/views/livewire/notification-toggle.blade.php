<div class="flex">
    <x-input-checkbox name="" wire:model="active"/>
    <div wire:loading.remove class="ml-2">
        @if ($active)
            <x-badge color="green">
                <x-fas>check</x-fas> Subscribed
            </x-badge>
        @else
            <x-badge color="red">
                <x-fas>times</x-fas> Not Subscribed
            </x-badge>
        @endif
    </div>
    <div wire:loading class="ml-2">
        <x-badge color="yellow">
            <x-fas>spinner fa-spin</x-fas> Updating...
        </x-badge>
    </div>
</div>
