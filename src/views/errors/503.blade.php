@extends('layouts.wrapper', [
    'pageTitle' => '503'
])

@section('content')
    <x-h2>503 - Service Unavailable</x-h2>
    <x-p>The application is down for maintenance, or experiencing an issue with availability.</x-p>
@endsection
