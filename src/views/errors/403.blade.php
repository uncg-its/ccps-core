@extends('layouts.wrapper', [
    'pageTitle' => '403'
])

@section('content')
    <x-h2>403 - Forbidden</x-h2>
    <x-alert color="red" icon="exclamantion-triangle">
        <strong>Error:</strong> Permission denied. <em>{{ $exception->getMessage() }}</em>
    </x-alert>

    <x-p><x-a href="{{ \URL::previous() }}">Go Back</x-a></x-p>
@endsection
