@extends('layouts.wrapper', [
    'pageTitle' => '404'
])

@section('content')
    <x-h2>404 - Not Found</x-h2>
    <x-p>{{ $exception->getMessage() }}</x-p>
@endsection
