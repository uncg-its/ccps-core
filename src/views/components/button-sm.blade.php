<x-base.button :color="$color ?? 'blue'" :text-color="$textColor ?? 'white'" :target="$target ?? '#'" :type="$type ?? 'a'" :class="$class ?? ''">
    <x-slot name="sizeClasses">px-2 py-1 text-xs leading-4 rounded</x-slot>
    <x-slot name="text">{{ $slot }}</x-slot>
</x-base.button>
