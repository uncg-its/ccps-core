<x-alert icon="{{ $icon ?? 'info-circle' }}" dismissable="{{ $dismissable ?? 'true' }}">{{ $slot }}</x-alert>
