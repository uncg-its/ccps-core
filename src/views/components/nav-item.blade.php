<div class="relative ml-4" @if($item->hasChildren()) x-data="{open:false}" @endif>
    <a href="{{ $item->url() }}" class="px-3 py-2 rounded-md text-sm font-medium leading-5 text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out" @if($item->hasChildren()) @click.prevent="open=true" @endif>
        {{ $item->title }} @if($item->hasChildren()) <x-fas classes="ml-1">chevron-down</x-fas> @endif
    </a>
    @if($item->hasChildren())
    <div class="absolute origin-top-left absolute left-0 mt-2 w-48 rounded-md shadow-lg" x-show="open" @click.away="open=false" x-cloak>
        <div class="py-1 rounded-md bg-white shadow-xs" role="menu" aria-orientation="vertical" aria-labelledby="dropdown-menu">
            @foreach ($item->children() as $child)
                <a href="{{ $child->url() }}" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out" role="menuitem">
                    {{ $child->title }}
                </a>
            @endforeach
        </div>
    </div>
    @endif
</div>
