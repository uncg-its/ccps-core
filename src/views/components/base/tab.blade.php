<li class="-mb-px mr-1">
    <a {{ $attributes->merge(['class' => 'bg-white inline-block py-2 px-4 font-semibold']) }} x-on:click.prevent href="#">{{ $slot }}</a>
</li>
