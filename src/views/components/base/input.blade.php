@if ($label ?? false)
    <label class="block text-gray-700 text-sm font-bold mb-2" for="{{ $name }}">{{ $label }} @if($required ?? false)<span class="text-red-500">*</span>@endif</label>
@endif
{{ $slot }}
@error($name) <span class="text-red-500 text-md">{{ $message }}</span> @enderror
