<form action="{{ $action }}" method="{{ $method === 'get' ? 'get' : 'post' }}" @if($confirm) onsubmit="return confirm('{{ $confirm }}')" @endif {{ $attributes->exceptProps(['method', 'action', 'onsubmit']) }}>
    @if (in_array($method, ['patch', 'delete']))
        {{ method_field($method) }}
    @endif
    @if ($method !== 'get') {{ csrf_field() }} @endif
    {{ $inputs }}
</form>
