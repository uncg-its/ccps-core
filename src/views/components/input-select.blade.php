<x-input-group :help="$help ?? null" :title="$title ?? null">
    <x-base.input
        :label="$label ?? \Str::of($name)->title()->replace('_', ' ')->replace('-', ' ')"
        :required="$required ?? null"
        :name="$name"
    >
        <select
            name="{{ $name }}"
            id="{{ $name }}"
            {{ $attributes->exceptProps(['options', 'selected'])->merge(['class' => 'shadow border rounded w-full py-1 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline']) }}
        >
            @if ($options ?? false)
                @foreach ($options as $value => $text)
                    <x-input-option :value="$value" :text="$text" :selected="$selected ?? null"/>
                @endforeach
            @else
                {{ $slot }}
            @endif
        </select>
    </x-base.input>
</x-input-group>
