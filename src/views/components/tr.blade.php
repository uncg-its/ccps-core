<tr {{ $attributes->merge(['class' => 'bg-white hover:bg-gray-100']) }}>
    {{ $slot }}
</tr>
