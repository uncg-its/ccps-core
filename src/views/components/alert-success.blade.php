<x-alert color="green" icon="{{ $icon ?? 'check' }}" dismissable="{{ $dismissable ?? 'true' }}">{{ $slot }}</x-alert>
