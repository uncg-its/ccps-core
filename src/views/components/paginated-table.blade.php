{{--
    PAGINATED TABLE COMPONENT
    Required props: $collection (paginated Collection)
--}}
<div class="w-full">
    <x-table :th="$th" :tbody="$tbody"></x-table>
    {{ $collection->links() }}
</div>
