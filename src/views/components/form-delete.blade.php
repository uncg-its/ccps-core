<x-base.form method="delete" :action="$action" :confirm="$confirm ?? false" :attributes="$attributes">
    <x-slot name="inputs">
        {{ $slot }}
    </x-slot>
</x-base.form>
