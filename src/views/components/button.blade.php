<span class="inline-flex rounded-md shadow-sm mx-1">
  @if ($href ?? false)
      <a href="{{ $href }}" {{ $attributes->merge(['class' => $baseClasses]) }} {{ $disabled ?? false ? 'disabled' : '' }}>{!! $slot !!}</a>
  @else
      <button {{ $attributes->merge(['class' => $baseClasses]) }} {{ $disabled ?? false ? 'disabled' : '' }}>{!! $slot !!}</button>
  @endif
</span>
