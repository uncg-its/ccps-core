<x-base.button :color="$color ?? 'blue'" :text-color="$textColor ?? 'white'" :target="$target ?? '#'" :type="$type ?? 'a'" :class="$class ?? ''">
    <x-slot name="sizeClasses">px-6 py-3 text-base leading-6 rounded-md</x-slot>
    <x-slot name="text">{{ $slot }}</x-slot>
</x-base.button>
