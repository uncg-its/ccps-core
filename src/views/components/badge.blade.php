<span class="inline-flex items-center gap-2 px-3 py-1 rounded-full text-xs font-medium leading-4 bg-{{ $color ?? 'gray' }}-100 text-{{ $color ?? 'gray' }}-800">
  {!! $slot !!}
</span>
