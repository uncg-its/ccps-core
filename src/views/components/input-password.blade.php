<x-input-group :help="$help ?? null" :title="$title ?? null">
    <x-base.input
        :label="$label ?? \Str::of($name)->title()->replace('_', ' ')->replace('-', ' ')"
        :required="$required ?? null"
        :name="$name"
    >
        <input
            class="shadow appearance-none border rounded w-full py-1 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="{{ $name }}"
            name="{{ $name }}"
            type="password"
            placeholder="{{ $placeholder ?? '' }}"
            value="{{ $value ?? null }}"
            {{ $attributes->merge() }}
        />
    </x-base.input>
</x-input-group>
