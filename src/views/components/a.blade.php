{{--
    A COMPONENT
    Optional props: $color (Tailwind-supported color)
--}}
<a {{ $attributes->merge(['class' => $color ?? 'text-blue-500']) }}>{!! $slot !!}</a>
