<div x-data="{on: '{{ $on }}'}" >
    <!-- On: "bg-green-500", Off: "bg-gray-200" -->
    <span
        role="checkbox"
        tabindex="0"
        aria-checked="false"
        class="relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:shadow-outline"
        :class="{'{{ $onBgColor }}': on, '{{ $offBgColor }}': !on}"
    >
        <!-- On: "translate-x-5", Off: "translate-x-0" -->
        <span
            aria-hidden="true"
            class="translate-x-0 relative inline-block h-5 w-5 rounded-full bg-white shadow transform transition ease-in-out duration-200"
            :class="{'translate-x-5': on, 'translate-x-0': !on}"
        >
            <!-- On: "opacity-0 ease-out duration-100", Off: "opacity-100 ease-in duration-200" -->
            <span
                class="opacity-100 ease-in duration-200 absolute inset-0 h-full w-full flex items-center justify-center transition-opacity"
                :class="{'opacity-0 ease-out duration-100': on, 'opacity-100 in duration-200': !on}"
            >
                <x-fas class="text-xs {{ $offTextColor }}">{{ $offIcon }}</x-fas>
            </span>
            <!-- On: "opacity-100 ease-in duration-200", Off: "opacity-0 ease-out duration-100" -->
            <span
                class="opacity-0 ease-out duration-100 absolute inset-0 h-full w-full flex items-center justify-center transition-opacity"
                :class="{'opacity-100 ease-in duration-200': on, 'opacity-0 ease-out duration-100': !on}"
            >
                <x-fas class="text-xs {{ $onTextColor }}">{{ $onIcon }}</x-fas>
            </span>
        </span>
    </span>
</div>
