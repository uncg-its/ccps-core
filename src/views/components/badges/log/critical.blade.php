<x-badge color="red">
    <div class="flex gap-1">
        <x-fas>exclamation-triangle</x-fas> critical
    </div>
</x-badge>
