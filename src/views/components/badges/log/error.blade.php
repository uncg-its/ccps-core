<x-badge color="orange">
    <div class="flex gap-1">
        <x-fas>exclamation-triangle</x-fas> error
    </div>
</x-badge>
