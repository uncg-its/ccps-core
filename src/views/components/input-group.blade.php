<div {{ $attributes->only(['class'])->merge(['class' => 'my-4']) }}>
    @if ($title ?? false)
        <p class="block text-gray-700 text-sm font-bold mb-2">{{ $title }}</p>
    @endif
    {!! $slot !!}
    @if($help ?? false)
        <x-input-help>{!! $help !!}</x-input-help>
    @endif
</div>
