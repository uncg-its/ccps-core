<x-alert
    color="red"
    icon="{{ $icon ?? 'times-circle' }}"
    dismissable="{{ $dismissable ?? 'true' }}"
>
    {{ $slot }}
</x-alert>
