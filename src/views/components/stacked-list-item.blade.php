<li {{ $attributes->merge(['class' => 'border-t border-gray-200']) }}>
    @if ($href ?? false) <a href="{{ $href }}" class="block hover:bg-gray-50 focus:outline-none focus:bg-gray-50 transition duration-150 ease-in-out"> @endif
        <div class="flex items-center px-4 py-4 sm:px-6">
            {!! $slot !!}
        </div>
    @if ($href ?? false) </a> @endif
</li>
