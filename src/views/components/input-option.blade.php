<option
    value="{{ $value }}"
    {{ $attributes->exceptProps(['selected'])->merge() }}
    @if($selected == $value) selected @endif
>{{ $text }}</option>
