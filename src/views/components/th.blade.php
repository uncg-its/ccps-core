<th
    {{ $attributes->merge(['class' => 'px-6 py-3 border-b border-gray-200 bg-gray-200 text-left text-xs leading-4 font-medium text-gray-800 uppercase tracking-wider']) }}
>
    {!! $slot !!}
</th>
