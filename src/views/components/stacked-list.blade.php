<div class="bg-white shadow overflow-hidden sm:rounded-md w-full">
    <ul {{ $attributes->merge() }}>
        {!! $slot !!}
    </ul>
</div>
