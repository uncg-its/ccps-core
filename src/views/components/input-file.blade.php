<x-input-group :help="$help ?? null" :title="$title ?? null">
    <x-base.input
    :label="$label ?? \Str::of($name)->title()->replace('_', ' ')->replace('-', ' ')"
    :required="$required ?? null"
    :name="$name"
    >
        <input
            id="{{ $name }}"
            name="{{ $name }}"
            type="file"
            placeholder="{{ $placeholder ?? '' }}"
            value="{{ $value ?? null }}"
            {{ $attributes->merge(['class' => 'shadow appearance-none border rounded w-full py-1 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline']) }}
        />
    </x-base.input>
</x-input-group>
