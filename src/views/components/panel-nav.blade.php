<div class="inline-block border border-gray-500 p-4 rounded-lg m-2 bg-transparent hover:bg-gray-100 transition ease-in-out duration-300 text-blue-500 hover:text-blue-800">
    <a href="{{ $url }}" class="flex flex-col text-center">
        <div>
            <x-fa class="fa-5x mb-3">{{ $fa }}</x-fa>
        </div>
        <div>
            {{ $title }}
        </div>
    </a>
</div>
