<x-alert color="yellow" icon="{{ $icon ?? 'exclamation-triangle' }}" dismissable="{{ $dismissable ?? 'true' }}">{{ $slot }}</x-alert>
