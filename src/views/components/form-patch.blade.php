<x-base.form method="patch" :action="$action" :confirm="$confirm ?? false" :attributes="$attributes">
    <x-slot name="inputs">
        {{ $slot }}
    </x-slot>
</x-base.form>
