{{--
    MODAL COMPONENT
    Required props: $trigger (string - JS expression)
    Optional props: $size (tailwind-compatible width class); $footer (HTML); $awayTrigger (string - JS expression)
--}}
<div class="fixed z-10 inset-0 overflow-y-auto" x-show="{{ $trigger }}" {{ $attributes->merge() }} x-cloak x-on:keydown.escape.window="{{ $awayTrigger ?? $trigger . '=false' }}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0 transition ease-out duration-300">
        <div class="fixed inset-0 transition-opacity"
        x-show="{{ $trigger }}"
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-in duration-200"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>

        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;

        <!-- Modal panel, show/hide based on modal state. -->
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle {{ $size ?? "w-10/12" }}
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-headline"
            x-on:click.away="{{ $awayTrigger ?? $trigger . '=false' }}"
            x-show="{{ $trigger }}"
            x-transition:enter="transition ease-out duration-300"
            x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave="transition ease-in duration-200"
            x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
            x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
        >
            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                {!! $slot !!}
            </div>
            @if ($footer ?? false)
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse items-center">
                     {!! $footer !!}
                </div>
            @endif
        </div>
    </div>
</div>
