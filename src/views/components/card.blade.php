<div {{ $attributes->merge(['class' => 'rounded-lg overflow-hidden shadow-lg mb-4 pb-4']) }}>
  <div class="flex flex-col">
    @if ($title ?? false)
      <div class="font-bold text-xl mb-3 bg-gray-200 px-6 py-3">{{ $title }}</div>
    @endif
    <div class="text-gray-700 text-base px-6">
      {!! $slot !!}
    </div>
  </div>
  @if ($footer ?? false)
    <div class="bg-gray-200 px-6 py-4">
        {!! $footer !!}
    </div>
    @endif
</div>
