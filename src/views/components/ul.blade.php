<ul {{ $attributes->merge(['class' => 'list-disc list-inside mb-4']) }}>
    {!! $slot !!}
</ul>
