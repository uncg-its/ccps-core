<x-base.button :color="$color ?? 'blue'" :text-color="$textColor ?? 'white'" :target="$target ?? '#'" :type="$type ?? 'a'" :class="$class ?? ''">
    <x-slot name="sizeClasses">px-4 py-2 text-sm leading-5 rounded-md</x-slot>
    <x-slot name="text">{{ $slot }}</x-slot>
</x-base.button>
