<div class="flex">
    <input
        type="checkbox"
        class="mr-2"
        name="{{ $name }}"
        id="{{ $id ?? $name }}"
        value="{{ $value ?? null }}"
        {{ $attributes->merge() }}
    />
    <label for="{{ $id ?? $name }}">{{ $label ?? \Str::title($name) }}</label>
    @if($help ?? false)
        <x-input-help>{{ $help }}</x-input-help>
    @endif
</div>
