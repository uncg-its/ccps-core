@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    <x-h1>{{ config('app.name') }}</x-h1>
    <div class="flex">
        @if($user)
            @role('admin')
            @include('components.panel-nav', [
                'url' => route('admin'),
                'fa' => 'fas fa-key',
                'title' => 'Administrator'
            ])
            @endrole

            @foreach($modules as $key => $module)
                @if(empty($module['parent']))
                    @permission($module['required_permissions'])
                    @include('components.panel-nav', [
                        'url' => route($module['index']),
                        'fa' => $module['icon'],
                        'title' => $module['title']
                    ])
                    @endpermission
                @endif
            @endforeach
        @else
            <p>You are not logged in - please <x-a href="{{ route('login') }}">log in</x-a> to proceed.</p>
        @endif
    </div>
@endsection
