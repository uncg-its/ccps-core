<?php

Route::group(['prefix' => 'cache'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\CacheController::class, 'index'])->name('cache');
    Route::get('clear', [\App\Http\Controllers\CcpsCore\CacheController::class, 'clear'])->name('cache.clear');
    Route::get('purge', [\App\Http\Controllers\CcpsCore\CacheController::class, 'purge'])->name('cache.purge');
});
