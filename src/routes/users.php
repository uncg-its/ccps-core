<?php

Route::get('users', [\App\Http\Controllers\CcpsCore\UserController::class, 'index'])->name('users');
Route::get('users/create', [\App\Http\Controllers\CcpsCore\UserController::class, 'create'])->name('user.create');
Route::post('users/create', [\App\Http\Controllers\CcpsCore\UserController::class, 'store'])->name('user.store');
Route::get('user/{userToShow}', [\App\Http\Controllers\CcpsCore\UserController::class, 'show'])->name('user.show');
Route::get('user/{userToEdit}/edit', [\App\Http\Controllers\CcpsCore\UserController::class, 'edit'])->name('user.edit');
Route::patch('user/{user}', [\App\Http\Controllers\CcpsCore\UserController::class, 'update'])->name('user.update');
Route::delete('user/{user}', [\App\Http\Controllers\CcpsCore\UserController::class, 'destroy'])->name('user.destroy');
