<?php

Route::group(['prefix' => 'cronjobs'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'index'])->name('cronjobs');

    Route::post('enable', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'enable'])->name('cronjob.enable');
    Route::post('disable', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'disable'])->name('cronjob.disable');
    Route::post('run', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'run'])->name('cronjob.run');
    Route::post('clearLockFile', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'clearLockFile'])->name('cronjob.clear-lockfile');

    Route::get('{cronjob}/edit', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'edit'])->name('cronjob.edit');
    Route::post('{cronjob}/edit', [\App\Http\Controllers\CcpsCore\CronJobController::class, 'update'])->name('cronjob.update');
});
