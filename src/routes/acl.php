<?php

Route::group(['prefix' => 'acl'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\AclController::class, 'index'])->name('acl');

    Route::get('roles', [\App\Http\Controllers\CcpsCore\RoleController::class, 'index'])->name('roles');
    Route::get('roles/create', [\App\Http\Controllers\CcpsCore\RoleController::class, 'create'])->name('role.create');
    Route::post('roles/create', [\App\Http\Controllers\CcpsCore\RoleController::class, 'store'])->name('role.store');
    Route::get('role/{role}', [\App\Http\Controllers\CcpsCore\RoleController::class, 'show'])->name('role.show');
    Route::get('role/{role}/edit', [\App\Http\Controllers\CcpsCore\RoleController::class, 'edit'])->name('role.edit');
    Route::patch('role/{role}', [\App\Http\Controllers\CcpsCore\RoleController::class, 'update'])->name('role.update');
    Route::delete('role/{role}', [\App\Http\Controllers\CcpsCore\RoleController::class, 'destroy'])->name('role.destroy');

    Route::get('permissions', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'index'])->name('permissions');
    Route::get('permissions/create', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'create'])->name('permission.create');
    Route::post('permissions/create', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'store'])->name('permission.store');
    Route::get('permission/{permission}', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'show'])->name('permission.show');
    Route::get('permission/{permission}/edit', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'edit'])->name('permission.edit');
    Route::patch('permission/{permission}', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'update'])->name('permission.update');
    Route::delete('permission/{permission}', [\App\Http\Controllers\CcpsCore\PermissionController::class, 'destroy'])->name('permission.destroy');

    Route::resource('mapped-role-groups', \App\Http\Controllers\CcpsCore\MappedRoleGroupController::class);

    Route::prefix('role-mappings')->name('role-mappings.')->group(function () {
        Route::get('/group/{mapped_role_group}', [\App\Http\Controllers\CcpsCore\RoleMappingController::class, 'index'])->name('index');
        Route::get('/group/{mapped_role_group}/create', [\App\Http\Controllers\CcpsCore\RoleMappingController::class, 'create'])->name('create');
        Route::post('/group/{mapped_role_group}', [\App\Http\Controllers\CcpsCore\RoleMappingController::class, 'store'])->name('store');
        Route::delete('/group/{mapped_role_group}/mapping/{role_mapping}', [\App\Http\Controllers\CcpsCore\RoleMappingController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('token-abilities')->name('token-abilities.')->group(function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\TokenAbilitiesController::class, 'index'])->name('index');
        Route::get('{token}/edit', [\App\Http\Controllers\CcpsCore\TokenAbilitiesController::class, 'edit'])->name('edit');
        Route::patch('{token}', [\App\Http\Controllers\CcpsCore\TokenAbilitiesController::class, 'update'])->name('update');
    });
});
