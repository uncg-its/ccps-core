<?php

Route::group(['prefix' => 'email'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\EmailController::class, 'index'])->name('email');

    Route::group(['prefix' => 'queue'], function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\EmailQueueController::class, 'index'])->name('email.queue');
        Route::get('sent', [\App\Http\Controllers\CcpsCore\EmailQueueController::class, 'sent'])->name('email.sent');
        Route::get('{job}', [\App\Http\Controllers\CcpsCore\EmailQueueController::class, 'show'])->name('email.show');
        Route::get('sent/{sentEmail}', [\App\Http\Controllers\CcpsCore\EmailQueueController::class, 'showSentEmail'])->name('email.sent.show');
        Route::delete('{job}', [\App\Http\Controllers\CcpsCore\EmailQueueController::class, 'delete'])->name('email.delete');
    });
});
