<?php

Route::get('config', [\App\Http\Controllers\CcpsCore\DbConfigController::class, 'index'])->name('config');
Route::patch('config', [\App\Http\Controllers\CcpsCore\DbConfigController::class, 'save'])->name('config.save');
