<?php

Route::prefix('notifications-log')->name('notifications-log.')->group(function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\NotificationLogController::class, 'index'])->name('index');
});
