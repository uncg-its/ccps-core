<?php

Route::group(['prefix' => 'queues'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\QueueController::class, 'index'])->name('queues');
    Route::get('pending', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showPending'])->name('queues.pending');
    Route::get('failed', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showFailed'])->name('queues.failed');
    Route::get('successful', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showSuccessful'])->name('queues.successful');

    Route::get('showPendingJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showPendingJob'])->name('queues.pending.show');
    Route::get('showSuccessfulJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showSuccessfulJob'])->name('queues.successful.show');
    Route::get('showFailedJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'showFailedJob'])->name('queues.failed.show');

    Route::delete('deleteJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'deleteJob'])->name('queues.delete');
    Route::delete('forgetFailedJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'forgetFailedJob'])->name('queues.forgetfailed');
    Route::get('requeueFailedJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'requeueFailedJob'])->name('queues.requeuefailed');

    Route::get('runJob/{job}', [\App\Http\Controllers\CcpsCore\QueueController::class, 'runJob'])->name('queues.run');
});
