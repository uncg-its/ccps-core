<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\MappedRoleGroup;

class MappedRoleGroupCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('acl.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id'        => 'required|exists:ccps_roles,id',
            'provider'       => 'required|in:' . implode(',', config('ccps.login_methods')),
            'type'           => 'required|in:ad-hoc,synced',
            'sync_service'   => 'sometimes|required_if:type,synced',
            'sync_entity_id' => 'sometimes|required_if:type,synced'
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist()
    {
        $mappedRoleGroup = MappedRoleGroup::create([
            'type'           => $this->type,
            'sync_service'   => $this->sync_service,
            'sync_entity_id' => $this->sync_entity_id,
            'provider'       => $this->provider,
            'role_id'        => $this->role_id,
        ]);

        \Log::channel('acl')->info('Mapped Role Group created', [
            'category'  => 'model',
            'operation' => 'create',
            'result'    => 'success',
            'data'      => [
                'mapped_role_group' => $mappedRoleGroup,
                'current_user'      => \Auth::user()->email,
            ]
        ]);

        flash('Mapped role group created successfully.')->success();
    }
}
