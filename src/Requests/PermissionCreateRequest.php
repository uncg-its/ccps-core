<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\Permission;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;

class PermissionCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required',
            'database_key' => 'required',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist()
    {
        \DB::beginTransaction();
        try {
            $permission = Permission::create(
                array_merge($this->only(['display_name', 'description']), ['name' => \Str::slug($this->database_key, '.')])
            );

            $permission->roles()->sync($this->roles);

            \DB::commit();

            event(new AclChanged(
                'add',
                'permission',
                [
                    'permission' => $permission->load('roles'),
                ]
            ));

            flash("Successfully created Permission '$this->display_name'")->success();
            return $permission;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to create Permission. Please check application logs.')->error();
            \Log::channel('database')->error('Error creating Permission', [
                'category'  => 'database',
                'operation' => 'store',
                'result'    => 'error',
                'data'      => [
                    'request'         => $this->all(),
                    'current_user'    => auth()->user(),
                    'message'         => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('Permission was created, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error creating Permission', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'error',
                'data'      => [
                    'request'         => $this->all(),
                    'current_user'    => auth()->user(),
                    'message'         => $e->getMessage()
                ]
            ]);
        }
    }
}
