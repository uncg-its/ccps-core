<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\Role;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;

class RoleUpdateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required_with:description',
            'database_key' => 'required_with:description',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(Role $role)
    {
        \DB::beginTransaction();
        try {
            $originalRole = clone($role);
            $originalRole->load(['permissions', 'users']);

            if ($role->editable) {
                $name = $this->database_key ? \Str::slug($this->database_key, '.') : $role->name;

                $role->update(
                    array_merge($this->only(['display_name', 'description']), ['name' => $name])
                );
            }

            $role->permissions()->sync($this->permissions);

            \DB::commit();

            event(new AclChanged(
                'edit',
                'role',
                [
                    'old_role' => $originalRole,
                    'new_role' => $role->load(['permissions', 'users']),
                ]
            ));

            flash("Successfully edited role '$role->display_name'")->success();
            return $role;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to edit Role. Please check application logs.')->error();
            \Log::channel('database')->error('Error updating Role', [
                'category'  => 'database',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'role'         => $role,
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('Role was edited, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error updating Role', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'role'         => $role,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
        }
    }
}
