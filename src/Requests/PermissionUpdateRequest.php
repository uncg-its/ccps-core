<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\Permission;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;

class PermissionUpdateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required_with:description',
            'database_key' => 'required_with:description',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(Permission $permission)
    {
        \DB::beginTransaction();
        try {
            $originalPermission = clone($permission);
            $originalPermission->load('roles');

            if ($permission->editable) {
                $name = $this->database_key ? \Str::slug($this->database_key, '.') : $permission->name;

                $permission->update(
                    array_merge($this->only(['display_name', 'description']), ['name' => $name])
                );
            }

            $permission->roles()->sync($this->roles);

            \DB::commit();

            event(new AclChanged(
                'edit',
                'permission',
                [
                    'old_permission' => $originalPermission,
                    'new_permission' => $permission->load('roles'),
                ]
            ));

            flash("Successfully edited Permission '$this->display_name'")->success();
            return $permission;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to edit Permission. Please check application logs.')->error();
            \Log::channel('database')->error('Error updating Permission', [
                'category'  => 'database',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'permission'   => $permission,
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('Permission was edited, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error updating Permission', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'permission'   => $permission,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
        }
    }
}
