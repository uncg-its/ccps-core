<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\User;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class UserUpdateRequest extends FlashedRequest
{
    use SendsPasswordResetEmails;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_zone' => 'required',
            'email'     => 'email',
        ];
    }

    /**
     * Messages when validation rules are not met.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(User $user, $roles = null)
    {

        // for related roles
        \DB::beginTransaction();

        try {
            $originalUser = clone($user);
            $originalUser->load('roles');

            $updateArray = $this->only(['time_zone']);
            if (!is_null($this->email)) {
                $updateArray['email'] = $this->email;
            }


            $user->update($updateArray);

            $rolesToSync = $roles ?: $this->roles;
            $user->roles()->sync($rolesToSync);

            \DB::commit();

            event(new AclChanged(
                'edit',
                'user',
                [
                    'old_user' => $originalUser,
                    'new_user' => $user->load('roles'),
                ]
            ));

            flash("Successfully edited user '$user->email'")->success();

            // password reset?
            if ($this->reset_user_password) {
                $email = is_null($this->email) ? $originalUser->email : $this->email;

                // change user password to something random
                $newPass = bin2hex(random_bytes(20));

                $user->update(['password' => bcrypt($newPass)]);

                $this->sendResetLinkEmail($this);
                event(new AclChanged(
                    'password-reset',
                    'user',
                    [
                        'old_user' => $originalUser,
                        'new_user' => $user,
                    ]
                ));

                flash("Successfully reset password for user '$user->email'")->success();
            }

            return $user;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to edit User. Please check application logs.')->error();
            \Log::channel('database')->error('Error updating User', [
                'category'   => 'database',
                'operation'  => 'update',
                'result'     => 'error',
                'data'       => [
                    'user'         => $user,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('User was updated, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error updating User', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'user'         => $user,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
        }
    }
}
