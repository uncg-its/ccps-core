<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\RoleMapping;
use App\CcpsCore\MappedRoleGroup;
use Uncgits\Ccps\Rules\EmailListContainsEmailAddresses;

class RoleMappingCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('acl.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emails' => [
                'required',
                new EmailListContainsEmailAddresses,
            ],
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(MappedRoleGroup $mappedRoleGroup)
    {
        $emails = collect(preg_split('/[\r\n,;]+/', strtolower($this->emails), -1, PREG_SPLIT_NO_EMPTY));

        $existingMappings = $mappedRoleGroup->role_mappings
            ->pluck('email')
            ->map(function ($item) {
                return strtolower($item);
            })->toArray();

        $roleMappings = $emails->map(function ($email) use ($mappedRoleGroup, $existingMappings) {
            $email = strtolower($email);
            if (in_array($email, $existingMappings)) {
                flash('Email ' . $email . ' already exists on this role group; skipping.')->warning();
                return null;
            }

            return RoleMapping::create([
                'email'                => $email,
                'mapped_role_group_id' => $mappedRoleGroup->id,
            ]);
        });

        \Log::channel('acl')->info('Role Mappings created', [
            'category'  => 'model',
            'operation' => 'create',
            'result'    => 'success',
            'data'      => [
                'role_mappings' => $roleMappings->pluck('id'),
                'current_user'  => \Auth::user()->email,
            ]
        ]);

        flash('Role Mappings created successfully.')->success();
    }
}
