<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\User;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;
use Uncgits\Ccps\Rules\UniqueEmailAndProvider;

class UserCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                new UniqueEmailAndProvider('local')
            ]
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist()
    {
        \DB::beginTransaction();

        try {
            $user = User::create(
                array_merge($this->only(['email']), [
                    'provider'  => 'local',
                    'time_zone' => config('app.timezone'),
                    'settings'  => json_encode(['skin' => 'default'])
                ])
            );

            // add roles that were granted manually by the form
            $user->grantMappedRoles($this->roles ?? []);

            \DB::commit();

            event(new AclChanged(
                'add',
                'user',
                [
                    'user' => $user->load('roles'),
                ]
            ));

            flash("Successfully created User with email '$this->email'")->success();
            return $user;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to create User. Please check application logs.')->error();
            \Log::channel('database')->error('Error creating User', [
                'category'  => 'database',
                'operation' => 'store',
                'result'    => 'error',
                'data'      => [
                    'request'         => $this->all(),
                    'current_user'    => auth()->user(),
                    'message'         => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('User was created, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error creating User', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'error',
                'data'      => [
                    'request'         => $this->all(),
                    'current_user'    => auth()->user(),
                    'message'         => $e->getMessage()
                ]
            ]);
        }
    }
}
