<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\QueryException;

class CronjobUpdateRequest extends FlashedRequest
{
    protected $nonDataFields = ["_token" => "csrf token", "_method" => "custom method"];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schedule' => 'valid_cron_expression|nullable'
        ];
    }

    public function messages()
    {
        return [
            'schedule.valid_cron_expression' => "The schedule must be a valid cron expression."
        ];
    }

    /**
     * Additional validation rules for this object only (define global validation rules in ValidatorServiceProvider)
     *
     *
     */

    public function customValidation($validator)
    {
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist(CronjobMeta $cronjobMeta)
    {
        // strip duplicate tags and whitespace from each

        $tags = array_map('trim', explode(',', request('tags')));
        $tagsWithoutDuplicates = array_unique($tags);
        $tagsWithoutDuplicates = implode(',', $tagsWithoutDuplicates);

        if ($tagsWithoutDuplicates == '') {
            // if no tags are put in, don't override the DB at all and fall back to the property definition in the code
            $tagsWithoutDuplicates = null;
        }

        \DB::beginTransaction();

        $updateArray = [
            'display_name' => request('display_name'),
            'description'  => request('description'),
            'schedule'     => request('schedule'),
            'tags'         => $tagsWithoutDuplicates,
        ];

        try {
            $result = $cronjobMeta->update($updateArray);
            \DB::commit();
            flash("Successfully edited cron job metadata.")->success();
            \Log::channel('cron')->info('Cronjob metadata updated', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'success',
                'data'      => [
                    'current_user' => auth()->user(),
                    'cronjob_meta' => $cronjobMeta,
                ]
            ]);
            return true;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to edit cronjob metadata. Please check application logs.')->error();
            \Log::channel('database')->error('Error updating cron job metadata', [
                'category'  => 'database',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'cronjob_meta' => $cronjobMeta,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('Cronjob metadata was updated, with another error: ' . $e->getMessage())->warning();
            \Log::channel('cron')->error('Error updating cron job metadata', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'cronjob_meta' => $cronjobMeta,
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
        }
    }
}
