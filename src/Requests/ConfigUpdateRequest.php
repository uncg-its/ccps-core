<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\DbConfig;
use App\Events\CcpsCore\ConfigUpdated;

class ConfigUpdateRequest extends FlashedRequest
{
    protected $nonDataFields = ["_token" => "csrf token", "_method" => "custom method"];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Additional validation rules for this object only (define global validation rules in ValidatorServiceProvider)
     *
     *
     */

    public function customValidation($validator)
    {

        /* punting on this for now. TODO: keep, or remove?
        $inputsToCheck = array_diff_key($this->input(), $this->nonDataFields);

        $dbConfig = DbConfig::all()->keyBy('key');

//        $inputsToCheck["testing"] = "bad data";

        foreach($inputsToCheck as $key => $value) {
            if (!$dbConfig->has($key)) {
                $validator->errors()->add($key, "Configuration item '$key' does not currently exist in the config table. Value cannot be updated.");
            }
        }

//        return $validator;
        */
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist()
    {

        // we know all keys exist in database.
        $inputsToUpdate = array_diff_key($this->input(), $this->nonDataFields);

        \DB::beginTransaction();

        try {
            // for each input on the page
            foreach ($inputsToUpdate as $key => $value) {
                // update value if it exists, or create new key-value pair if not
                $config = DbConfig::firstOrCreate(['key' => $key], ['value' => $value])->update(['value' => $value]);
            }

            \DB::commit();
            flash("Successfully edited app configuration.")->success();
            event(new ConfigUpdated($inputsToUpdate));
            return true;
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error trying to edit app configuration. Please check application logs.')->error();
            \Log::channel('database')->error('Error updating application configuration', [
                'category'  => 'config',
                'result'    => 'update',
                'operation' => 'error',
                'data'      => [
                    'request' => $this->all(),
                    'message' => $e->getMessage()
                ]
            ]);
            return false;
        }
    }
}
