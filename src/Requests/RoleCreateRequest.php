<?php

namespace Uncgits\Ccps\Requests;

use App\CcpsCore\Role;
use App\Events\CcpsCore\AclChanged;
use Illuminate\Database\QueryException;

class RoleCreateRequest extends FlashedRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required',
            'database_key' => 'required',
        ];
    }

    /**
     * Actions to take after successful validation.
     */
    public function persist()
    {
        \DB::beginTransaction();
        try {
            $role = Role::create(
                array_merge($this->only(['display_name', 'description']), ['name' => \Str::slug($this->database_key, '.')])
            );

            $role->permissions()->sync($this->permissions);

            \DB::commit();

            event(new AclChanged(
                'add',
                'role',
                [
                    'role' => $role->load(['permissions', 'users']),
                ]
            ));

            flash("Successfully created role '$this->display_name'")->success();
            return $role;
        } catch (QueryException $e) {
            \DB::rollBack();
            flash('Database error trying to create Role. Please check application logs.')->error();
            \Log::channel('database')->error('Error creating Role', [
                'category'  => 'database',
                'operation' => 'store',
                'result'    => 'error',
                'data'      => [
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            return false;
        } catch (\Exception $e) {
            flash('Role was created, with another error: ' . $e->getMessage())->warning();
            \Log::channel('acl')->error('Error creating Role', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'error',
                'data'      => [
                    'request'      => $this->all(),
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
        }
    }
}
