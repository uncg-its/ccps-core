<?php

namespace Uncgits\Ccps\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class FlashedRequest extends FormRequest
{
    public function withValidator($validator) {
        $validator->after(function($validator) {

            // run any additional rules defined in the child class
            if(method_exists($this, 'customValidation')){
                $this->customValidation($validator);
            }

            if (count($validator->errors()->all()) > 0) {
                $html = "<p>There were errors with your request:</p><ul>";
                foreach($validator->errors()->all() as $m) {
                    $html .= "<li>$m</li>";
                }
                $html .= "</ul>";
                flash($html)->error();
            }
        });
    }
}
