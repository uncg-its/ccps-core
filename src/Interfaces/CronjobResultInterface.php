<?php

namespace Uncgits\Ccps\Interfaces;

interface CronjobResultInterface
{
    public function wasSuccessful(): bool;
    public function getMessage(): string;
}
