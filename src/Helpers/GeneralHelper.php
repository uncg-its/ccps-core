<?php


namespace Uncgits\Ccps\Helpers;
use DateTime;
use DateTimeZone;


class GeneralHelper {

    /*
     * getTimeZoneList() - returns an array of timezones for a <select> element, keyed by PHP-friendly value
     *
     * @return array
     */
    public static function getTimeZoneList() {
        static $regions = [
            DateTimeZone::AFRICA,
            DateTimeZone::AMERICA,
            DateTimeZone::ANTARCTICA,
            DateTimeZone::ASIA,
            DateTimeZone::ATLANTIC,
            DateTimeZone::AUSTRALIA,
            DateTimeZone::EUROPE,
            DateTimeZone::INDIAN,
            DateTimeZone::PACIFIC,
        ];

        $timezones = [];
        foreach($regions as $region) {
            $timezones = array_merge($timezones, DateTimeZone::listIdentifiers( $region ));
        }

        $timezone_offsets = [];
        foreach($timezones as $timezone) {
            $tz = new DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
        }

        // sort timezone by offset
        asort($timezone_offsets);

        $timezone_list = [];
        foreach($timezone_offsets as $timezone => $offset) {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }

        return $timezone_list;
    }
}