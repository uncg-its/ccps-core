<?php

namespace Uncgits\Ccps\Helpers;

use Uncgits\Ccps\Interfaces\CronjobResultInterface;

class CronjobResult implements CronjobResultInterface
{
    protected $success = false;
    protected $message = '';

    public function __construct(bool $success = false, string $message = '')
    {
        $this->success = $success;
        $this->message = $message;
        return $this;
    }

    public function wasSuccessful(): bool
    {
        return $this->success;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    // fluent setters

    public function success(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function message(string $message)
    {
        $this->message = $message;
        return $this;
    }
}
