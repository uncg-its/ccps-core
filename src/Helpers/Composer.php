<?php


namespace Uncgits\Ccps\Helpers;


class Composer {
    // includes methods to figure out currently-installed versions of packages

    protected $packages = [];

    public function __construct() {
        $composerInstalledJson = json_decode(file_get_contents(base_path('vendor/composer/installed.json')), true);

        foreach ($composerInstalledJson as $package) {
            $this->packages[$package['name']] = $package['version'];
        }
    }

    public function ccpsCoreVersion($safeFilename = false) {
        $version = $this->packages['uncgits/ccps-core'];
        return $safeFilename ? str_replace('/', '-', $version) : $version;
    }

    public function packageInstalled($packageName) {
        return isset($this->packages[$packageName]);
    }
}