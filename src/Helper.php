<?php

namespace Uncgits\Ccps;

use Carbon\Carbon;

class Helper
{
    public static function getViewsFolder()
    {
        // determine install location for assets and default views
        $themes_config = config('themes');
        if (is_null($themes_config)) {
            // use Laravel default
            $viewPath = resource_path('views');
        } else {
            // igaster Theme plugin
            $viewPath = config('themes')['themes_path'];
        }

        return $viewPath;
    }

    public static function makeCronjobMigrationFromStub(string $name, string $status = 'disabled')
    {
        // define paths
        $migrationPath = base_path('database/migrations/');
        $stubPath = 'vendor/uncgits/ccps-core/src/stubs/cronjobMigration.stub';

        // prep migration filename
        $datePrefix = Carbon::now()->format('Y_m_d_His_');
        $nameSuffix = 'add_' . \Str::snake($name) . '_cronjob_meta';

        // check if migration file already exists
        $destinationFile = $migrationPath . $datePrefix . $nameSuffix . '.php';
        if (file_exists($destinationFile)) {
            throw new \Exception("Migration file $destinationFile already exists.");
        }

        // generate stub
        $stub = file_get_contents($stubPath);
        $stub = str_replace('CRONJOBNAME', \Str::studly($name), $stub);
        $stub = str_replace('CLASS', 'App\\Cronjobs\\' . $name, $stub);
        $stub = str_replace('STATUS', $status, $stub);

        if (file_put_contents($destinationFile, $stub)) {
            return $datePrefix . $nameSuffix;
        } else {
            throw new \Exception("Could not create " . $destinationFile . "!");
        }
    }

    public static function makeNotificationEventMigrationFromStub(string $notificationEventName, string $eventClassName)
    {
        // define paths
        $migrationPath = base_path('database/migrations/');
        $stubPath = 'vendor/uncgits/ccps-core/src/stubs/notificationEventMigration.stub';

        // prep migration filename
        $datePrefix = Carbon::now()->format('Y_m_d_His_');
        $nameSuffix = 'add_' . \Str::snake($notificationEventName) . '_notification_event';

        // check if migration file already exists
        $destinationFile = $migrationPath . $datePrefix . $nameSuffix . '.php';
        if (file_exists($destinationFile)) {
            throw new \Exception("Migration file $destinationFile already exists.");
        }

        // generate stub
        $stub = file_get_contents($stubPath);
        $stub = str_replace('NOTIFICATIONEVENTNAME', \Str::studly($notificationEventName), $stub);
        $stub = str_replace('EVENTCLASSNAME', $eventClassName, $stub);

        if (file_put_contents($destinationFile, $stub)) {
            return $datePrefix . $nameSuffix;
        } else {
            throw new \Exception("Could not create " . $destinationFile . "!");
        }
    }
}
