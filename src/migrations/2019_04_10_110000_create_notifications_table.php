<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_channel_id');
            $table->unsignedInteger('notification_event_id');
            $table->timestamp('queued_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_notifications');
    }
}
