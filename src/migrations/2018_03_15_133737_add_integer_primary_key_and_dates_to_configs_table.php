<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntegerPrimaryKeyAndDatesToConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_db_configs', function(Blueprint $table) {
            $table->increments('id')->unsigned()->first();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_db_configs', function(Blueprint $table) {
            $table->dropColumn('id');
        });
    }
}
