<?php

use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddTokenRoleAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\TokenPermissionsTableSeeder',
            '--force' => true
        ]);

        \Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\TokenPermissionRoleTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissionsToFind = [
            // fill in permission keys here for each permission to remove
            'tokens.create',
            'tokens.edit',
            'tokens.revoke',
            'tokens.admin'
        ];

        $permissions = Permission::whereIn('name', $permissionsToFind)->get();

        foreach ($permissions as $permission) {
            $permission->delete();
        }
    }
}
