<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedAclTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // roles
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\RolesTableSeeder',
            '--force' => true
        ]);

        // permissions
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\PermissionsTableSeeder',
            '--force' => true
        ]);

        // role-user
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\RoleUserTableSeeder',
            '--force' => true
        ]);

        // permission-role
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\PermissionRoleTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('ccps_roles')->truncate();
        DB::table('ccps_permissions')->truncate();
        DB::table('ccps_permission_role')->truncate();
        DB::table('ccps_role_user')->truncate();
    }
}
