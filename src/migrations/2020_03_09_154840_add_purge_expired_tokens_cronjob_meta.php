<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class AddPurgeExpiredTokensCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\CcpsCore\PurgeExpiredTokens',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\CcpsCore\PurgeExpiredTokens')->delete();
    }
}
