<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUserNamesAndAvatarFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_users', function (Blueprint $table) {
            $table->dropColumn(['username', 'first_name', 'last_name', 'avatar_url']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // put back columns.... ? data won't be there.
        Schema::table('ccps_users', function (Blueprint $table) {
            $table->string('username')->nullable()->after('id');
            $table->string('first_name')->after('password');
            $table->string('last_name')->after('first_name');
            $table->string('avatar_url')->after('id_from_provider');
        });
    }
}
