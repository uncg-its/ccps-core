<?php

use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\DbConfig;
use Illuminate\Support\Facades\Artisan;

class RemoveDebugbarFromDbconfigs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DbConfig::where('key', 'debugBar')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Seeders\\DbConfigsTableSeeder',
            '--force' => true
        ]);
    }
}
