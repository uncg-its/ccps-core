<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetImmutableRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // update roles and permissions tables to have "editable" attribute
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->boolean('editable')->default(1);
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->boolean('editable')->default(1);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->dropColumn('editable');
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->dropColumn('editable');
        });
    }
}
