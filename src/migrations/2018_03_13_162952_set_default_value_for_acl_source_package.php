<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultValueForAclSourcePackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->string('source_package')->nullable(false)->default('app')->change();
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->string('source_package')->nullable(false)->default('app')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->string('source_package')->nullable()->default(null)->change();
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->string('source_package')->nullable()->default(null)->change();
        });
    }
}
