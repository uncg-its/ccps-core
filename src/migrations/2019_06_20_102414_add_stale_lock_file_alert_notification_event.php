<?php

use Illuminate\Support\Facades\DB;
use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddStaleLockFileAlertNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'StaleLockFileAlert',
                'description'        => 'StaleLockFileAlert',
                'event_class'        => 'App\\Events\\CcpsCore\\StaleLockFileDetected',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('class', 'App\\Events\\CcpsCore\\StaleLockFileDetected')->delete();
    }
}
