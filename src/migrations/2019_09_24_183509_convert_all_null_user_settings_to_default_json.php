<?php

use App\CcpsCore\User;
use Illuminate\Database\Migrations\Migration;

class ConvertAllNullUserSettingsToDefaultJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $usersWithNullSettings = User::whereNull('settings')->orWhere('settings', '')->get();
        $usersWithNullSettings->each(function ($user) {
            $user->settings = json_encode(['skin' => 'default']);
            $user->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // there is no going back.
    }
}
