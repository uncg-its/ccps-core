<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationChannelNotificationEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_notification_channel_notification_event', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_event_id');
            $table->unsignedInteger('notification_channel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_notification_channel_notification_event');
    }
}
