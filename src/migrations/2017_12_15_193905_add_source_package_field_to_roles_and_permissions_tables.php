<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSourcePackageFieldToRolesAndPermissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->string('source_package')->nullable()->after('id');
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->string('source_package')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_roles', function (Blueprint $table) {
            $table->dropColumn('source_package');
        });

        Schema::table('ccps_permissions', function (Blueprint $table) {
            $table->dropColumn('source_package');
        });
    }
}
