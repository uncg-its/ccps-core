<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronjobMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_cronjob_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_name')->nullable();
            $table->text('description')->nullable();
            $table->string('class')->unique();
            $table->string('schedule')->nullable();
            $table->enum('status', ['enabled', 'disabled'])->default('disabled');
            $table->string('tags')->nullable();
            $table->timestamp('last_run')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_cronjob_meta');
    }
}
