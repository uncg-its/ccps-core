<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueEmailProviderOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_users', function (Blueprint $table) {
            // grab prefix ourselves since Laravel doesn't add it on the dropIndex() command
            $connection = config('database.default');
            $prefix = config('database.connections.' . $connection . '.prefix');

            $table->dropIndex($prefix . 'ccps_users_email_unique');
            $table->unique(['email', 'provider']); // the combination of these fields must be unique
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_users', function (Blueprint $table) {
            $table->string('email')->unique()->change();
            $table->dropIndex('ccps_users_email_provider_unique');
        });
    }
}
