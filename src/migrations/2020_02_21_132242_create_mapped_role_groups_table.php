<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMappedRoleGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_mapped_role_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('role_id');
            $table->string('type');
            $table->string('sync_service')->nullable();
            $table->string('sync_entity_id')->nullable();
            $table->string('provider')->nullable();
            $table->timestamps();

            $table->foreign('role_id')
                ->references('id')
                ->on('ccps_roles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_mapped_role_groups');
    }
}
