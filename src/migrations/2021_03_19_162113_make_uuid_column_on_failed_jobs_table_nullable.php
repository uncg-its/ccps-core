<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeUuidColumnOnFailedJobsTableNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ccps_jobs_failed', function(Blueprint $table) {
            $table->string('uuid')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ccps_jobs_failed', function(Blueprint $table) {
            $table->string('uuid')->nullable(false)->change();
        });
    }
}
