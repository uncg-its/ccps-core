<?php

namespace Uncgits\Ccps\Logging;

use Monolog\Logger;
use GuzzleHttp\Client;
use Monolog\Handler\AbstractProcessingHandler;

class GoogleChatHandler extends AbstractProcessingHandler
{
    private $webHookUrl;
    private $client;

    public function __construct($webHookUrl, $level = Logger::DEBUG, $bubble = true, $client = null)
    {
        parent::__construct($level, $bubble);

        $this->webHookUrl = $webHookUrl;
        $this->client = ($client) ?: new Client();
    }

    public function write(array $record): void
    {
        // handle max length restriction for Google Chat
        $message = $record['message'];
        if (strlen($message) > 4000) {
            $message = substr($message, 0, 4000) . '...';
        }

        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json'    => [
                'text' => $message,
            ],
            'proxy' => config('ccps.http_proxy.enabled') ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port') : ''
        ];

        $this->client->request('POST', $this->webHookUrl, $options);
    }
}
