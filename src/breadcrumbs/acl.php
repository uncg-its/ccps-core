<?php

// ACL

// ROLES

Breadcrumbs::register('acl', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('ACL', route('acl'));
});

Breadcrumbs::register('roles', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('acl');
    $breadcrumbs->push('Roles', route('roles'));
});
Breadcrumbs::register('role.create', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push('New Role', route('role.create'));
});
Breadcrumbs::register('role.show', function ($breadcrumbs, $role) use ($module) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push($role->display_name, route('role.show', $role->id));
});
Breadcrumbs::register('role.edit', function ($breadcrumbs, $role) use ($module) {
    $breadcrumbs->parent('roles');
    $breadcrumbs->push($role->display_name, route('role.show', $role->id));
    $breadcrumbs->push('Edit', route('role.edit', $role->id));
});

// PERMISSIONS
Breadcrumbs::register('permissions', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('acl');
    $breadcrumbs->push('Permissions', route('permissions'));
});
Breadcrumbs::register('permission.create', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push('New Permission', route('permission.create'));
});
Breadcrumbs::register('permission.show', function ($breadcrumbs, $permission) use ($module) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push($permission->display_name, route('permission.show', $permission->id));
});
Breadcrumbs::register('permission.edit', function ($breadcrumbs, $permission) use ($module) {
    $breadcrumbs->parent('permissions');
    $breadcrumbs->push($permission->display_name, route('permission.show', $permission->id));
    $breadcrumbs->push('Edit', route('permission.show', $permission->id));
});

// MAPPED ROLE GROUPS
Breadcrumbs::register('mapped-role-groups.index', function ($breadcrumbs) {
    $breadcrumbs->parent('acl');
    $breadcrumbs->push('Mapped Role Groups', route('mapped-role-groups.index'));
});
Breadcrumbs::register('mapped-role-groups.create', function ($breadcrumbs) {
    $breadcrumbs->parent('mapped-role-groups.index');
    $breadcrumbs->push('Create New', route('mapped-role-groups.create'));
});
Breadcrumbs::register('mapped-role-groups.mappings.index', function ($breadcrumbs, $mappedRoleGroup) {
    $breadcrumbs->parent('mapped-role-groups.index');
    $breadcrumbs->push($mappedRoleGroup->id, route('role-mappings.index', $mappedRoleGroup));
});
Breadcrumbs::register('mapped-role-groups.edit', function ($breadcrumbs, $mappedRoleGroup) {
    $breadcrumbs->parent('mapped-role-groups.mappings.index', $mappedRoleGroup);
    $breadcrumbs->push('Edit', route('mapped-role-groups.edit', $mappedRoleGroup));
});
Breadcrumbs::register('mapped-role-groups.mappings.create', function ($breadcrumbs, $mappedRoleGroup) {
    $breadcrumbs->parent('mapped-role-groups.mappings.index', $mappedRoleGroup);
    $breadcrumbs->push('Add Mappings', route('role-mappings.create', $mappedRoleGroup));
});
