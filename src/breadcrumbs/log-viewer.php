<?php

// CONFIG

Breadcrumbs::register('log-viewer', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Log Viewer', route('log-viewer'));
});