<?php

// Notifications Log
Breadcrumbs::register('notifications-log', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Notifications Log', route('notifications-log.index'));
});
