<?php

// CONFIG

Breadcrumbs::register('config', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Config', route('config'));
});