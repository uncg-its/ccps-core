<?php

// USERS

Breadcrumbs::register('users', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Users', route('users'));
});

Breadcrumbs::register('user.create', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push('New User', route('user.create'));
});
Breadcrumbs::register('user.show', function ($breadcrumbs, $userToShow) use ($module) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($userToShow->email, route('user.show', $userToShow->id));
});
Breadcrumbs::register('user.edit', function ($breadcrumbs, $userToEdit) use ($module) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($userToEdit->email, route('user.show', $userToEdit->id));
    $breadcrumbs->push('Edit', route('user.edit', $userToEdit->id));
});