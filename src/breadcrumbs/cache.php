<?php

// CONFIG

Breadcrumbs::register('cache', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Application Cache', route('cache'));
});