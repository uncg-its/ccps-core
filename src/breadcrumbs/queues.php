<?php

// USERS

Breadcrumbs::register('queues', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Queues', route('queues'));
});

Breadcrumbs::register('queues.successful', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('queues');
    $breadcrumbs->push('Successful Jobs', route('queues.successful'));
});
Breadcrumbs::register('queues.pending', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('queues');
    $breadcrumbs->push('Pending Jobs', route('queues.pending'));
});
Breadcrumbs::register('queues.failed', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('queues');
    $breadcrumbs->push('Failed Jobs', route('queues.failed'));
});

Breadcrumbs::register('queues.pending.show', function ($breadcrumbs, $job) use ($module) {
    $breadcrumbs->parent('queues.pending');
    $breadcrumbs->push('Pending Job: ' . $job->id, route('queues.pending.show', ['job' => $job->id]));
});
Breadcrumbs::register('queues.successful.show', function ($breadcrumbs, $job) use ($module) {
    $breadcrumbs->parent('queues.successful');
    $breadcrumbs->push('Successful Job: ' . $job->id, route('queues.successful.show', ['job' => $job->id]));
});
Breadcrumbs::register('queues.failed.show', function ($breadcrumbs, $job) use ($module) {
    $breadcrumbs->parent('queues.failed');
    $breadcrumbs->push('Failed Job: ' . $job->id, route('queues.failed.show', ['job' => $job->id]));
});
