<?php

// CRONJOBS

Breadcrumbs::register('cronjobs', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Cron Jobs', route('cronjobs'));
});

Breadcrumbs::register('cronjob.edit', function ($breadcrumbs, $cronjob) use ($module) {
    $breadcrumbs->parent('cronjobs');
    $breadcrumbs->push('Edit Cron Job: ' . $cronjob->getDisplayName(), route('cronjob.edit', $cronjob->getMeta()->id));
});
