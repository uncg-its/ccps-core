<?php

// EMAIL


Breadcrumbs::register('email', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push('Email', route('email'));
});

// QUEUE
Breadcrumbs::register('email.queue', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('email');
    $breadcrumbs->push('Email Queue', route('email.queue'));
});

Breadcrumbs::register('email.sent', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('email');
    $breadcrumbs->push('Sent', route('email.sent'));
});