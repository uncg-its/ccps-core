<?php

namespace Uncgits\Ccps\Repositories\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

trait EloquentCrud
{
    public $paginated = false;
    public $withTrashed = false;
    public $sortField = 'id';
    public $sortDirection = 'asc';
    public $request = null;

    public function all()
    {
        $sort = $this->getSortMethod();

        $query = $this->modelName::$sort($this->sortField);

        if ($this->withTrashed) {
            $query = $query->withTrashed();
        }

        return $this->paginated ?
            $query->paginate(25) :
            $query->get();
    }

    public function search(array $terms = null)
    {
        $sort = $this->getSortMethod();

        $query = $this->modelName::$sort($this->sortField);

        if ($this->withTrashed) {
            $query = $query->withTrashed();
        }

        foreach ($this->searchable ?? [] as $key => $searchParams) {
            if (isset($terms[$key])) {
                $value = $terms[$key];

                if ($searchParams['cast'] ?? '' === 'datetime') {
                    $value = Carbon::parse($value)->toDateTimeString();
                }

                $column = $searchParams['column'] ?? $key;

                $term = ($searchParams['operand'] === 'like') ? '%' . $value . '%' : $value;
                $query->where($column, $searchParams['operand'], $term);
            }
        }

        return $this->paginated ?
            $query->paginate(25) :
            $query->get();
    }

    public function get($id) : Model
    {
        return $this->withTrashed ?
            $this->modelName::withTrashed()->findOrFail($id) :
            $this->modelName::findOrFail($id);
    }

    public function create(array $createData) : Model
    {
        return $this->modelName::create($createData);
    }

    public function update($id, array $updateData) : Model
    {
        $model = $this->get($id);
        $model->update($updateData);

        return $model;
    }

    public function delete($id) : bool
    {
        return $this->modelName::whereKey($id)->delete();
    }

    public function restore($id) : bool
    {
        return $this->modelName::whereKey($id)->restore();
    }

    public function paginated()
    {
        $this->paginated = true;
        return $this;
    }

    public function sorted($field, $direction)
    {
        $this->sortField = $field;
        $this->sortDirection = $direction;

        return $this;
    }

    public function withRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function withTrashed($trashed = true)
    {
        $this->withTrashed = $trashed;
        return $this;
    }

    private function getSortMethod()
    {
        return strtolower($this->sortDirection === 'desc') ? 'orderByDesc' : 'orderBy';
    }
}
