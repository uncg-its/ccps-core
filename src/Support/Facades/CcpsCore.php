<?php

namespace Uncgits\Ccps\Support\Facades;

use Illuminate\Support\Facades\Facade;

// See the Auth facade for inspiration here.

class CcpsCore extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ccps-core';
    }

    /**
     * Register the typical authentication routes for an application.
     *
     * @param  array $options
     *
     * @return void
     */
    public static function routes()
    {
        // base admin route
        \Route::prefix('admin')->name('admin')->middleware('role:admin')->get('/', [\App\Http\Controllers\CcpsCore\AdminController::class, 'index']);

        // Routes from Modules
        foreach (collect(config('ccps.modules')) as $name => $module) {
            if (!$module['use_custom_routes']) {
                if ($module['parent'] == 'admin') {
                    \Route::middleware('role:admin')->prefix('admin')->group(function () use ($module, $name) {
                        require(base_path('vendor/' . $module['package'] . '/src/routes/' . $name . '.php'));
                    });
                } else {
                    require(base_path('vendor/' . $module['package'] . '/src/routes/' . $name . '.php'));
                }
            }
        }
    }
}
