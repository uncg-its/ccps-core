<?php

namespace Uncgits\Ccps\Support;

use Illuminate\Pagination\LengthAwarePaginator;

class CcpsPaginator extends LengthAwarePaginator
{
    public $startingRecord;
    public $endingRecord;

    public function __construct($entireCollection, $perPage = null, $currentPage = null)
    {
        if (is_null($perPage)) {
            $perPage = config('ccps.paginator_per_page');
        }

        if (is_null($currentPage)) {
            $currentPage = request()->page ?: 1;
        }

        $totalRecords = $entireCollection->count();

        // calculate starting and ending
        $this->startingRecord = ($currentPage - 1) * $perPage + 1;
        $this->endingRecord = $this->startingRecord + ($perPage - 1);
        if ($this->endingRecord > $totalRecords) {
            $this->endingRecord = $totalRecords;
        }

        parent::__construct(
            $entireCollection->forPage($currentPage, $perPage),
            $totalRecords,
            $perPage,
            $currentPage,
            ['path' => self::resolveCurrentPath()]
        );
    }

    public function header($noun = "record")
    {
        $noun = \Str::plural($noun);
        return "<h4 class='text-gray-700 mb-3 text-sm'>Displaying {$noun} {$this->startingRecord} - {$this->endingRecord} of {$this->total()} total</h4>";
    }
}
