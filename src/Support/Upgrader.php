<?php


namespace Uncgits\Ccps\Support;

use Illuminate\Support\Str;

class Upgrader
{
    /**
     * Resolve an upgrade instance from a file. (stolen from Illuminate's database migrations)
     *
     * @param  string  $file
     * @return object
     */
    public function resolve($file)
    {
        // require the file
        $path = base_path('vendor/uncgits/ccps-core/src/upgrades');

        require_once $path . '/' . $file;

        $class = Str::studly(implode('_', array_slice(explode('_', $file), 4)));
        $class = str_replace('.php', '', $class);

        return new $class;
    }
}
