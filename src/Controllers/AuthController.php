<?php

namespace Uncgits\Ccps\Controllers;

use Auth;
use App\CcpsCore\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        $hd = config('ccps.socialite.allow_only_domain', '*');
        $with = ['hd' => $hd];
        if ($provider === 'azure' && config('services.azure.force_mfa', false)) {
            $with['amr_values'] = 'mfa';
        }
        return Socialite::driver($provider)->with($with)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        // double check that they are from approved domain, if one is set.

        $allowedDomain = config('ccps.socialite.allow_only_domain', '');

        if (!empty($allowedDomain)) {
            switch ($provider) {
                case 'google':
                case 'azure':
                    $emailField = 'email';
                    break;
                default:
                    $emailField = '';
                    break;
            }

            $emailAddress = $user->$emailField;

            list($address, $domain) = explode("@", $emailAddress); // array destructuring

            if ($domain != $allowedDomain) {
                abort(403, 'This email domain is not permitted by this application.');
            }
        }

        $authUser = $this->findOrCreateUser($user, $provider);

        if (!$authUser) {
            abort(403, 'You do not have an account, and this app does not allow new account registration.');
        }

        Auth::login($authUser, true);

        // true up roles if config is set to do so
        if (config('ccps.role_mapping.method') === 'login') {
            $authUser->grantMappedRoles();
        }

        if (\Session::has('originalTarget')) {
            flash()->clear(); // just in case the original login message still exists for existing SSO
            flash('Login successful!')->success();
        }

        return redirect(\Session::get('originalTarget', $this->redirectTo));
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     *
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     *
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('id_from_provider', $user->id)->first();

        if ($authUser) {
            return $authUser;
        }

        if (config('ccps.allow_signups')) {
            $createdUser = User::create([
                'email'            => $user->email,
                'provider'         => $provider,
                'id_from_provider' => $user->id,
                'time_zone'        => 'America/New_York',
                'settings'         => json_encode([
                    'skin' => 'default'
                ]),
            ]);

            $createdUser->grantMappedRoles();

            return $createdUser;
        }

        return false;
    }
}
