<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class AclController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.*');
    }

    public function index()
    {
        return view($this->viewPath . 'acl.index');
    }
}
