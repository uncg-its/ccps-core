<?php

namespace Uncgits\Ccps\Controllers;

use App\CcpsCore\SentEmail;
use App\CcpsCore\EmailQueue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class EmailQueueController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'email';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:email.view')->only(['index', 'sent', 'showEmail']);
    }

    public function index(Request $request)
    {
        $queuedEmails = EmailQueue::inQueue('email')->sortable('id')->get();

        // hack in for sortability by display_name, description, schedule
        $extraSortProcessingFields = ['mailable' => 'getMailableClassAttribute', 'subject' => 'getSubjectAttribute', 'recipients' => 'getRecipientsArrayAttribute'];

        $sort = $request->query('sort', null);

        if (!is_null($sort) && isset($extraSortProcessingFields[$sort])) {
            $order = $request->query('order', 'asc');
            $sortMethod = ($order == 'desc') ? 'sortByDesc' : 'sortBy';

            $queuedEmails = $queuedEmails->$sortMethod(function ($item) use ($sort, $extraSortProcessingFields) {
                $getMethod = $extraSortProcessingFields[$sort];

                return $item->$getMethod();
            });
        }

        $queuedEmails = new CcpsPaginator($queuedEmails);

        return view($this->viewPath . 'email.queue.index')->with(compact('queuedEmails'));
    }

    public function show(EmailQueue $job)
    {
        return $job->mailable;
    }

    public function sent(Request $request)
    {
        $sentEmails = SentEmail::with('successful_job')->sortable('id')->get();

        // hack in for sortability by display_name, description, schedule
        $extraSortProcessingFields = ['sender' => 'getFromAttribute', 'recipients' => 'getToAttribute', 'subject' => 'getSubjectAttribute'];

        $sort = $request->query('sort', null);

        if (!is_null($sort) && isset($extraSortProcessingFields[$sort])) {
            $order = $request->query('order', 'asc');
            $sortMethod = ($order == 'desc') ? 'sortByDesc' : 'sortBy';

            $sentEmails = $sentEmails->$sortMethod(function ($item) use ($sort, $extraSortProcessingFields) {
                $getMethod = $extraSortProcessingFields[$sort];

                return $item->$getMethod();
            });
        }

        $sentEmails = new CcpsPaginator($sentEmails);

        return view($this->viewPath . 'email.queue.sent')->with(compact('sentEmails'));
    }

    public function showSentEmail(SentEmail $sentEmail)
    {
        return view($this->viewPath . 'email.queue.sent-show')->with(compact('sentEmail'));
    }

    public function delete(EmailQueue $job)
    {
        \DB::beginTransaction();

        try {
            $result = $job->delete();
            \DB::commit();
            flash('Email on job ' . $job->id . ' has been deleted successfully.')->success();
        } catch (\Exception $e) {
            \DB::rollback();
            flash('Email on job ' . $job->id . ' could not be deleted: ' . $e->getMessage())->error();
        }

        return redirect(route('email.queue'));
    }
}
