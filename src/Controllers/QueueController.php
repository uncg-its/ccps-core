<?php

namespace Uncgits\Ccps\Controllers;

use App\CcpsCore\Job;
use App\CcpsCore\FailedJob;
use Illuminate\Http\Request;
use App\CcpsCore\SuccessfulJob;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class QueueController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'queues';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:queues.view')->except(['deleteJob', 'forgetFailedJob', 'requeueFailedJob', 'runJob']);
        $this->middleware('permission:queues.edit')->only(['deleteJob', 'forgetFailedJob', 'requeueFailedJob', 'runJob']);
    }

    public function index()
    {
        return view($this->viewPath . 'queues.index');
    }

    public function showPending(Request $request)
    {
        $queues = ['all'];

        if (request()->input('queue')) {
            $queues = explode(',', $request->queue);
            $pendingJobs = Job::inQueue($queues)->sortable('id')->get();
        } else {
            $pendingJobs = Job::sortable('id')->get();
        }

        // sorting hack for class
        $extraSortProcessingFields = ['class'];

        $sort = $request->query('sort', null);

        if (!is_null($sort) && in_array($sort, $extraSortProcessingFields)) {
            $order = $request->query('order', 'asc');
            $sortMethod = ($order == 'desc') ? 'sortByDesc' : 'sortBy';

            $pendingJobs = $pendingJobs->$sortMethod($sort);
        }

        return view($this->viewPath . 'queues.pending')->with([
            'jobs'     => new CcpsPaginator($pendingJobs),
            'queues'   => $queues,
            'viewPath' => $this->viewPath,
        ]);
    }

    public function showSuccessful(Request $request)
    {
        $queues = ['all'];

        if (request()->input('queue')) {
            $queues = explode(',', $request->queue);
            $successfulJobs = SuccessfulJob::inQueue($queues)->sortable(['completed_at' => 'desc'])->get();
        } else {
            $successfulJobs = SuccessfulJob::sortable(['completed_at' => 'desc'])->get();
        }

        // sorting hack for class
        $extraSortProcessingFields = ['class'];

        $sort = $request->query('sort', null);

        if (!is_null($sort) && in_array($sort, $extraSortProcessingFields)) {
            $order = $request->query('order', 'asc');
            $sortMethod = ($order == 'desc') ? 'sortByDesc' : 'sortBy';

            $successfulJobs = $successfulJobs->$sortMethod($sort);
        }

        return view($this->viewPath . 'queues.successful')->with([
            'jobs'     => new CcpsPaginator($successfulJobs),
            'queues'   => $queues,
            'viewPath' => $this->viewPath,
        ]);
    }


    public function showFailed(Request $request)
    {
        $queues = ['all'];

        if (request()->input('queue')) {
            $queues = explode(',', $request->queue);
            $failedJobs = FailedJob::inQueue($queues)->sortable(['failed_at' => 'desc'])->get();
        } else {
            $failedJobs = FailedJob::sortable(['failed_at' => 'desc'])->get();
        }

        // sorting hack for class
        $extraSortProcessingFields = ['class'];

        $sort = $request->query('sort', null);

        if (!is_null($sort) && in_array($sort, $extraSortProcessingFields)) {
            $order = $request->query('order', 'asc');
            $sortMethod = ($order == 'desc') ? 'sortByDesc' : 'sortBy';

            $failedJobs = $failedJobs->$sortMethod($sort);
        }

        return view($this->viewPath . 'queues.failed')->with([
            'jobs'     => new CcpsPaginator($failedJobs),
            'queues'   => $queues,
            'viewPath' => $this->viewPath,
        ]);
    }

    public function showPendingJob(Job $job)
    {
        return view($this->viewPath . 'queues.pending-show')->with([
            'job'      => $job,
            'viewPath' => $this->viewPath,
        ]);
    }

    public function showSuccessfulJob(SuccessfulJob $job)
    {
        return view($this->viewPath . 'queues.successful-show')->with([
            'job'      => $job,
            'viewPath' => $this->viewPath,
        ]);
    }

    public function showFailedJob(FailedJob $job)
    {
        return view($this->viewPath . 'queues.failed-show')->with([
            'job'      => $job,
            'viewPath' => $this->viewPath,
        ]);
    }

    public function deleteJob(Job $job)
    {
        \DB::beginTransaction();

        try {
            $result = $job->delete();
            \DB::commit();
            flash('Job ' . $job->id . ' has been deleted successfully.')->success();
        } catch (\Exception $e) {
            \DB::rollback();
            flash('Job ' . $job->id . ' could not be deleted: ' . $e->getMessage())->error();
        }

        return redirect(route('queues.pending'));
    }

    public function forgetFailedJob(FailedJob $job)
    {
        \DB::beginTransaction();

        try {
            $result = $job->delete();
            \DB::commit();
            flash('Failed Job ' . $job->id . ' has been removed successfully.')->success();
        } catch (\Exception $e) {
            \DB::rollback();
            flash('Failed Job ' . $job->id . ' could not be removed: ' . $e->getMessage())->error();
        }

        return redirect(route('queues.failed'));
    }

    public function requeueFailedJob(FailedJob $job)
    {
        try {
            $exitCode = \Artisan::call('queue:retry', [
                'id' => $job->id
            ]);
            flash('Successfully re-queued job ' . $job->id)->success();
        } catch (\Exception $e) {
            flash('Error re-queueing failed job ' . $job->id . ': ' . $e->getMessage())->error();
        }

        return redirect(route('queues.failed'));
    }

    public function runJob(Job $job)
    {
        try {
            \DB::beginTransaction();
            // change the id on the job to 0 to push it to first in line
            $oldId = $job->id;

            $job->id = 0;
            $job->save();

            // run the queue once to pick up the job with ID 0
            $exitCode = \Artisan::call('queue:work', [
                '--queue' => $job->queue,
                '--once'  => true,
                '--tries' => config('ccps.queue_failures_allowed.' . $job->queue),
            ]);

            \DB::commit();
            flash('Job ' . $oldId . ' was manually run successfully.')->success();
        } catch (\Exception $e) {
            \DB::rollback();
            flash('Job ' . $oldId . ' failed to run manually: ' . $e->getMessage())->error();
        }

        return redirect(route('queues.pending'));
    }
}
