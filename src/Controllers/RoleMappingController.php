<?php

namespace Uncgits\Ccps\Controllers;

use App\CcpsCore\MappedRoleGroup;
use App\CcpsCore\RoleMapping;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Requests\RoleMappingCreateRequest;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class RoleMappingController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.view')->only('index');
        $this->middleware('permission:acl.create')->only(['create', 'store']);
        $this->middleware('permission:acl.delete')->only('destroy');
    }

    public function index(MappedRoleGroup $mappedRoleGroup)
    {
        $mappedRoleGroup->load('role_mappings');
        $mappedRoleGroup->role_mappings = new CcpsPaginator($mappedRoleGroup->role_mappings);

        return view($this->viewPath . 'acl.role-mappings.index')->with([
            'mappedRoleGroup' => $mappedRoleGroup,
        ]);
    }

    public function create(MappedRoleGroup $mappedRoleGroup)
    {
        if ($mappedRoleGroup->type === 'synced') {
            flash('You may not create ad-hoc mappings for a synced Role Group.')->error();
            return redirect()->back();
        }

        return view($this->viewPath . 'acl.role-mappings.create')->with([
            'mappedRoleGroup' => $mappedRoleGroup
        ]);
    }

    public function store(RoleMappingCreateRequest $request, MappedRoleGroup $mappedRoleGroup)
    {
        if ($mappedRoleGroup->type === 'synced') {
            flash('You may not create ad-hoc mappings for a synced Role Group.')->error();
            return redirect()->back();
        }

        $request->persist($mappedRoleGroup);
        return redirect()->route('role-mappings.index', $mappedRoleGroup);
    }

    public function destroy(MappedRoleGroup $mappedRoleGroup, RoleMapping $roleMapping)
    {
        $roleMapping->delete();
        flash('Role Mapping deleted successfully.')->success();
        return redirect()->route('role-mappings.index', $mappedRoleGroup);
    }
}
