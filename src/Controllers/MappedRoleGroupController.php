<?php

namespace Uncgits\Ccps\Controllers;

use App\CcpsCore\Role;
use App\CcpsCore\MappedRoleGroup;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Requests\MappedRoleGroupCreateRequest;
use Uncgits\Ccps\Requests\MappedRoleGroupUpdateRequest;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class MappedRoleGroupController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.*');
    }

    public function index()
    {
        $mappedRoleGroups = MappedRoleGroup::get();

        return view($this->viewPath . 'acl.mapped-role-groups.index')->with([
            'mappedRoleGroups' => new CcpsPaginator($mappedRoleGroups),
        ]);
    }

    public function show(MappedRoleGroup $mappedRoleGroup)
    {
        $mappedRoleGroup->load('role_mappings');

        return view($this->viewPath . 'acl.mapped-role-groups.show')->with([
            'mappedRoleGroup' => $mappedRoleGroup,
        ]);
    }

    public function create()
    {
        return view($this->viewPath . 'acl.mapped-role-groups.create')->with([
            'roles'      => $this->getRoles(),
            'providers'  => $this->getProviders(),
            'viewPath'   => $this->viewPath,
        ]);
    }

    public function store(MappedRoleGroupCreateRequest $request)
    {
        $mappedRoleGroup = $request->persist();
        return redirect()->route('mapped-role-groups.index');
    }

    public function edit(MappedRoleGroup $mappedRoleGroup)
    {
        return view($this->viewPath . 'acl.mapped-role-groups.edit')->with([
            'mappedRoleGroup' => $mappedRoleGroup,
            'roles'           => $this->getRoles(),
            'providers'       => $this->getProviders(),
            'viewPath'        => $this->viewPath
        ]);
    }

    public function update(MappedRoleGroupUpdateRequest $request, MappedRoleGroup $mappedRoleGroup)
    {
        $mappedRoleGroup = $request->persist($mappedRoleGroup);
        return redirect()->route('mapped-role-groups.index');
    }

    public function destroy(MappedRoleGroup $mappedRoleGroup)
    {
        $mappedRoleGroup->delete(); // cascades to mappings
        flash('Mapped role group and its mappings deleted successfully.')->success();
        return redirect()->route('mapped-role-groups.index');
    }

    // additional helpers

    protected function getRoles()
    {
        return Role::get()->mapWithKeys(function ($role) {
            return [$role->id => $role->name];
        })->prepend('--- Select Role ---', '')->toArray();
    }

    protected function getProviders()
    {
        return collect(config('ccps.login_methods'))->mapWithKeys(function ($provider) {
            return [$provider => $provider];
        })->prepend('--- Select Provider ---', '')->toArray();
    }
}
