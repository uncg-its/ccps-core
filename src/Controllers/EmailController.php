<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class EmailController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'email';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:email.view')->only(['index']);
    }

    public function index()
    {
        return view($this->viewPath . 'email.index');
    }
}
