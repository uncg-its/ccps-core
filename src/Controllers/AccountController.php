<?php

namespace Uncgits\Ccps\Controllers;

use App\CcpsCore\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\CcpsCore\NotificationEvent;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Helpers\GeneralHelper;
use Laravel\Sanctum\PersonalAccessToken;
use Uncgits\Ccps\Requests\UserUpdateRequest;

class AccountController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->acl();
    }

    protected function acl()
    {
        $this->middleware('auth');
        $this->middleware('permission:tokens.*')->only(['tokens']);
        $this->middleware('permission:tokens.create')->only(['createToken', 'storeToken']);
        $this->middleware('permission:tokens.edit')->only(['editToken', 'updateToken']);
        $this->middleware('permission:tokens.revoke')->only(['revokeToken']);
    }

    public function index()
    {
        return view('account.index');
    }

    public function profile()
    {
        return view('account.profile.show');
    }

    public function editProfile()
    {
        $timezone_list = GeneralHelper::getTimeZoneList();

        $roles = Role::all();
        $userToEdit = auth()->user();
        return view('account.profile.edit')->with(compact(['userToEdit', 'roles', 'timezone_list']));
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        $user = auth()->user();
        $roles = $user->roles;
        $result = $request->persist($user, $roles);

        return redirect(route('profile.show'));
    }

    // notifications


    public function notifications()
    {
        $notificationChannels = \Auth::user()->notification_channels()->get();
        $notificationEvents = NotificationEvent::notifiableForUser();

        $types = [
            'email'       => 'Email Address',
            'sms'         => 'Mobile (text message, US number only)',
            'google-chat' => 'Google Chat (channel webhook)',
            'slack'       => 'Slack (channel webhook)',
        ];

        $carriers = [
            ''        => '--- Select ---',
            'verizon' => 'Verizon',
            'att'     => 'AT&T',
            'sprint'  => 'Sprint',
            'tmobile' => 'T-Mobile',
        ];

        return view('account.notifications.index')->with([
            'notificationChannels' => $notificationChannels,
            'notificationEvents'   => $notificationEvents,
            'types'                => $types,
            'carriers'             => $carriers,
        ]);
    }

    public function settings()
    {
        $skins = collect(config('ccps.bootstrap_skins'))->mapWithKeys(function ($attributes, $key) {
            return [$key => $attributes['display_name']];
        });
        $selected = auth()->user()->settings->skin ?? 'default';

        return view('account.settings')->with([
            'skins'    => $skins,
            'selected' => $selected
        ]);
    }

    public function updateSettings(Request $request)
    {
        $approvedSettings = ['skin'];
        $skins = array_keys(config('ccps.bootstrap_skins'));

        $validated = $request->validate([
            'skin' => [
                'required',
                Rule::in($skins),
            ]
        ]);

        $user = auth()->user();

        $existingSettings = $user->settings ?? new \stdClass();
        foreach ($approvedSettings as $setting) {
            $existingSettings->{$setting} = $request->$setting;
        }

        try {
            $user->settings = json_encode($existingSettings);
            $user->save();

            \Log::channel('general')->info('User settings updated', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'success',
                'data'      => [
                    'current_user' => $user,
                ]
            ]);

            flash('Settings updated!')->success();
        } catch (\Exception $e) {
            \Log::channel('general')->error('Error while updating settings for user ' . $user->id, [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'current_user' => $user,
                    'request'      => $request->all(),
                    'message'      => $e->getMessage()
                ]
            ]);
            flash('There was an error updating your account settings. Please contact an administrator.');
        }
        return redirect()->back();
    }

    public function tokens(Request $request)
    {
        if ($request->get('show', '') === 'all') {
            if (\Auth::user()->hasPermission('tokens.admin')) {
                return view('account.tokens.index')->with([
                    'tokens'  => PersonalAccessToken::get(),
                    'showAll' => true
                ]);
            }

            abort(403);
        }

        return view('account.tokens.index')->with([
            'tokens'  => \Auth::user()->tokens,
            'showAll' => false
        ]);
    }

    public function createToken()
    {
        return view('account.tokens.create');
    }
}
