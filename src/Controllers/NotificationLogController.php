<?php

namespace Uncgits\Ccps\Controllers;

use Illuminate\Http\Request;
use App\CcpsCore\Notification;
use App\CcpsCore\NotificationEvent;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class NotificationLogController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'notifications-log';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:logs.*')->only('index');
    }

    public function index(Request $request)
    {
        $query = Notification::orderBy('queued_at', 'desc');

        $filters = $request->only(['user', 'type', 'event']);

        if ($request->has('user') && $request->user != '') {
            $query->whereHas('notification_channel.user', function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->user . '%');
            });
        }

        if ($request->has('type') && $request->type != '') {
            $query->whereHas('notification_channel', function ($q) use ($request) {
                $q->where('type', $request->type);
            });
        }

        if ($request->has('event') && $request->event != '') {
            $query->whereHas('notification_event', function ($q) use ($request) {
                $q->where('id', $request->event);
            });
        }

        $notificationsSent = new CcpsPaginator($query->get());


        $types = ['' => 'all Types', 'email' => 'email', 'sms' => 'sms', 'google-chat' => 'google-chat', 'slack' => 'slack'];
        $events = NotificationEvent::all()->mapWithKeys(function ($item) {
            return [$item->id => $item->display_name];
        })->prepend('all Events', '')->toArray();

        return view($this->viewPath . 'notifications-log.index')->with([
            'notificationsSent' => $notificationsSent,
            'types'             => $types,
            'events'            => $events,
            'filters'           => $filters,
        ]);
    }
}
