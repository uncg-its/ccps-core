<?php

namespace Uncgits\Ccps\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    // overrides AuthenticatesUsers version
    public function showLoginForm()
    {
        $appLoginMethods = config('ccps.login_methods');

        // store previous URL for redirecting
        \Session::put('originalTarget', \URL::previous());

        // if a single login method, and it's a third party, go straight there.
        if (count($appLoginMethods) == 1) {
            if ($appLoginMethods[0] != 'local') {
                return redirect()->route('oauth', ['provider' => $appLoginMethods[0]]);
            }
        }

        // format for javascript
        $appLoginMethods = collect($appLoginMethods)->map(function ($method, $index) {
            return [
                'id'      => $index,
                'name'    => \Str::title($method),
                'partial' => 'auth.partials.login.' . $method
            ];
        })->toArray();

        // show the login screen with tabs to select method of auth
        return view('auth.login')->with(compact('appLoginMethods'));
    }

    protected function authenticated(Request $request, $user)
    {
        // true up roles if config is set to do so
        if (config('ccps.role_mapping.method') === 'login') {
            $user->grantMappedRoles();
        }

        if (\Session::has('originalTarget')) {
            flash()->clear(); // just in case the original login message still exists for existing SSO
            flash('Login successful!')->success();
        }


        return redirect(\Session::get('originalTarget', RouteServiceProvider::HOME));
    }
}
