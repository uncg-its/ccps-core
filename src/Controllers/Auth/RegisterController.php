<?php

namespace Uncgits\Ccps\Controllers\Auth;

use App\CcpsCore\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'email'    => 'required|string|email|max:255|unique:ccps_users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        // assumes local, since oauth provider would handle this in other places

        $createdUser = User::create([
            'email'            => $data['email'],
            'password'         => bcrypt($data['password']),
            'provider'         => 'local',
            'time_zone'        => 'America/New_York',
            'settings'         => json_encode([
                'skin' => 'default'
            ]),
        ]);

        $createdUser->grantMappedRoles();

        return $createdUser;
    }

    // overrides RegistersUsers method
    public function showRegistrationForm()
    {
        $appLoginMethods = config('ccps.login_methods');

        if (!config('ccps.allow_signups')) {
            return redirect()->route('login');
        }

        // if a single login method, and it's a third party, go straight there.
        if (count($appLoginMethods) == 1) {
            if ($appLoginMethods[0] != 'local') {
                return redirect()->route('oauth', ['provider' => $appLoginMethods[0]]);
            }
        }

        // format for javascript
        $appLoginMethods = collect($appLoginMethods)->map(function ($method, $index) {
            return [
                'id'      => $index,
                'name'    => \Str::title($method),
                'partial' => 'auth.partials.register.' . $method
            ];
        })->toArray();

        return view('auth.register')->with(compact('appLoginMethods'));
    }
}
