<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use Uncgits\Ccps\Models\FileCache as Cache;
use App\Events\CcpsCore\CacheCleared;

class CacheController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'cache';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:cache.clear');
    }


    public function index()
    {
        return view($this->viewPath . 'cache.index', ['cache' => Cache::getCache()]);
    }

    public function clear()
    {
        Cache::flush();
        flash('Cache cleared successfully.')->success();
        event(new CacheCleared());
        return redirect(route('cache'));
    }

    public function purge()
    {
        $results = Cache::flushExpired();
        flash('Cache purged - ' . count($results['expired']) . ' expired ' . \Str::plural('entry', count($results['expired'])) . ' removed; ' . count($results['notExpired']) . ' ' . \Str::plural('entry', count($results['notExpired'])) . ' not yet expired.')->success();
        return redirect(route('cache'));
    }
}
