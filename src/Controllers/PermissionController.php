<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use Illuminate\Http\Request;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\Events\CcpsCore\AclChanged;
use Uncgits\Ccps\Requests\PermissionCreateRequest;
use Uncgits\Ccps\Requests\PermissionUpdateRequest;
use Uncgits\Ccps\Support\CcpsPaginator;

class PermissionController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.view')->only(['index','show']);
        $this->middleware('permission:acl.edit')->only(['edit','update']);
        $this->middleware('permission:acl.delete')->only('destroy');
        $this->middleware('permission:acl.create')->only(['create','store']);
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $permissions = new CcpsPaginator(Permission::sortable(['name' => 'asc'])->get());
        return view($this->viewPath . 'acl.permissions.index')->with(compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = Role::all();
        return view($this->viewPath . 'acl.permissions.create')->with([
            'roles'      => $roles,
            'permission' => new Permission,
            'viewPath'   => $this->viewPath
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PermissionCreateRequest $request
     */
    public function store(PermissionCreateRequest $request)
    {
        $permission = $request->persist();
        return redirect(route('permissions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Permission $permission
     */
    public function show(Permission $permission)
    {
        $permission->load('roles');
        return view($this->viewPath . 'acl.permissions.show')->with([
            'permission' => $permission,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission $permission
     */
    public function edit(Permission $permission)
    {
        $roles = Role::all();
        return view($this->viewPath . 'acl.permissions.edit')->with([
            'roles'      => $roles,
            'permission' => $permission,
            'viewPath'   => $this->viewPath
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PermissionUpdateRequest $request
     * @param  Permission $permission
     */
    public function update(PermissionUpdateRequest $request, Permission $permission)
    {
        $result = $request->persist($permission);
        return redirect(route('permission.show', ['permission' => $permission->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission $permission
     */
    public function destroy(Permission $permission)
    {
        if (!$permission->editable) {
            flash("Error: permission '$permission->display_name' is not editable; cannot delete.")->error();
        } else {
            if ($permission->delete()) {
                event(new AclChanged(
                    'delete',
                    'permission',
                    [
                        'permission' => $permission
                    ]
                ));
                flash("Permission '$permission->display_name' deleted successfully.")->success();
            } else {
                flash("Permission '$permission->display_name' could not be deleted.")->error();
            }
        }

        return redirect(route('permissions'));
    }
}
