<?php

namespace Uncgits\Ccps\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Laravel\Sanctum\PersonalAccessToken;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class TokenAbilitiesController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.view')->only('index');
        $this->middleware('permission:acl.create')->only(['create', 'store']);
        $this->middleware('permission:acl.delete')->only('destroy');
    }

    public function index()
    {
        $tokens = PersonalAccessToken::get();
        return view($this->viewPath . 'acl.token-abilities.index')->with([
            'tokens' => new CcpsPaginator($tokens)
        ]);
    }

    public function edit(PersonalAccessToken $token)
    {
        return view($this->viewPath . 'acl.token-abilities.edit')->with([
            'token' => $token
        ]);
    }

    public function update(Request $request, PersonalAccessToken $token)
    {
        $validated = $request->validate([
            'abilities' => 'nullable'
        ]);

        try {
            $token->abilities = explode(',', $request->abilities);
            $token->save();
            flash('Token abilities updated succesfully.')->success();
        } catch (\Exception $e) {
            flash('Error saving token!')->error();
        }

        return redirect()->route('token-abilities.index');
    }
}
