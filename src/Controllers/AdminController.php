<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->acl();
    }

    protected function acl()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $modules = config('ccps.modules');
        return view('admin.index')->with(compact('modules'));
    }
}
