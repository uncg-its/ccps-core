<?php

namespace Uncgits\Ccps\Controllers;

use Illuminate\Http\Request;
use App\CcpsCore\CronjobMeta;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Requests\CronjobUpdateRequest;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class CronJobController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'cronjobs';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:cronjobs.view')->only(['index']);
        $this->middleware('permission:cronjobs.edit')->only(['edit', 'update', 'enable', 'disable', 'clearLockFile']);
        $this->middleware('permission:cronjobs.run')->only(['run']);
    }

    public function index(Request $request)
    {
        return view($this->viewPath . 'cronjobs.index');
    }

    protected function changeCronjobStatus($status)
    {
        $id = request('id');
        $cronjobMeta = CronjobMeta::findOrFail($id);

        if ($cronjobMeta->status == $status) {
            flash("This cron job is already $status.")->warning();
        } else {
            $result = $cronjobMeta->update(['status' => $status]);

            if ($result) {
                flash("Cron job {$cronjobMeta->display_name} set to $status.")->success();
            } else {
                flash('Could not change cron job status!')->error();
            }
        }
    }


    public function enable(Request $request)
    {
        $this->changeCronjobStatus('enabled');
        return redirect(route('cronjobs', $request->except('_token')));
    }

    public function disable(Request $request)
    {
        $this->changeCronjobStatus('disabled');
        return redirect(route('cronjobs', $request->except('_token')));
    }

    public function run(Request $request)
    {
        //
    }

    public function clearLockFile(Request $request)
    {
        //
    }

    public function edit(CronjobMeta $cronjob)
    {
        $cronjob = new $cronjob->class;
        return view($this->viewPath . 'cronjobs.edit')->with(compact('cronjob'));
    }

    public function update(CronjobUpdateRequest $request, CronjobMeta $cronjob)
    {
        $result = $request->persist($cronjob);
        return redirect(route('cronjobs'));
    }
}
