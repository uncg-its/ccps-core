<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Helpers\GeneralHelper;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use App\CcpsCore\User;
use App\CcpsCore\Role;
use App\Events\CcpsCore\AclChanged;
use Uncgits\Ccps\Requests\UserCreateRequest;
use Uncgits\Ccps\Requests\UserUpdateRequest;
use Uncgits\Ccps\Support\CcpsPaginator;

class UserController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'users';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:users.view')->only(['index', 'show']);
        $this->middleware('permission:users.edit')->only(['edit', 'update']);
        $this->middleware('permission:users.delete')->only('destroy');
        $this->middleware('permission:users.create')->only(['create', 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $users = new CcpsPaginator(User::sortable(['email' => 'asc'])->get());
        return view($this->viewPath . 'users.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $roles = Role::all();
        return view($this->viewPath . 'users.create')->with(compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(UserCreateRequest $request)
    {
        $user = $request->persist();

        return redirect(route('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     */
    public function show(User $userToShow)
    {
        return view($this->viewPath . 'users.show')->with(compact('userToShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     */
    public function edit(User $userToEdit)
    {
        $timezone_list = GeneralHelper::getTimeZoneList();

        $roles = Role::all();
        return view($this->viewPath . 'users.edit')->with(compact(['userToEdit', 'roles', 'timezone_list']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  User $user
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user = $request->persist($user);

        return redirect(route('user.show', $user->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            event(new AclChanged(
                'delete',
                'user',
                [
                    'user' => $user
                ]
            ));

            flash("User '$user->email' deleted successfully.")->success();
        } else {
            flash("User '$user->email' could not be deleted.")->error();
        }

        return redirect(route('users'));
    }
}
