<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use App\Http\Requests\CcpsCore\ConfigUpdateRequest;

class DbConfigController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'config';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:config.view')->only('index');
        $this->middleware('permission:config.edit')->only('save');
    }

    public function index()
    {
        return view($this->viewPath . 'config.index')->with(['dbConfig' => app('dbConfig')]);
    }

    public function save(ConfigUpdateRequest $request)
    {
        $request->persist();
        return redirect(route('config'));
    }
}
