<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class LogViewerController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'log-viewer';

    public function __construct()
    {
        $this->middleware('permission:logs.view')->only('index');
        $this->resolveViewPath();
    }

    public function index()
    {
        return view('modules.log-viewer.index');
    }
}
