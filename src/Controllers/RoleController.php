<?php

namespace Uncgits\Ccps\Controllers;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use Illuminate\Http\Request;
use App\Events\CcpsCore\AclChanged;
use Uncgits\Ccps\Requests\RoleCreateRequest;
use Uncgits\Ccps\Requests\RoleUpdateRequest;
use Uncgits\Ccps\Support\CcpsPaginator;

class RoleController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'acl';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    protected function acl()
    {
        $this->middleware('permission:acl.view')->only(['index','show']);
        $this->middleware('permission:acl.edit')->only(['edit','update']);
        $this->middleware('permission:acl.delete')->only('destroy');
        $this->middleware('permission:acl.create')->only(['create','store']);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $roles = new CcpsPaginator(Role::sortable(['name' => 'asc'])->get());
        return view($this->viewPath . 'acl.roles.index')->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $permissions = Permission::all();
        return view($this->viewPath . 'acl.roles.create')->with([
            'permissions' => $permissions,
            'role'        => new Role,
            'viewPath'    => $this->viewPath
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(RoleCreateRequest $request)
    {
        $role = $request->persist();

        return redirect(route('roles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Role $role
     */
    public function show(Role $role)
    {
        $role->load(['users', 'permissions']);

        return view($this->viewPath . 'acl.roles.show')->with(compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all();
        return view($this->viewPath . 'acl.roles.edit')->with([
            'permissions' => $permissions,
            'role'        => $role,
            'viewPath'    => $this->viewPath
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Role $role
     */
    public function update(RoleUpdateRequest $request, Role $role)
    {
        $role = $request->persist($role);

        return redirect(route('role.show', ['role' => $role->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role $role
     */
    public function destroy(Role $role)
    {
        if (!$role->editable) {
            flash("Error: role'$role->display_name' is not editable; cannot delete.")->error();
        } else {
            if ($role->delete()) {
                event(new AclChanged(
                    'delete',
                    'role',
                    [
                        'role' => $role
                    ]
                ));

                flash("Role '$role->display_name' deleted successfully.")->success();
            } else {
                flash("Role '$role->display_name' could not be deleted.")->error();
            }
        }

        return redirect(route('roles'));
    }
}
