<?php

namespace Uncgits\Ccps\Components\Log\Formatter;

use Monolog\Formatter\JsonFormatter;

class SplunkJsonFormatter extends JsonFormatter
{
    // protected $includeStacktraces = true;

    /**
     * {@inheritdoc}
     */
    public function format(array $originalRecord): string
    {
        $record = [
            'environment'          => $originalRecord['channel'],
            'datetime'             => $originalRecord['datetime'],
            'application'          => config('app.name'),
            'level'                => $originalRecord['level'],
            'level_name'           => $originalRecord['level_name'],
            'message'              => $originalRecord['message']
        ];

        if (!empty($originalRecord['extra'])) {
            $record['extra'] = $originalRecord['extra'];
        }

        if (!empty($originalRecord['context'])) {
            $expectedKeys = [
                'category'  => '',
                'operation' => '',
                'result'    => '',
                'data'      => '',
                'exception' => '',
                'userId'    => ''
            ];

            $context = [
                'category'  => $originalRecord['context']['category'] ?? 'unknown',
                'operation' => $originalRecord['context']['operation'] ?? 'unknown',
                'result'    => $originalRecord['context']['result'] ?? null,
                'data'      => $originalRecord['context']['data'] ?? []
            ];
            if (isset($originalRecord['context']['exception'])) {
                $context['category'] = 'exception';
                $context['operation'] = 'exception';
                $context['data'] = [
                    'exception' => $originalRecord['context']['exception'],
                    'userId'    => $originalRecord['context']['userId'] ?? null
                ];
            }

            if (!empty($extraKeys = array_diff_key($originalRecord['context'], $expectedKeys))) {
                $context['other'] = $extraKeys;
            }

            $record['context'] = $context;
        }

        $json = $this->toJson($this->normalize($record), true) . ($this->appendNewline ? "\n" : '');

        return $json;
    }
}
