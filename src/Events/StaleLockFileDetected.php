<?php

namespace Uncgits\Ccps\Events;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Notifications\CcpsCore\StaleLockFileAlert;

class StaleLockFileDetected
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($filename, $createdAt)
    {
        $message = 'Cronjob lock file ' . $filename . ' has existed for over one hour (since ' . $createdAt->toDateTimeString() . ')';

        \Log::channel('cron')->warning($message, [
            'category'   => 'cron',
            'operation'  => 'monitor',
            'data'       => [
                'event'      => 'stale_lock_file',
                'filename'   => $filename,
                'created_at' => $createdAt->toDateTimeString()
            ]
        ]);

        $this->notificationClass = StaleLockFileAlert::class;
        $this->notificationClassArgs = [
            $message
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->hasRole('slms|admin');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
