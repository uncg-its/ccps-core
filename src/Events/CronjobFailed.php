<?php

namespace Uncgits\Ccps\Events;

use Uncgits\Ccps\Models\Cronjob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CronjobFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $cronjob;
    public $exception;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Cronjob $cronjob, \Exception $exception)
    {
        $this->cronjob = $cronjob;
        $this->exception = $exception;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('cron');
    }
}
