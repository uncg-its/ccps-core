<?php

namespace Uncgits\Ccps\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class AclChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $changeType;
    public $affectedType;
    public $aclObjects;
    public $request;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($changeType, $affectedType, $aclObjects)
    {
        $this->changeType = $changeType;
        $this->affectedType = $affectedType;
        $this->aclObjects = $aclObjects;

        $this->user = auth()->user();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('acl');
    }
}
