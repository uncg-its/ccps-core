<?php

namespace Uncgits\Ccps\Events;

use Uncgits\Ccps\Models\Cronjob;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CronLockFileConflict
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $cronjob;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Cronjob $cronjob)
    {
        $this->cronjob = $cronjob;
        \Log::channel('cron')->warning('Lock file exists for Cron job ' . $cronjob->getClassName() . '! Cannot run job while lock file exists.', [
            'category'  => 'cron',
            'operation' => 'run',
            'result'    => 'failure',
            'data'      => [
                'event'         => $this,
                'cronjob_class' => $cronjob->getClassName(),
            ]
        ]);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
