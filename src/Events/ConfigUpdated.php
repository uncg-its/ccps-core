<?php

namespace Uncgits\Ccps\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConfigUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $configsUpdated;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($configsUpdated)
    {
        $this->configsUpdated = $configsUpdated;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('config');
    }
}
