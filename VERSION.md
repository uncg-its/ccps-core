## 3.3.1

- fixes mobile/sms notification channel
- fixes an ID/label issue on `input-checkbox` component
- fixes `value` of `input-textarea` component

## 3.3.0

- option to force MFA on Azure OAuth requests
- upgrades default version of TailwindCSS to 1.9.0
- upgrades default version of AlpineJS to 2.8.2
- additions to default PurgeCss whitelist in `tailwind.config.js` so that disabled button opacity is honored
- updates default CSS and JS to apply the updates to Tailwind configuration
- fixes a migration that changes the `uuid` column on the `failed_jobs` table to ensure that it is changing the column, not adding
- upgrades Laravel Mix to ^6.0.16 for security purposes, including Webpack ^5
- updates to other NPM dependencies as well

### 3.2.3

- fix for failed jobs batch migration (incorrect table name)

### 3.2.2

- tiny fix for card component to add padding to bottom

### 3.2.1

- fix for route definition in Facade so that `route:list` artisan command works again

### 3.2

- README link fixes for latest Laravel
- update repository references to omit `.git` extension
- compatibiliy with PHP 8

### 3.1.3

- implements `x-cloak` on items that should be hidden on page load
- new max-width implementation on modal
- default deploy script now utilizes `php artisan horizon:terminate` for general flexibility
- starting to document components
- adds keybinding of `esc` to modals for alternate closing option sans mouse
- add toggle for 'disabled' property on `x-button`
- fixes Horizon shim to add `isDownForMaintenance` view variable
- fixes `@nopermission` Blade directive to use Laratrust `isAbleTo` since `can` is no longer available.
- fixes a missing class import on `PurgeExipredTokens` cronjob

### 3.1.2

- Fix `<x-h3>` component
- remove api token editing UI to fix view-not-found bug

### 3.1.1

- Change to using PHP callable syntax for routes, to support automatic namespace prefix removal

### 3.1

- Laravel 8
- Fixes a mis-configured route in notification channel verification email
- New components: Toggle, Stacked List
- Laravel Dump Server is now a core requirement
- Fixes a display issue with the "debug" badge in log viewer

### 3.0.4

- Change `mix()` directives to `asset()` for simpler asset handling

### 3.0.3

- Component updates: forms, table, card, etc.

### 3.0.2

- Clean up old Log Viewer config
- Adds reload button to Log Viewer

### 3.0.1

- Upgrade flash package to 0.3
- Fix missing nav on error pages

### 3.0

- TALL Stack transition: TailwindCSS, AlpineJS, Livewire, Laravel. Bootstrap is completely gone with this iteration.

### 2.0.2

- Fixes a route-model-binding issue with local email queue

### 2.0.1

- Removes a reference to a non-existent `Run` model on the `Notification` class.
- Changes `EloquentCrud::withTrashed()` to accept a boolean (defaults to `true`) so that the flag can be changed on the fly.

### 2.0.0

- Re-work the cronjob flow and introduce the `CronjobResult` and `CronjobResultInterface` classes to better handle a cron job's results.
- Remove the broken `scopeWithTag()` method from `CronjobMeta`, resolving [#51](https://bitbucket.org/uncg-its/ccps-core/issues/51/cronjobmeta-scope-scopewithtag-does-not)
- Adds a fallback value as second argument to `DbConfig::getConfig()`
- Requires forked version of Laravel Breadcrumbs package, replacing abandoned `davejamesmiller/laravel-breadcrumbs`
- Adds token ability assignment through the GUI (manual for now)
- Changes default token abilities to `''` (empty) rather than granting the all-powerful `*`.
- Complete overhaul of exception handler's `render` method to remove inefficient and confusing custom code. This also fully-resolves the double menu error first reported in [#24](https://bitbucket.org/uncg-its/ccps-core/issues/24/double-menus-on-403-error), which was thought to be resolved already.
- removes `web_errors` middleware group as part of a revamp of the exception handler
- tweak upgrade stub to be more general, not referring to views specifically.
- remove reference to fake user data in service provider
- remove `notifications` binding to all view via view composer

### 1.17.2

- exception handler bypasses logical checks if expecting JSON response
- role mapping cronjob now returns User model
- another attempt for namespace fixes by re-naming the `seeders` folder to `Seeders`

### 1.17.1

- Updates Horizon controller method shim with new `Horizon::assetsAreCurrent()` directive

### 1.17.0

- Transitions from Laravel Airlock package to renamed version (Laravel Sanctum)
- Tweaks for true psr-4 compliance, future-proofing for Composer 2.0

### 1.16.0

- Support for Laravel version 7.0+
- Removes `owen-it/laravel-auditing` package and functionality
- Adds Laravel Airlock for implementing simple API access tokens
- Removes built-in stub functionality for the `api_key` column on the `ccps_users` table
- Changes permissions check in `DefineAppMenu` middleware to use `hasPermission` to make it easier for using Policies in addition to Laratrust.

### 1.15.1

- During Grouper sync for mapped role groups, filters the member list to exclude group entities (e.g. get only users)

### 1.15.0

- Introduces Mapped Role Groups

### 1.14.3

- Attempts to fix an error with user registration where the settings field wouldn't be set correctly

### 1.14.2

- Abstracts the base user one more level, so that you have the option to extend either the `User` model with the Laratrust trait, or without (e.g. if you want to implement the workaround in Laratrust to use Laravel policies as well as Laratrust).

### 1.14.1

- More RMB fixes - this time for role.show and permission.show.

### 1.14.0

- Introduces the Application Snapshot logging - daily log of the application's package and repository information.
- Changes `markdown()` directive to `view()` in the notification stub's `toSms()` method.

### 1.13.3

- `EloquentCrud::get()` now supports soft deletes

### 1.13.2

- Adds support for `withTrashed()` to `EloquentCrud` trait

### 1.13.1

- Fix for `EloquentCrud::delete()` method not working properly
- Adds `EloquentCrud::restore()` method for soft deletes

### 1.13.0

- Fixes a direct call to `env()` method from inside a Blade component (`nav.blade.php`)
- Changes a behavior in the `audit-table` Blade partial so that actions not performed by a user (e.g. automated actions, without an actor) do not cause an error

### 1.12.3

- Fix for misconfigured namespaces trying to reference `RouteServiceProvider` on auth scaffolding controllers

### 1.12.2

- Adds support for basic deployment script out of the box.

### 1.12.1

- Fix namespacing issue on `EloquentRepositoryInterface` and `EloquentCrud` trait

### 1.12.0

- Adopts new Laravel Auth scaffolding (including `ConfirmPasswordController` and `VerificationController`)
- Adopts Laravel's new DRY redirect behavior for Auth actions, by defining `HOME` in the `RouteServiceProvider`, which is referenced in Auth controllers
- More fixes for route-model binding inconsistency as in previous two releases
- Better handling of Cronjob tag searching parameters after pagination and actions taken on the index page (changing status, running, clearing lock file, etc.).
- Changes `acl()` method visibility on all CCPS Core controllers to protected, so it can be overridden in child classes.
- Adds generators for `Service` and `Repository` classes, along with the `EloquentRepositoryInterface` contract and `EloquentCrud` trait to use on the repository classes
- Allows clearing a Cronjob lock file even when the job is disabled

### 1.11.3

- Fixes for more route-model binding issues (users.update, users.show, audit history)

### 1.11.2

- Fix for inconsistent route-model binding on cronjob index page (parameter was being passed as `id` but `cronjob` was expected)
- Some cleanup of HTML code on cronjob index page

### 1.11.1

#### Fixes

- Adjusts handling of cronjobs that are run manually, so that an error will be displayed if one occurs.

### 1.11.0

#### Features

- Adds the option to redirect unauthorized users to the login page, and then back to the intended page, rather than just showing the 403 error. Becomes default unless changed.

### 1.10.1

#### Fixes

- Adjustment to `SplunkJsonFormatter` to remove the 'host' field, since this is a reserved field name in Splunk. It has been deemed superfluous since the Splunk default value for this field is sufficient.

### 1.10.0

#### Fixes

- Updates `GoogleChatHandler` definition to be compatible with the parent `Monolog\Handler\AbstractProcessingHandler` class

#### Features

- True semantic versioning is now in place. Patch releases will not contain upgrade scripts.

### 1.9.2

#### Fixes

- Updates `SplunkJsonFormatter` definition to be compatible with the parent `Monolog\Formatter\JsonFormatter` class

### 1.9.1

#### Features

- Requires Ignition package

### 1.9.0

#### Features

- Uses Laravel ^6.0
- Changes to `laracasts/flash` for flash messaging
- Removes `uncgits/ccps-crud` as a dependency and moves it to an optional install
- Updates Queues navigation item to point to Horizon when Redis is the `QUEUE_DRIVER`

### 1.8.2

#### Fixes

- Fixes a bug introduced in the previous release, where seeding the Users table would fail because the `settings` column was not yet present on the `users` table. Now fully resolves [#60](https://bitbucket.org/uncg-its/ccps-core/issues/60/cannot-record-login-time-for-default-user)

### 1.8.1

#### Features

- Updated Cronjob stub to more clearly indicate that an exception should be thrown to indicate job failure.

#### Fixes

- New users are initialized with properly-formatted empty JSON string instead of null, which satisfies a constraint in the database. Resolves [#60](https://bitbucket.org/uncg-its/ccps-core/issues/60/cannot-record-login-time-for-default-user)
- The `Encryptable` trait no longer incorrectly references a static `$encryptable` property. Resolves [#61](https://bitbucket.org/uncg-its/ccps-core/issues/61/static-property-problem-on-encryptable)

### 1.8.0

#### Features

- complete logging revamp to be better prepared for Splunk JSON logging format
- adds new default listeners for logging notifications and failed cron jobs
- removes all explicit `use` statements for Facades in favor of root-level inline calls (e.g. don't `use Illuminate\Support\Facades\DB;` but instead call with `\DB::whatever()`)

#### Fixes

- relocates logic in `AccountController` from published class back to base class

### 1.7.0

#### Features
- adds new Log Formatter specifically for Splunk JSON (`SplunkJsonFormatter`)
- alters `Log::` directives to add `context` array (for Splunk logging)

#### Fixes
- adds proxy handling to `GoogleChatHandler`, along with CCPS Core base notifications and stubs
- addresses a problem with `CronjobController::run()` that improperly tried to get the display name of the cronjob for logging purposes
- wraps the entire notification process in `try-catch` so that failures in sending notifications don't result in an abort of the process that spawned them. Notifications are nice but they shouldn't break core business logic and processes.
- removes a stray `dump()` that was being used for debugging from the `NotifiesChannels` trait.

### 1.6.2

#### Features
- adds `CheckForStaleLockFiles` cronjob, along with notifications, so that we can track when a cron job has taken too long to execute.
- changes the `notification-email-plaintext` stub to be consistent with preferred formatting of "[APP NAME] message" for shorter SMS.

#### Fixes
- changes the `notificationEventMigration` stub to fix an issue in the `down()` method of the migration adding a notification event - there was an incorrect reference to the `event_class` column as `class` which caused the `down()` method to throw an error.

### 1.6.1

#### Features
- adds handling of `appends()` to the `paginated-table` view component [#55](https://bitbucket.org/uncg-its/ccps-core/issues/55)
- adds `CronjobFailed` event so that action can be taken upon failure of a Cronjob (e.g. Notifications can be generated) [#58](https://bitbucket.org/uncg-its/ccps-core/issues/58)

#### Fixes
- addresses an incorrect total count on paginated tables when current page results were only 1 result. [#56](https://bitbucket.org/uncg-its/ccps-core/issues/56)
- sets default value for `settings` field on `users` table so that local users can be created via GUI [#57](https://bitbucket.org/uncg-its/ccps-core/issues/57)
- removes commented-out code for old Cron package, including `register()` method from `Cronjob` model class

### 1.6.0

#### Features
- can now opt to skip `composer dump-autoload` when using `php artisan ccps:make:cronjob` with `-N` flag

#### Fixes
- Notifications scaffolding from 1.5.0 was horrendously mis-located and mis-configured. This update reworks that whole module so that permissions for only the notifications log are dictated by the module config; all users can now sign in and access their Notifications settings in the Account area.

### 1.5.1

#### Fixes
- Hotfix - change the injected console kernel so that the schedule is not loaded if we cannot connect to the database. This fixes errors on `php artisan ccps:deploy`
- Hotfix - attempting to delete a lock file no longer throws an error (old reference to Cron package)

### 1.5.0

#### Features
- Using Laravel version 5.8.* [#49](https://bitbucket.org/uncg-its/ccps-core/issues/49/laravel-58-support)
- Requires PHP 7.3
- Re-tools the Cron system to utilize Laravel scheduler - phases out `uncgits/cron` package entirely
- Upgrades many dependencies to latest versions:
    - `davejamesmiller/laravel-breadcrumbs` to `^5.2.1`
    - `barryvdh/laravel-debugbar` to `^3.2`
    - `barryvdh/laravel-ide-helper` to `^2.6`
    - `rap2hpoutre/laravel-log-viewer` to `^1.1`
    - `spatie/laravel-backup` to `^6.2`
    - `doctrine/dbal` to `^2.9`
    - `owen-it/laravel-auditing` to `^9.0`
    - `kyslik/column-sortable` to `^5.8`
    - `phlak/twine` to `^4.0`
    - `propaganistas/laravel-phone` to `^4.2`
- Updates configuration file for Laratrust package to new version [per 5.1 update requirements](https://laratrust.santigarcor.me/docs/5.1/upgrade.html)

#### Fixes
- Fixes a typo in the migration stub for `ccps:make:notification` - the `down()` method would not work since a nonexistent field was referenced in the `where()` clause


### 1.4.1

#### Features

- adds `Encryptable` trait for easy model encryption
- adds Google Chat logger for logging purposes - [#48](https://bitbucket.org/uncg-its/ccps-core/issues/48/officially-onboard-google-chat-handler-for)
- adds full customizable-per-user Notification system and generator

#### Fixes

- fixes a lingering reference to `$user->fullname` on the Audits table (`audit-table.blade.php` partial) [#50](https://bitbucket.org/uncg-its/ccps-core/issues/50/audits-table-does-not-show-usernames)
- conforms to Laravel 5.6+ logging standards - default logging config for new apps (in `config/logging.php`) now references `env(APP_LOG_MAX_FILES)` properly. Additionally a search/replace in `ccps:init` was removed as it is no longer necessary.

### 1.4.0

- adds functional user Settings screen
- adds Bootstrap skin (theme) picker
- updates default Bootstrap to v4.3.1
- updates default FontAwesome to v5.7.2
- requires `netojose/laravel-bootstrap-4-forms` package
- updates Socialite dependency to ^4.0 in prep for the Google+ API shutdown (March 7, 2019)

- introduces support for Laravel Horizon (including modifications for subfolder installs) ([#47](https://bitbucket.org/uncg-its/ccps-core/issues/47/update-for-laravel-horizon-usage))
- Fixes a bug in Cronjob metadata where default tags would be overwritten with blank value when editing the cronjob in the GUI. ([#46](https://bitbucket.org/uncg-its/ccps-core/issues/46/default-cronjob-tags-are-overwritten-when))
- Addresses an issue where incorrect timezone is shown for Successful Jobs ([#45](https://bitbucket.org/uncg-its/ccps-core/issues/45/completed-jobs-table-ignores-app-timezone))

### 1.3.0

- Removes 'First Name' and 'Last Name' fields from local registration page `resources/views/auth/partials/register/local.blade.php` ([#36](https://bitbucket.org/uncg-its/ccps-core/issues/36/first-last-names-still-on-register-form))
- Fails gracefully with Log Warning if invalid Cronjob classname is provided in `cronjob_meta` table. ([#38](https://bitbucket.org/uncg-its/ccps-core/issues/38/bad-cronjobmeta-data-not-easy-to-fix))
- Refactors route definitions from modules so that we can utilize `php artisan route:cache` command. ([#40](https://bitbucket.org/uncg-its/ccps-core/issues/40/optimize-routes-and-route-detection-for))
- Removes default entry for `LOG_MAX_FILE_SIZE` in `.env.example`, which was throwing errors if not set. ([#42](https://bitbucket.org/uncg-its/ccps-core/issues/42/fresh-application-installs-throw-error))
- Updates Lavary Menu Blade directive usage ([#43](https://bitbucket.org/uncg-its/ccps-core/issues/43/need-to-update-lavary-laravel-menu-and))
- Clarifies custom menu item development in `DefineAppMenu` middleware ([#44](https://bitbucket.org/uncg-its/ccps-core/issues/44/placement-of-the-admin-menu))
- Adds support for Redis-based queues, and Laravel Horizon
- Updates `uncgits/cron` package requirement to minimum 1.4.2 so that route caching will still work.
- Updates for compatibility with Laravel Mix version 4.0+
- Fixes a rendering problem on the local user registration page that did not show validation error messages
- Adds 'instance-mariadb', '127.0.0.1', and 'localhost' as `anticipate` values when setting up the `DB_HOST` field during init or deploy operations.

### 1.2.2

- Hotfix: Cronjob tags were not being displayed / parsed properly if the tags were in the class file and not overridden in the database. ([#41](https://bitbucket.org/uncg-its/ccps-core/issues/41/cronjobmeta-search-does-not-fall-back-to))
- Hotfix: Module routes were still using hard-coded view paths in places. Changed to using `route()` helper and named routes.

### 1.2.1

- Hotfix: Manual insertion / use of the database prefix in the `2018_02_05_224504_unique_email_provider_on_users_table.php` migration to allow for successful new app installs

### 1.2.0

- Simplification of User model - removes `username`, `first_name`, `last_name`, and `avatar` from local app. User now contains only `email` and other app-specific data.
- Upgrade stub: addition of boilerplate / default `upgrade()` method for replacing text, since that is what most upgrades to this point have been used for.

### 1.1.1

- Full integration for Socialite + Azure


### 1.1.0

- Upgrades core Laravel installation to Laravel 5.7
- adds default log channel / stack for Cron package, using "daily"
- adds ability to customize the largest log file displayed in the Log Viewer, to 500 out-of-memory errors

### 1.0.5

- cronjob creation via `ccps:make:cronjob` command generates a migration file instead of directly inserting the metadata into the database.
- adds (`phlak/twine`) string-manipulation package
- adds (`beyondcode/laravel-dump-server`) debugging package

### 1.0.4

- updates CCPS Core's `composer.json` file to use PSR-4 instead of PSR-0.
- fixes a problem introduced with `symfony/console:4.1.0` where CCPS Core commands that extend the `Installer` class (including `Init` and `Deploy`) would error out when writing output to the console.
- on a related note to the previous item, abandons using our own verbosity-tracking in the `Installer` class, and refactors to utilize the built-in verbosity handling from the `Illuminate\Console\Command` class (e.g. using `$this->info('message', OutputInterface::VERBOSITY_VERBOSE)` to output only when the `-v` flag was used, instead of rolling our own property and method in `Uncgits\CcpsCore\Command\Installer` to do the same)

### 1.0.3

- fixes the Exception handler to properly handle `ValidationException` when `APP_DEBUG` is set to `false` (for production environments)

### 1.0.2

- tweaks the assignment logic of `app('dbConfig')` to the IoC container so that it can be referenced in console mode.
- adds the `ccps:make:acl-seeders` command to generate ACL seeders and migrations for easier scaffolding
- changes the generation methodology of the application key for `ccps:init` and `ccps:deploy` so that it will work regardless of whether the app has been created inside/outside the Docker container
- adds profile picture controls for users under `local` provider (add/edit/remove)
- changes user-show and profile-show views to place avatar in line with user metadata horizontally (columns) - this reduces the size of the avatar
- changes user-edit code so that "Reset Password" looks the same between user-edit and profile-edit views
- adds extra `vendor:publish` tags for publishing only CSS and JS assets (needed for this upgrade)
- symlinks the `public/storage` folder to `storage/app/public` for access to uploaded avatars
- adds the `FILESYSTEM_DRIVER=public` directive to the `.env` file if it does not already exist

### 1.0.1

- Fixes an issue with `ccps:deploy` where Timezone was not being set early enough - caused errors during database seeding
- changes markup on the view that shows user details - makes the profile image responsive
- changes default DebugBar behavior - now hidden by default (sets `DEBUGBAR_ENABLED` to `false` in `.env.example`)
- updates CRUD stub for ACL Migration to address the inability to perform `down()` action in generated migrations
- some tweaks and improvements to the upgrade stub for better exception handling
- requirement for `uncgits/crud` updated to `^4.2.2` to make sure we pull in new CRUD stubs
- fixes a trivial spelling error in a comment in `EventServiceProvider.php`
- implements use of HTTP Proxy in the Google Chat Notification Channel
- adds `http_proxy` information to `config/ccps.php`, referencing values in `.env` file
- reintroduces `VERSION.md` for a version history, now that we are out of beta.
- introduces `UPGRADE.md` for instructions on how to upgrade from the previous version to this version

### 1.0.0

- Official release!
