# Upgrade information

This file describes how to upgrade from CCPS Core version **3.2** to version **3.3**. If you have not performed prior upgrades yet, please view this file in a previously tagged version as necessary to ensure that you are caught up with Manual Upgrades.

Refer to `README.md` for details on the general upgrade process.

## Additional setup

No additional setup is required.

-------

# Upgrade Overview

## Automatic upgrades

This upgrade contains **1 upgrade file(s)** to run via `php artisan ccps:upgrade`.

## Manual upgrades

This upgrade contains **1 manual upgrade(s)** you may need to perform.

---

# Upgrade details

This section describes what each upgrade does in detail. This is provided primarily for transparency, but in cases where (for some reason) you are not using the upgrades function of CCPS Core, or if you know you may need to make some manual upgrades to published files, this section will save you from having to parse the upgrade files.

## Automatic upgrades

### Add Azure Force MFA config to `services.php`

Version 3.3 adds an ability to require any Azure OAuth sign-ins to have passed through MFA as part of the completion of the flow. This upgrade adds the requisite configuration item to `config/services.php`:

Replaces:

```php
env('SOCIALITE_AZURE_REDIRECT_URI'),
```

With:

```php
env('SOCIALITE_AZURE_REDIRECT_URI'),
        'force_mfa' => env('SOCIALITE_AZURE_FORCE_MFA', false),
```

## Manual upgrades

### Update assets

Version 3.3 supplies updates to a number of asset files. If upgrading from a previous version of CCPS Core, it is recommended to do the following:
1. Re-copy `publish/package.json` to your project root (or merge in the changes if you have modified your `package.json`)
2. Run `npm update`
3. Re-copy `tailwind.config.js` to your project root (or merge in the changes if you have modified your `tailwind.config.js`)
4. Re-run `npm run prod` to regenerate your project's assets